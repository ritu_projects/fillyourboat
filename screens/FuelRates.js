import React from 'react';
import { StatusBar, TextInput, FlatList, ActivityIndicator, ImageBackground, View, ScrollView, Text, Button, Dimensions, TouchableOpacity, Linking, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from "../styles/styles";
import strings from '../strings/strings';
import { Dropdown } from 'react-native-material-dropdown';
import RBSheet from "react-native-raw-bottom-sheet";
import { Card } from "native-base";
import PopupDialog, {
    DialogTitle, DialogContent, DialogFooter, DialogButton, SlideAnimation, ScaleAnimation,
} from 'react-native-popup-dialog';

import { WebView } from 'react-native-webview'
import moment from 'moment';
const { width, height } = Dimensions.get("window");
import Image from 'react-native-fast-image'

export default class FuelRates extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            animating: false,
            lodingDialog: false,
            data: [],
            userid: '',
            token: '',
            price: '',
        }

    }

    componentDidMount() {

        AsyncStorage.getItem("userid")
            .then(userid => {
                this.setState({ user_id: userid });

                AsyncStorage.getItem("token")
                    .then(token => {
                        var accessToken = JSON.parse(token);
                        this.setState({ token: accessToken });
                        this.setState({
                            animating: true,
                            lodingDialog: true,

                        });

                        fetch(strings.base_Url + "getFuelrates", {
                            method: 'GET',
                            headers: {
                                'Content-Type': 'application/json',
                                'User-Id': this.state.user_id,
                                'token': this.state.token,
                            },

                        })
                            .then((response) => response.json())
                            .then((responseData) => {
                                console.log(" responseData getFuelList resoJSon===" + JSON.stringify(responseData));
                                console.log("resoJSon getFuelList===" + responseData.error);
                                if (responseData.error == "false") {
                                    console.log("Error false massage getFuelList" + JSON.stringify(responseData.record));

                                    var vr_amount = responseData.record[0].amount;
                                    console.log("Amount of Fuel ", vr_amount)
                                    this.setState({
                                        animating: false,
                                        lodingDialog: false,
                                        data: responseData.record,
                                        price: vr_amount,
                                    });
                                    this.replaceTextFunction()
                                    // this.createOrder4()
                                    //this.RBSheet.close()
                                } else if (responseData.error == "true") {

                                    this.showalerts(responseData.errorMessage)
                                    this.setState({
                                        animating: false,
                                        lodingDialog: false

                                    });
                                }
                            }
                            )
                    })
            })
    }
    replaceTextFunction = () => {


        var SampleText = this.state.price;

        console.log("Amount of Fuel Replace ", SampleText)

        var NewText = SampleText.replace(".", ",");

        console.log("Amount of Fuel Replace NewText ", NewText)

        this.setState({ price: NewText });
        console.log("Amount of Fuel Replace Price State ", this.state.price)


    }

    render() {
        return (
            <View style={{
                flex: 1,
                // backgroundColor:'white',
                flexDirection: 'column',

            }}>

                <ScrollView>

                    <Card style={{ width: '100%', marginTop: Platform.OS == 'ios' ? 20 :-5, height: 50, flexDirection: 'row' }}>
                        <TouchableOpacity
                            onPress={() => Actions.pop()}
                            style={{ width: '15%', height: 50, justifyContent: 'center', alignItems: 'center', }}>
                            <Image source={require("../assets/back.png")}
                                style={{ width: 15, height: 10, }}
                                resizeMode="stretch" />
                        </TouchableOpacity>
                        <View style={{
                            width: '75%', height: 50,
                            justifyContent: 'center', alignItems: 'center',
                        }}>
                            <Text style={{ fontSize: 14, fontFamily: 'Poppins-SemiBold', }}>Tarifs carburant et zones de livraison</Text>

                        </View>


                    </Card>




                    {this.state.data.length > 0 ?

                        <View style={{
                            width: "90%", alignSelf: 'center', marginLeft: 25, justifyContent: 'center', alignItems: 'center', alignSelf: 'center',
                        }}>
                            <Text style={{ fontSize: 13, fontFamily: 'Poppins-SemiBold', marginTop: 10 }}>Tarifs du carburant du jour</Text>



                            <Card style={{
                                width: 110, justifyContent: 'space-between',
                                marginTop: 10,
                                flexDirection: 'column', padding: 15, borderRadius: 15,
                                backgroundColor: '#1A578A', borderBottomColor: 'grey', borderBottotmWidth: 1,
                                alignItems: 'center', alignSelf: 'center',
                            }}>
                                <Image
                                    source={require("../assets/7.png")}
                                    style={{ width: 40, height: 40, }} />


                                <Text style={{ fontSize: 13, fontFamily: 'Poppins-Bold', color: 'white', }}>{this.state.data[0].fuel}</Text>
                                <Text style={{ fontSize: 13, fontFamily: 'Poppins-Bold', color: 'white', }}>{this.state.data[0].amount}€ /L</Text>
                                {/* <Text style={{ fontSize: 13, fontFamily: 'Poppins-Bold', color: 'white', }}>50€</Text> */}


                            </Card>
                            <View style={{ borderColor: 'grey', marginTop: 10, borderWidth: 0.5, width: '90%' }}></View>
                            <View
                                style={{ flexDirection: 'row', paddingTop: 10, marginTop: 10, width: '90%', justifyContent: 'space-between', alignItems: 'center' }}>
                                <Text style={{ fontSize: 12, fontFamily: 'Poppins-Medium', }}>Livraison de Carburant</Text>
                                {/* <Text style={{ fontSize: 12, fontFamily: 'Poppins-Medium', }}>{this.state.price}€ /L</Text> */}
                                <Text style={{ fontSize: 12, fontFamily: 'Poppins-Medium', }}>15€</Text>



                            </View>

                            <View style={{ borderColor: 'grey', marginTop: 10, borderWidth: 0.5, width: '90%', marginBottom: 10, }}></View>
                            {/* <View
                                style={{ flexDirection: 'row', marginTop: 1, width: '90%', marginTop: 10, justifyContent: 'space-between', alignItems: 'center' }}>
                                <Text style={{ fontSize: 12, fontFamily: 'Poppins-SemiBold', color: '#1A578A' }}>Zone de livraison</Text>
                                <Text style={{ fontSize: 12, fontFamily: 'Poppins-Medium', }}></Text>



                            </View> */}
                            {/* <View
                                style={{ flexDirection: 'row', marginTop: 1, width: '90%', marginTop: 5, alignItems: 'center' }}>
                                <Text style={{ fontSize: 12, fontFamily: 'Poppins-SemiBold' }}>Fill Your Boat livre dans les zones délimitées en couleur sur la carte, aux ports et aux corps-morts. SAUF, au port d'Arcachon et au port de la Vigne.</Text>

                            </View> */}



                        </View>
                        :

                        <Text style={{ fontFamily: "Poppins-Medium", marginTop: '40%', textAlign: 'center' }}>Aucune donnée</Text>

                    }
                    <View
                        style={{ flex: 1, bottom: 20, }}>

                        <View
                            style={{
                                width: width,
                                height: height * 40 / 100,
                            }}>

                            <View
                                style={{ flexDirection: 'row', marginTop: 1, width: '90%',
                                 marginTop: 20, justifyContent: 'space-between', alignItems: 'center',marginLeft :45, marginRight:25, }}>
                                <Text style={{ fontSize: 12, fontFamily: 'Poppins-SemiBold', color: '#1A578A' }}>Zone de livraison</Text>
                                <Text style={{ fontSize: 12, fontFamily: 'Poppins-Medium', }}></Text>



                            </View>

                            <WebView
                                source={{ html: '<iframe src="https://www.google.com/maps/d/embed?mid=1ZObyaRrst9_KthXLOmCuqy8OpTrk466U" width="950" height="650"></iframe>' }}
                                style={{ marginTop: 0 }}
                                originWhitelist={['*']}

                            />
                        </View>


                    </View>


                    {/* <Image source={require("../assets/home_map.png")}
                        style={{ width: '90%', alignSelf: 'center' }}
                        resizeMode='cover'
                    /> */}

                    {/* <View style={{ width: '100%', marginTop: 5, marginBottom: 5, }}>


                        <View style={{ width: '90%', alignSelf: 'center', height: 400 }}>

                            <WebView
                                source={{ html: '<iframe src="https://www.google.com/maps/d/embed?mid=1ZObyaRrst9_KthXLOmCuqy8OpTrk466U" width="930" height="400"></iframe>' }}
                                style={{ marginTop: 0 }}
                            />

                        </View> */}




                    {/* </View> */}




                    <PopupDialog
                        onHardwareBackPress={() => { this.setState({ lodingDialog: false }) }}
                        width={0.3}
                        visible={this.state.lodingDialog}
                        dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
                        <DialogContent>
                            <View style={{ alignItems: 'center', }}>
                                <ActivityIndicator
                                    animating={this.state.animating}
                                    style={[{ height: 10, marginBottom: 10, marginTop: 30, marginLeft: 20, marginRight: 20 }]}
                                    color="#C00"
                                    size="large"
                                    hidesWhenStopped={true}
                                />
                            </View>
                        </DialogContent>
                    </PopupDialog>
                </ScrollView>


            </View>
        );
    }
}
