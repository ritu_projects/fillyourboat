import React from 'react';
import { StatusBar, ScrollView, Alert, TextInput, View, ActivityIndicator, ImageBackground, Modal, Text, Dimensions, TouchableOpacity, Linking, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from "../styles/styles";
import Strings from '../strings/strings';
import { Card, Radio, CheckBox } from 'native-base';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';
import DateTimePicker from "react-native-modal-datetime-picker";
import PopupDialog, {
    DialogTitle, DialogContent, DialogFooter, DialogButton, SlideAnimation, ScaleAnimation,
} from 'react-native-popup-dialog';


const { width, height } = Dimensions.get("window");

import Image from 'react-native-fast-image'


export default class FinalDeliver extends React.Component {

    constructor(props, context) {
        super(props);
        this.state = {
            user_id: '',
            token: '',
            orderid: '',
            order_record: '',
            boat_record: '',
            photo: '',
            reserve_capacity: '',
            animating: false,
            lodingDialog: false,
            modalVisible: false,
            modalVisible2: false,
            checked1: false,
            picture_without_url: null,
        }
    };

    componentDidMount() {
        this.getOrderSummery();
    }


    getOrderSummery = () => {
        AsyncStorage.getItem("userid")
            .then(userid => {
                var userid = JSON.parse(userid);

                this.setState({ user_id: userid });

                AsyncStorage.getItem("token")
                    .then(token => {
                        var accessToken = JSON.parse(token);

                        this.setState({ token: accessToken });
                        console.log("Order accessToken ", accessToken)


                        AsyncStorage.getItem("orderid")
                            .then(orderid => {

                                this.setState({ orderid: orderid });
                                console.log("Order Id ", orderid)
                                this.setState({
                                    animating: true,
                                    lodingDialog: true

                                });

                                fetch(Strings.base_Url + "getOrderBeforeComplition", {
                                    method: 'POST',
                                    headers: {
                                        // 'Accept': 'application/json',
                                        'Content-Type': 'application/json',
                                        'User-Id': this.state.user_id,
                                        'token': this.state.token,
                                    },
                                    body: JSON.stringify({
                                        order_id: this.state.orderid,

                                    })


                                })
                                    .then((response) => response.json())
                                    .then((responseData) => {
                                        console.log(" responseData getBoatInfo Delivery resoJSon===" + JSON.stringify(responseData));
                                        console.log("resoJSon getBoatInfo Delivery===" + responseData.error);
                                        if (responseData.error == "false") {

                                            console.log("Error false massage getBoatInfo Delivery " + responseData);
                                            console.log("Error false massage getBoatInfo Delivery " + responseData.boat_record);


                                            this.setState({
                                                animating: false,
                                                lodingDialog: false,
                                                order_record: responseData.order_record,
                                                boat_record: responseData.boat_record,
                                                reserve_capacity: responseData.boat_record.reserve_capacity,
                                                picture_without_url: responseData.order_record.photo,

                                                photo: responseData.photo_url + responseData.order_record.photo


                                            });
                                            console.log("Picture without url " + this.state.picture_without_url);


                                            //this.RBSheet.close()
                                        } else if (responseData.error == "true") {

                                            alert(responseData.errorMessage)
                                            this.setState({
                                                animating: false,
                                                lodingDialog: false

                                            });
                                        }
                                    }
                                    )
                            })
                    })
            })



    }

    userConformation1 = () => {

        if (this.state.checked1 == false) {
            alert('Veuillez cocher les cases suivantes')
        }
        else {
            this.setState({
                modalVisible: true,
            })
        }
    }

    userConformation2 = () => {
        this.setState({
            modalVisible: false,
          //  modalVisible2: true,
        })
        this.userConformation()

    }

    userConformation3 = () => {

        this.setState({
            modalVisible2: false,
        })

        this.userConformation()

    }



    userConformation = () => {
        console.log("Selected Zone ", this.state.orderid)
        console.log("Selected Zone fuel_type ", this.state.fuel_type)
        console.log("Selected Zone fuel_value ", this.state.fuel_value)


        this.setState({
            animating: true,
            lodingDialog: true,

        }); fetch(Strings.base_Url + "userConformation", {
            method: 'POST',
            headers: {
                // 'Accept': 'application/json',
                'Content-Type': 'application/json',
                'user-id': this.state.user_id,
                'token': this.state.token,
            },
            body: JSON.stringify({
                order_id: this.state.orderid,

            })
        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(" responseData addAddress resoJSon===" + JSON.stringify(responseData));
                console.log("resoJSon===" + responseData.error);
                if (responseData.error == "false") {
                    console.log("Error false massage" + JSON.stringify(responseData.errorMessage));

                    this.setState({
                        animating: false,
                        lodingDialog: false,

                    });
                    Actions.push("Trackorders", { currentOrderId: this.state.orderid })

                 //   Actions.push("Location")

                } else if (responseData.error == "true") {

                    alert(responseData.errorMessage)
                    this.setState({
                        animating: false,
                        lodingDialog: false

                    });
                }
            }
            )
    }


    render() {
        return (
            <View style={styles.containerWhite}>
                <View style={{ width: '100%', height: 50, flexDirection: 'row',marginTop: Platform.OS == 'ios' ? 20 :-5, }}>
                    <TouchableOpacity onPress={() => Actions.pop()} 
                    style={{ width: '15%', height: 50, justifyContent: 'center', alignItems: 'center', }}>
                        <Image source={require("../assets/back.png")}
                            style={{ width: 25, height: 25, }}
                            resizeMode="contain" />
                    </TouchableOpacity>
                    <View style={{ width: '80%', height: 50, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontSize: 16, fontWeight: 'bold' }}>{Strings.createReservation_text}</Text>

                    </View>

                </View>
                <ScrollView style={{}}>

                    <Text style={{
                        fontSize: 12.5,
                        marginLeft: 20,
                        marginRight: 10, color: '#1A578A', marginTop: 10, fontFamily: 'Poppins-SemiBold'
                    }}>Numéro de commande : {this.state.order_record.show_order_id}</Text>

                    <Card
                        style={{
                            width: width * 90 / 100, alignSelf: 'center',
                            backgroundColor: 'white',
                            borderRadius: 10, padding: 8
                        }}>

                        <View>
                            <View style={{ flexDirection: 'row', paddingBottom: 3, paddingTop: 3, width: '100%', justifyContent: 'space-between', alignItems: 'center' }}>

                                {/* {this.state.picture_without_url == null || this.state.picture_without_url == 'null' ?
                                  <Image style={{
                                    width: width * 28 / 100,
                                    height: width * 28 / 100,
                                    borderRadius: 20
                                }}
                                source={require("../assets/box_icon.png")}  />
                                :
                                <Image style={{
                                    width: width * 28 / 100,
                                    height: width * 28 / 100,
                                    borderRadius: 20
                                }}
                                    source={{ uri: this.state.photo }}
                                />

                             } */}

                                {this.state.picture_without_url == null || this.state.picture_without_url == 'null' ?
                                  
                                    null
                                    :
                                    <Image style={{
                                        width: width * 28 / 100,
                                        height: width * 28 / 100,
                                        borderRadius: 20
                                    }}
                                        source={{ uri: this.state.photo }}
                                    />

                                }


                                <View>


                                    <View
                                        style={{
                                            flexDirection: "row",
                                            marginLeft: 5,

                                        }}>

                                        <Text style={{
                                            fontSize: 11, marginLeft: 9, width: width * 62 / 100 - 25, fontFamily: 'Poppins-SemiBold'
                                        }}>Récapitulatif de la commande</Text>
                                    </View>

                                    <View
                                        style={{
                                            flexDirection: "row", paddingTop: 2, alignItems: 'center', paddingLeft: 10, width: width * 62 / 100 - 25,
                                        }}>
                                        <Text style={{
                                            paddingLeft: 10, width: width * 32 / 100,
                                            fontSize: 11, fontFamily: 'Poppins-Regular'


                                        }}>Tarif du carburant</Text>
                                        <Text style={{
                                            paddingLeft: 10,
                                            fontSize: 11, fontFamily: 'Poppins-Regular'


                                        }}>: {this.state.order_record.price_value}€</Text>

                                    </View>

                                    <View
                                        style={{
                                            flexDirection: "row", alignItems: 'center', paddingLeft: 10, width: width * 62 / 100 - 25,
                                        }}>
                                        <Text style={{
                                            paddingLeft: 10, width: width * 32 / 100,
                                            fontSize: 11, fontFamily: 'Poppins-Regular'


                                        }}>Équivalence en litre</Text>
                                        <Text style={{
                                            paddingLeft: 10,
                                            fontSize: 11, fontFamily: 'Poppins-Regular'


                                        }}>: {this.state.order_record.fuel_value}L</Text>

                                    </View>

                                    <View
                                        style={{
                                            flexDirection: "row", alignItems: 'center', paddingLeft: 10, width: width * 62 / 100 - 25,
                                        }}>
                                        <Text style={{
                                            paddingLeft: 10, width: width * 32 / 100,
                                            fontSize: 11, fontFamily: 'Poppins-Regular'

                                        }}>Tarif de livraison</Text>
                                        <Text style={{
                                            paddingLeft: 10,
                                            fontSize: 11, fontFamily: 'Poppins-Regular'


                                        }}>: {this.state.order_record.shipping_price}€</Text>
                                    </View>

                                    <View
                                        style={{
                                            flexDirection: "row", alignItems: 'center', paddingLeft: 10, width: width * 62 / 100 - 25,
                                        }}>
                                        <Text style={{
                                            paddingLeft: 10, width: width * 32 / 100,
                                            fontSize: 11, fontFamily: 'Poppins-Regular'

                                        }}>TVA</Text>
                                        <Text style={{
                                            paddingLeft: 10,
                                            fontSize: 11, fontFamily: 'Poppins-Regular'


                                        }}>: {this.state.order_record.tax}€</Text>
                                    </View>



                                    <View
                                        style={{
                                            flexDirection: "row", alignItems: 'center', paddingLeft: 10, width: width * 62 / 100 - 25,
                                        }}>
                                        <Text style={{
                                            paddingLeft: 10, width: width * 32 / 100,
                                            fontSize: 11, fontFamily: 'Poppins-SemiBold'

                                        }}>Total</Text>
                                        <Text style={{
                                            paddingLeft: 10,
                                            fontSize: 11, fontFamily: 'Poppins-SemiBold'


                                        }}>: {this.state.order_record.total} €</Text>
                                    </View>


                                </View>

                            </View>



                        </View>

                    </Card>
                    <Card
                        style={{
                            width: width * 90 / 100,
                            marginTop: 10, alignSelf: 'center',
                            backgroundColor: 'white',
                            padding: 7, borderRadius: 10
                        }}>

                        <View style={{ paddingBottom: 15, paddingTop: 15, width: width * 90 / 100, }}>

                            <View>
                                <View
                                    style={{
                                        flexDirection: "row",
                                        marginLeft: 10,

                                    }}>
                                    <Image style={{
                                        width: 16,
                                        height: 16,
                                        marginRight: 5,
                                    }}
                                        source={require("../assets/9.png")} />
                                    <Text style={{
                                        fontSize: 11, fontFamily: 'Poppins-SemiBold'
                                    }}>Mon emplacement et mon bateau </Text>
                                </View>
                            </View>
                            <View
                                style={{
                                    flexDirection: "row", marginLeft: 20,
                                }}>
                                <Text style={{
                                    paddingLeft: 10, color: 'grey',
                                    fontSize: 11, fontFamily: 'Poppins-Regular'


                                }}>Nom du bateau :{this.state.boat_record.ship_name}</Text>
                            </View>
                            <View
                                style={{
                                    flexDirection: "row", marginLeft: 20,
                                }}>
                                <Text style={{
                                    paddingLeft: 10,
                                    color: 'grey',
                                    fontSize: 11, fontFamily: 'Poppins-Regular'


                                }}>Sous-zone: {this.state.order_record.subzone}</Text>
                            </View>


                            {this.state.order_record.in_water == "1" ?
                                <View
                                    style={{
                                        flexDirection: "row", marginLeft: 20,
                                    }}>
                                    <Text style={{
                                        paddingLeft: 10,
                                        color: 'grey',
                                        fontSize: 11, fontFamily: 'Poppins-Regular'


                                    }}>Numéro de place: {this.state.order_record.seat_number}</Text>
                                </View>
                                :
                                <View
                                    style={{
                                        flexDirection: "row", marginLeft: 20,
                                    }}>
                                    <Text style={{
                                        paddingLeft: 10,
                                        color: 'grey',
                                        fontSize: 11, fontFamily: 'Poppins-Regular'


                                    }}>Numéro de corps-mort: {this.state.order_record.body_count}</Text>
                                </View>
                            }
    
                       
                            {/* <View
                                style={{
                                    flexDirection: "row", marginLeft: 20,
                                }}>
                                <Text style={{
                                    paddingLeft: 10,
                                    color: 'grey',
                                    fontSize: 11, fontFamily: 'Poppins-Regular'


                                }}>Port: {this.state.order_record.port_name}</Text>
                            </View>
                            <View
                                style={{
                                    flexDirection: "row", marginLeft: 20,
                                }}>
                                <Text style={{
                                    paddingLeft: 10,
                                    color: 'grey',
                                    fontSize: 11, fontFamily: 'Poppins-Regular'


                                }}>numéro de place ou numéro de corps-mort: {this.state.order_record.seat_number}</Text>
                            </View> */}

                        </View>

                        <View >
                            <View
                                style={{
                                    flexDirection: "row",
                                    marginLeft: 10, alignItems: 'center'

                                }}>
                                <Image style={{
                                    width: 15,
                                    height: 15,
                                    marginRight: 5,
                                }}
                                    source={require("../assets/calender.png")} />
                                <Text style={{
                                    fontSize: 11, fontFamily: 'Poppins-SemiBold'
                                }}>Date et horaire de livraison </Text>
                            </View>
                            <View style={{ flexDirection: 'row', paddingBottom: 15, marginLeft: 10, width: '100%', justifyContent: 'space-between', alignItems: 'center' }}>




                                <View
                                    style={{
                                        flexDirection: "row", paddingLeft: 10,
                                    }}>
                                    <Text style={{
                                        fontSize: 12, color: 'grey',
                                        paddingLeft: 10, fontFamily: 'Poppins-Regular'


                                    }}>{this.state.order_record.reservation_date} </Text>

                                    <Text style={{
                                        fontSize: 12, color: 'grey',
                                        paddingLeft: 10, fontFamily: 'Poppins-Regular'


                                    }}>{this.state.order_record.reservation_time}</Text>
                                </View>


                            </View>
                            {this.state.order_record.payment_type == 'Paypal' ?
                                <View
                                    style={{
                                        flexDirection: "row",
                                        marginLeft: 10,
                                        width: width * 80 / 100, alignSelf: 'center', alignItems: 'center', justifyContent: 'space-between'

                                    }}>

                                    <Text style={{
                                        fontSize: 11, fontFamily: 'Poppins-SemiBold'
                                    }}> Paiement effectué par</Text>
                                    <Image style={{
                                        width: 70,
                                        height: 60,
                                        marginRight: 5,
                                    }}
                                        source={require("../assets/paypal.png")} />
                                </View> : null}
                            {this.state.order_record.payment_type == 'Stripe' ?
                                <View
                                    style={{
                                        flexDirection: "row",
                                        marginLeft: 10,
                                        width: width * 80 / 100, alignSelf: 'center', alignItems: 'center', justifyContent: 'space-between'

                                    }}>

                                    <Text style={{
                                        fontSize: 11, fontFamily: 'Poppins-SemiBold'
                                    }}> Paiement effectué par</Text>
                                    <Image style={{
                                        width: 70,
                                        height: 60,
                                        marginRight: 5,
                                    }}
                                        source={require("../assets/21.png")} />
                                </View> : null}

                            {/* <View
                                            style={{
                                                flexDirection: "row",
                                                marginLeft: 10,
                                                width:width*80/100,alignSelf:'center',alignItems:'center',justifyContent:'space-between'

                                            }}>

                                            <Text style={{
                                                fontSize: 13,fontFamily:'Poppins-SemiBold'
                                            }}> Paiement effectué par</Text>
                                      <Image style={{
                               width: 70,
                               height: 60,
                               marginRight:5,
                            }}
                            source={require("../assets/21.png")} />
                                        </View> */}

                        </View>


                    </Card>



                    <TouchableOpacity
                        onPress={() => this.userConformation1()}
                        style={{
                            width: 270, backgroundColor: 'rgba(26,87,138,1)', marginTop: 20,
                            height: 37, alignSelf: 'center', borderRadius: 20,
                            flexDirection: 'row',
                            justifyContent: 'center', alignItems: 'center', marginBottom: 15, paddingLeft: 5,
                            paddingRight: 5,
                        }}>

                        {this.state.checked1 == true ?
                            <TouchableOpacity onPress={() => this.setState({ checked1: false })}
                                style={{ width: 25, height: 25, }}>
                                <Image source={require('../assets/4.png')}
                                    style={{ width: 15, height: 15, marginTop: 4 }} />

                            </TouchableOpacity> :
                            <TouchableOpacity
                                onPress={() => this.setState({ checked1: true })}
                                style={{ width: 25, height: 25, }}>
                                <View style={{
                                    width: 15, height: 15, borderRadius: 3,
                                    borderColor: 'grey', borderWidth: 1, marginTop: 4
                                }} />

                            </TouchableOpacity>}
                        <Text style={{ fontSize: 11.5, fontFamily: 'Poppins-SemiBold', color: 'white', }}>Je confirme ma commande</Text>

                    </TouchableOpacity>

                    {/* <TouchableOpacity
                        onPress={() => this.userConformation()}
                        style={{ flexDirection: 'row', height: 50, backgroundColor: 'rgba(26,87,138,1)', marginBottom: 20, borderRadius: 10, marginRight: 20 }}>

                        <View style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: 14, fontWeight: 'bold', color: 'white', }}>Je confirme ma commande</Text>
                        </View>
                    </TouchableOpacity> */}
                </ScrollView>


                <TouchableOpacity
                    style={{

                        width: 40,
                        height: 40,
                        borderRadius: 40 / 2,
                        backgroundColor: 'rgba(26,87,138,1)',
                        position: 'absolute',
                        justifyContent: 'center', alignItems: 'center', bottom: 10, right: 5,

                    }}

                    onPress={() => Actions.push("ChatwithAdmin", { currentOrderId: this.state.orderid })}
                >
                    <Image source={require("../assets/chat_white_icon.png")}
                        style={{ width: 20, height: 20, }}
                    />


                </TouchableOpacity>

                <Modal
                    animationType={'fade'}
                    transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        this.setState({ modalVisible: false });
                    }}
                >

                    <View
                        style={{
                            position: 'absolute',
                            width: width,
                            height: '100%',
                            alignItems: 'center',
                            justifyContent: 'center',
                            backgroundColor: 'rgba(0,0,0,0.5)',
                            padding: 20,
                        }}
                    >
                        <Image
                            resizeMode='stretch'
                            source={require('../assets/bg_image.png')}
                            style={{ width: width * 85 / 100, padding: 15, alignSelf: 'center', justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={require("../assets/12.png")}
                                style={{ width: 100, height: 100, borderRadius: 50, }}
                            />
                            <Text style={{
                                fontSize: 15, fontFamily: 'Poppins-SemiBold',
                                textAlign: 'center', marginTop: 2
                            }}>Confirmation de commande</Text>


                            <Text style={{
                                fontFamily: 'Poppins-Regular', fontSize: 12,
                                paddingLeft: 10, paddingRight: 10, paddingTop: 7, textAlign:'center',
                            }}>Vous allez recevoir la confirmation de </Text>
                            <Text style={{
                                fontFamily: 'Poppins-Regular', fontSize: 12,
                                paddingLeft: 15, marginRight: 15, paddingTop: 7,
                            }}>votre commande par mail</Text>
                            <Text style={{
                                fontFamily: 'Poppins-Regular', fontSize: 12,
                                paddingLeft: 13, marginRight: 13, paddingTop: 7,textAlign:'center',
                            }}>Fill Your Boat vous remercie !</Text>




                            <TouchableOpacity
                                onPress={() => this.userConformation2()}
                                style={{
                                    height: 40, width: 170,
                                    justifyContent: 'center', alignItems: 'center',
                                    backgroundColor: 'rgba(26,87,138,1)', marginBottom: 10,
                                    borderRadius: 20, marginTop: 15,
                                }}>

                                <Text style={{
                                    fontSize: 14, fontFamily: 'Poppins-SemiBold',
                                    color: 'white',
                                }}>D'accord </Text>
                            </TouchableOpacity>


                        </Image>
                    </View>
                </Modal>

                <Modal
                    animationType={'fade'}
                    transparent={true}
                    visible={this.state.modalVisible2}
                    onRequestClose={() => {
                        this.setState({ modalVisible2: false });
                    }}
                >

                    <View
                        style={{
                            position: 'absolute',
                            width: width,
                            height: '100%',
                            alignItems: 'center',
                            justifyContent: 'center',
                            backgroundColor: 'rgba(0,0,0,0.5)',
                            padding: 20,
                        }}
                    >
                        <Image
                            resizeMode='stretch'
                            source={require('../assets/bg_image.png')}
                            style={{ width: width * 85 / 100, padding: 15, alignSelf: 'center', justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={require("../assets/12.png")}
                                style={{ width: 100, height: 100, borderRadius: 50, }}
                            />
                            <Text style={{
                                fontSize: 15, fontFamily: 'Poppins-SemiBold',
                                textAlign: 'center', marginTop: 2
                            }}>Commande envoyée</Text>


                            <Text style={{
                                fontFamily: 'Poppins-Regular', fontSize: 12,
                                paddingLeft: 10, paddingRight: 10, paddingTop: 7,
                            }}> Votre commande a été passée, elle sera confirmée prochainement.</Text>
                            {/* <Text style={{
                                fontFamily: 'Poppins-Regular', fontSize: 12,
                                paddingLeft: 15, marginRight: 15, paddingTop: 7,
                            }}>vous pouvez suivre votre </Text> */}
                            {/* <Text style={{
                                fontFamily: 'Poppins-Regular', fontSize: 12,
                                paddingLeft: 13, marginRight: 13, paddingTop: 7,
                            }}>commande maintenant.</Text> */}




                            <TouchableOpacity
                                onPress={() => this.userConformation3()}
                                style={{
                                    height: 40, width: 180,
                                    justifyContent: 'center', alignItems: 'center',
                                    backgroundColor: 'rgba(26,87,138,1)',
                                    marginBottom: 10, borderRadius: 20,
                                    marginTop: 15,
                                }}>

                                <Text style={{
                                    fontSize: 11, fontFamily: 'Poppins-SemiBold',
                                    color: 'white',
                                }}>D'accord </Text>
                            </TouchableOpacity>


                        </Image>
                    </View>
                </Modal>
                <PopupDialog
                    onHardwareBackPress={() => { this.setState({ lodingDialog: false }) }}
                    width={0.3}
                    visible={this.state.lodingDialog}
                    dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
                    <DialogContent>
                        <View style={{ alignItems: 'center', }}>
                            <ActivityIndicator
                                animating={this.state.animating}
                                style={[{ height: 10, marginBottom: 10, marginTop: 30, marginLeft: 20, marginRight: 20 }]}
                                color="#C00"
                                size="large"
                                hidesWhenStopped={true}
                            />
                        </View>
                    </DialogContent>
                </PopupDialog>

            </View>
        );
    }
}