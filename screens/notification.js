import React from 'react';
import { StatusBar, Button,ActivityIndicator, FlatList, ScrollView, PixelRatio, TextInput, View, ImageBackground, Text, Dimensions, TouchableOpacity, Linking, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from "../styles/styles";
import Strings from '../strings/strings'
import { Card } from 'native-base';
import { Rating, AirbnbRating } from 'react-native-elements';
import PopupDialog, {
    DialogTitle, DialogContent, DialogFooter, DialogButton, SlideAnimation, ScaleAnimation,
} from 'react-native-popup-dialog';
import Image from 'react-native-fast-image'

export default class notification extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            animating: false,
            lodingDialog: false,
        };
    }
    componentWillMount() {
        this.setState({
          animating: true,
          lodingDialog: true,
    
        })
        AsyncStorage.getItem("userid")
          .then(userid => {
            this.setState({ user_id: userid });
    
            AsyncStorage.getItem("token")
              .then(token => {
                var accessToken = JSON.parse(token);
                this.setState({ token: accessToken });
                console.log("accessToken====$$$========" + accessToken);
                console.log("state userId====$$$========" + userid);
    
                fetch(Strings.base_Url + "userNotify", {
                  method: 'POST',
                  headers: {
                    // 'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'User-Id': userid,
                    'token': accessToken,
                  },
                  body: JSON.stringify({
                    user_id: userid,
                  })
                })
                  .then((response) => response.json())
                  .then((responseData) => {
                    console.log(" responseData resoJSon===" + JSON.stringify(responseData));
                    console.log("resoJSon===" + responseData.error);
                    if (responseData.error == "false") {
                      console.log("Error false massage" + JSON.stringify(responseData.errorMessage));
                      this.setState({
                        data:responseData.detail,
                        animating: false,
                        lodingDialog: false,
                      });
                    } else if (responseData.error == "true") {
                      this.setState({
                        passwordErr: responseData.errorMessage,
                        animating: false,
                        lodingDialog: false
    
                      });
                    }
                  }
                  )
              });
          });
      }



    render() {
        return (
            <View style={styles.containerWhite}>
                 <Image source={require("../assets/simble.png")}
                    style={{ width: '100%', height: 78, }}>
                    <View style={{ width: '100%', height: 50, flexDirection: 'row' }}>
                        <TouchableOpacity onPress={() => Actions.pop()} style={{ width: '15%', height: 50, justifyContent: 'center', alignItems: 'center', }}>
                            <Image source={require("../assets/back.png")}
                                style={{ width: 25, height: 25, }}
                                resizeMode="contain" />
                        </TouchableOpacity>
                        <View style={{ width: '80%', height: 50, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: 16, fontWeight: 'bold' }}>{Strings.notificatiin_text}</Text>
                        </View>

                    </View>
                </Image>
                {/* <Image source={require("../assets/Artboard–2.png")}
                            style={{ width:'100%', height:30, }}
                            resizeMode='stretch' /> */}

                <ScrollView style={{marginTop:-10}}> 
                    <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }}>
                   {/* <View style={{width:'100%',height:40,alignItems:'flex-end'}}>
                       <View style={{flexDirection:'row',alignItems:'center'}}>
                       <TouchableOpacity style={{width:30,height:30,alignItems:'center',justifyContent:'center'}}>
                          <Image source={require("../assets/filter.png")}
                            style={{ width: 25, height:25, }}
                            resizeMode="contain" />
                          </TouchableOpacity>
                           <Text style={{fontSize:14,padding:10}}>{Strings.lis_tout_text}</Text>
                          <TouchableOpacity style={{width:30,height:30,alignItems:'center',justifyContent:'center'}}>
                          <Image source={require("../assets/close-button.png")}
                            style={{ width: 15, height: 15, }}
                            resizeMode="contain" />
                          </TouchableOpacity>
                          
                       </View>
                   </View> */}
                   
                    <FlatList
                            keyExtractor={item => item.id}
                            data={this.state.data}
                            renderItem={({ item }) => (
                                <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 10 }}>
                                    <Card style={{ width: "95%", justifyContent: 'space-between', alignItems: 'center', flexDirection: 'column', padding: 10, borderRadius: 15 }}>
                                        <View style={{ width: '100%', flexDirection: 'row'}}>
                                            {/* <View style={{ width: '20%', justifyContent: 'center', alignItems: 'center' }}>
                                                <Image source={require("../assets/5.png")}
                                                    style={{ width: 50, height: 50, }}
                                                    resizeMode="contain" />
                                            </View> */}
                                            <View style={{ width: '100%', flexDirection: 'column' }}>
                                                <Text style={{fontSize: 14, fontWeight: 'bold',paddingLeft:10 }}>{item.message}</Text>
                                                <Text style={{fontSize: 14 ,textAlign:'right'}}>{item.created_at}</Text>
                                            </View>                                                                
                                        </View>
                                    </Card>
                                </View>
                            )}
                        />
                    </View>


                    <PopupDialog
                            onHardwareBackPress={() => { this.setState({ lodingDialog: false }) }}
                            width={0.3}
                            visible={this.state.lodingDialog}
                            dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
                            <DialogContent>
                                <View style={{ alignItems: 'center', }}>
                                    <ActivityIndicator
                                        animating={this.state.animating}
                                        style={[{ height: 10, marginBottom: 10, marginTop: 30, marginLeft: 20, marginRight: 20 }]}
                                        color="#C00"
                                        size="large"
                                        hidesWhenStopped={true}
                                    />
                                </View>
                            </DialogContent>
                        </PopupDialog>
                </ScrollView>
            </View>
        );
    }
}
