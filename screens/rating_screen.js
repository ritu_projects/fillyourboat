import React from 'react';
import { StatusBar, Button, ActivityIndicator, Alert, Modal, FlatList, ScrollView, PixelRatio, TextInput, View, ImageBackground, Text, Dimensions, TouchableOpacity, Linking, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from "../styles/styles";
import Strings from '../strings/strings'
import { Card } from 'native-base';
import { Rating, AirbnbRating } from 'react-native-elements';
const { width, height } = Dimensions.get("window");
import PopupDialog, {
    DialogTitle, DialogContent, DialogFooter, DialogButton, SlideAnimation, ScaleAnimation,
} from 'react-native-popup-dialog';

let tech_rating = '';
import Image from 'react-native-fast-image'

export default class Rating_screen extends React.Component {
    constructor(props) {
        // console.log("currentOrderId::::" + this.props.currentOrderId)
        super(props);
        this.state = {
            user_id: '', token: '', ratingErr: '',
            modalVisible: true, animating: false, lodingDialog: false, rating_textErr: '', comment: '',
        }


    }

    validaion() {
        console.log('rating   ' + tech_rating)
        if (tech_rating != '' && tech_rating != null) {
            this.submitRating();
        }
        else {
            this.setState({
                ratingErr: Strings.ShouldemptyText
            })
        }

    }


    componentDidMount() {

        console.log("Did mount Past%%%" + "Did mount Past%%%");

        // this.setState({
        //     animating: true,
        //     lodingDialog: true,

        // });

        AsyncStorage.getItem("token")
            .then(token => {
                var accessToken = JSON.parse(token);
                console.log("accessToken============" + accessToken);

                this.setState({ token: accessToken });

            });


        AsyncStorage.getItem("userid")

            .then(userid => {
                var accessToken = JSON.parse(userid);
                this.setState({ userid: userid });
                console.log("userid============" + userid);
            })
    }
    ratingCompleted(rating) {
        tech_rating = rating

        //  this.setState({ratingErr:''})
        console.log("Rating is: " + rating)
    }
    submitRating() {

        this.setState({
            modalVisible: false,
            animating: true,
            lodingDialog: true
        });
        fetch(Strings.base_Url + "addTechnicianRating", {
            method: 'POST',
            headers: {
                // 'Accept': 'application/json',
                'Content-Type': 'application/json',
                'User-Id': this.state.userid,
                'token': this.state.token,
            },
            body: JSON.stringify({
                order_id: this.props.currentOrderId,
                //order_id:219,
                user_id: this.state.userid,
                rating: tech_rating,
                description: this.state.comment
            })
        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(" responseData resoJSon===" + JSON.stringify(responseData));
                console.log("resoJSon===" + responseData.error);
                if (responseData.error == "false") {
                    console.log("Error false massage" + JSON.stringify(responseData.errorMessage));
                    this.setState({
                        modalVisible: false,

                        animating: false,
                        lodingDialog: false,
                    });

                    //   this.getOrderStatus();
                    Actions.push('Location')

                } else if (responseData.error == "true") {
                    Actions.push('Location')
                    alert(responseData.errorMessage)
                    this.setState({
                        animating: false,
                        lodingDialog: false,
                        //modalVisible:true
                    });
                }
            })
    }
    render() {

        return (
            <View>
                <Image 
                source={require("../assets/100.png")}
                    style={{
                        width: width,
                        height: height,
                        resizeMode: 'stretch',

                    }}
                    resizeMode="stretch">
                    <Modal
                        animationType={'fade'}
                        transparent={true}
                        visible={this.state.modalVisible}
                        onRequestClose={() => {
                            this.setState({ modalVisible: false });
                        }}
                    >

                        <View
                            style={{
                                position: 'absolute',
                                width: width,
                                height: '100%',
                                alignItems: 'center',
                                justifyContent: 'center',
                                backgroundColor: 'rgba(0,0,0,0.5)',
                                padding: 20,
                            }}
                        >
                            <Image
                                resizeMode='stretch'
                                source={require('../assets/bg_image.png')}
                                style={{ width: width * 85 / 100, padding: 20, alignSelf: 'center', justifyContent: 'center', alignItems: 'center' }}>
                                <Image source={require("../assets/12.png")}
                                    style={{ width: width * 22 / 100, height: width * 22 / 100, borderRadius: 50, }}
                                />
                                <Text style={{ fontSize: 15, fontFamily: 'Poppins-SemiBold', textAlign: 'center', marginTop: 5 }}>Commande livrée</Text>
                                <Text style={{ fontSize: 11, fontFamily: 'Poppins-Regular', marginTop: height * 1.3 / 100, textAlign: 'center', color: 'grey' }}>Recommanderiez-vous ce service ?</Text>
                                <View style={{ width: width * 76 / 100, alignSelf: 'center' }}>
                                    <View style={{ marginTop: 10 }}>
                                        <AirbnbRating
                                            showRating={false}
                                            count={5}
                                            ratingColor='rgba(26,87,138,1)'
                                            defaultRating={0}
                                            ratingColor='red'
                                            ratingBackgroundColor='red'
                                            size={30}
                                            onFinishRating={this.ratingCompleted} />
                                        {/* <Rating
  type='custom'
  imageSize={30}
  ratingCount={5}
  defaultRating={0}
//   ratingImage={WATER_IMAGE}
  ratingColor='rgba(26,87,138,1)'
  onFinishRating={this.ratingCompleted}
  ratingBackgroundColor='#D3D3D3'/> */}
                                    </View>


                                    {this.state.ratingErr != '' ?
                                        <Text style={{ color: 'red', textAlign: 'center', marginLeft: 10, fontSize: 12 }}>{this.state.ratingErr}</Text>
                                        : null}
                                </View>

                                <Card style={{ width: width * 72 / 100, padding: 2, alignSelf: 'center', borderRadius: 10 }}>
                                    <TextInput
                                        placeholder='Tout commentaire'
                                        onChangeText={(comment) => this.setState({ comment, commentErr: '' })}

                                        style={{ fontFamily: 'Poppins-Medium', color: 'grey', textAlign: 'center', fontSize: 11 }} />



                                </Card>
                                {!!this.state.commentErr && (
                                    <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.commentErr}</Text>
                                )}

                                <TouchableOpacity
                                    onPress={() => this.validaion()}
                                    style={{
                                        height: 32, width: 200, marginTop: height * 2 / 100,
                                        justifyContent: 'center', alignItems: 'center',
                                        backgroundColor: 'rgba(26,87,138,1)', marginBottom: 20, borderRadius: 20,
                                    }}>

                                    <Text style={{ fontSize: 13, fontFamily: 'Poppins-SemiBold', color: 'white', }}>Soumettre </Text>
                                </TouchableOpacity>


                            </Image>
                        </View>
                    </Modal>
                    <PopupDialog
                        onHardwareBackPress={() => { this.setState({ lodingDialog: false }) }}
                        width={0.3}
                        visible={this.state.lodingDialog}
                        dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
                        <DialogContent>
                            <View style={{ alignItems: 'center', }}>
                                <ActivityIndicator
                                    animating={this.state.animating}
                                    style={[{ height: 10, marginBottom: 10, marginTop: 30, marginLeft: 20, marginRight: 20 }]}
                                    color="#C00"
                                    size="large"
                                    hidesWhenStopped={true}
                                />
                            </View>
                        </DialogContent>
                    </PopupDialog>
                </Image>
            </View>

        )
    }
}