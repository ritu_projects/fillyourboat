import React from 'react';
import { StatusBar, DrawerLayoutAndroid, TextInput, ActivityIndicator, View, ScrollView, Text, Button, Dimensions, TouchableOpacity, Linking, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from "../styles/styles";
import strings from '../strings/strings';
import Sidebar from '../screens/sideBar'
import RBSheet from "react-native-raw-bottom-sheet";
import { Container, Header, Content, Tab, Tabs } from 'native-base';
import ImagePicker from 'react-native-image-picker';
import CurrentTabs from '../screens/CurrentTab';
import PastTabs from '../screens/PastTabs';
import Drawer from 'react-native-drawer'

const { width, height } = Dimensions.get("window");
import PopupDialog, {
    DialogTitle, DialogContent, DialogFooter, DialogButton, SlideAnimation, ScaleAnimation,
} from 'react-native-popup-dialog';
import Image from 'react-native-fast-image'





export default class MyOrders extends React.Component {

    constructor(props) {
        super(props);

        new PastTabs();

        this.state = {
            isHidden: false,
            currentTab: '',
        };
    }


    callPast() {

        console.log("Did mount%%%" + "Did mount%%%");
        this.pasttabs.setState({
            order_from: '',
            order_to: '',
            start_date: "De",
            end_date: "À",
        })
        this.pasttabs.componentDidMount();

    }

    _setDrawer() {
        this._drawer.open()
    }
    render() {
        var navigationView = (
            <Sidebar />
        );

        return (
            console.log("sseltce tab:::::::" + !!this.state.isHidden),
            <Drawer
            openDrawerOffset={100}
           type="overlay"
             ref={(ref) => this._drawer = ref}
             content={<Sidebar />}
             >
                <View style={styles.containerWhite}>
                    <View style={{ flex: 1, flexDirection: 'column', backgroundColor: 'white',marginTop: Platform.OS == 'ios' ? 20 :0 }}>

                        <View style={{ width: '100%', alignItems: 'center', height: 50, flexDirection: 'row', justifyContent: 'space-between' }}>
                            <TouchableOpacity onPress={this._setDrawer.bind(this)} style={{
                                height: 50,
                                justifyContent: 'center',
                            }}>
                                <Image source={require("../assets/menu_img.png")}
                                    style={{ width: 40, height: 40, }}
                                    resizeMode="contain" />
                            </TouchableOpacity>
                            <View style={{ height: 50, justifyContent: 'center', alignItems: 'center', }}>
                                <Text style={{ fontFamily: "Poppins-SemiBold", fontSize: 16, }}>Mes commandes</Text>
                            </View>
                            <View style={{ width: 20, height: 20, marginRight: 8 }} />

                        </View>


                        <Container>

                            <Tabs

                                tabBarUnderlineStyle={{ backgroundColor: '#1A578A' }}
                                onChangeTab={({ i }) => {
                                    console.log("sseltce:::::::" + i);
                                    this.setState.bind({ currentTab: i })
                                    if (i == 0) {
                                        this.setState({ isHidden: false });

                                    }

                                    if (i == 1) {
                                        console.log("sseltce one:::::::" + i);

                                        this.setState({ isHidden: true });

                                    }
                                }
                                }>

                                <Tab heading='Commandes en cours' tabStyle={{ backgroundColor: 'white', }}
                                    textStyle={{ color: 'grey', fontFamily: 'Poppins-Medium', fontSize: 13 }} activeTabStyle={{ backgroundColor: 'white' }}
                                    activeTextStyle={{ color: '#1A578A', fontFamily: 'Poppins-Medium', fontSize: 13 }}>
                                    <CurrentTabs />
                                </Tab>
                                <Tab



                                    heading='Commandes passées' tabStyle={{ backgroundColor: 'white' }}
                                    textStyle={{ color: 'grey', fontFamily: 'Poppins-Medium', fontSize: 13 }} activeTabStyle={{ backgroundColor: 'white' }}
                                    activeTextStyle={{ color: '#1A578A', fontFamily: 'Poppins-Medium', fontSize: 13 }}

                                >

                                    <PastTabs

                                        ref={ref => (this.pasttabs = ref)}      //<--- By using ref you can call child class method
                                    />
                                </Tab>
                                {/* <Tab heading={strings.avenirText} tabStyle={{backgroundColor: 'white'}}
           textStyle={{color: 'black'}} activeTabStyle={{backgroundColor: 'white'}}
            activeTextStyle={{color: 'red', fontWeight: 'normal'}}>
            <ComeUpTabs />
          </Tab> */}
                            </Tabs>
                        </Container>


                    </View>


                </View>
             </Drawer>

        );
    }
}
