import React from "react";
import { View, Text, TouchableOpacity, Modal,AsyncStorage} from "react-native";
import { WebView } from 'react-native-webview';
import { Actions } from 'react-native-router-flux';
export default class FinalPaypalPayment extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
          order_id:'',
          amount:null,
        }

        console.log("currentOrderId paypal page::::"+this.props.req_id +"total:::"+this.props.total_amount +"cat id::"+this.props.current_cat_id)
    }

    handleNavigation(event){
        console.log(event)
        console.log("Url::::"+event.url)

      //  let path = `${match.event.url}/http://swissexpress.info/orderPaymentSuccess`;
      var String_url=JSON.stringify(event.url);
        console.log("Url: path:::"+String_url)
        var name = String_url.substring(39,58);
        var cancel_order=String_url.substring(39,57)
        console.log("payment Succefully:::"+name);
        console.log("payment Not Succefully:::"+cancel_order);

        if(name === "finalPaymentSuccess"){
            Actions.push("Rating",{req_Id:this.props.req_id})
        }else if(cancel_order=== "orderPaymentCancel"){
            alert("commande Paiement Annuler")
            Actions.pop();
            Actions.push("AddPayment");
        }
    }
    // handleMessage(event){
    //     let data = event.nativeEvent.data;
    //     console.log("data::::::"+data)
    //     data = JSON.parse(data);
       
    //     if(data.status == 'success'){
    //         alert(data.reference);
            
    //     }else{
    //         this.setState({loading: false});
    //         alert('Failed, '+ data.message);
            
    //     }
        
    // }

  
    render() {
        return (
            <WebView
               style={{overflow: 'scroll'}}
               source={{uri: "https://www.curzenncloud.fr/hopassist/api/job_final_payment?req_id="+this.props.req_id+"&amount="+this.props.total_amount}}
               originWhitelist={["*"]}
               mixedContentMode={'always'}
               useWebKit={Platform.OS == 'ios'}
               //onError={() => {alert('Error Occured'); Actions.pop()}}
              // onLoadEnd={() => this.passValues()}
               ref="webview"
               thirdPartyCookiesEnabled={true}
               scrollEnabled={true}
               domStorageEnabled={true}
               startInLoadingState={true}
               injectedJavaScript={this.patchPostMessageJsCode}
               allowUniversalAccessFromFileURLs={true}
               onMessage={(event) => this.handleMessage(event)}
               onNavigationStateChange={(event) => this.handleNavigation(event)}
               javaScriptEnabled={true}
             />
        );
    }
}