import React from 'react';
import { StatusBar, Alert, DrawerLayoutAndroid, ActivityIndicator, StyleSheet, ScrollView, FlatList, AsyncStorage, TextInput, View, ImageBackground, Text, Dimensions, TouchableOpacity, Linking, } from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from "../styles/styles";
import Svg, { Ellipse } from "react-native-svg";
import Strings from '../strings/strings'
const { width, height } = Dimensions.get("window");
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';
import RBSheet from "react-native-raw-bottom-sheet";
import SearchCategory from "react-native-raw-bottom-sheet"
import RNGooglePlaces from 'react-native-google-places';
import Sidebar from '../screens/sideBar'
import { FlatGrid } from 'react-native-super-grid';
import Geocoder from 'react-native-geocoder';
import { Card } from "native-base";
import PopupDialog, {
    DialogTitle, DialogContent, DialogFooter, DialogButton, SlideAnimation, ScaleAnimation,
} from 'react-native-popup-dialog';
import { Dropdown } from 'react-native-material-dropdown';
import Drawer from 'react-native-drawer'
import Image from 'react-native-fast-image'

let drop_down_categorydata = [];

const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = 0.0421

export default class Location1 extends React.Component {
    constructor(props, context) {
        super(props);
        this.state = {
        }
    }

    componentDidMount() {
        //this.getZoneList()

        this._drawer.close()
    }

    _setDrawer() {
        this._drawer.open()
    }

    alertFunc() {
        alert('Hello');
    }
    render() {
        var navigationView = (
            <Sidebar />
        );
        return (
            <Drawer
            openDrawerOffset={100}
           type="overlay"
             ref={(ref) => this._drawer = ref}
             content={<Sidebar />}
             >
                <View style={styles.containerWhite}>
                    <View style={{ width: '100%', alignItems: 'center', height: 50, flexDirection: 'row', justifyContent: 'space-between' }}>
                        <TouchableOpacity onPress={this._setDrawer.bind(this)} style={{
                            height: 50,
                            justifyContent: 'center',
                        }}>
                            <Image source={require("../assets/menu_img.png")}
                                style={{ width: 40, height: 40, }}
                                resizeMode="contain" />
                        </TouchableOpacity>
                        <View style={{ height: 50, justifyContent: 'center', alignItems: 'center', }}>
                            <Text style={{ fontFamily: "Poppins-SemiBold", fontSize: 16, }}>{Strings.alert_tital}</Text>
                        </View>
                        <View style={{ width: 32, height: 20, marginRight: 8 }}
                        />

                    </View>
                    <View style={{ flex: 1, }}>
                        <Image
                            source={require("../assets/MAP.png")}
                            resizeMode="stretch"
                            style={{
                                width: width,
                                height: height * 65 / 100,
                            }}
                            imageStyle={{}}
                        >


                        </Image>
                    </View>

                    <Image
                        resizeMode='stretch'
                        style={{ width: width * 105 / 100, marginBottom: -height * 15 / 100, height: height * 54 / 100, alignSelf: 'center', padding: 13, }}
                        source={require("../assets/bg_image.png")}>
                        <View style={{ alignItems: 'center', }}>

                            <View style={{ flexDirection: 'row', width: width * 26 / 100, height: width * 26 / 100, alignItems: 'center' }}>
                                <Image source={require("../assets/12.png")}
                                    resizeMode={'stretch'}
                                    style={{ width: width * 26 / 100, height: width * 26 / 100, resizeMode: 'stretch' }}
                                />
                            </View>
                            <View style={{ justifyContent: 'center', }}>
                                <Text style={{
                                    alignSelf: 'center',
                                    fontSize: width * 3.3 / 100, width: width * 87 / 100, textAlign: 'center', fontFamily: "Poppins-Bold", color: '#303030'
                                    , marginTop: height * 2.8 / 100,
                                }}>Sélectionnez la zone où votre
            </Text>
                                <Text style={{
                                    alignSelf: 'center',
                                    fontSize: width * 3.3 / 100, width: width * 87 / 100, textAlign: 'center', fontFamily: "Poppins-Bold", color: '#303030'
                                    , marginTop: -2,
                                }}>
                                    bateau est amarré</Text>
                                <Text style={{
                                    alignSelf: 'center', color: '#28528F',
                                    fontSize: width * 3.3 / 100, width: width * 87 / 100, textAlign: 'center',
                                     fontFamily: "Poppins-Bold"
                                    , marginTop: height * 3 / 100,
                                }}>Balayer
            </Text>
                                <TouchableOpacity onPress={() => Actions.push('Location')} 
                                style={{ marginTop: height * 1.5 / 100, alignSelf: 'center' }}>
                                    <Image source={require('../assets/Group_10398.png')} 
                                    style={{ width: 18, height: 15, resizeMode: 'stretch' }} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Image>
                </View>
             </Drawer>
        )
    }
}