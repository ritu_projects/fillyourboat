import React from 'react';
import { StatusBar, ScrollView, Alert, TextInput, View, ActivityIndicator, ImageBackground, Text, Dimensions, TouchableOpacity, Linking, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from "../styles/styles";
import Strings from '../strings/strings';
import { Card, Radio, CheckBox } from 'native-base';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';
import DateTimePicker from "react-native-modal-datetime-picker";
import PopupDialog, {
    DialogTitle, DialogContent, DialogFooter, DialogButton, SlideAnimation, ScaleAnimation,
} from 'react-native-popup-dialog';
import { Dropdown } from 'react-native-material-dropdown';
let drop_down_categorydata = [];
const { width, height } = Dimensions.get("window");
import Image from 'react-native-fast-image'

export default class Editinforfororder extends React.Component {
    // onRegionChange(region) {
    //   this.setState({ region });
    // }
    constructor(props, context) {
        super(props);
        this.state = {
            animating: false,
            lodingDialog: false,

            first_name: '',
            first_nameErr: '',
            last_name: '',
            last_nameErr: '',
            address: '',
            addressErr: '',
            email: '',
            emailErr: '',
            password: '',
            passwordErr: '',
            selected: '',
            selectederror: '',
            user_id: '',
            token: '',
            confirm_password: '',
            confirm_password_err: '',
            telephone: '',
            telephoneErr: '',
            uploads3: '',
            uploads_Err3: '',
            orderid: '',
            full_name: '',
            full_name_err: '',
            phone_number: '',
            phone_number_err: '',
            comment: '',
            comment_err: '',
            body_count: '',
            body_count_err: ','

        };
    }
    componentDidMount() {
        this.getFuelList();
    }

    valueExtractor = val => {
        console.log("vehicle id::::****:$$::" + JSON.stringify(val));
    };
    onChangeTextPress(id) {

        this.setState({
            selected: id,
            selectederror: '',
        })
    }

    selectProfilePic = () => {
        const options = {
            title: 'Select Avatar',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };

        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                const source = { uri: response.uri };

                const imageUrl = response.uri
                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };
                console.log("image url:###:" + JSON.stringify(response))
                this.setState({
                    uploads3: response.uri ,
                    uploads_Err3: '',
                });
                console.log("image url::" + JSON.stringify(imageUrl))
                console.log("image url::" + JSON.stringify(source))


            }
        });
    }




    validationRegister() {
        // this.UserRegister();
        var isValidate = 0;
        if (this.state.first_name != "") {
            isValidate += 1;
        } else {
            isValidate -= 1
            this.setState({
                first_nameErr: Strings.ShouldemptyText,
            });
        }
        if (this.state.last_name != "") {
            isValidate += 1;
        } else {
            isValidate -= 1
            this.setState({
                last_nameErr: Strings.ShouldemptyText,
            });
        }
        if (this.state.email != "") {
            isValidate += 1;

        } else {
            console.log("cat empty::::");
            isValidate -= 1;
            this.setState({
                emailErr: Strings.ShouldemptyText,
            });
        }

        if (this.state.telephone != "") {
            isValidate += 1
        } else {
            isValidate -= 1;
            this.setState({
                telephoneErr: Strings.ShouldemptyText,
            });
        }

        if (this.state.password != "") {
            isValidate += 1;

        } else {
            console.log("cat empty::::");
            isValidate -= 1;
            this.setState({
                passwordErr: Strings.ShouldemptyText,
            });
        }

        if (this.state.selected != "" && this.state.selected != "Type de carburant") {

            isValidate += 1;

        }
        else {
            console.log("cat empty::::");
            isValidate -= 1;
            this.setState({
                selectederror: Strings.ShouldemptyText,
            });
        }

        if (this.state.full_name != "") {

            isValidate += 1;

        }
        else {
            console.log("cat empty::::");
            isValidate -= 1;
            this.setState({
                full_name_err: Strings.ShouldemptyText,
            });
        }
        if (this.state.phone_number != "") {

            isValidate += 1;

        }
        else {
            console.log("cat empty::::");
            isValidate -= 1;
            this.setState({
                phone_number_err: Strings.ShouldemptyText,
            });
        }

        if (this.state.comment != "") {

            isValidate += 1;

        }
        else {
            console.log("cat empty::::");
            isValidate -= 1;
            this.setState({
                comment_err: Strings.ShouldemptyText,
            });
        }
        if (this.state.body_count != "") {

            isValidate += 1;

        }
        else {
            console.log("cat empty::::");
            isValidate -= 1;
            this.setState({
                body_count_err: Strings.ShouldemptyText,
            });
        }



        if (this.state.uploads3 != '') {

            isValidate += 1;

        }
        else {
            console.log("cat empty::::");
            isValidate -= 1;
            this.setState({
                uploads_Err3: Strings.ShouldemptyText,
            });
        }




        console.log("Use Login::::" + isValidate)


        if (isValidate == 11) {

            console.log("Use Login::::" + this.state.first_name + this.state.last_name)

            this.userAddBoat();
            this.setState({
                animating: true,
                lodingDialog: true,

            });
        }
        else {

        }

    }

    getFuelList = () => {


        AsyncStorage.getItem("userid")
            .then(userid => {
                var userid = JSON.parse(userid);

                this.setState({ user_id: userid });

                AsyncStorage.getItem("token")
                    .then(token => {
                        var accessToken = JSON.parse(token);
                        this.setState({ token: accessToken });

                        AsyncStorage.getItem("orderid")
                            .then(orderid => {

                                this.setState({ orderid: orderid });

                                this.setState({
                                    animating: true,
                                    lodingDialog: true,

                                });

                                fetch(Strings.base_Url + "getFuelrates", {
                                    method: 'GET',
                                    headers: {
                                        // 'Accept': 'application/json',
                                        'Content-Type': 'application/json',
                                        'User-Id': this.state.user_id,
                                        'token': this.state.token,
                                    },

                                })
                                    .then((response) => response.json())
                                    .then((responseData) => {
                                        console.log(" responseData getFuelList resoJSon===" + JSON.stringify(responseData));
                                        console.log("resoJSon getFuelList===" + responseData.error);
                                        if (responseData.error == "false") {
                                            console.log("Error false massage getFuelList" + JSON.stringify(responseData.record));



                                            this.setState({
                                                animating: false,
                                                lodingDialog: false,

                                            });

                                            var obj = responseData.record;
                                            console.log("resoJSon GetVehicle obj===" + obj);

                                            var count = Object.keys(obj).length;
                                            console.log("resoJSon GetVehicle count===" + count);
                                            this.setState({
                                                drop_down_categorydata: []
                                            });
                                            for (var i = 0; i < count; i++) {
                                                drop_down_categorydata.push({
                                                    value: obj[i].id,

                                                    label: obj[i].fuel
                                                });
                                            }


                                            //this.RBSheet.close()
                                        } else if (responseData.error == "true") {

                                            this.showalerts(responseData.errorMessage)
                                            this.setState({
                                                animating: false,
                                                lodingDialog: false

                                            });
                                        }
                                    }
                                    )
                            })
                    })
            })

        this.getBoatInformation();

    }
    getBoatInformation = () => {

        AsyncStorage.getItem("userid")
            .then(userid => {
                var userid = JSON.parse(userid);

                this.setState({ user_id: userid });

                AsyncStorage.getItem("token")
                    .then(token => {
                        var accessToken = JSON.parse(token);
                        this.setState({ token: accessToken });

                        AsyncStorage.getItem("orderid")
                            .then(orderid => {

                                this.setState({ orderid: orderid });

                                fetch(Strings.base_Url + "orderGetBoatInfo", {
                                    method: 'POST',
                                    headers: {
                                        // 'Accept': 'application/json',
                                        'Content-Type': 'application/json',
                                        'User-Id': this.state.user_id,
                                        'token': this.state.token,
                                    },
                                    body: JSON.stringify({
                                        order_id: this.state.orderid,

                                    })


                                })
                                    .then((response) => response.json())
                                    .then((responseData) => {
                                        console.log(" responseData getBoatInfo resoJSon===" + JSON.stringify(responseData));
                                        console.log("resoJSon getBoatInfo===" + responseData.error);
                                        if (responseData.error == "false") {
                                            console.log("Error false massage getBoatInfo" + JSON.stringify(responseData.boat_record));


                                            this.setState({
                                                animating: false,
                                                lodingDialog: false,
                                                first_name: responseData.boat_record.ship_name,
                                                last_name: responseData.boat_record.brand,
                                                email: responseData.boat_record.size,
                                                telephone: responseData.boat_record.registration,
                                                password: responseData.boat_record.reserve_capacity,
                                                selected: responseData.boat_record.fuel,
                                                selectederror: '',

                                            });


                                            //this.RBSheet.close()
                                        } else if (responseData.error == "true") {

                                            this.showalerts(responseData.errorMessage)
                                            this.setState({
                                                animating: false,
                                                lodingDialog: false

                                            });
                                        }
                                    }
                                    )
                            })
                    })
            })

    }
    validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    validatePassword(password) {
        if (password.length < 6) {
            return false;
        } else if (password.length > 16) {
            return false;
        } else {
            return true;
        }
    }

    userAddBoat() {

        AsyncStorage.getItem("userid")
        .then(userid => {

          //  var userid = JSON.parse(userid);

            this.setState({ user_id: userid });

            AsyncStorage.getItem("token")
                .then(token => {
                    var accessToken = JSON.parse(token);
                    this.setState({ token: accessToken });

                    AsyncStorage.getItem("orderid")
                        .then(orderid => {

                            this.setState({ orderid: orderid });
                           
                            console.log("Photo is  ",this.state.uploads3)
                            
        let headers = {
            'Content-Type': 'application/json',
            'user-id': this.state.user_id,
            'token': this.state.token,
        };
        RNFetchBlob.fetch('POST', Strings.base_Url + 'createOrderStep5', headers, [
            { name: 'order_id', data: this.state.orderid},
            { name: 'full_name', data: this.state.full_name},

            { name: 'telephone_number', data: this.state.phone_number},
            { name: 'body_count', data: this.state.body_count},
            { name: 'comment', data: this.state.comment},
            { name: 'photo', filename: 'photo.jpg', type: 'image/png', data: RNFetchBlob.wrap(this.state.uploads3)},
            

        ],
        ).then((resp) => {

            console.log("response:::::::" + JSON.stringify(resp.text()));

            console.log("error:::" + resp.json().error)
            var error = resp.json().error

            if (resp.json().error === "false") {
                console.log("error:::" + resp.json().error)

                this.setState({
                    animating: false,
                    lodingDialog: false,
                });
                Actions.push("Delvey_confirm_Screen")

            } else if (resp.json().error === "true") {

                this.showalerts(resp.json().errorMessage)
                this.setState({
                    animating: false,
                    lodingDialog: false,
                });

            }



        }).catch((err) => {
            this.setState({
                animating: false,
                lodingDialog: false,
            });
            console.log("response::::err:::" + err);
        });
    })
})
        })

    }


    showalerts(body) {
        Alert.alert(
            Strings.alert_tital,
            body,
            [
                { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
            ],
            { cancelable: false });
    }



    render() {
        return (
            <View style={styles.containerWhite}>


                <ScrollView style={{  }}>

                <Card style={{ width: '100%',marginTop: Platform.OS == 'ios' ? 20 :-8, height: 50, flexDirection: 'row' }}>
                        <TouchableOpacity onPress={() => Actions.pop()} style={{ marginLeft:5, height: 50, justifyContent: 'center', alignItems: 'center', }}>
                            <Image source={require("../assets/back1.png")}
                                style={{ width: 15, height: 15, }}
                                resizeMode="contain" />
                        </TouchableOpacity>
                        <View style={{ width: '80%', height: 50, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: 15, fontFamily:'Poppins-SemiBold'}}>Ajouter un bateau {this.state.zone_name}</Text>

                        </View>

                    </Card>
                    
                

                        <View style={{ margin: 20, width: '90%' }}>
                        <Text style={{fontSize:15,fontFamily:'Poppins-SemiBold',marginBottom:7}}>Ma place u port</Text>

                            <View style={{ width: '100%', height: 45, borderWidth: 1, borderColor:'grey', borderRadius: 10 }}>
                                <Text

                                    style={{ padding: 10 }}>{this.state.first_name}</Text>
                            </View>
                            {!!this.state.first_nameErr && (
                                <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.first_nameErr}</Text>
                            )}

                            <View style={{ width: '100%', marginTop: 20, height: 45, borderWidth: 1, borderColor:'grey', borderRadius: 10 }}>
                                <Text
                                  

                                    style={{ padding: 10 }}>{this.state.last_name}</Text>
                            </View>
                            {!!this.state.last_nameErr && (
                                <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.last_nameErr}</Text>
                            )}

                            <View style={{ width: '100%', marginTop: 20, height: 45, borderWidth: 1, borderColor:'grey', borderRadius: 10 }}>
                                <Text
                              
                                    style={{ padding: 10 }}>{this.state.email}</Text>
                            </View>
                            {!!this.state.emailErr && (
                                <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.emailErr}</Text>
                            )}
                            <View style={{ width: '100%', height: 45, marginTop: 20, borderWidth: 1, borderColor:'grey', borderRadius: 10 }}>
                                <Text
                                    
                            style={{ padding: 10 }}>{this.state.telephone}</Text>
                            </View>
                            {!!this.state.telephoneErr && (
                                <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.telephoneErr}</Text>
                            )}

                            <View style={{ width: '100%', height: 45, marginTop: 20, borderWidth: 1, borderColor:'grey', borderRadius: 10 }}>
                                <Text
                             
                                    style={{ padding: 10 }}>{this.state.password}</Text>
                            </View>
                            {!!this.state.passwordErr && (
                                <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.passwordErr}</Text>
                            )}

                            <View style={{ width: '100%', height: 45, marginTop: 20, borderWidth: 1, borderColor:'grey', borderRadius: 10 }}>
                                <Text
                                  
                                    style={{ padding: 10 }}>{this.state.selected}</Text>
                            </View>
                            {!!this.state.selectederror && (
                                <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.selectederror}</Text>
                            )}
                             {/* <View style={{ width: '100%', alignItems: 'center', marginTop: 30, marginBottom: 10 }}>
                                <TouchableOpacity onPress={() => this.validationRegister()} style={{ width: '50%', height: 40, borderRadius: 25, backgroundColor: '#1A578A', alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ fontSize: 16, color: 'white' }}>Enregister mon </Text>
                                </TouchableOpacity>
                            </View> */}
                           
                            <View style={{ width: '100%', height: 45, marginTop: 20, borderWidth: 1, borderColor:'grey', borderRadius: 10 }}>
                                <TextInput
                                    placeholder='Nom complet'

                                    onChangeText={(full_name) => this.setState({ full_name, full_name_err: '' })}
                                    style={{ padding: 10 }}>

                                </TextInput>
                            </View>
                            {!!this.state.full_name_err && (
                                <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.full_name_err}</Text>
                            )}

                            <View style={{ width: '100%', height: 45, marginTop: 20, borderWidth: 1, borderColor:'grey', borderRadius: 10 }}>
                                <TextInput
                                    placeholder={Strings.telephone_text}
                                    keyboardType="number-pad"

                                    onChangeText={(phone_number) => this.setState({ phone_number, phone_number_err: '' })}
                                    style={{ padding: 10 }}>

                                </TextInput>
                            </View>
                            {!!this.state.phone_number_err && (
                                <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.phone_number_err}</Text>
                            )}



                            <View style={{ width: '100%', height: 45, marginTop: 20, borderWidth: 1, borderColor:'grey', borderRadius: 10 }}>
                                <TextInput
                                    placeholder='Commentaire'

                                    onChangeText={(comment) => this.setState({ comment, comment_err: '' })}
                                    style={{ padding: 10 }}>

                                </TextInput>
                            </View>
                            {!!this.state.comment_err && (
                                <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.comment_err}</Text>
                            )}

                            <View style={{ width: '100%', height: 45, marginTop: 20, borderWidth: 1, borderColor:'grey', borderRadius: 10 }}>
                                <TextInput
                                    placeholder='Comptage des corps'
                                    keyboardType="decimal-pad"

                                    onChangeText={(body_count) => this.setState({ body_count, body_count_err: '' })}
                                    style={{ padding: 10 }}>

                                </TextInput>
                            </View>
                            {!!this.state.body_count_err && (
                                <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.body_count_err}</Text>
                            )}
                            {this.state.uploads3 =='' || this.state.uploads3 == null?
<View style={{Width:'100%',flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
<Text style={{ fontSize: 14,fontFamily:'Poppins-SemiBold' }}>Ajouter une photo</Text>
<TouchableOpacity onPress={() => this.selectProfilePic()}>
<Card style={{width:40,height:30,justifyContent:'center',alignItems:'center'}}>
    <Text style={{color:'grey',fontFamily:'Poppins-Bold',marginTop:5,fontSize:20,alignSelf:'center'}}>+</Text>
</Card>
</TouchableOpacity>
</View>:
                            <TouchableOpacity onPress={() => this.selectProfilePic()}>
                                <Image source={{ uri: this.state.uploads3 }} 
                                style={{ marginTop: 25, height: 100, width: 100, alignSelf: 'center', resizeMode: 'stretch',  borderRadius: 10 }} />
                            </TouchableOpacity>
    }
                           {!!this.state.uploads_Err3 && (
                                <Text style={{ marginLeft: 7, color: 'red', marginTop: 7 }}>
                                    {this.state.uploads_Err3}
                                </Text>
                          )}
                          
                       

                       <View style={{ width: '100%', alignItems: 'center', marginTop: 30, marginBottom: 10 }}>
                                <TouchableOpacity onPress={() => this.validationRegister()}
                                 style={{ width: 220, height: 38, borderRadius: 25, backgroundColor: '#28528F', alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ fontSize: 14, color: 'white',fontFamily:'Poppins-SemiBold' }}>Enregister mon beteau</Text>
                                </TouchableOpacity>
                            </View>


                        </View>
                   
                   
                    <PopupDialog
                        onHardwareBackPress={() => { this.setState({ lodingDialog: false }) }}
                        width={0.3}
                        visible={this.state.lodingDialog}
                        dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
                        <DialogContent>
                            <View style={{ alignItems: 'center', }}>
                                <ActivityIndicator
                                    animating={this.state.animating}
                                    style={[{ height: 10, marginBottom: 10, marginTop: 30, marginLeft: 20, marginRight: 20 }]}
                                    color="#C00"
                                    size="large"
                                    hidesWhenStopped={true}
                                />
                            </View>
                        </DialogContent>
                    </PopupDialog>
                </ScrollView>
            </View>
        );
    }
}
