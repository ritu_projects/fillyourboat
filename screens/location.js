import React from 'react';
import {
    StatusBar, Alert, DrawerLayoutAndroid, ActivityIndicator, ScrollView, FlatList, AsyncStorage,
    TextInput, View, ImageBackground, Text, Dimensions, TouchableOpacity, Linking, Platform,
} from 'react-native';
import Drawer from 'react-native-drawer'
import { Actions } from 'react-native-router-flux';
import styles from "../styles/styles";
import Strings from '../strings/strings'
const { width, height } = Dimensions.get("window");
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';
import RBSheet from "react-native-raw-bottom-sheet";
import SearchCategory from "react-native-raw-bottom-sheet"
import RNGooglePlaces from 'react-native-google-places';
import Sidebar from '../screens/sideBar'
import { FlatGrid } from 'react-native-super-grid';
import Geocoder from 'react-native-geocoder';
import { Card } from "native-base";
import PopupDialog, {
    DialogTitle, DialogContent, DialogFooter, DialogButton, SlideAnimation, ScaleAnimation,
} from 'react-native-popup-dialog';
import { Dropdown } from 'react-native-material-dropdown';
import { WebView } from 'react-native-webview'

let drop_down_categorydata = [];
import { EventRegister } from 'react-native-event-listeners'

const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = 0.0421;
import Image from 'react-native-fast-image'

export default class location extends React.Component {

    constructor(props, context) {
        super(props);
        this.state = {
            userid: '',
            currentlatitude: 37.78825,
            currentlongitude: -122.4324,
            markerArray: [],
            marker_contiditon: false,
            animating: false,
            lodingDialog: false,
            address: '',
            addressErr: '',
            codePostal: '',
            codePostalErr: '',
            ville: '',
            villeErr: '',
            codePorte: '',
            codePorteErr: '',
            etage: '',
            etageErr: '',
            autres: '',
            autresErr: '',
            telephone: '',
            telephoneErr: '',
            drop_down_categorydata_stte: '',
            subzone_name: '',
            user_id: '',
            token: '',
            categoryList: [],
            cateIconPath: '',
            current_cat_id: '',
            headerAddress: '',
            selected: '',
            selectederror: '',
            user_id: '',
            token: '',
            data: '',
            data2: [{
                my_boat_place: 'Port'
            },
            {
                my_boat_place: 'Corps-mort'
            },
            ],
            zone_id: '',
            subzone_id: '',
            zone_name: '',
            selected_zone: 0,
            selection_boat_place: 0,
            subzone_err: '',

        };
    }
    // componentWillMount() {
    //     this.getZoneList()


    // }

    componentDidMount() {
   
        // this.listener = EventRegister.addEventListener('myCustomEvent', (data) => {

        //     console.log("Demo Data ", data)
      
        //     if(data.fcm_push_response == "adminSendChtMessagePush"){
        //         Alert.alert(
        //             data.title, data.body,
        //           [
        //               { text: 'OK', onPress: () => Actions.push("ChatwithAdmin",{currentOrderId:data.fcm_order_id}) },
        //           ],
        //           { cancelable: false },
        //         );  
        //         console.log("fcm_push_response :$$$Done$$:::"+data.fcm_push_response);
        //       }
        
        //     else  if(data.fcm_push_response == "order_chat_user_side"){
        //         Alert.alert(
        //             data.title, data.body,
        //           [
        //               { text: 'OK', onPress: () => Actions.push("Chat",{currentOrderId:data.fcm_order_id}) },
        //           ],
        //           { cancelable: false },
        //         );  
        //         console.log("fcm_push_response :$$$Done$$:::"+data.fcm_push_response);
        //       }
        
        //       else
        //       {
        
        //         if(data.fcm_push_response == "order_completed")
        //         {
        //           Alert.alert(
        //             data.title, data.body,
        //             [
        //                 { text: 'OK', onPress: () => Actions.push("Rating_screen",{currentOrderId : data.fcm_order_id}) },
        //             ],
        //             { cancelable: false },
        //           );  
        //           console.log("fcm_push_response :$$$Done$$:::"+data.fcm_push_response);
        //         }       
        
        
        //         else
        //         {
        //           Alert.alert(
        //             data.title, data.body,
        //             [
        //                 { text: 'OK', onPress: () => Actions.push("Trackorders",{currentOrderId:data.fcm_order_id}) },
        //             ],
        //             { cancelable: false },
        //           );  
        //           console.log("fcm_push_response :$$$Done$$:::"+data.fcm_push_response);
        //         }
        //       }
        
      
      
        //   })

          this.getZoneList()
          this._drawer.close()
    }
    setDrawer = () => {
        console.log("open ", "open ")
        this._drawer.open()
    }

    showalerts(body) {
        Alert.alert(
            Strings.alert_tital,
            body,
            [
                { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
            ],
            { cancelable: false });
    }

    cancelButton() {
        this.setState({
            marker_contiditon: false
        })
    }

    validation_address = () => {
        var isValidate = 0;
        if (this.state.address != "") {
            isValidate += 1;
        } else {
            isValidate -= 1
            this.setState({
                addressErr: Strings.ShouldemptyText,
            });
        }
        if (this.state.codePostal != "") {
            isValidate += 1;
        } else {
            isValidate -= 1
            this.setState({
                codePostalErr: Strings.ShouldemptyText,
            });
        }
        if (this.state.ville != "") {
            isValidate += 1;
        } else {
            isValidate -= 1
            this.setState({
                villeErr: Strings.ShouldemptyText,
            });
        }
        // if (this.state.codePorte != "") {
        //     isValidate += 1;
        // } else {
        //     isValidate -= 1
        //     this.setState({
        //         codePorteErr: Strings.ShouldemptyText,
        //     });
        // }
        // if (this.state.etage != "") {
        //     isValidate += 1;
        // } else {
        //     isValidate -= 1
        //     this.setState({
        //         etageErr: Strings.ShouldemptyText,
        //     });
        // }
        // if (this.state.autres != "") {
        //     isValidate += 1;
        // } else {
        //     isValidate -= 1
        //     this.setState({
        //         autresErr: Strings.ShouldemptyText,
        //     });
        // }
        if (this.state.telephone != "") {
            isValidate += 1;
        } else {
            isValidate -= 1
            this.setState({
                telephoneErr: Strings.ShouldemptyText,
            });
        }
        console.log("Use Login::::" + isValidate)

        if (isValidate == 4) {
            this.addAddressApi();
        }
        else {

        }

    }


    getZoneList = () => {

        AsyncStorage.getItem("userid")
            .then(userid => {
                this.setState({ user_id: userid });

                AsyncStorage.getItem("token")
                    .then(token => {
                        var accessToken = JSON.parse(token);
                        this.setState({ token: accessToken });
                        this.setState({
                            animating: true,
                            lodingDialog: true,

                        });

                        fetch(Strings.base_Url + "zoneList", {
                            method: 'GET',
                            headers: {
                                // 'Accept': 'application/json',
                                'Content-Type': 'application/json',
                                'User-Id': this.state.user_id,
                                'token': this.state.token,
                            },

                        })
                            .then((response) => response.json())
                            .then((responseData) => {
                                console.log(" responseData getZonelist resoJSon===" + JSON.stringify(responseData));
                                console.log("resoJSon getZonelist===" + responseData.error);
                                if (responseData.error == "false") {
                                    console.log("Error false massage getZonelist" + JSON.stringify(responseData.record));

                                    this.setState({
                                        animating: false,
                                        lodingDialog: false,
                                        data: responseData.record,
                                        zone_id: responseData.record[0].id,
                                        zone_name: responseData.record[0].zone_name,

                                    });

                                    this.getSubzoneList(this.state.data[0].id)

                                    //this.RBSheet.close()
                                } else if (responseData.error == "true") {

                                    this.alert(responseData.errorMessage)
                                    this.setState({
                                        animating: false,
                                        lodingDialog: false

                                    });
                                }
                            }
                            )
                    })
            })


    }
    getSubzoneDetials = (zone_id, zone_name) => {

        this.setState({
            zone_id: zone_id,
            zone_name: zone_name

        })

        this.getSubzoneList(zone_id)

    }

    getSubzoneList = (zone_id) => {

        this.setState({
            animating: true,
            lodingDialog: true,

        });
        fetch(Strings.base_Url + "subzoneList", {
            method: 'POST',
            headers: {
                // 'Accept': 'application/json',
                'Content-Type': 'application/json',
                'User-Id': this.state.user_id,
                'token': this.state.token,
            },
            body: JSON.stringify({
                zone_id: zone_id,

            })
        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(" responseData SubzoneLisr resoJSon===" + JSON.stringify(responseData));
                console.log("resoJSon SubzoneLisr===" + responseData.error);
                if (responseData.error == "false") {
                    console.log("Error false SubzoneLisr massage" + JSON.stringify(responseData.errorMessage));
                    drop_down_categorydata = []

                    this.setState({
                        animating: false,
                        lodingDialog: false,
                        selected: '',
                        subzone_id: '',
                        drop_down_categorydata_stte: '',
                        subzone_name: '',

                    });

                    if (this.state.zone_id == 1 || this.state.zone_id == 2 || this.state.zone_id == 3 || this.state.zone_id == 4) {

                        if (this.state.selection_boat_place == 1) {
                            var obj = responseData.record[1].value;
                            console.log("resoJSon GetVehicle obj===" + obj);

                            var count = Object.keys(obj).length;
                            console.log("resoJSon GetVehicle count===" + count);



                            for (var i = 0; i < count; i++) {

                                drop_down_categorydata.push({
                                    value: obj[i].id,

                                    label: obj[i].name
                                });

                                this.setState({
                                    drop_down_categorydata_stte: drop_down_categorydata
                                })
                            }
                        }

                        else if (this.state.selection_boat_place == 0) {
                            var obj = responseData.record[0].value;
                            console.log("resoJSon GetVehicle obj===" + obj);

                            var count = Object.keys(obj).length;
                            console.log("resoJSon GetVehicle count===" + count);



                            for (var i = 0; i < count; i++) {

                                drop_down_categorydata.push({
                                    value: obj[i].id,

                                    label: obj[i].name
                                });

                                this.setState({
                                    drop_down_categorydata_stte: drop_down_categorydata
                                })
                            }
                        }
                    }

                    else {
                        var obj = responseData.record[0].value;
                        console.log("resoJSon GetVehicle obj===" + obj);

                        var count = Object.keys(obj).length;
                        console.log("resoJSon GetVehicle count===" + count);



                        for (var i = 0; i < count; i++) {

                            drop_down_categorydata.push({
                                value: obj[i].id,

                                label: obj[i].name
                            });

                            this.setState({
                                drop_down_categorydata_stte: drop_down_categorydata
                            })
                        }
                    }





                } else if (responseData.error == "true") {
                    drop_down_categorydata = [];

                    this.setState({
                        animating: false,
                        lodingDialog: false,
                        drop_down_categorydata_stte: [],


                    });
                }
            }
            )
    }

    validation_for_subZone = () => {
        var isValidate = 0;


        if (this.state.subzone_name != "" && this.state.subzone_name != "Veuillez sélectionner la sous-zone") {
            isValidate += 1;

        }

        else {
            isValidate -= 1;


            // this.setState({
            //     subzone_err: "Veuillez sélectionner la sous-zone",
            // });

            alert('Veuillez sélectionner la sous-zone')
        }
        if (isValidate == 1) {
            this.createOrder();
            this.setState({
                animating: true,
                lodingDialog: true,

            });
        }


    }
    createOrder = () => {

        this.setState({
            animating: true,
            lodingDialog: true,

        }); fetch(Strings.base_Url + "createOrderStep1", {
            method: 'POST',
            headers: {
                // 'Accept': 'application/json',
                'Content-Type': 'application/json',
                'User-Id': this.state.user_id,
                'token': this.state.token,
            },
            body: JSON.stringify({
                user_id: this.state.user_id,
                zone: this.state.zone_name,
                subzone: this.state.subzone_id,

            })
        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(" responseData addAddress resoJSon===" + JSON.stringify(responseData));
                console.log("resoJSon===" + responseData.error);
                if (responseData.error == "false") {
                    console.log("Error false massage" + JSON.stringify(responseData.errorMessage));

                    this.setState({
                        animating: false,
                        lodingDialog: false,

                    });
                    console.log("Subzone Id ", this.state.subzone_id)
                    AsyncStorage.setItem("zonename", this.state.zone_name);
                    AsyncStorage.setItem("suboneid", this.state.subzone_id + '');
                    AsyncStorage.setItem("orderid", responseData.order_id + '');

                    AsyncStorage.setItem("selection_boat_place", this.state.selection_boat_place + '');
                    AsyncStorage.setItem("subzonename", this.state.subzone_name + '');


                    // Actions.push("Calender_Screen")


                    Actions.push("Calender_Screen")

                } else if (responseData.error == "true") {

                    this.alert(responseData.errorMessage)
                    this.setState({
                        animating: false,
                        lodingDialog: false

                    });
                }
            }
            )
    }

    set3rdValue = (index) => {


        console.log("Index  ", index)
        console.log("ZoneId  ", this.state.zone_id)


        this.setState({
            selection_boat_place: index,
        })
        this.getSubzoneList(this.state.zone_id)

    }



    addAddressApi() {
        this.setState({ animating: true })
        fetch(Strings.base_Url + "createOrderStep1", {
            method: 'POST',
            headers: {
                // 'Accept': 'application/json',
                'Content-Type': 'application/json',
                'User-Id': this.state.user_id,
                'token': this.state.token,
            },
            body: JSON.stringify({
                user_id: this.state.user_id,
                rue: this.state.address,
                postal_code: this.state.codePostal,

            })
        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(" responseData addAddress resoJSon===" + JSON.stringify(responseData));
                console.log("resoJSon===" + responseData.error);
                if (responseData.error == "false") {
                    console.log("Error false massage" + JSON.stringify(responseData.errorMessage));

                    this.setState({
                        animating: false,
                        lodingDialog: false,

                    });



                } else if (responseData.error == "true") {

                    this.alert(responseData.errorMessage)
                    this.setState({
                        animating: false,
                        lodingDialog: false

                    });
                }
            }
            )
    }

    AddressCheck() {

        var isValidate = 0;
        // if (this.state.telephone != "") {
        //     isValidate += 1;
        // } else {
        //     isValidate -= 1
        //     this.showAlert(Strings.Complement_De_Adresse_text);
        // }

        // if(isValidate == 1)
        this.SearchCategory.open()
    }

    showAlert(body) {
        Alert.alert(
            Strings.alert_tital,
            body,
            [
                { text: 'Ok', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
            ],
            { cancelable: false });
    } x


    onChangeTextPress(value, index) {
        console.log("Cat Value is " + this.state.drop_down_categorydata_stte[index].label)


        this.setState({
            selected: value,
            selectederror: '',
            subzone_id: value,
            subzone_name: this.state.drop_down_categorydata_stte[index].label,
            subzone_err: '',

        })


    }


    openSearchModal() {
        // RNGooglePlaces.getCurrentPlace(['placeID', 'location', 'name', 'address'])
        // .then((results) => console.log("results:::::"+results))
        // .catch((error) => console.log("message:::::"+error.message));
        RNGooglePlaces.openAutocompleteModal()
            .then((place) => {
                console.log("place :::::" + JSON.stringify(place))
                console.log("place :::::" + JSON.stringify(place.addressComponents[5].shortName))
                this.setState({
                    addressErr: '',
                    address: place.address,
                    ville: place.addressComponents[1].shortName,
                    codePostal: addressComponents[5].shortName

                })

                console.log("address::::" + place.address);
                // place represents user's selection from the
                // suggestions and it is a simplified Google Place object.
            })
            .catch(error => console.log(error.message));  // error is a Javascript Error object
    }


    clickDone() {
        this.setState({ animating: true })
    }
    markerClick(id) {

        Actions.push("CreateReservations", { currentTechId: id })
    }
    selection_for_zone(index, item_id, item_name) {
        console.log("Console Index Click  ", index)
        console.log("Console Id Click  ", item_id)
        console.log("Console Name Click  ", item_name)

        this.setState({



            selected_zone: index,
            selection_boat_place: 0,

        })

        this.getSubzoneDetials(item_id, item_name)
    }
    render() {
        var navigationView = (
            <Sidebar />
        );
        return (
            <Drawer
                openDrawerOffset={100}
                type="overlay"
                ref={(ref) => this._drawer = ref}
                content={<Sidebar />}>

                <View style={styles.containerWhite}>
                    <View style={{ width: '100%', alignItems: 'center', height: 50, flexDirection: 'row', justifyContent: 'space-between',
                marginTop: Platform.OS == 'ios' ? 20 :0 }}>
                        <TouchableOpacity
                            //onPress={this._setDrawer.bind(this)} 
                            onPress={() => this.setDrawer()}

                            style={{
                                height: 50,
                                justifyContent: 'center',
                            }}>
                            <Image source={require("../assets/menu_img.png")}
                                style={{ width: 40, height: 40, }}
                                resizeMode="contain" />
                        </TouchableOpacity>
                        <View style={{ height: 50, justifyContent: 'center', alignItems: 'center', }}>
                            <Text style={{ fontFamily: "Poppins-SemiBold", fontSize: 16, }}>{Strings.alert_tital}</Text>
                        </View>
                        <View style={{ width: 32, height: 20, marginRight: 8 }}
                        />

                    </View>

                    <View
                        style={{ flex: 1, }}>

                        <View
                            style={{
                                width: width,
                                height: height * 40 / 100,
                            }}>

                            <WebView
                                source={{ html: '<iframe src="https://www.google.com/maps/d/embed?mid=1ZObyaRrst9_KthXLOmCuqy8OpTrk466U" width="950" height="650"></iframe>' }}
                                style={{ marginTop: 0 }}
                                //    injectedJavaScript={false}
                                originWhitelist={['*']}

                            />
                        </View>


                    </View>


                    {this.state.selected_zone != 0 && this.state.selected_zone != 1 && this.state.selected_zone != 2 && this.state.selected_zone != 3 ?
                        <Image
                            resizeMode='stretch'
                            style={{
                                width: width * 105 / 100,
                                marginTop: height * 1 / 100,
                                marginBottom: -height * 5 / 100, height: height * 56 / 100, alignSelf: 'center', padding: 13,
                            }}
                            source={require("../assets/bg_image.png")}>

                            <View>


                                <View style={{ alignItems: 'center', }}>

                                    <View style={{ flexDirection: 'row', width: width * 27 / 100, height: width * 26 / 100, alignItems: 'center' }}>
                                        <Image source={require("../assets/12.png")}
                                            style={{ width: width * 27 / 100, height: width * 26 / 100, }}
                                        />
                                    </View>

                                </View>

                                <View>

                                    <View style={{ justifyContent: 'center', }}>
                                        <Text style={{
                                            alignSelf: 'center',
                                            fontSize: width * 3.3 / 100, width: width * 87 / 100, fontFamily: "Poppins-SemiBold", color: '#303030'
                                            , marginTop: height * 1.1 / 100,
                                        }}>Sélectionnez la zone où votre bateau est amarré</Text>
                                    </View>
                                    <View style={{ width: width * 87 / 100, alignSelf: 'center', justifyContent: 'center', alignItems: 'center' }}>

                                        {this.state.data.length > 0 ?

                                            <FlatList
                                                keyExtractor={item => item.id}
                                                data={this.state.data}
                                                horizontal
                                                showsHorizontalScrollIndicator={false}
                                                renderItem={({ item, index }) => (



                                                    <TouchableOpacity
                                                        style={{ alignSelf: 'center', justifyContent: 'center', alignItems: 'center' }}
                                                        onPress={() => {
                                                            console.log("Index  ", index)

                                                            this.selection_for_zone(index, item.id, item.zone_name)


                                                        }}

                                                        style={{ justifyContent: 'center', alignItems: 'center', marginTop: height * 0.7 / 100 }}>
                                                        <View style={{ width: width * 22.5 / 100, flexDirection: 'row' }}>
                                                            {this.state.selected_zone == index ?
                                                                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', alignSelf: 'center' }}>
                                                                    <Image
                                                                        style={{ width: 10, height: 10, marginRight: 3 }}
                                                                        source={require("../assets/fill_home_radio.png")}
                                                                    >


                                                                    </Image>
                                                                    <Text style={{
                                                                        fontFamily: "Poppins-Medium", fontSize: width * 3 / 100,
                                                                        color: '#1A588B'
                                                                    }}>{item.zone_name}</Text>
                                                                </View>

                                                                :
                                                                <View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: 'center', alignItems: 'center', }}>

                                                                    <Image
                                                                        style={{ width: 10, height: 10, marginRight: 3 }}
                                                                        source={require("../assets/unfill_home_radio.png")}
                                                                    >


                                                                    </Image>

                                                                    <Text style={{
                                                                        fontFamily: "Poppins-Medium",
                                                                        color: '#ACA6A6', fontSize: width * 3 / 100,
                                                                    }}>{item.zone_name}</Text>
                                                                </View>

                                                            }




                                                        </View>

                                                    </TouchableOpacity>
                                                )}
                                            />

                                            :
                                            <Text style={{ fontFamily: "Poppins-Medium", marginTop: height * 0.8 / 100, color: 'white' }}>Aucune donnée</Text>

                                        }
                                    </View>







                                    {this.state.selected_zone == 0 || this.state.selected_zone == 1 || this.state.selected_zone == 2 || this.state.selected_zone == 3 ?
                                        <View>
                                            <View style={{ justifyContent: 'center' }}>
                                                <Text style={{ fontSize: 11, fontFamily: "Poppins-Medium", marginTop: 20, marginBottom: 10, }}>Mon bateau est au : </Text>
                                            </View>
                                            <FlatList
                                                keyExtractor={item => item.id}
                                                data={this.state.data2}
                                                horizontal

                                                renderItem={({ item, index }) => (
                                                    <TouchableOpacity
                                                        onPress={() => {

                                                            this.set3rdValue(index)
                                                        }}>


                                                        {this.state.selection_boat_place == index ?
                                                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                                                                <Image
                                                                    style={{ width: 10, height: 10, padding: 10, }}
                                                                    source={require("../assets/fill_home_radio.png")}
                                                                >


                                                                </Image>
                                                                <Text style={{
                                                                    fontFamily: "Poppins-Medium", padding: 10, fontSize: 11,
                                                                    color: '#1A588B'
                                                                }}>{item.my_boat_place}</Text>
                                                            </View>

                                                            :
                                                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>

                                                                <Image
                                                                    style={{ width: 10, height: 10, padding: 10, }}
                                                                    source={require("../assets/unfill_home_radio.png")}
                                                                >


                                                                </Image>

                                                                <Text style={{
                                                                    fontFamily: "Poppins-Medium", padding: 10,
                                                                    color: '#ACA6A6', fontSize: 11,
                                                                }}>{item.my_boat_place}</Text>
                                                            </View>

                                                        }
                                                    </TouchableOpacity>

                                                )}
                                            />
                                        </View>


                                        :

                                        null

                                    }
                                    <View style={{ justifyContent: 'center', }}>
                                        <Text style={{
                                            fontSize: width * 2.9 / 100, width: width * 87 / 100, alignSelf: 'center', fontFamily: "Poppins-Medium", color: '#303030'
                                            , marginTop: height * 0.8 / 100,
                                        }}>Choisissez dans la liste, le port ou la zone de corps-mort qui vous correspond :</Text>
                                        {/* <Text style={{
                                            fontSize: width * 2.9 / 100, width: width * 87 / 100, alignSelf: 'center', fontFamily: "Poppins-Medium", color: '#303030'
                                            ,
                                        }}>
                                            qui vous correspond :</Text> */}
                                    </View>

                                    {this.state.drop_down_categorydata_stte.length > 0 ?


                                        <Dropdown
                                            style={{ flex: 1, }}
                                            label='Sélectionnez une sous-zone'
                                            data={this.state.drop_down_categorydata_stte}
                                            rippleCentered={true}
                                            // pickerStyle={{borderBottomColor:'transparent',borderWidth: 0}}
                                            containerStyle={{
                                                width: width * 87 / 100, marginTop: -height * 1.6 / 100,
                                                fontFamily: 'Poppins-SemiBold', alignSelf: 'center'
                                            }}
                                            pickerStyle={{ paddingBottom: 10, }}
                                            fontSize={12}
                                            // valueExtractor={({ value }) => value}
                                            onChangeText={(value, index) => { this.onChangeTextPress(value, index) }}
                                        />

                                        :
                                        null
                                    }

                                    <TouchableOpacity
                                        onPress={() => this.validation_for_subZone()}
                                        style={{ width: 195, marginTop: height * 0.5 / 100, alignSelf: 'center', borderRadius: 20, backgroundColor: '#28528F', height: 34, justifyContent: 'center', alignItems: 'center' }}>
                                        <Text style={{
                                            fontSize: 11.5, fontFamily: "Poppins-SemiBold",
                                            color: 'white',
                                        }}>Commender mon plain</Text>

                                    </TouchableOpacity>



                                </View>
                            </View>

                        </Image>

                        :


                        <Image
                            resizeMode='stretch'
                            style={{
                                width: width * 105 / 100,
                                height: height * 60 / 100,
                                marginTop: height * 7 / 100, alignSelf: 'center', padding: 13,
                            }}
                            source={require("../assets/bg_image.png")}>

                            <View>


                                <View style={{ alignItems: 'center', }}>

                                    <View style={{ flexDirection: 'row', width: width * 26 / 100, height: width * 26 / 100, alignItems: 'center' }}>
                                        <Image source={require("../assets/12.png")}
                                            resizeMode={'stretch'}
                                            style={{ width: width * 26 / 100, height: width * 26 / 100, resizeMode: 'stretch' }}
                                        />
                                    </View>

                                </View>

                                <View>

                                    <View style={{ justifyContent: 'center', }}>
                                        <Text style={{
                                            alignSelf: 'center',
                                            fontSize: width * 3.3 / 100, width: width * 87 / 100, fontFamily: "Poppins-SemiBold", color: '#303030'
                                            , marginTop: height * 0.4 / 100,
                                        }}>Sélectionnez la zone où votre bateau est amarré</Text>
                                    </View>
                                    <View style={{ width: width * 87 / 100, alignSelf: 'center', justifyContent: 'center', alignItems: 'center' }}>

                                        {this.state.data.length > 0 ?

                                            <FlatList
                                                keyExtractor={item => item.id}
                                                data={this.state.data}
                                                horizontal
                                                showsHorizontalScrollIndicator={false}
                                                renderItem={({ item, index }) => (



                                                    <TouchableOpacity
                                                        style={{ alignSelf: 'center', justifyContent: 'center', alignItems: 'center' }}
                                                        onPress={() => {
                                                            console.log("Index  ", index)
                                                            this.selection_for_zone(index, item.id, item.zone_name)
                                                        }}

                                                        style={{ justifyContent: 'center', alignItems: 'center', marginTop: height * 0.4 / 100 }}>
                                                        <View style={{ width: width * 22.5 / 100, flexDirection: 'row' }}>
                                                            {this.state.selected_zone == index ?
                                                                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', alignSelf: 'center' }}>
                                                                    <Image
                                                                        style={{ width: 10, height: 10, marginRight: 3 }}
                                                                        source={require("../assets/fill_home_radio.png")}
                                                                    >


                                                                    </Image>
                                                                    <Text style={{
                                                                        fontFamily: "Poppins-Medium", fontSize: width * 3 / 100,
                                                                        color: '#1A588B'
                                                                    }}>{item.zone_name}</Text>
                                                                </View>

                                                                :
                                                                <View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: 'center', alignItems: 'center', }}>

                                                                    <Image
                                                                        style={{ width: 10, height: 10, marginRight: 3 }}
                                                                        source={require("../assets/unfill_home_radio.png")}
                                                                    >


                                                                    </Image>

                                                                    <Text style={{
                                                                        fontFamily: "Poppins-Medium",
                                                                        color: '#ACA6A6', fontSize: width * 3 / 100,
                                                                    }}>{item.zone_name}</Text>
                                                                </View>

                                                            }




                                                        </View>

                                                    </TouchableOpacity>
                                                )}
                                            />

                                            :
                                            <Text style={{ fontFamily: "Poppins-Medium", marginTop: 10, color: 'white' }}>Aucune donnée</Text>

                                        }
                                    </View>


                                    {this.state.selected_zone == 0 || this.state.selected_zone == 1 || this.state.selected_zone == 2 || this.state.selected_zone == 3 ?
                                        <View>
                                            <View style={{ justifyContent: 'center' }}>
                                                <Text style={{
                                                    fontSize: width * 2.9 / 100,
                                                    width: width * 87 / 100, alignSelf: 'center', fontFamily: "Poppins-Medium", marginTop: height * 0.7 / 100,
                                                }}>Mon bateau est au : </Text>
                                            </View>
                                            <View style={{ width: width * 87 / 100, alignSelf: 'center' }}>
                                                <FlatList
                                                    keyExtractor={item => item.id}
                                                    data={this.state.data2}
                                                    horizontal

                                                    renderItem={({ item, index }) => (
                                                        <TouchableOpacity
                                                            style={{ width: width * 40 / 100 }}
                                                            onPress={() => {
                                                                console.log("Index inside list  ", index)


                                                                this.set3rdValue(index)

                                                            }}>


                                                            {this.state.selection_boat_place == index ?
                                                                <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                                                                    <Image
                                                                        style={{ width: 10, height: 10, marginRight: 5 }}
                                                                        source={require("../assets/fill_home_radio.png")}
                                                                    >


                                                                    </Image>
                                                                    <Text style={{
                                                                        fontFamily: "Poppins-Medium", fontSize: width * 2.9 / 100,
                                                                        color: '#1A588B'
                                                                    }}>{item.my_boat_place}</Text>
                                                                </View>

                                                                :
                                                                <View style={{ flexDirection: 'row', alignItems: 'center', }}>

                                                                    <Image
                                                                        style={{ width: 10, height: 10, marginRight: 5 }}
                                                                        source={require("../assets/unfill_home_radio.png")}
                                                                    >


                                                                    </Image>

                                                                    <Text style={{
                                                                        fontFamily: "Poppins-Medium",
                                                                        color: '#ACA6A6', fontSize: width * 2.9 / 100,
                                                                    }}>{item.my_boat_place}</Text>
                                                                </View>

                                                            }
                                                        </TouchableOpacity>

                                                    )}
                                                />
                                            </View>
                                        </View>


                                        :

                                        null

                                    }
                                    <View style={{ justifyContent: 'center', }}>
                                        <Text style={{
                                            fontSize: width * 2.9 / 100, width: width * 87 / 100, alignSelf: 'center',
                                            fontFamily: "Poppins-Medium", color: '#303030'
                                            , marginTop: height * 0.1 / 100,
                                        }}>Choisissez dans la liste, le port ou la zone de corps-mort qui vous correspond :</Text>
                                        {/* <Text style={{
                                            fontSize: width * 2.9 / 100, width: width * 87 / 100, alignSelf: 'center',
                                            fontFamily: "Poppins-Medium", color: '#303030'
                                            ,
                                        }}>
                                            qui vous correspond :</Text> */}
                                    </View>


                                    {this.state.drop_down_categorydata_stte.length > 0 ?


                                        <Dropdown
                                            style={{ flex: 1, }}
                                            label='Sélectionnez une sous-zone'
                                            data={this.state.drop_down_categorydata_stte}
                                            rippleCentered={true}
                                            // pickerStyle={{borderBottomColor:'transparent',borderWidth: 0}}
                                            containerStyle={{ width: width * 87 / 100, marginTop: -height * 1.6 / 100, fontFamily: 'Poppins-SemiBold', alignSelf: 'center' }}
                                            pickerStyle={{ paddingBottom: 20, marginBottom: 5, }}

                                            fontSize={12}
                                            // valueExtractor={({ value }) => value}
                                            onChangeText={(value, index) => { this.onChangeTextPress(value, index) }}
                                        />

                                        :
                                        null
                                    }






                                    <TouchableOpacity
                                        onPress={() => this.validation_for_subZone()}
                                        style={{
                                            width: '80%', alignSelf: 'center',
                                            marginTop: height * 0.9 / 100, borderRadius: 20, 
                                            backgroundColor: '#28528F', height: 32, 
                                            justifyContent: 'center', alignItems: 'center',
                                        }}>
                                        <Text style={{
                                            fontSize: 10.5, fontFamily: "Poppins-SemiBold",
                                            color: 'white', paddingLeft: 15, paddingRight: 15,
                                        }}>Commander mon plein !</Text>

                                    </TouchableOpacity>



                                </View>
                            </View>

                        </Image>




                    }


                    <PopupDialog
                        onHardwareBackPress={() => { this.setState({ lodingDialog: false }) }}
                        width={0.3}
                        visible={this.state.lodingDialog}
                        dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
                        <DialogContent>
                            <View style={{ alignItems: 'center', }}>
                                <ActivityIndicator
                                    animating={this.state.animating}
                                    style={[{ height: 10, marginBottom: 10, marginTop: 30, marginLeft: 20, marginRight: 20 }]}
                                    color="#C00"
                                    size="large"
                                    hidesWhenStopped={true}
                                />
                            </View>
                        </DialogContent>
                    </PopupDialog>

                </View>
            </Drawer>
        );
    }
}
