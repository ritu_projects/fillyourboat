import React from 'react';
import { StatusBar, Button,DrawerLayoutAndroid, ActivityIndicator, FlatList, ScrollView, PixelRatio, TextInput, View, ImageBackground, Text, Dimensions, TouchableOpacity, Linking, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from "../styles/styles";
import Strings from '../strings/strings'
import { Card } from 'native-base';
import { Rating, AirbnbRating } from 'react-native-elements';
const { width, height } = Dimensions.get("window");
import PopupDialog, {
    DialogTitle, DialogContent, DialogFooter, DialogButton, SlideAnimation, ScaleAnimation,
} from 'react-native-popup-dialog';
import Sidebar from '../screens/sideBar'
import Moment from 'moment';
import Images from 'react-native-image-progress';
import Progress from 'react-native-progress';
let sources;
import Image from 'react-native-fast-image'

let   notifications1= [{text:'First notification',time:'12-07-2020 3:40PM'},
{text:'Second notification',time:'13-07-2020 1:20PM'}
   ]
export default class Notification1 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {lodingDialog: false,token:'',notificationCount:'',
        notifications1: [{text:'First notification',time:'12-07-2020 3:40PM'},
        {text:'Second notification',time:'13-07-2020 1:20PM'}
           ]}}
        render() {
            return (
                <View style={{
                    width: "100%",
                    flexDirection: 'column',
                    height: "100%",
                    marginBottom: 20,
                }}>
                    <View style={{
                        width: "100%",
                        height: 60, flexDirection: 'row', justifyContent: "center", alignItems: "center",
                    }}>
                        <TouchableOpacity
                            style={{ width: "10%", flexDirection: 'row', marginRight: 0, }}
                            onPress={()=>Actions.pop()}>
                            <Image
                                source={require('../assets/back.png')}
                                style={{ width: 15, height: 15, marginLeft: 8, }} />
                        </TouchableOpacity>
                        <Text
                            style={{
                                width: "81%",
                                textAlign: "center",
                                fontFamily: "Poppins-Bold",
                            }}>Notification</Text>
                        <TouchableOpacity
                            style={{ width: "9%", marginRight: 5, }}>
                        </TouchableOpacity>
                    </View>
                    {this.state.notifications1 != null &&this.state.notifications1 !=''?
                    <View
                        style={{
                            marginTop: 15,
                            marginLeft: 10,
                            marginRight: 10,
                            marginBottom:20
                        }}>
                        {/* <Text
                            style={{
                                fontFamily: "Poppins-Medium",
                                fontSize: 15,
                            }}>{Appsconstant.remainder}</Text> */}
                        <FlatList
                            ref={flatList1 => {
                                this.flatList1 = flatList1
                            }}
                            style={{ width: "100%", marginBottom: 20, }}
                            data={notifications1}
                            keyExtractor={item => item.id}
                            renderItem={({ item,index }) => (
                                <Card
                                    style={{
                                        borderRadius: 15,
                                        paddingLeft: 15,
                                        paddingTop: 15,
                                        paddingBottom: 15,
                                        marginBottom: 20,
                                        flexDirection: "row",
                                    }}>
                                    <TouchableOpacity
                                        >
                                       
                                        <Text numberOfLines={3}
                                        style={{
                                            
                                            width: width*85/100,
                                            paddingLeft: 10,
                                            fontSize:13,
                                            fontFamily: "Poppins-Regular",
                                        }}>{index+1}.   {item.text}</Text>
                                        <Text numberOfLines={3}  style={{
                                            
                                            width: width*85/100,marginTop:5,
                                            //paddingLeft: 25,
                                            fontSize:10,
                                            textAlign:'right',
                                            fontFamily: "Poppins-Regular",
                                        }}>{item.time}
                                            </Text>
                                        {/* <View style={{ width: 6, height: 40, }}>
                                            <Image
                                                style={{ width: 6, height: 40, }}
                                                source={item.clr}></Image>
                                        </View> */}
                                    </TouchableOpacity>
                                </Card>
                            )}
                        />
                    </View>
                    :
                    <View style={{marginTop:height*35/100}}><Text style={{color:'grey', fontSize:16, fontFamily: "Poppins-Bold",textAlign:'center'}}>Aucune notification trouee</Text></View>}
                    <PopupDialog
                        onHardwareBackPress={() => { this.setState({ lodingDialog: false }) }}
                        width={0.3}
                        visible={this.state.lodingDialog}
                        dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
                        <DialogContent>
                            <View style={{ alignItems: 'center', }}>
                                <ActivityIndicator
                                    animating={this.state.animating}
                                    style={[{ height: 10, marginBottom: 10, marginTop: 30, marginLeft: 20, marginRight: 20 }]}
                                    color="#C00"
                                    size="large"
                                    hidesWhenStopped={true} />
                            </View>
                        </DialogContent>
                    </PopupDialog>
                </View>
            )
        }
    }