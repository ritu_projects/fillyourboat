import React from 'react';
import { StatusBar, Button,ActivityIndicator, FlatList, ScrollView, PixelRatio, TextInput, View, ImageBackground, Text, Dimensions, TouchableOpacity, Linking, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from "../styles/styles";
import Strings from '../strings/strings'
import { Card } from 'native-base';
import { Rating, AirbnbRating } from 'react-native-elements';
import PopupDialog, {
    DialogTitle, DialogContent, DialogFooter, DialogButton, SlideAnimation, ScaleAnimation,
} from 'react-native-popup-dialog';

import Image from 'react-native-fast-image'

let Sources;
export default class interventionDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            animating: false,
            lodingDialog: false,
            username:'',
            email:'',
            job_complete:'',
            hour_cost:'',
            status:'',
            user_telephone:'',
            user_date:'',
            user_address:'',
            category:'',
            details:'',
            order_date:'',
            tech_name:'',
            tech_phone:'',
            rating:'',
            profile_pic:'',
            commentaire:'',
            job_desc:'',

        };
console.log("req_id:::::"+this.props.req_id)
    }

    componentWillMount() {
        this.setState({
            animating: true,
            lodingDialog: true,
        });


        AsyncStorage.getItem("userid")
            .then(userid => {
                this.setState({ user_id: userid });
                AsyncStorage.getItem("token")
                    .then(token => {
                        var accessToken = JSON.parse(token);
                        this.setState({ token: accessToken });
                        console.log("accessToken====$$$========" + accessToken);
                        console.log("state userId====$$$========" + userid);
                       // this.refs.loading2.show();
                        fetch(Strings.base_Url + "user_complete_request_detail", {
                            method: 'POST',
                            headers: {
                                // 'Accept': 'application/json',
                                'Content-Type': 'application/json',
                                'User-Id': userid,
                                'token': accessToken,
                            },
                            body: JSON.stringify({
                                req_id: this.props.req_id,
                            })
                        })
                            .then((response) => response.json())
                            .then((responseData) => {
                                console.log(" responseData resoJSon===" + JSON.stringify(responseData));
                                console.log("resoJSon===" + responseData.error);
                                if (responseData.error == "false") {
                                    console.log("Error false massage" + JSON.stringify(responseData.errorMessage));

                                   if(responseData.reqDetail.profile_pic != "NULL"){
                                    Sources= {uri :responseData.reqDetail.profile_pic_url}
                                    console.log("url:::")
                                }else{
                                    Sources= require("../assets/images.jpeg")
                                   }
                                   
                                    this.setState({
                                        username:responseData.reqDetail.username,
                                        email:responseData.reqDetail.email,
                                        job_complete:responseData.reqDetail.tech_complete_job,
                                        hour_cost:responseData.reqDetail.price_per_hour,
                                        status:responseData.reqDetail.request_status,
                                        user_telephone:responseData.reqDetail.alternate_no,
                                        user_date:responseData.reqDetail.end_job_time +" "+responseData.reqDetail.end_job_date,
                                        user_address:responseData.reqDetail.address,
                                        category:responseData.reqDetail.category,
                                        details:responseData.reqDetail.detail,
                                        rating:responseData.reqDetail.rating,
                                        job_desc:responseData.reqDetail.work_desc,
                                        profile_pic:Sources,
                                        commentaire:responseData.reqDetail.comment,
                                        order_date:responseData.reqDetail.job_create_time+" "+responseData.reqDetail.job_create_date,
                                        animating: false,
                                        lodingDialog: false,
                                    });

                                    //this.getCancelApi();
                                } else if (responseData.error == "true") {
                                    this.setState({
                                        NoRecord: responseData.errorMessage,
                                        animating: false,
                                        lodingDialog: false

                                    });
                                    //this.refs.loading2.close();
                                }
                            })

                    });
            });

    }

    render() {
        return (
            <View style={styles.containerWhite}>
                <View style={{ width: '100%', height: 50, flexDirection: 'row', backgroundColor: 'white' }}>
                    <TouchableOpacity onPress={() => Actions.pop()} 
                    style={{ width: '20%', height: 50, justifyContent: 'center', alignItems: 'center', borderTopRightRadius: 20, borderBottomRightRadius: 20 }}>
                        <Image source={require("../assets/left-arrow.png")}
                            style={{ width: 30, height: 20, }}
                            resizeMode="contain" />
                    </TouchableOpacity>
                    <View style={{ width: '70%', height: 50, justifyContent: 'center', alignItems: 'center', }}>
                        <Text style={{ fontSize: 16, fontWeight: 'bold' }}>{Strings.l_intervention_text}</Text>
                    </View>
                </View>
                <ScrollView>
                    <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }}>
                        <Image source={require("../assets/image.png")}
                            style={{ width: '100%', height: 150, backgroundColor: 'red' }}
                        />

                        <View style={{ width: 100, height: 100, borderRadius: 100 / 2, backgroundColor: 'white', marginTop: -50, alignItems: 'center', justifyContent: 'center' }}>
                            <Image source={this.state.profile_pic}
                                style={{ width: 95, height: 95, borderRadius: 95 / 2 }} />
                        </View>

                        <Text style={{ fontSize: 18, fontWeight: 'bold', padding: 5 }}>{this.state.username}</Text>
                        <Text style={{ fontSize: 16, }}>{this.state.email}</Text>
                        <View style={{ flexDirection: 'row', marginTop: 10, alignItems: 'center' }}>
                            <View style={{ width: 25, height: 25, borderWidth: 1, borderRadius: 4, borderColor: Strings.color_green_code, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ fontWeight: 'bold', color: Strings.color_green_code }}>{this.state.job_complete}</Text>
                            </View>
                            <Text style={{ fontSize: 14, }}>{Strings.travaux_text}</Text>
                        </View>
                        <Text style={{ fontSize: 14, padding: 5 }}>{Strings.cout_text}  {this.state.hour_cost} € </Text>
                        <AirbnbRating
                            showRating={false}
                            count={5}
                            color={Strings.color_green_code}
                            defaultRating={this.state.rating}
                            size={15} />
                        <View style={{width:'90%',marginTop:40}}>
                            <Text style={{fontSize:16,fontWeight:'bold'}}>{Strings.information_text}</Text>
                            <Card style={{width:'100%',backgroundColor:'white',borderRadius:10,flexDirection:'column',marginTop:15}}>
                                <View style={{width:'100%',flexDirection:'row',margin:10}}>
                                    <View style={{width:'50%'}}>
                                        <Text style={{color:Strings.color_green_code}}>{Strings.status_text}</Text>
                                    </View>
                                    <View style={{width:'50%'}}>
                                    <Text>{this.state.status}</Text>
                                    </View>
                                </View>
                                <View style={{width:'100%',flexDirection:'row',margin:10}}>
                                    <View style={{width:'50%'}}>
                                        <Text style={{color:Strings.color_green_code}}>{Strings.numero_identification_text}</Text>
                                    </View>
                                    <View style={{width:'50%'}}>
                                    <Text>{this.state.user_telephone}</Text>
                                    </View>
                                </View>

                                <View style={{width:'100%',flexDirection:'row',margin:10}}>
                                    <View style={{width:'50%'}}>
                                        <Text style={{color:Strings.color_green_code}}>{Strings.temp_de_text}</Text>
                                    </View>
                                    <View style={{width:'50%'}}>
                                    <Text>{this.state.user_date}</Text>
                                    </View>
                                </View>
                                <View style={{width:'100%',flexDirection:'row',margin:10}}>
                                    <View style={{width:'50%'}}>
                                        <Text style={{color:Strings.color_green_code}}>{Strings.Addresse_text}</Text>
                                    </View>
                                    <View style={{width:'47%'}}>
                                    <Text>{this.state.user_address}</Text>
                                    </View>
                                </View>

                                <View style={{width:'100%',flexDirection:'row',margin:10}}>
                                    <View style={{width:'50%'}}>
                                        <Text style={{color:Strings.color_green_code}}>{Strings.category_text}</Text>
                                    </View>
                                    <View style={{width:'50%'}}>
                                    <Text>{this.state.category}</Text>
                                    </View>
                                </View>

                                <View style={{width:'100%',flexDirection:'row',margin:10}}>
                                    <View style={{width:'50%'}}>
                                        <Text style={{color:Strings.color_green_code}}>{Strings.mots_text}</Text>
                                    </View>
                                    <View style={{width:'50%'}}>
                                    <Text>{this.state.details}</Text>
                                    </View>
                                </View>

                                <View style={{width:'100%',flexDirection:'row',margin:10}}>
                                    <View style={{width:'50%'}}>
                                        <Text style={{color:Strings.color_green_code}}>{Strings.job_desc}</Text>
                                    </View>
                                    <View style={{width:'50%'}}>
                                    <Text>{this.state.job_desc}</Text>
                                    </View>
                                </View>

                                <View style={{width:'100%',flexDirection:'row',margin:10}}>
                                    <View style={{width:'50%'}}>
                                        <Text style={{color:Strings.color_green_code}}>{Strings.commande_text}</Text>
                                    </View>
                                    <View style={{width:'50%'}}>
                                    <Text>{this.state.order_date}</Text>
                                    </View>
                                </View>

                                  <View style={{width:'100%',flexDirection:'row',margin:10}}>
                                    <View style={{width:'50%'}}>
                                        <Text style={{color:Strings.color_green_code}}>{"Commentaire"}</Text>
                                    </View>
                                    <View style={{width:'50%'}}>
                                    <Text>{this.state.commentaire}</Text>
                                    </View>
                                </View>

                                
                            </Card>
                        </View>

                        <View style={{width:'90%',marginTop:50,marginBottom:100}}>
                            <Text style={{fontSize:16,fontWeight:'bold'}}>{Strings.information_expert_text}</Text>
                            <Card style={{width:'100%',backgroundColor:'white',borderRadius:10,flexDirection:'column',marginTop:10}}>
                                <View style={{width:'100%',flexDirection:'row',margin:10}}>
                                    <View style={{width:'50%'}}>
                                        <Text style={{color:Strings.color_green_code}}>{"Nom"}</Text>
                                    </View>
                                    <View style={{width:'50%'}}>
                                    <Text>{this.state.username}</Text>
                                    </View>
                                </View>
                                <View style={{width:'100%',flexDirection:'row',margin:10}}>
                                    <View style={{width:'50%'}}>
                                        <Text style={{color:Strings.color_green_code}}>{Strings.numero_identification_text}</Text>
                                    </View>
                                    <View style={{width:'50%'}}>
                                    <Text>{this.state.user_telephone}</Text>
                                    </View>
                                </View>

                                <View style={{width:'100%',flexDirection:'row',margin:10}}>
                                    <View style={{width:'50%'}}>
                                        <Text style={{color:Strings.color_green_code}}>{Strings.travaux_text}</Text>
                                    </View>
                                    <View style={{width:'50%'}}>
                                    <Text>{this.state.job_complete}</Text>
                                    </View>
                                </View>
                                <View style={{width:'100%',flexDirection:'row',margin:10}}>
                                    <View style={{width:'50%'}}>
                                        <Text style={{color:Strings.color_green_code}}>{Strings.cout_text}</Text>
                                    </View>
                                    <View style={{width:'50%'}}>
                                    <Text>{"€" +this.state.hour_cost}</Text>
                                    </View>
                                </View>

                                <View style={{width:'100%',flexDirection:'row',margin:10}}>
                                    <View style={{width:'50%'}}>
                                        <Text style={{color:Strings.color_green_code}}>{"Experte"}</Text>
                                    </View>
                                    <View style={{width:'50%'}}>
                                    <Text>{this.state.category}</Text>
                                    </View>
                                </View>

                             
                            </Card>
                        </View>
                    </View>

                    <PopupDialog
                    onHardwareBackPress={() => { this.setState({ lodingDialog: false }) }}
                    width={0.3}
                    visible={this.state.lodingDialog}
                    dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
                    <DialogContent>
                        <View style={{ alignItems: 'center', }}>
                            <ActivityIndicator
                                animating={this.state.animating}
                                style={[{ height: 10, marginBottom: 10, marginTop: 30, marginLeft: 20, marginRight: 20 }]}
                                color="#C00"
                                size="large"
                                hidesWhenStopped={true}
                            />
                        </View>
                    </DialogContent>
                </PopupDialog>
                </ScrollView>
            </View>
        );
    }
}
