import React from 'react';
import { StatusBar,DrawerLayoutAndroid,Alert, TextInput, FlatList, ActivityIndicator, ImageBackground, View, ScrollView, Text, Button, Dimensions, TouchableOpacity, Linking, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from "../styles/styles";
import strings from '../strings/strings';
import { Dropdown } from 'react-native-material-dropdown';
import RBSheet from "react-native-raw-bottom-sheet";
const { width, height } = Dimensions.get("window");
import { Card } from "native-base";
import Sidebar from '../screens/sideBar'
import PopupDialog, {
    
    DialogTitle, DialogContent, DialogFooter, DialogButton, SlideAnimation, ScaleAnimation,
} from 'react-native-popup-dialog';
import Drawer from 'react-native-drawer'
import Image from 'react-native-fast-image'



import moment from 'moment';

export default class MyBoats extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            animating: false,
            lodingDialog: false,
            data: [],
            userid: '',
            token: '',

        }

    }

    _setDrawer() {
        this._drawer.open()
    }
    componentDidMount() {
        this._drawer.close()
        this.getBoats()
      

    }

    getBoats= () => {

    AsyncStorage.getItem("userid")
    .then(userid => {
        this.setState({ userid: userid });

        console.log("state userId============" + userid);


        AsyncStorage.getItem("token")
            .then(token => {
                var accessToken = JSON.parse(token);
                this.setState({ token: accessToken });


                this.setState({
                    animating: true,
                    lodingDialog: true,

                });

                fetch(strings.base_Url + "getBoatInfo", {
                    method: 'POST',
                    headers: {
                        // 'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'User-Id': this.state.userid,
                        'token': this.state.token,
                    },
                    body: JSON.stringify({
                        user_id: this.state.userid


                    })
                })


                    .then((response) => response.json())
                    .then((responseData) => {
                        console.log(" responseData getBoatInfo resoJSon===" + JSON.stringify(responseData));
                        console.log("resoJSon getBoatInfo===" + responseData.error);
                        if (responseData.error == "false") {
                            console.log("Error false massage getBoatInfo" + JSON.stringify(responseData.boat_record));

                            this.setState({
                                animating: false,
                                lodingDialog: false,
                                data: responseData.boat_record,
                            });

                        } else if (responseData.error == "true") {

                            alert('Aucune donnée')
                            this.setState({
                                animating: false,
                                lodingDialog: false,

                            });
                        }
                    }
                    )

            })


    })

    }

    valueExtractor = val => {
        console.log("vehicle id::::****:::" + JSON.stringify(val.value));
    };
    onChangeTextPress(value) {

        if (value != 'Sort by') {



        }


    }

    DatePickerMainFunctionCall = () => {

        let DateHolder = this.state.DateHolder;

        if (!DateHolder || DateHolder == null) {

            DateHolder = new Date();
            this.setState({
                DateHolder: DateHolder
            });
        }

        //To open the dialog
        this.refs.DatePickerDialog.open({

            date: DateHolder,

        });
    }

    onDatePickedFunction = (date) => {

        this.setState({
            //   dobDate: date,
            start_date: moment(date).format('DD-MM-YYYY'),
            order_from: moment(date).format('DD-MM-YYYY'),
        });
    }


    DatePickerMainFunctionCall2 = () => {

        let DateHolder2 = this.state.DateHolder2;

        if (!DateHolder2 || DateHolder2 == null) {

            DateHolder2 = new Date();
            this.setState({
                DateHolder2: DateHolder2
            });
        }

        //To open the dialog
        this.refs.DatePickerDialog2.open({

            date: DateHolder2,

        });
    }

    deleteBoatAlert = (boat_id) => {
        console.log("Boat id  ",boat_id)
        Alert.alert(
            'Alerte',
            'Voulez-vous supprimer le bateau',

            [
               
                {
                    text: "Oui",
                    onPress: () => { this.deleteBoat(boat_id) },
                    style: 'cancel',
                },
                { text: strings.cancelText, onPress: () => console.log('OK Pressed') },
            ],
            { cancelable: false },
        )
    }



    deleteBoat = (boat_id) => {
        this.setState({
            animating: true,
            lodingDialog: true,
        })
        console.log('user id  '+this.state.userid)
        console.log('user id  '+this.state.token)
        fetch(strings.base_Url + "deleteBoatInfo", {
            method: 'POST',
            headers: {
                // 'Accept': 'application/json',
                'Content-Type': 'application/json',
                'user-id': this.state.userid,
                'token': this.state.token,
            },
            body: JSON.stringify({
                id: boat_id,
            })
        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(" responseData resoJSon===" + JSON.stringify(responseData));
                console.log("resoJSon===" + responseData.error);
                if (responseData.error == "false") {

                    console.log("Error false massage" + JSON.stringify(responseData.errorMessage));

                    this.setState({
                        animating: false,
                        lodingDialog: false,
                    });
this.getBoats()
                } else if (responseData.error == "true") {

                    // alert(responseData.errorMessage)
                    alert(responseData.errorMessage)
                    this.setState({
                        animating: false,
                        lodingDialog: false
                    });
                }
            })


    }

    onDatePickedFunction2 = (date) => {

        this.setState({
            //   dobDate: date,
            end_date: moment(date).format('DD-MM-YYYY'),
            order_to: moment(date).format('DD-MM-YYYY')


        });
    }

    search = () => {
        this.componentDidMount();

    }

    render() {
        var navigationView = (
            <Sidebar />
        );
        return (
          <Drawer
            openDrawerOffset={100}
           type="overlay"
             ref={(ref) => this._drawer = ref}
             content={<Sidebar />}
             >
            <View style={{
                flex: 1,
                // backgroundColor:'white',
                flexDirection: 'column',

            }}>
                <ScrollView>

                <Card style={{ width: '100%',marginTop: Platform.OS == 'ios' ? 20 :-5,alignItems:'center', height: 50, flexDirection: 'row',justifyContent:'space-between' }}>
                        <TouchableOpacity onPress={this._setDrawer.bind(this)} style={{
                             height: 50,
                            justifyContent: 'center',marginLeft:-4
                        }}>
                            <Image source={require("../assets/menu_img.png")}
                                style={{ width: 40, height: 40,  }}
                                resizeMode="contain" />
                        </TouchableOpacity>
                        <View style={{  height: 50, justifyContent: 'center', alignItems: 'center', }}>
                            <Text style={{ fontFamily: "Poppins-SemiBold", fontSize: 16, }}>mon bateau</Text>
                        </View>
                        <TouchableOpacity onPress={()=>Actions.push('AddBoat')} style={{ width: 20, height: 20, marginRight:8 }}>
                        <Image source={require("../assets/plus(1).png")}
                                style={{ width: 15, height: 15,  }}
                                resizeMode="contain" />
                            </TouchableOpacity>

                    </Card>




                {this.state.data.length > 0 ?

                    <View style={{
                        width: "100%",



                    }}>
                        <FlatList
                            style={
                                {
                                    marginBottom:20,
                                }
                            }
                            ref={flatList1 => {
                                this.flatList1 = flatList1
                            }}
                            keyExtractor={item => item.id}
                            data={this.state.data}
                            renderItem={({ item, index }) => (
                                <View style={{ alignItems: 'center', marginTop: 5, }}>
                                    <Card style={{
                                        width: "90%", justifyContent: 'space-between',
                                        flexDirection: 'column', padding: 10, borderRadius: 15
                                    }}>
                                        <Text style={{ fontSize: 15, fontFamily:'Poppins-Bold' }}>{item.ship_name}</Text>

                                        <View style={{ flexDirection: 'row' ,width:width*88/100,justifyContent:'space-between'}}>
                                            

                                            <View style={{width:width*83/100,justifyContent:'space-between',flexDirection:'row',alignItems:'center' }}>
<View>
                                                <View style={{ flexDirection: 'row' }}>
                                                    <Text style={{ fontSize: 11,fontFamily:'Poppins-SemiBold',color:'#28528F' }}>Numéro d'enregistrement: </Text>
                                                    <Text style={{ fontSize: 11,fontFamily:'Poppins-SemiBold',color:'#28528F' }}>{item.registration}</Text>
                                                </View>
                                                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                    <Text style={{ fontSize: 11,fontFamily:'Poppins-SemiBold',color:'#28528F'  }}>Marque : </Text>
                                                    <Text style={{fontSize: 11,fontFamily:'Poppins-SemiBold',color:'#28528F'  }}>{item.brand}</Text>
                                                </View>
                                                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                    <Text style={{ fontSize: 11,fontFamily:'Poppins-SemiBold',color:'#28528F'  }}>Taille : </Text>
                                                    <Text style={{ fontSize: 11,fontFamily:'Poppins-SemiBold',color:'#28528F'  }}>{item.size}</Text>

                                                </View>
                                                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                    <Text style={{ fontSize: 11,fontFamily:'Poppins-SemiBold',color:'#28528F'  }}>Capacité du réservoir : </Text>
                                                    <Text style={{ fontSize: 11,fontFamily:'Poppins-SemiBold',color:'#28528F'  }}>{item.reserve_capacity}</Text>

                                                </View>
                                                </View>
                                                <View>
                                                    <TouchableOpacity
                                                    style={{width:30,height:30,marginTop:-25,justifyContent:'center',}}
                                                  onPress={() => Actions.push("EditBoat3", { currentboatid: item.id })}

                                                    >
                                                    <Image source={require('../assets/edit-button.png')} 
                                                    style={{width:15,height:15}}/>
                                                    </TouchableOpacity>
                                                    <TouchableOpacity
                                                              style={{width:30,height:30,   marginTop:25,justifyContent:'center',}}
                                                              onPress={() => this.deleteBoatAlert(item.id)}>
                                                    <Image source={require('../assets/25.png')} style={{width:15,height:15,
                                                     }}/>
                                                     </TouchableOpacity>
                                                 

                                                </View>
                                            </View>
                                        </View>


                                    </Card>
                                </View>

                            )}
                        />
                    </View>
                    :
                    <Text style={{ fontFamily: "Poppins-Medium", marginTop: height*25/100,textAlign:'center',color:'grey' }}>Aucune donnée
                    </Text>

                }
                <PopupDialog
                    onHardwareBackPress={() => { this.setState({ lodingDialog: false }) }}
                    width={0.3}
                    visible={this.state.lodingDialog}
                    dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
                    <DialogContent>
                        <View style={{ alignItems: 'center', }}>
                            <ActivityIndicator
                                animating={this.state.animating}
                                style={[{ height: 10, marginBottom: 10, marginTop: 30, marginLeft: 20, marginRight: 20 }]}
                                color="#C00"
                                size="large"
                                hidesWhenStopped={true}
                            />
                        </View>
                    </DialogContent>
                </PopupDialog>
                </ScrollView>


            </View>
        </Drawer>
        );
    }
}
