import React from 'react';
import { StatusBar, Button, Alert, ActivityIndicator, ScrollView, PixelRatio, TextInput, View, ImageBackground, Text, Dimensions, TouchableOpacity, Linking, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import DateTimePicker from "react-native-modal-datetime-picker";
import DatePickers from 'react-native-modal-datetime-picker'
import styles from "../styles/styles";
import Strings from '../strings/strings'
import { DatePicker, Card } from "native-base";
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';
import RNGooglePlaces from 'react-native-google-places';
import AmountDailog from 'react-native-raw-bottom-sheet';
import moment from 'moment';
import Geolocation from '@react-native-community/geolocation';
import Geocoder from 'react-native-geocoder';
import { decode, encode } from 'base-64'



const { width, height } = Dimensions.get("window");
import PopupDialog, {
    DialogTitle, DialogContent, DialogFooter, DialogButton, SlideAnimation, ScaleAnimation,
} from 'react-native-popup-dialog';
let payment_type;
//let date_select;
let refernce_codeno = "";
import SoundRecorder from 'react-native-sound-recorder';
import DocumentPicker from 'react-native-document-picker';

import AudioRecorderPlayer from 'react-native-audio-recorder-player';

const audioRecorderPlayer = new AudioRecorderPlayer();

var Sound = require('react-native-sound');
var whoosh;
let audio_file = '';
let audio_file1 = '';
let dates = new Date();
console.log("dates::" + dates)
var current_Date = moment(Date(dates)).format('DD/MM/YYYY');
var current_Time = dates.getHours() + ':' + dates.getMinutes()
console.log("dates::" + dates.getHours() + ':' + dates.getMinutes())
import Image from 'react-native-fast-image'

export default class createReservation extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            animating: false,
            lodingDialog: false,
            chosenDate: '',
            chosenDateErr: '',
            time: current_Time,
            timeErr: '',
            select_oneErr: '',
            ImageSource1: null,
            data: [],
            ImageSource2: null,
            ImageSource3: null,
            ImageSource2true: false,
            ImageSource3true: false,
            isDateTimePickerVisible: false,
            datePickervisible: false,
            visa_radio: false,
            card_radio: false,
            reference_radio: false,
            writing_radio: false,
            audio_radio: false,
            photo_radio: false,
            select_cardErr: '',
            description: '',
            descriptionErr: '',
            user_id: '',
            token: '',
            current_cat_id: '',
            date_select: current_Date,
            client_type: '',
            imageErr: '',
            reference_code: '',
            referenceErr: '',
            timer: null,
            minutes_Counter: '00',
            seconds_Counter: '00',
            startDisable: false,
            address: '',
            addressErr: '',
            codePostal: '',
            codePostalErr: '',
            ville: '',
            villeErr: '',
            codePorte: '',
            codePorteErr: '',
            etage: '',
            etageErr: '',
            autres: '',
            autresErr: '',
            telephone: '',
            telephoneErr: '',
            name: '',
            nameErr: '',
            Total_amount: '',
            total_show: '',
            req_id: '',
            recordSecs: '',
            recordTime: '',
            reference_enable: '',
            referance_code1: '',
            currentlatitude:'',
            currentlongitude:'',


        };
        this.setDate = this.setDate.bind(this);
        console.log("current_cat_id:::" + this.props.current_cat_id)
    }

    componentWillUnmount() {

        clearInterval(this.state.timer);
    }
    async selectOneFile() {
        //Opening Document Picker for selection of one file
        try {
          const res = await DocumentPicker.pick({
            type: [DocumentPicker.types.audio],
            //There can me more options as well
            // DocumentPicker.types.allFiles
            // DocumentPicker.types.images
            // DocumentPicker.types.plainText
            // DocumentPicker.types.audio
            // DocumentPicker.types.pdf
          });
          //Printing the log realted to the file
          console.log('res : ' + JSON.stringify(res));
          console.log('URI : ' + res.uri);
          console.log('Type : ' + res.type);
          console.log('File Name : ' + res.name);
          console.log('File Size : ' + res.size);
          var encodedString = encode(res);
          audio_file= "file://"+res.uri;

          console.log(' audio_file File Size url : ' + audio_file);
          //Setting the state to show single file attributes
          this.setState({ singleFile: res
             });
        } catch (err) {
          //Handling any exception (If any)
          if (DocumentPicker.isCancel(err)) {
            //If user canceled the document selection
            alert('Canceled from single doc picker');
          } else {
            //For Unknown Error
            alert('Unknown Error: ' + JSON.stringify(err));
            throw err;
          }
        }
      }

    showaudioFile(){
        
    }


    openSearchModal() {
        // RNGooglePlaces.getCurrentPlace(['placeID', 'location', 'name', 'address'])
        // .then((results) => console.log("results:::::"+results))
        // .catch((error) => console.log("message:::::"+error.message));
        RNGooglePlaces.openAutocompleteModal()
            .then((place) => {
                this.setState({

                    addressErr: '',
                    address: place.address,

                })

                console.log("address::::" + place.address);
                // place represents user's selection from the
                // suggestions and it is a simplified Google Place object.
            })
            .catch(error => console.log(error.message));  // error is a Javascript Error object
    }


    onButtonStart = () => {


        let timer = setInterval(() => {

            SoundRecorder.start(SoundRecorder.PATH_CACHE + '/test1.mp3', {
                SampleRate: 22050,
                Channels: 1,
                AudioQuality: "Low",
                AudioEncoding: "aac"
            })
                .then(function () {
                    console.log('started recording');
                });
            var num = (Number(this.state.seconds_Counter) + 1).toString(),
                count = this.state.minutes_Counter;
            if (Number(this.state.seconds_Counter) == 59) {
                count = (Number(this.state.minutes_Counter) + 1).toString();
                num = '00';
            }
            this.setState({
                minutes_Counter: count.length == 1 ? '0' + count : count,
                seconds_Counter: num.length == 1 ? '0' + num : num
            });
        }, 1000);
        this.setState({ timer });
        this.setState({ startDisable: true })
    }
    onButtonStop = () => {
        clearInterval(this.state.timer);
        SoundRecorder.stop()
            .then(function (options) {

                console.log("url audio::::" + options.path)

                var encodedString = encode('file:/' + options.path);
                console.log(encodedString);
                audio_file = encodedString
                this.setState({
                    audio_path: options.path
                })

                whoosh = new Sound('https://www.curzenncloud.fr/hopassist/public/upload/request_audio/1581672439_7388.m4a', Sound.MAIN_BUNDLE, (error) => {
                    if (error) {

                        console.log('failed to load the sound', error);

                        return;
                    }
                    console.log('duration in seconds: ' + whoosh.getDuration() + 'number of channels: ' + whoosh.getNumberOfChannels());

                });
                console.log('stopped recording, audio file saved at: ' + JSON.stringify(options) + "duration::" + (options.duration) * 1000 / 60);
            });
        this.setState({
            startDisable: false,
        })
    }
    onButtonClear = () => {
        audio_file = '';
        this.setState({
            timer: null,
            minutes_Counter: '00',
            seconds_Counter: '00',
        });
    }

    onplay() {
        whoosh.play();
    }

    componentWillMount() {

        Geolocation.getCurrentPosition(info => {
            console.log("info :::" + JSON.stringify(info));
            this.setState({
                currentlatitude: info.coords.latitude,
                currentlongitude: info.coords.longitude,
            });

            var pos = {
                lat: info.coords.latitude,
                lng: info.coords.longitude
            };
            console.log("currentLong::" + JSON.stringify(pos))
            Geocoder.geocodePosition(pos).then(res => {
                console.log("address::::" + JSON.stringify(res[0].locality + " " + res[0].postalCode))
                console.log("address::::" + JSON.stringify(res[0].formattedAddress))

                this.setState({
                    address: res[0].formattedAddress,
                    codePostal: res[0].postalCode,
                    ville: res[0].locality
                })
            })
                .catch(error => this.showalerts(error));


        }
        );

        var tt = "49.99";
        var tts = tt.replace('.', ',')
        console.log("tts::::" + tts)
        AsyncStorage.getItem("userid")
            .then(userid => {
                this.setState({ user_id: userid });

                AsyncStorage.getItem("token")
                    .then(token => {
                        var accessToken = JSON.parse(token);
                        console.log("accessToken::" + accessToken + ":::User:::" + userid)
                        this.setState({ token: accessToken });
                        fetch(Strings.base_Url + "getAddress", {
                            method: 'POST',
                            headers: {
                                // 'Accept': 'application/json',
                                'Content-Type': 'application/json',
                                'User-Id': userid,
                                'token': accessToken,
                            },
                            body: JSON.stringify({
                                user_id: userid,
                            })
                        })
                            .then((response) => response.json())
                            .then((responseData) => {
                                console.log(" responseData resoJSon===" + JSON.stringify(responseData));
                                console.log("resoJSon===" + responseData.error);
                                if (responseData.error == "false") {
                                    console.log("Error false massage" + JSON.stringify(responseData.errorMessage));
                                    this.setState({
                                        animating: false,
                                        lodingDialog: false,

                                        telephone: responseData.address.alternate_no,

                                    });
                                } else if (responseData.error == "true") {
                                    this.setState({
                                        passwordErr: responseData.errorMessage,
                                        animating: false,
                                        lodingDialog: false

                                    });
                                }
                            }
                            )

                    });

                AsyncStorage.getItem("client_type")
                    .then(client_type => {
                        var client_type = JSON.parse(client_type);
                        console.log("client_type::" + client_type);
                        this.setState({ client_type: client_type });

                    });

            });
            this.getprofile();
    }

    getprofile(){
        this.setState({
            animating: true,
            lodingDialog: true,

        })
        AsyncStorage.getItem("userid")
            .then(userid => {
                this.setState({ user_id: userid });

                AsyncStorage.getItem("token")
                    .then(token => {
                        var accessToken = JSON.parse(token);
                        this.setState({ token: accessToken });
                        console.log("accessToken====$$$========" + accessToken);
                        console.log("state userId====$$$========" + userid);

                        fetch(Strings.base_Url + "getProfile", {
                            method: 'POST',
                            headers: {
                                // 'Accept': 'application/json',
                                'Content-Type': 'application/json',
                                'User-Id': userid,
                                'token': accessToken,
                            },
                            body: JSON.stringify({
                                user_id: userid,
                            })
                        })
                            .then((response) => response.json())
                            .then((responseData) => {
                                console.log(" responseData resoJSon===" + JSON.stringify(responseData));
                                console.log("resoJSon===" + responseData.error);
                                if (responseData.error == "false") {
                                    console.log("Error false massage" + JSON.stringify(responseData.errorMessage));
                                    
                                    this.setState({
                                        animating: false,
                                        lodingDialog: false,
                                        reference_enable: responseData.usersDetails.reference_enable,
                                        referance_code1: responseData.usersDetails.referance_code,      
                                    });
                                } else if (responseData.error == "true") {
                                    this.setState({
                                        passwordErr: responseData.errorMessage,
                                        animating: false,
                                        lodingDialog: false

                                    });
                                }
                            }
                            )
                    });
            });
    }

    setDate(newDate) {
        this.setState({ chosenDate: newDate });

        console.log("chosenDate:::" + this.state.chosenDate)
    }

    showDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: true });
    };

    showDate = () => {
        this.setState({ datePickervisible: true });

    }

    hideDateTimePicker = () => {

        this.setState({ isDateTimePickerVisible: false, datePickervisible: false });
    };
    handleDatePicked = date => {


        console.log("A date has been picked: ", date);
        // var date_select = date.toString().substr(3, 12);
        var current_Date = moment(date).format('DD/MM/YYYY');

        console.log("A date has been picked $$$$$ ###: " + current_Date);
        //date_select=date_select
        this.setState({
            date_select: current_Date
        })
        this.hideDateTimePicker();
    };
    _handleDatePicked = time => {
        console.log("A date time has been picked: ", time);
        const selected1 = time.getHours() + ':' + time.getMinutes();
        console.log("A date time has been picked: ", selected1);
        this.setState({ time: selected1 })
        this.hideDateTimePicker();
    };
    selectPhotoTapped(num) {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,

            storageOptions: {
                skipBackup: true
            }
        };
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };
                switch (num) {
                    case '1':
                        this.setState({
                            ImageSource2true: true,
                            ImageSource1: source,
                            data: [...this.state.data, response.data],
                        });
                        break;
                    case '2':
                        this.setState({
                            ImageSource3true: true,
                            ImageSource2: source,
                            data: [...this.state.data, response.data],
                        });
                        break;
                    case '3':
                        this.setState({
                            ImageSource3: source,
                            data: [...this.state.data, response.data],
                        });
                        break;
                    default:
                        break;
                }
            }
        });
    }

    validation() {
        var isVisible = 0;
        if (this.state.date_select != "") {
            isVisible += 1
        } else {
            isVisible -= 1
            this.setState({
                chosenDateErr: "Sélectionner la date et l'heure"
            })
        }
        if (this.state.time != "") {
            isVisible += 1
        } else {
            isVisible -= 1
            this.setState({
                chosenDateErr: "Sélectionner la date et l'heure"
            })
        }

        if (this.state.description != '') {
            isVisible += 1
        } else {
            isVisible -= 1
            this.setState({
                descriptionErr: Strings.ShouldemptyText
            })
        }

        if (this.state.client_type == "1") {
            if (this.state.visa_radio != false || this.state.card_radio != false || this.state.reference_radio != false) {
                isVisible += 1

                if (this.state.visa_radio == true) {
                    payment_type = "1"
                } else if (this.state.card_radio == true) {
                    payment_type = "2"
                }

            } else {
                isVisible -= 1
                this.setState({
                    select_cardErr: "Sélectionnez le type de paiement"
                })
            }
        } else if (this.state.client_type == "2" || this.state.client_type == "3") {

            var isValidate = 0;
            if (this.state.address != "") {
                isValidate += 1;
            } else {
                isValidate -= 1

                this.setState({
                    addressErr: Strings.ShouldemptyText,
                });
            }
            if (this.state.codePostal != "") {
                isValidate += 1;
            } else {
                isValidate -= 1
                this.setState({
                    codePostalErr: Strings.ShouldemptyText,
                });
            }
            if (this.state.ville != "") {
                isValidate += 1;
            } else {
                isValidate -= 1
                this.setState({
                    villeErr: Strings.ShouldemptyText,
                });
            }
            // if (this.state.codePorte != "") {
            //     isValidate += 1;
            // } else {
            //     isValidate -= 1
            //     this.setState({
            //         codePorteErr: Strings.ShouldemptyText,
            //     });
            // }
            // if (this.state.etage != "") {
            //     isValidate += 1;
            // } else {
            //     isValidate -= 1
            //     this.setState({
            //         etageErr: Strings.ShouldemptyText,
            //     });
            // }
            // if (this.state.autres != "") {
            //     isValidate += 1;
            // } else {
            //     isValidate -= 1
            //     this.setState({
            //         autresErr: Strings.ShouldemptyText,
            //     });
            // }
            if (this.state.telephone != "") {
                isValidate += 1;
            } else {
                isValidate -= 1
                this.setState({
                    telephoneErr: Strings.ShouldemptyText,
                });
            }
            if (this.state.client_type == "2")
             {
                 console.log("reference code :::"+this.state.referance_code1)
                 if(this.state.reference_enable == 1){
                    payment_type = "1"
                    refernce_codeno = this.state.referance_code1
                    this.setState({
                        reference_code:this.state.referance_code1
                    }) 
                    if (this.state.name != '') {
                        isValidate += 1;
                    } else {
                        isVisible -= 1
                        this.setState({
                            nameErr: Strings.ShouldemptyText
                        })
                    }

                 } else{

                    if (this.state.visa_radio != false || this.state.card_radio != false || this.state.reference_radio != false) {
                        // isValidate += 1;
                        if (this.state.visa_radio == true) {
                            payment_type = "1"
                        } else if (this.state.card_radio == true) {
                            payment_type = "2"
                        }
                        if (this.state.visa_radio == true || this.state.card_radio) {
                            if (this.state.name != '') {
                                isValidate += 1;
                            } else {
                                isVisible -= 1
                                this.setState({
                                    nameErr: Strings.ShouldemptyText
                                })
                            }
                        } else {
                        }
                    } else {
                        isVisible -= 1
                        this.setState({
                            select_cardErr: "Sélectionnez le type de paiement"
                        })
                    }

                 }

                
            } else if (this.state.client_type == "3") {
                payment_type = "3"
                if (this.state.reference_code == '') {
                    isValidate += 1;
                    // isVisible += 1
                } else {
                    isVisible -= 1
                    this.setState({
                        referenceErr: Strings.ShouldemptyText
                    })
                }
            }
            console.log("Use Login::::" + isValidate)
            if (isValidate == 5) {
                isVisible += 1
            }
            else {
                // alert(strings.Pleasefillallthefields)
            }
        }

        console.log("response:::::::" + isVisible);
        console.log("response:::::::" + this.state.date_select);

        if (isVisible == 4) {
            console.log("response::&&&:::::" + isVisible);
            this.createReservation();
        }
    }

    createReservation() {

        this.setState({
            animating: true,
            lodingDialog: true,
        });


        var request_date = JSON.stringify(this.state.chosenDate)
        var request_time = JSON.stringify(this.state.time)
        // var image_data= [];
        console.log("audio path :::::" + audio_file)
        console.log("accessToken::" + this.state.token + ":::User:::" + audio_file)
        console.log("payment_type: refrence code:::" + refernce_codeno + "Date" + request_date + request_time)
        let headers = {
            'Content-Type': 'multipart/form-data',
            'User-Id': this.state.user_id,
            'token': this.state.token,
        };
        RNFetchBlob.fetch('POST', Strings.base_Url + 'putRequest', headers, [
            { name: 'user_id', data: this.state.user_id },
            { name: 'request_date', data: this.state.date_select },
            { name: 'request_time', data: this.state.time },
            { name: 'pay_by', data: payment_type },
            { name: 'reference_client', data: refernce_codeno },
            { name: 'cat_id', data: JSON.stringify(this.props.current_cat_id) },
            { name: 'detail', data: this.state.description },
            { name: 'audio', filename: 'test.mp3', type: 'audio/mpeg', data: audio_file },
            { name: 'image1', filename: 'photo.jpg', type: 'image/png', data: this.state.data[0] },
            { name: 'image2', filename: 'photo.jpg', type: 'image/png', data: this.state.data[1] },
            { name: 'image3', filename: 'photo.jpg', type: 'image/png', data: this.state.data[2] },
            { name: 'image4', filename: 'photo.jpg', type: 'image/png', data: this.state.data[3] },
            { name: 'image5', filename: 'photo.jpg', type: 'image/png', data: this.state.data[4] },
            { name: 'attende_name', data: this.state.name },
            { name: 'rue', data: this.state.address },
            { name: 'postal_code', data: this.state.codePostal },
            { name: 'ville', data: this.state.ville },
            { name: 'door_code', data: this.state.codePorte },
            { name: 'etage', data: this.state.etage },
            { name: 'alternate_no', data: this.state.telephone },
        ],
        ).then((resp) => {
            console.log("response: put Request :::$$$$:::" + JSON.stringify(resp.json()));
            console.log("error:::" + resp.json().error)
            if (resp.json().error === "false") {

                var tt = resp.json().result.advance_payment;
                var tts = tt.replace('.', ',')
                console.log("tts $$$::::" + tts)

                this.setState({
                    total_show: tts,
                    Total_amount: resp.json().result.advance_payment,
                    req_id: resp.json().request_id,
                    animating: false,
                    lodingDialog: false,
                });
                if (resp.json().result.client_type == "3") {
                    AsyncStorage.setItem("request_id", JSON.stringify(resp.json().request_id))
                    Actions.push("ExpertsList", { current_cat_id: this.props.current_cat_id })
                } else {
                    if(resp.json().result.reference_used == "1"){
                        AsyncStorage.setItem("request_id", JSON.stringify(resp.json().request_id))
                        Actions.push("ExpertsList", { current_cat_id: this.props.current_cat_id })
                    }else{
                        this.AmountDailog.open();
                    }
                    
                }
                console.log("error:::" + resp.json().error)


            } else if (resp.json().error === "true") {
                // alert(resp.json().errorMessage)
                this.showalerts(resp.json().errorMessage)
                this.setState({
                    animating: false,
                    lodingDialog: false,
                });
            }
        }).catch((err) => {
            this.setState({
                animating: false,
                lodingDialog: false,
            });
            console.log("response::::err:::" + err);
            console.log("response::::err:::" + JSON.stringify(err));
        });
    }

    showalerts(body) {
        Alert.alert(
            Strings.alert_tital,
            body,
            [
                { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
            ],
            { cancelable: false });
    }

    render() {

        return (
            <View style={styles.containerWhite}>

                <Image 
                source={require("../assets/simble.png")}
                    style={{ width: '100%', height: 78, }}>
                    <View style={{ width: '100%', height: 50, flexDirection: 'row' }}>
                        <TouchableOpacity onPress={() => Actions.pop()} style={{ width: '15%', height: 50, justifyContent: 'center', alignItems: 'center', }}>
                            <Image source={require("../assets/back.png")}
                                style={{ width: 25, height: 25, }}
                                resizeMode="contain" />
                        </TouchableOpacity>
                        <View style={{ width: '80%', height: 50, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: 16, fontWeight: 'bold' }}>{Strings.createReservation_text}</Text>

                        </View>

                    </View>
                </Image>
                <ScrollView style={{ marginTop: -25 }}>
                    <View style={{ flex: 1, flexDirection: 'column', margin: 10 }}>
                    <TouchableOpacity onPress={()=>this.selectOneFile()}>
                        <Text>Show Id</Text>
                    </TouchableOpacity>

                        <Text style={{ fontSize: 16, fontWeight: 'bold', padding: 10, }}>{Strings.je_souhaite_text}</Text>
                        <View style={{ width: '100%', flexDirection: 'row', height: 50 }} >
                            <Card style={{ width: '50%', justifyContent: 'center', flexDirection: 'row', alignItems: 'center', borderRadius: 10 }}>
                                <View style={{ width: '20%' }}>
                                    <Text style={{ padding: 5, fontSize: 15 }}>{Strings.le_text}</Text>
                                </View>
                                <TouchableOpacity onPress={this.showDate} style={{ width: '80%', backgroundColor: 'white', flexDirection: 'row', height: 40, borderRadius: 10, alignItems: 'center' }}>
                                    <Image source={require("../assets/calender.png")}
                                        style={{ width: 20, height: 20, marginLeft: 2 }}
                                        resizeMode="contain" />
                                    <Text style={{ paddingLeft: 10 }}>{this.state.date_select}</Text>

                                    <DatePickers
                                        // mode="date"
                                        locale="en_GB" // Use "en_GB" here
                                        // is24Hour={false}
                                        isVisible={this.state.datePickervisible}
                                        onConfirm={this.handleDatePicked}
                                        onCancel={this.hideDateTimePicker}
                                    />
                                </TouchableOpacity>
                            </Card>
                            <Card style={{ width: '50%', justifyContent: 'center', flexDirection: 'row', alignItems: 'center', borderRadius: 10 }}>
                                <View style={{ width: '50%' }}>
                                    <Text style={{ padding: 5, fontSize: 15 }}>{Strings.À_partir_de_text}</Text>
                                </View>
                                <TouchableOpacity onPress={this.showDateTimePicker} style={{ width: '50%', backgroundColor: 'white', flexDirection: 'row', height: 40, borderRadius: 10, alignItems: 'center' }}>
                                    <Image source={require("../assets/clock.png")}
                                        style={{ width: 30, height: 20, }}
                                        resizeMode="contain" />
                                    <Text>{this.state.time}</Text>
                                    <DateTimePicker
                                        mode="time"
                                        locale="en_GB" // Use "en_GB" here
                                        is24Hour={true}
                                        isVisible={this.state.isDateTimePickerVisible}
                                        onConfirm={this._handleDatePicked}
                                        onCancel={this.hideDateTimePicker}
                                    />
                                </TouchableOpacity>
                            </Card>
                        </View>
                        {!!this.state.chosenDateErr && (
                            <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.chosenDateErr}</Text>
                        )}

                        {/* <Text style={{ fontSize: 14, padding: 10, marginTop: 20 }}>{Strings.Aidez_nous_text}</Text> */}
                        {/* <Text style={{ fontSize: 14, fontWeight: 'bold', color: Strings.color_green_code, paddingLeft: 10, }}>{}</Text> */}

                        {/* <View style={{ flexDirection: 'row', marginTop: 10 }}>
                            <View style={{ width: '50%', flexDirection: 'row' }}>
                                <TouchableOpacity onPress={() => this.setState({ writing_radio: true, audio_radio: false, photo_radio: false, select_oneErr: '' })} style={{ width: '20%', height: 30, alignItems: 'center', justifyContent: 'center' }}>
                                    <View style={{ width: 20, height: 20, borderWidth: 2, borderRadius: 20 / 2, alignItems: 'center', justifyContent: 'center', borderColor: Strings.light_color }}>
                                        {this.state.writing_radio === true && (<View style={{ width: 16, height: 16, borderRadius: 16 / 2, backgroundColor: '#F1C368', alignItems: 'center', justifyContent: 'center' }}>
                                            <Image source={require("../assets/right.png")}
                                                style={{ width: 10, height: 10, }}
                                                resizeMode="contain" />
                                        </View>)}

                                    </View>
                                </TouchableOpacity>
                                <View style={{ alignItems: 'center', marginLeft: 20, justifyContent: 'center' }}>
                                    <Text>{"Par écrit "}</Text>
                                </View>
                            </View>
                            <View style={{ width: '50%', flexDirection: 'row' }}>
                                <TouchableOpacity onPress={() => this.setState({ writing_radio: false, audio_radio: true, photo_radio: false, select_oneErr: '' })} style={{ width: '20%', height: 30, alignItems: 'center', justifyContent: 'center' }}>
                                    <View style={{ width: 20, height: 20, borderWidth: 2, borderRadius: 20 / 2, alignItems: 'center', justifyContent: 'center', borderColor: Strings.light_color }}>
                                        {this.state.audio_radio === true && (<View style={{ width: 16, height: 16, borderRadius: 16 / 2, backgroundColor: '#F1C368', alignItems: 'center', justifyContent: 'center' }}>
                                            <Image source={require("../assets/right.png")}
                                                style={{ width: 10, height: 10, }}
                                                resizeMode="contain" />
                                        </View>)}

                                    </View>
                                </TouchableOpacity>
                                <View style={{ alignItems: 'center', marginLeft: 20, justifyContent: 'center' }}>
                                    <Text>{"audio"}</Text>

                                </View>
                            </View>
                        </View> */}

                        <View style={{ flexDirection: 'row', marginTop: 10 }}>
                            {/* <View style={{ width: '50%', flexDirection: 'row' }}>
                                <TouchableOpacity onPress={() => this.setState({ writing_radio: false, audio_radio: false, photo_radio: true, select_oneErr: '' })} style={{ width: '20%', height: 30, alignItems: 'center', justifyContent: 'center' }}>
                                    <View style={{ width: 20, height: 20, borderWidth: 2, borderRadius: 20 / 2, alignItems: 'center', justifyContent: 'center', borderColor: Strings.light_color }}>
                                        {this.state.photo_radio === true && (<View style={{ width: 16, height: 16, borderRadius: 16 / 2, backgroundColor: '#F1C368', alignItems: 'center', justifyContent: 'center' }}>
                                            <Image source={require("../assets/right.png")}
                                                style={{ width: 10, height: 10, }}
                                                resizeMode="contain" />
                                        </View>)}

                                    </View>
                                </TouchableOpacity>
                                <View style={{ alignItems: 'center', marginLeft: 20, justifyContent: 'center' }}>
                                    <Text>{"par photo"}</Text>
                                </View>
                            </View> */}
                            {/* <View style={{ width: '50%', flexDirection: 'row' }}>
                                        <TouchableOpacity onPress={() => this.setState({ Particulier_radio: false, Entreprise_radio: true, Clienten_radio: false })} style={{ width: '20%', height: 30, alignItems: 'center', justifyContent: 'center' }}>
                                            <View style={{ width: 20, height: 20, borderWidth: 2, borderRadius: 20 / 2, alignItems: 'center', justifyContent: 'center', borderColor: Strings.light_color }}>
                                                {this.state.Entreprise_radio === true && (<View style={{ width: 16, height: 16, borderRadius: 16 / 2, backgroundColor: '#F1C368', alignItems: 'center', justifyContent: 'center' }}>
                                                    <Image source={require("../assets/right.png")}
                                                        style={{ width: 10, height: 10, }}
                                                        resizeMode="contain" />
                                                </View>)}

                                            </View>
                                        </TouchableOpacity>
                                        <View style={{ alignItems: 'center', marginLeft: 20, justifyContent: 'center' }}>
                                            <Text>{"audio"}</Text>
                                        </View>
                                    </View> */}
                        </View>

                        {!!this.state.select_oneErr && (
                            <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.select_oneErr}</Text>
                        )}

                        {this.state.writing_radio === false && (<View style={{ width: '100%', justifyContent: 'center' }}>
                            <Text style={{ fontSize: 14, fontWeight: 'bold', padding: 10, marginTop: 20 }}>{Strings.dites_nous_text}</Text>
                            <Card style={{ width: '100%', backgroundColor: 'white', borderRadius: 10 }}>
                                <TextInput
                                    multiline={true}
                                    placeholder="Description "
                                    onChangeText={(description) => this.setState({ description, descriptionErr: '' })}
                                    style={{ width: '100%', padding: 10 }}>

                                </TextInput>
                            </Card>


                            {!!this.state.descriptionErr && (
                                <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.descriptionErr}</Text>
                            )}
                        </View>)
                        }

                        {this.state.audio_radio === false && (<View style={{ width: '100%', justifyContent: 'center' }}>
                            <Card style={{ marginTop: 30, width: '100%', flexDirection: 'column', justifyContent: 'center', borderRadius: 10 }}>
                                <View style={{ flexDirection: 'row', marginTop: 15 }}>
                                    <Image source={require("../assets/microphone.png")}
                                        style={{ width: 30, height: 30, marginLeft: 10 }}
                                        resizeMode="contain" />
                                    <Text style={{ fontSize: 14, fontWeight: 'bold', paddingLeft: 10, }}>{Strings.start_recording_text}</Text>
                                </View>
                                <View style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 20 }}>

                                    <Text style={{ marginLeft: 10 }}>{this.state.minutes_Counter} : {this.state.seconds_Counter}</Text>

                                    <TouchableOpacity
                                        onPress={this.onButtonStart}
                                        activeOpacity={0.6}
                                        style={[{ width: 80, height: 40, margin: 5, borderRadius: 10, justifyContent: 'center', alignItems: 'center' }, { backgroundColor: this.state.startDisable ? '#B0BEC5' : '#F1C368' }]}
                                        disabled={this.state.startDisable} >
                                        <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 14 }}>Démarrer</Text>
                                    </TouchableOpacity>

                                    <TouchableOpacity
                                        onPress={this.onButtonStop}
                                        activeOpacity={0.6}
                                        style={[{ width: 80, height: 40, margin: 5, borderRadius: 10, justifyContent: 'center', alignItems: 'center' }, { backgroundColor: '#F1C368' }]} >

                                        <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 14 }}>Stop</Text>

                                    </TouchableOpacity>

                                    <TouchableOpacity
                                        onPress={this.onButtonClear}
                                        activeOpacity={0.6}
                                        style={[{ width: 80, height: 40, margin: 5, borderRadius: 10, justifyContent: 'center', alignItems: 'center' }, { backgroundColor: this.state.startDisable ? '#B0BEC5' : '#F1C368' }]}
                                        disabled={this.state.startDisable} >

                                        <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 14 }}> {Strings.clear_text}</Text>

                                    </TouchableOpacity>


                                    {/* <TouchableOpacity
                                    onPress={() => this.onplay()}
                                    activeOpacity={0.6}
                                    style={[styles.button, { backgroundColor: this.state.startDisable ? '#B0BEC5' : '#FF6F00' }]}
                                    disabled={this.state.startDisable} >
                                    <Text style={styles.buttonText}> Play </Text>
                                </TouchableOpacity> */}

                                </View>
                            </Card>


                        </View>)}

                        {this.state.photo_radio === false && (<View style={{ width: '100%', justifyContent: 'center' }}>


                            <Text style={{ fontSize: 14, fontWeight: 'bold', color: Strings.color_green_code, paddingLeft: 10, marginTop: 30 }}>{Strings.Ajouter_une_photo_text}</Text>

                            <View style={{ flexDirection: 'row', alignItems: 'center', margin: 10 }}>
                                <TouchableOpacity onPress={() => this.selectPhotoTapped('1')}>
                                    <Card style={{
                                        borderRadius: 10,
                                        width: 100,
                                        margin: 5,
                                        height: 100,
                                        borderColor: 'white',
                                        borderWidth: 1 / PixelRatio.get(),
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        backgroundColor: 'white',
                                    }}>

                                        {this.state.ImageSource1 === null ? <Image source={require("../assets/plus(1).png")}
                                            style={{ width: 30, height: 30 }}></Image> :
                                            <Image style={{
                                                borderRadius: 10,
                                                width: 100,
                                                margin: 5,
                                                height: 100,
                                                borderColor: 'white',
                                                borderWidth: 1 / PixelRatio.get(),
                                                justifyContent: 'center',
                                                alignItems: 'center',
                                                backgroundColor: 'white',
                                            }} source={this.state.ImageSource1} />
                                        }

                                    </Card>
                                </TouchableOpacity>

                                {this.state.ImageSource2true === true && (
                                    <TouchableOpacity onPress={() => this.selectPhotoTapped('2')}>
                                        <Card style={{
                                            borderRadius: 10,
                                            width: 100,
                                            margin: 10,
                                            height: 100,
                                            borderColor: 'white',
                                            borderWidth: 1 / PixelRatio.get(),
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            backgroundColor: 'white',
                                        }}>

                                            {this.state.ImageSource2 === null ? <Image source={require("../assets/plus(1).png")}
                                                style={{ width: 30, height: 30 }}></Image> :
                                                <Image style={{
                                                    borderRadius: 10,
                                                    width: 100,
                                                    margin: 5,
                                                    height: 100,
                                                    borderColor: 'white',
                                                    borderWidth: 1 / PixelRatio.get(),
                                                    justifyContent: 'center',
                                                    alignItems: 'center',
                                                    backgroundColor: 'white',
                                                }} source={this.state.ImageSource2} />
                                            }

                                        </Card>
                                    </TouchableOpacity>)}

                                {this.state.ImageSource3true === true && (
                                    <TouchableOpacity onPress={() => this.selectPhotoTapped('3')}>
                                        <Card style={{
                                            borderRadius: 10,
                                            width: 100,
                                            margin: 5,
                                            height: 100,
                                            borderColor: 'white',
                                            borderWidth: 1 / PixelRatio.get(),
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            backgroundColor: 'white',
                                        }}>

                                            {this.state.ImageSource3 === null ? <Image source={require("../assets/plus(1).png")}
                                                style={{ width: 30, height: 30 }}></Image> :
                                                <Image style={{
                                                    borderRadius: 10,
                                                    width: 100,
                                                    margin: 5,
                                                    height: 100,
                                                    borderColor: 'white',
                                                    borderWidth: 1 / PixelRatio.get(),
                                                    justifyContent: 'center',
                                                    alignItems: 'center',
                                                    backgroundColor: 'white',
                                                }} source={this.state.ImageSource3} />
                                            }

                                        </Card>
                                    </TouchableOpacity>
                                )}
                            </View>
                        </View>)
                        }
                        {!!this.state.imageErr && (
                            <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.imageErr}</Text>
                        )}



                        <Text style={{ fontSize: 14, padding: 10, marginTop: 30 }}>{Strings.si_vous_prenez_text}</Text>

                        {this.state.client_type == 3 ? <View>

                            {/* <Text style={{ fontSize: 14, marginLeft: 15, marginTop: 20 }}>{Strings.reference_text}</Text>
                            <Card style={{ width: '90%', height: 40, borderRadius: 10, marginLeft: 20, justifyContent: 'center' }}>
                                <TextInput
                                    onChangeText={(reference_code) => this.setState({ reference_code, referenceErr: '' })}
                                    placeholder="Enter Reference code" style={{ paddingLeft: 10 }}></TextInput>
                            </Card>

                            {!!this.state.referenceErr && (
                            <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.referenceErr}</Text>
                        )} */}
                        </View> :
                            <View>
                                {this.state.reference_enable == 1 ?
                                <View>
                                    <Text style={{ fontSize: 14, marginLeft: 15, marginTop: 20 }}>{Strings.reference_text}</Text>
                            <Card style={{ width: '90%', height: 40, borderRadius: 10, marginLeft: 20, justifyContent: 'center' }}>
                                <Text
                                    onChangeText={(reference_code) => this.setState({ reference_code, referenceErr: '' })}
                                    placeholder="Enter Reference code" style={{ paddingLeft: 10 }}>{this.state.referance_code1}</Text>
                            </Card>

                                </View>:
                                <View>
                                     <Text style={{ fontSize: 14, fontWeight: 'bold', color: Strings.color_green_code, paddingLeft: 10, marginTop: 10 }}>{Strings.Paiement_par_text}</Text>

<View style={{ width: '100%', flexDirection: 'row', marginTop: 10, marginLeft: 15 }}>
    <TouchableOpacity onPress={() => this.setState({ visa_radio: true, card_radio: false, reference_radio: false, select_cardErr: '' })} style={{ width: '8%', justifyContent: 'center', height: 30, alignItems: 'center' }}>
        <View style={{ width: 20, height: 20, borderWidth: 1, borderRadius: 20 / 2, alignItems: 'center', justifyContent: 'center', borderColor: '#F1C368' }}>
            {this.state.visa_radio === true && (<View style={{ width: 16, height: 16, borderRadius: 16 / 2, backgroundColor: '#F1C368', alignItems: 'center', justifyContent: 'center' }}>
                <Image source={require("../assets/right.png")}
                    style={{ width: 10, height: 10, }}
                    resizeMode="contain" />
            </View>)}
        </View>
    </TouchableOpacity>
    <View style={{ alignItems: 'center', marginLeft: 20, justifyContent: 'center' }}>
        <Text style={{ fontSize: 14 }}>{"paypal"}</Text>
    </View>
</View>
<View style={{ width: '100%', flexDirection: 'row', marginTop: 10, marginLeft: 15 }}>
    <TouchableOpacity onPress={() => this.setState({ visa_radio: false, card_radio: true, reference_radio: false, select_cardErr: '' }) + Actions.push("AddCart")} style={{ width: '8%', justifyContent: 'center', height: 30, alignItems: 'center' }}>
        <View style={{ width: 20, height: 20, borderWidth: 1, borderRadius: 20 / 2, alignItems: 'center', justifyContent: 'center', borderColor: '#F1C368' }}>
            {this.state.card_radio === true && (<View style={{ width: 16, height: 16, borderRadius: 16 / 2, backgroundColor: '#F1C368', alignItems: 'center', justifyContent: 'center' }}>
                <Image source={require("../assets/right.png")}
                    style={{ width: 10, height: 10, }}
                    resizeMode="contain" />
            </View>)}
        </View>
    </TouchableOpacity>
    <View style={{ alignItems: 'center', marginLeft: 20, justifyContent: 'center' }}>
        <Text style={{ fontSize: 14 }}>{Strings.other_card_text}</Text>
    </View>
</View>

                                </View>
                                }
                                
                               
                            </View>

                        }

                        {!!this.state.select_cardErr && (
                            <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.select_cardErr}</Text>
                        )}

                        {this.state.client_type == 2 && (
                            <View style={{ width: '100%', marginTop: 20 }}>
                                {this.state.client_type == 2 && (
                                    <View >
                                        <Text style={{ paddingLeft: 15, fontSize: 14, fontWeight: 'bold', color: Strings.color_green_code }}>{Strings.prenom_text}</Text>
                                        <Card style={{ marginLeft: 14, width: '95%', backgroundColor: 'white', borderRadius: 10 }}>
                                            <TextInput
                                                multiline={true}
                                                placeholder={Strings.prenom_text}
                                                onChangeText={(name) => this.setState({ name, nameErr: '' })}
                                                style={{ width: '100%', padding: 10 }}>
                                            </TextInput>
                                        </Card>
                                    </View>
                                )
                                }
                                {!!this.state.nameErr && (
                                    <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.nameErr}</Text>
                                )}

                                <View style={{ width: '100%', flexDirection: 'column', marginTop: 10 }}>
                                    <Text style={{ paddingLeft: 15, fontSize: 14, fontWeight: 'bold', color: Strings.color_green_code }}>{Strings.Addresse_text}</Text>

                                    {/* <TouchableOpacity onPress={() => this.openSearchModal()} style={{ width: '100%', marginTop: 20, borderWidth: 1, borderColor: Strings.light_color, borderRadius: 10 }}>
                                    <Text
                                        style={{ padding: 10 }}>
                                        {this.state.address}
                                    </Text>
                                </TouchableOpacity> */}


                                    <View style={{ marginLeft: 20, marginTop: 10 }}>
                                        <Text style={{ fontSize: 14, }}>{Strings.street_text}</Text>
                                        <View onPress={() => this.openSearchModal()}>
                                            <TextInput
                                                multiline={true}
                                                onChangeText={(address) => this.setState({ address, addressErr: '' })}
                                                style={{ marginTop: 10, width: '90%' }}>{this.state.address}</TextInput>
                                        </View>
                                        <View style={{ width: '98%', borderWidth: 0.5, borderColor: Strings.light_color, marginTop: 10 }}>
                                        </View>
                                    </View>
                                    {!!this.state.addressErr && (
                                        <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.addressErr}</Text>
                                    )}
                                    <View style={{ marginLeft: 20, marginTop: 20 }}>
                                        <Text style={{ fontSize: 14, }}>{Strings.zip_code_text}</Text>
                                        <TextInput
                                            onChangeText={(codePostal) => this.setState({ codePostal, codePostalErr: '' })}
                                            maxLength={6} keyboardType="decimal-pad" style={{ marginTop: -10 }}>{this.state.codePostal}</TextInput>
                                        <View style={{ width: '98%', borderWidth: 0.5, borderColor: Strings.light_color, marginTop: -10 }}>
                                        </View>
                                    </View>
                                    {!!this.state.codePostalErr && (
                                        <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.codePostalErr}</Text>
                                    )}

                                    <View style={{ marginLeft: 20, marginTop: 20 }}>
                                        <Text style={{ fontSize: 14, }}>{Strings.city_text}</Text>
                                        <TextInput
                                            onChangeText={(ville) => this.setState({ ville, villeErr: '' })}
                                            style={{ marginTop: -10 }}>{this.state.ville}</TextInput>
                                        <View style={{ width: '98%', borderWidth: 0.5, borderColor: Strings.light_color, marginTop: -10 }}>
                                        </View>
                                    </View>

                                    {!!this.state.villeErr && (
                                        <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.villeErr}</Text>
                                    )}

                                    <View style={{ marginLeft: 20, marginTop: 20 }}>
                                        <Text style={{ fontSize: 14, }}>{Strings.door_code_text}</Text>
                                        <TextInput
                                            onChangeText={(codePorte) => this.setState({ codePorte, codePorteErr: '' })}
                                            maxLength={6} keyboardType="decimal-pad" style={{ marginTop: -10 }}>
                                            {this.state.codePorte}
                                        </TextInput>
                                        <View style={{ width: '98%', borderWidth: 0.5, borderColor: Strings.light_color, marginTop: -10 }}>
                                        </View>
                                    </View>

                                    {!!this.state.codePorteErr && (
                                        <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.codePorteErr}</Text>
                                    )}
                                    {this.state.animating && (
                                        <ActivityIndicator
                                            animating={this.state.animating}
                                            style={[{ height: 10, marginBottom: 10, marginTop: 30, marginLeft: 20, marginRight: 20 }]}
                                            color="#C00"
                                            size="large"
                                            hidesWhenStopped={true}
                                        />
                                    )
                                    }
                                    <View style={{ marginLeft: 20, marginTop: 20 }}>
                                        <Text style={{ fontSize: 14, }}>{Strings.floor_level_text}</Text>
                                        <TextInput
                                            onChangeText={(etage) => this.setState({ etage, etageErr: '' })}
                                            style={{ marginTop: -10 }}>{this.state.etage}</TextInput>
                                        <View style={{ width: '98%', borderWidth: 0.5, borderColor: Strings.light_color, marginTop: -10 }}>
                                        </View>
                                    </View>

                                    {!!this.state.etageErr && (
                                        <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.etageErr}</Text>
                                    )}

                                    <View style={{ marginLeft: 20, marginTop: 20 }}>
                                        <Text style={{ fontSize: 14, }}>{Strings.other_text}</Text>
                                        <TextInput
                                            onChangeText={(autres) => this.setState({ autres, autresErr: '' })}
                                            style={{ marginTop: -10 }}>{this.state.autres}</TextInput>
                                        <View style={{ width: '98%', borderWidth: 0.5, borderColor: Strings.light_color, marginTop: -10 }}>
                                        </View>
                                    </View>
                                    {!!this.state.autresErr && (
                                        <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.autresErr}</Text>
                                    )}
                                    <View style={{ marginLeft: 20, marginTop: 20 }}>
                                        <Text style={{ fontSize: 14, }}>{Strings.telephone_text}</Text>
                                        <TextInput
                                            onChangeText={(telephone) => this.setState({ telephone, telephoneErr: '' })}
                                            keyboardType="decimal-pad"
                                            style={{ marginTop: -10 }}>{this.state.telephone}</TextInput>
                                        <View style={{ width: '98%', borderWidth: 0.5, borderColor: Strings.light_color, marginTop: -10 }}>
                                        </View>
                                    </View>

                                    {!!this.state.telephoneErr && (
                                        <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.telephoneErr}</Text>
                                    )}
                                </View>
                            </View>
                        )
                        }

                        {this.state.client_type == 3 && (
                            <View style={{ width: '100%', marginTop: 20 }}>
                                <View style={{ width: '100%', flexDirection: 'column', marginTop: 10 }}>
                                    <Text style={{ paddingLeft: 15, fontSize: 14, fontWeight: 'bold', color: Strings.color_green_code }}>{Strings.Addresse_text}</Text>
                                    <View style={{ marginLeft: 20, marginTop: 10 }}>
                                        <Text style={{ fontSize: 14, }}>{Strings.street_text}</Text>
                                        <TouchableOpacity onPress={() => this.openSearchModal()}>
                                            <Text style={{ marginTop: 10, width: '90%' }}>{this.state.address}</Text>
                                        </TouchableOpacity>
                                        <View style={{ width: '98%', borderWidth: 0.5, borderColor: Strings.light_color, marginTop: 10 }}>
                                        </View>
                                    </View>
                                    {!!this.state.addressErr && (
                                        <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.addressErr}</Text>
                                    )}
                                    <View style={{ marginLeft: 20, marginTop: 20 }}>
                                        <Text style={{ fontSize: 14, }}>{Strings.zip_code_text}</Text>
                                        <TextInput
                                            onChangeText={(codePostal) => this.setState({ codePostal, codePostalErr: '' })}
                                            maxLength={6} keyboardType="decimal-pad" style={{ marginTop: -10 }}>{this.state.codePostal}</TextInput>
                                        <View style={{ width: '98%', borderWidth: 0.5, borderColor: Strings.light_color, marginTop: -10 }}>
                                        </View>
                                    </View>
                                    {!!this.state.codePostalErr && (
                                        <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.codePostalErr}</Text>
                                    )}
                                    <View style={{ marginLeft: 20, marginTop: 20 }}>
                                        <Text style={{ fontSize: 14, }}>{Strings.city_text}</Text>
                                        <TextInput
                                            onChangeText={(ville) => this.setState({ ville, villeErr: '' })}
                                            style={{ marginTop: -10 }}>{this.state.ville}</TextInput>
                                        <View style={{ width: '98%', borderWidth: 0.5, borderColor: Strings.light_color, marginTop: -10 }}>
                                        </View>
                                    </View>
                                    {!!this.state.villeErr && (
                                        <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.villeErr}</Text>
                                    )}
                                    <View style={{ marginLeft: 20, marginTop: 20 }}>
                                        <Text style={{ fontSize: 14, }}>{Strings.door_code_text}</Text>
                                        <TextInput
                                            onChangeText={(codePorte) => this.setState({ codePorte, codePorteErr: '' })}
                                            maxLength={6} keyboardType="decimal-pad" style={{ marginTop: -10 }}>
                                            {this.state.codePorte}
                                        </TextInput>
                                        <View style={{ width: '98%', borderWidth: 0.5, borderColor: Strings.light_color, marginTop: -10 }}>
                                        </View>
                                    </View>
                                    {!!this.state.codePorteErr && (
                                        <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.codePorteErr}</Text>
                                    )}
                                    {this.state.animating && (
                                        <ActivityIndicator
                                            animating={this.state.animating}
                                            style={[{ height: 10, marginBottom: 10, marginTop: 30, marginLeft: 20, marginRight: 20 }]}
                                            color="#C00"
                                            size="large"
                                            hidesWhenStopped={true}
                                        />
                                    )
                                    }
                                    <View style={{ marginLeft: 20, marginTop: 20 }}>
                                        <Text style={{ fontSize: 14, }}>{Strings.floor_level_text}</Text>
                                        <TextInput
                                            onChangeText={(etage) => this.setState({ etage, etageErr: '' })}
                                            style={{ marginTop: -10 }}>{this.state.etage}</TextInput>
                                        <View style={{ width: '98%', borderWidth: 0.5, borderColor: Strings.light_color, marginTop: -10 }}>
                                        </View>
                                    </View>
                                    {!!this.state.etageErr && (
                                        <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.etageErr}</Text>
                                    )}

                                    <View style={{ marginLeft: 20, marginTop: 20 }}>
                                        <Text style={{ fontSize: 14, }}>{Strings.other_text}</Text>
                                        <TextInput
                                            onChangeText={(autres) => this.setState({ autres, autresErr: '' })}
                                            style={{ marginTop: -10 }}>{this.state.autres}</TextInput>
                                        <View style={{ width: '98%', borderWidth: 0.5, borderColor: Strings.light_color, marginTop: -10 }}>
                                        </View>
                                    </View>
                                    {!!this.state.autresErr && (
                                        <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.autresErr}</Text>
                                    )}
                                    <View style={{ marginLeft: 20, marginTop: 20 }}>
                                        <Text style={{ fontSize: 14, }}>{Strings.telephone_text}</Text>
                                        <TextInput
                                            onChangeText={(telephone) => this.setState({ telephone, telephoneErr: '' })}
                                            keyboardType="decimal-pad"
                                            style={{ marginTop: -10 }}>{this.state.telephone}</TextInput>
                                        <View style={{ width: '98%', borderWidth: 0.5, borderColor: Strings.light_color, marginTop: -10 }}>
                                        </View>
                                    </View>

                                    {!!this.state.telephoneErr && (
                                        <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.telephoneErr}</Text>
                                    )}
                                </View>
                            </View>

                        )
                        }

                        <View style={{ width: '100%', alignItems: 'center', marginTop: 30, marginBottom: 30, flexDirection: 'row' }}>
                            <View style={{ width: '50%' }}>
                                <TouchableOpacity onPress={() => Actions.pop()} style={{ width: '90%', height: 40, borderRadius: 10, borderColor: '#F1C368', borderWidth: 1, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ fontSize: 16, color: Strings.color_green_code }}>{Strings.cancel_text}</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ width: '50%' }}>
                                <TouchableOpacity onPress={() => this.validation()} style={{ width: '100%', height: 40, borderRadius: 10, backgroundColor: '#F1C368', alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ fontSize: 16, color: 'white' }}>{Strings.next_text}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <PopupDialog
                            onHardwareBackPress={() => { this.setState({ lodingDialog: false }) }}
                            width={0.3}
                            visible={this.state.lodingDialog}
                            dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
                            <DialogContent>
                                <View style={{ alignItems: 'center', }}>
                                    <ActivityIndicator
                                        animating={this.state.animating}
                                        style={[{ height: 10, marginBottom: 10, marginTop: 30, marginLeft: 20, marginRight: 20 }]}
                                        color="#C00"
                                        size="large"
                                        hidesWhenStopped={true}
                                    />
                                </View>
                            </DialogContent>
                        </PopupDialog>

                        <AmountDailog
                            ref={ref => { this.AmountDailog = ref; }}
                            height={500}
                            duration={200}
                            closeOnPressMask={false}
                            customStyles={{
                                container: {
                                    alignItems: "center",
                                    borderRadius: 10,
                                    // backgroundColor: 'rgba(52, 52, 52, 0.8)'  
                                    backgroundColor: 'transparent'
                                }
                            }}>
                            <View style={{ flexDirection: 'column', width: '90%', height: '100%', borderRadius: 10, alignItems: 'center', justifyContent: 'center' }}>
                                <Image 
                                source={require("../assets/Box(1).png")}
                                    style={{ width: 360, height: 360, borderRadius: 50, alignItems: 'center' }}
                                    resizeMode='stretch'>
                                    <View style={{ width: '95%', height: 360, borderRadius: 10, alignItems: 'center' }}>
                                        <Text style={{ fontSize: 18, fontWeight: 'bold', marginTop: 10, padding: 10, textAlign: 'center' }}>{Strings.forfait_text}</Text>
                                        <View style={{ width: '95%', flexDirection: 'row', marginTop: 15 }}>
                                            <View style={{ width: '100%', alignItems: 'center', marginTop: 20 }}>
                                                {/* + this.StartRideRBSheet.open() */}

                                                <Text style={{ fontWeight: 'bold', fontSize: 16, color: Strings.color_green_code }}>{this.state.total_show} €</Text>

                                                <Text style={{ marginTop: 20, textAlign: 'center', paddingLeft: 10, paddingRight: 10, fontSize: 14 }}>{Strings.deplacement_text}</Text>
                                                <TouchableOpacity onPress={() => this.AmountDailog.close() + Actions.push("PaypalPayment", { req_id: this.state.req_id, total_amount: this.state.Total_amount, current_cat_id: this.props.current_cat_id })} style={{ width: '50%', height: 40, backgroundColor: Strings.color_green_code, borderRadius: 10, marginTop: 20, alignItems: 'center', justifyContent: 'center' }}>
                                                    <Text style={{ fontSize: 16, color: 'white' }}>{Strings.ok_text}</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>
                                </Image>
                            </View>
                        </AmountDailog>
                    </View>
                </ScrollView>
            </View>
        );
    }
}
