import React from 'react';
import { StyleSheet, RefreshControl, StatusBar, Alert, DrawerLayoutAndroid, ActivityIndicator, ScrollView, Modal, FlatList, ImageBackground, TextInput, View, Text, Dimensions, TouchableOpacity, Linking, AsyncStorage } from 'react-native';
import Strings from '../strings/strings'
import { Actions } from 'react-native-router-flux';
import { Card } from 'native-base'

import PopupDialog, {
    DialogTitle, DialogContent, DialogFooter, DialogButton, SlideAnimation, ScaleAnimation,
} from 'react-native-popup-dialog';
import Sidebar from '../screens/sideBar'
const { width, height } = Dimensions.get("window");
let data_fuel_amount = [];
import Drawer from 'react-native-drawer'
import Image from 'react-native-fast-image'

export default class FuelList_info extends React.Component {
    constructor(props, context) {
        super(props);
        this.state = {
            data: '',
            data2: '',
            checked1: false,
            checked2: false,
            zone_name: '',
            user_id: '',
            token: '',
            subzone_id: '',
            orderid: '',
            fuel_type: '1',
            fuel_value: '1',
            fuel_quant: '1',
            selection_boat_place: '',
            modalVisible: '',
            boat_id: '',
            animating: false,
            lodingDialog: false,
            refreshing: true,

        };
        data_fuel_amount = []

    }
    setDrawer = () => {
        console.log("open ", "open ")
        this._drawer.open()
    }
    componentDidMount() {
      this._drawer.close()
        AsyncStorage.getItem("userid")
            .then(userid => {
                this.setState({ user_id: userid });



                AsyncStorage.getItem("token")
                    .then(token => {
                        this.setState({ token: accessToken });



                    })
                AsyncStorage.getItem("zonename")
                    .then(zonename => {
                        this.setState({ zone_name: zonename });



                    })

                AsyncStorage.getItem("selection_boat_place")
                    .then(selection_boat_place => {
                        this.setState({ selection_boat_place: selection_boat_place });
                    })
                AsyncStorage.getItem("suboneid")
                    .then(suboneid => {
                        this.setState({ subzone_id: suboneid });



                    })
                AsyncStorage.getItem("orderid")
                    .then(orderid => {
                        this.setState({ orderid: orderid });



                    })
            })



        this.getFuelList()



    }
    // componentWillMount()
    // {
    //     this.getFuelList()

    // }

    createOrder4 = () => {

        console.log("Selected Zone ", this.state.orderid)
        console.log("Selected Zone fuel_type ", this.state.fuel_type)
        console.log("Selected Zone fuel_value ", this.state.fuel_value)
        console.log("Selected Zone user_id ", this.state.user_id)
        console.log("Selected Zone token ", this.state.token)
        console.log("Selected Zone Selected Boat Position ", this.state.selection_boat_place)



        this.setState({
            animating: true,
            lodingDialog: true,

        });
        fetch(Strings.base_Url + "createOrderStep4", {
            method: 'POST',
            headers: {
                // 'Accept': 'application/json',
                'Content-Type': 'application/json',
                'User-Id': this.state.user_id,
                'token': this.state.token,
            },
            body: JSON.stringify({
                order_id: this.state.orderid,
                fuel_type: this.state.fuel_type,
                fuel_value: this.state.fuel_quant,

            })
        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(" responseData createorder4 resoJSon===" + JSON.stringify(responseData));
                console.log("resoJSon createorder4===" + responseData.error);
                if (responseData.error == "false") {
                    console.log("Error false createorder4 massage" + JSON.stringify(responseData.errorMessage));

                    this.setState({
                        animating: false,
                        lodingDialog: false,

                    });
                    //Put Condition
                    if (this.state.selection_boat_place == 0) {
                        console.log("Inside selection_boat_place Zero ", this.state.selection_boat_place)
                        Actions.push('EditBoat1')

                    }
                    else if (this.state.selection_boat_place == 1) {
                        console.log("Inside selection_boat_place one ", this.state.selection_boat_place)
                        Actions.push('EditBoat2')


                    }
                    // Actions.push('Editinforfororder')

                    // this.getBoatinfo();
                } else if (responseData.error == "true") {

                    alert(responseData.errorMessage)
                    this.setState({
                        animating: false,
                        lodingDialog: false

                    });
                }
            }
            )
    }

    checkforContinue = (boat_id) => {
        this.setState({
            boat_id: boat_id,
            modalVisible: false,
        })
        if (this.state.boat_id != '') {
            this.createOrder3();
        }
        else {
            Actions.push('AddBoat_forOrder', { fuel_value: this.state.fuel_quant })

        }


    }
    createOrder3 = () => {
        console.log("Selected Zone ", this.state.orderid)
        console.log("Selected Zone fuel_type ", this.state.fuel_type)
        console.log("Selected Zone fuel_value ", this.state.fuel_quant)


        this.setState({
            animating: true,
            lodingDialog: true,

        }); fetch(Strings.base_Url + "createOrderStep3", {
            method: 'POST',
            headers: {
                // 'Accept': 'application/json',
                'Content-Type': 'application/json',
                'User-Id': this.state.user_id,
                'token': this.state.token,
            },
            body: JSON.stringify({
                order_id: this.state.orderid,
                boat_id: this.state.boat_id,

            })
        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(" responseData addAddress resoJSon===" + JSON.stringify(responseData));
                console.log("resoJSon===" + responseData.error);
                if (responseData.error == "false") {
                    console.log("Error false massage" + JSON.stringify(responseData.errorMessage));

                    this.setState({
                        animating: false,
                        lodingDialog: false,

                    });


                    this.createOrder4();
                } else if (responseData.error == "true") {

                    alert(responseData.errorMessage)
                    this.setState({
                        animating: false,
                        lodingDialog: false

                    });
                }
            }
            )
    }
    getBoatinfo = () => {
        if (this.state.checked1 == false || this.state.checked2 == false) {
            alert('Convenez d\'abord des termes et conditions')
        }
        else {
            console.log("Selected Zone ", this.state.orderid)
            this.setState({
                animating: true,
                lodingDialog: true,

            });

            fetch(Strings.base_Url + "getBoatInfo", {
                method: 'POST',
                headers: {
                    // 'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'User-Id': this.state.user_id,
                    'token': this.state.token,
                },
                body: JSON.stringify({
                    user_id: this.state.user_id


                })
            })


                .then((response) => response.json())
                .then((responseData) => {
                    console.log(" responseData getBoatInfo resoJSon===" + JSON.stringify(responseData));
                    console.log("resoJSon getBoatInfo===" + responseData.error);
                    if (responseData.error == "false") {
                        console.log("Error false massage getBoatInfo" + JSON.stringify(responseData.record));

                        this.setState({
                            animating: false,
                            lodingDialog: false,
                            data2: responseData.boat_record,
                            boat_id: responseData.boat_record[0].id,

                            modalVisible: true,

                        });




                    } else if (responseData.error == "true") {

                        //  alert(responseData.errorMessage)
                        this.setState({
                            animating: false,
                            lodingDialog: false,
                            modalVisible: true,

                        });
                    }
                }
                )
        }

    }

    // getFuelList = () => {


    //     AsyncStorage.getItem("userid")
    //         .then(userid => {
    //             this.setState({ user_id: userid });

    //             AsyncStorage.getItem("token")
    //                 .then(token => {
    //                     var accessToken = JSON.parse(token);
    //                     this.setState({ token: accessToken });
    //                     this.setState({
    //                         animating: true,
    //                         lodingDialog: true,

    //                     });

    //                     fetch(Strings.base_Url + "getFuelrates", {
    //                         method: 'GET',
    //                         headers: {
    //                             'Content-Type': 'application/json',
    //                             'User-Id': this.state.user_id,
    //                             'token': this.state.token,
    //                         },

    //                     })
    //                         .then((response) => response.json())
    //                         .then((responseData) => {
    //                             console.log(" responseData getFuelList resoJSon===" + JSON.stringify(responseData));
    //                             console.log("resoJSon getFuelList===" + responseData.error);
    //                             if (responseData.error == "false") {
    //                                 console.log("Error false massage getFuelList" + JSON.stringify(responseData.record));


    //                                 console.log("Fuel Parse ", JSON.parse(responseData.record[0].amount))
    //                                 data_fuel_amount = []


    //                                 var one_litre = JSON.parse(responseData.record[0].amount)
    //                                 console.log("Fuel ", one_litre)
    //                                 var two_litre = 2 * one_litre
    //                                 console.log("Fuel Two ", two_litre)
    //                                 //New Added
    //                                 var three_litre = 3 * one_litre
    //                                 console.log("Fuel Three ", three_litre)
    //                                 var fourth_litre = 4 * one_litre
    //                                 console.log("Fuel Fourth ", fourth_litre)
    //                                 var fifth_litre = 5 * one_litre
    //                                 console.log("Fuel Fifth ", fifth_litre)
    //                                 var sixth_litre = 6 * one_litre
    //                                 console.log("Fuel Sixth ", sixth_litre)
    //                                 var seventh_litre = 7 * one_litre
    //                                 console.log("Fuel Seventh ", seventh_litre)
    //                                 var eighthts_litre = 8 * one_litre
    //                                 console.log("Fuel Eight ", eighthts_litre)

    //                                 var ningth_litre = 9 * one_litre
    //                                 console.log("Fuel Nineth ", ningth_litre)
    //                                 var tenth_litre = 10 * one_litre
    //                                 console.log("Fuel Tenth ", tenth_litre)

    //                                 data_fuel_amount.push({
    //                                     'fuel_amount': one_litre,
    //                                     'fuel_ltr': '€/L',
    //                                     'fuel_quant': 1,

    //                                 },
    //                                     {
    //                                         'fuel_amount': two_litre,
    //                                         'fuel_ltr': '€/2L',  
    //                                         'fuel_quant': 2,

    //                                     },
    //                                     {
    //                                         'fuel_amount': three_litre,
    //                                         'fuel_ltr': '€/3L',
    //                                         'fuel_quant': 3,

    //                                     },
    //                                     {
    //                                         'fuel_amount': fourth_litre,
    //                                         'fuel_ltr': '€/4L',
    //                                         'fuel_quant': 4,

    //                                     },
    //                                     {
    //                                         'fuel_amount': fifth_litre,
    //                                         'fuel_ltr': '€/5L',
    //                                         'fuel_quant': 5,

    //                                     },
    //                                     {
    //                                         'fuel_amount': sixth_litre,
    //                                         'fuel_ltr': '€/6L',
    //                                         'fuel_quant': 6,

    //                                     },
    //                                     {
    //                                         'fuel_amount': seventh_litre,
    //                                         'fuel_ltr': '€/7L',
    //                                         'fuel_quant': 7,

    //                                     },
    //                                     {
    //                                         'fuel_amount': eighthts_litre,
    //                                         'fuel_ltr': '€/8L',
    //                                         'fuel_quant': 8,

    //                                     },
    //                                     {
    //                                         'fuel_amount': ningth_litre,
    //                                         'fuel_ltr': '€/9L',
    //                                         'fuel_quant': 9,

    //                                     },
    //                                     {
    //                                         'fuel_amount': tenth_litre,
    //                                         'fuel_ltr': '€/10L',
    //                                         'fuel_quant': 10,

    //                                     },

    //                                 )
    //                                 console.log("Fuel total ", data_fuel_amount)




    //                                 this.setState({
    //                                     animating: false,
    //                                     lodingDialog: false,
    //                                     fuel_amount: data_fuel_amount[0].fuel_amount,
    //                                     fuel_type: responseData.record[0].id,
    //                                     data: data_fuel_amount,


    //                                 });

    //                                 //  this.createOrder4()

    //                                 //this.RBSheet.close()
    //                             } else if (responseData.error == "true") {
    //                                 data_fuel_amount = []


    //                                 this.showalerts(responseData.errorMessage)
    //                                 this.setState({
    //                                     animating: false,
    //                                     lodingDialog: false

    //                                 });
    //                             }
    //                         }
    //                         )
    //                 })
    //         })
    // }

    getFuelList = () => {


        AsyncStorage.getItem("userid")
            .then(userid => {
                this.setState({ user_id: userid });

                AsyncStorage.getItem("token")
                    .then(token => {
                        var accessToken = JSON.parse(token);
                        this.setState({ token: accessToken });
                        this.setState({
                            animating: true,
                            lodingDialog: true,

                        });

                        fetch(Strings.base_Url + "getfuelPrice", {
                            method: 'GET',
                            headers: {
                                'Content-Type': 'application/json',
                                'User-Id': this.state.user_id,
                                'token': this.state.token,
                            },

                        })
                            .then((response) => response.json())
                            .then((responseData) => {
                                console.log(" responseData getFuelList resoJSon===" + JSON.stringify(responseData));
                                console.log("resoJSon getFuelList===" + responseData.error);
                                if (responseData.error == "false") {
                                    console.log("Error false massage getFuelList" + JSON.stringify(responseData.record));


                                  //  console.log("Fuel Parse ", JSON.parse(responseData.record[0].amount))
                                    data_fuel_amount = []





                                    this.setState({
                                        animating: false,
                                        lodingDialog: false,
                                        refreshing: false,


                                        fuel_value: responseData.record[0].amount,
                                        fuel_type: '1',
                                        fuel_amount: responseData.record[0].amount,
                                        data: responseData.record,


                                    });

                                    //  this.createOrder4()

                                    //this.RBSheet.close()
                                } else if (responseData.error == "true") {
                                    data_fuel_amount = []


                                    this.showalerts(responseData.errorMessage)
                                    this.setState({
                                        animating: false,
                                        lodingDialog: false,
                                        refreshing: false,

                                        data: [],

                                    });
                                }
                            }
                            )
                    })
            })
    }
    handleScroll = (event) => {
        let yOffset = event.nativeEvent.contentOffset.y
        let contentHeight = event.nativeEvent.contentSize.height
        let value = yOffset / contentHeight
    }
    addBoat() {
        this.setState({ modalVisible: false })
        //   Actions.push('AddBoat')
        console.log("Fuel Quantity ", this.state.fuel_quant)
        Actions.push('AddBoat_forOrder', { fuel_value: this.state.fuel_quant })

    }

    calculate_Fuel_amunt = (ful_amunt, fuel_quant) => {
        console.log("Fuel amount", ful_amunt)
        console.log("Fuel quantitiy", fuel_quant)


        this.setState({
            fuel_value: ful_amunt,
            fuel_quant: fuel_quant,
        })

        console.log("Fuel Value After ", this.state.fuel_value)


    }
    scrollToIndex = () => {
        console.log("Scroll Index ", "Scroll Index ")

        let randomIndex = Math.floor(Math.random(Date.now()) * this.state.data.length);
        this.myFlatList.scrollToIndex({ animated: true, index: randomIndex });
        console.log("Scroll Index ", randomIndex)

    }

    scrollToItem = () => {
        console.log("Scroll Item ", "Scroll Item ")

        let randomIndex = Math.floor(Math.random(Date.now()) * this.state.data.length);
        this.myFlatList.scrollToIndex({ animated: true, index: "" + randomIndex });
        console.log("Scroll Item ", randomIndex)
    }
    onRefresh() {
        //Clear old data of the list
        this.setState({ 
            refreshing: true,

            data: [] });
        //Call the Service to get the latest data
        this.getFuelList();
    }
    render() {

     

        return (
            <Drawer
                openDrawerOffset={100}
                type="overlay"
                ref={(ref) => this._drawer = ref}
                content={<Sidebar />}
            >
                <View style={{ flex: 1 }}>

                    <Card style={{ width: '100%',marginTop: Platform.OS == 'ios' ? 20 :-5, alignItems: 'center', height: 50, flexDirection: 'row', justifyContent: 'space-between' }}>
                        <TouchableOpacity
                            //  onPress={this._setDrawer.bind(this)} 
                            onPress={() => this.setDrawer()}

                            style={{
                                height: 50,
                                justifyContent: 'center', marginLeft: -4
                            }}>
                            <Image source={require("../assets/menu_img.png")}
                                style={{ width: 40, height: 40, }}
                                resizeMode="contain" />
                        </TouchableOpacity>
                        <View style={{ height: 50, justifyContent: 'center', alignItems: 'center', }}>
                            <Text style={{ fontFamily: "Poppins-SemiBold", fontSize: 16, }}>FYB</Text>
                        </View>
                        <View style={{ width: 40, height: 20, marginRight: 8 }} />

                    </Card>


                    <View style={{}}>

                        <View style={{ width: 270, height: 38, marginBottom: 10, marginTop: 35, alignSelf: 'center', alignItems: 'center', justifyContent: 'center', borderRadius: 20, borderWidth: 1, borderColor: '#28528F' }}>
                            <Text style={{ color: '#28528F', fontSize: 12, textAlign: 'center', fontFamily: 'Poppins-Regular' }}>Je souhaite mettre l'équivalent de</Text>
                        </View>
                        {this.state.data.length > 0 ?

                            <FlatList
                                ref={flatList1 => {
                                    this.flatList1 = flatList1
                                }}
                                keyExtractor={item => item.id}
                                data={this.state.data}
                                style={{ height: 150, }}
                                refreshControl={
                                    <RefreshControl
                                    refreshing={this.state.refreshing}
                                    onRefresh={this.componentDidMount.bind(this)}
                                    />
                                    }
                                // initialScrollIndex={1}
                                // initialNumToRender={2}         
                                renderItem={({ item, index }) => (

                                    <ScrollView>
                                        <TouchableOpacity

                                            style={{
                                                justifyContent: 'center', alignItems: 'center', marginTop: 10,
                                                flexDirection: 'row'
                                            }}
                                            onPress={() => this.calculate_Fuel_amunt(item.amount, item.number)}
                                        >

                                            {this.state.fuel_value == item.amount ?
                                                <View style={{ flexDirection: 'row' }}>


                                                    <Text style={{ fontFamily: "Poppins-Bold", color: '#ff7f50', fontSize: 13.5 }}>{item.amount}</Text>
                                                    <Text style={{ fontFamily: "Poppins-Bold", color: '#ff7f50', fontSize: 13.5 }}>€/{item.number}L</Text>

                                                </View>
                                                :
                                                <View style={{ flexDirection: 'row' }}>

                                                    <Text style={{ fontFamily: "Poppins-Medium", color: 'grey', fontSize: 12 }}>{item.amount}</Text>
                                                    <Text style={{ fontFamily: "Poppins-Medium", color: 'grey', fontSize: 12 }}>€/{item.number}L</Text>

                                                </View>}




                                        </TouchableOpacity>
                                    </ScrollView>
                                )}
                            


                            />
                            :
                            <Text style={{ fontFamily: "Poppins-Medium", marginTop: 10, color: 'white' }}>Aucune donnée</Text>

                        }

                        {this.state.checked1 == true ?
                            <TouchableOpacity onPress={() => this.setState({ checked1: false })}
                                style={{ flexDirection: 'row', width: width * 85 / 100, alignSelf: 'center', marginTop: 40 }}>
                                <Image source={require('../assets/4.png')} style={{ width: 15, height: 15, marginTop: 4 }} />
                                <Text style={{ color: '#28528F', fontSize: 10, marginLeft: 6, width: width * 85 / 100 - 30, fontFamily: 'Poppins-Regular' }}>Je confirme donner l'accès à Fill Your Boat pour
                                réaliser le plein de mon bateau et même en
mon absence.</Text>
                            </TouchableOpacity> :
                            <TouchableOpacity onPress={() => this.setState({ checked1: true })}
                                style={{ flexDirection: 'row', width: width * 85 / 100, alignSelf: 'center', marginTop: 40 }}>
                                <View style={{ width: 15, height: 15, borderRadius: 3, borderColor: 'grey', borderWidth: 1, marginTop: 4 }} />
                                <Text style={{ color: '#28528F', fontSize: 10, marginLeft: 6, width: width * 85 / 100 - 30, fontFamily: 'Poppins-Regular' }}>Je confirme donner l'accès à Fill Your Boat pour
                                réaliser le plein de mon bateau et même en
                       mon absence.</Text>
                            </TouchableOpacity>}

                        {this.state.checked2 == true ?
                            <TouchableOpacity onPress={() => this.setState({ checked2: false })}
                                style={{ flexDirection: 'row', width: width * 85 / 100, alignSelf: 'center', marginTop: 10 }}>
                                <Image source={require('../assets/4.png')} style={{ width: 15, height: 15, marginTop: 4 }} />
                                <Text style={{ color: '#28528F', fontSize: 10, marginLeft: 6, width: width * 85 / 100 - 30, fontFamily: 'Poppins-Regular' }}>Je confirme que la trappe à essence sur mon bateau est bien accessible pour les marins-pompistes de Fill Your Boat.</Text>
                            </TouchableOpacity> :
                            <TouchableOpacity onPress={() => this.setState({ checked2: true })}
                                style={{ flexDirection: 'row', width: width * 85 / 100, alignSelf: 'center', marginTop: 10 }}>
                                <View style={{ width: 15, height: 15, borderRadius: 3, borderColor: 'grey', borderWidth: 1, marginTop: 4 }} />
                                <Text style={{ color: '#28528F', fontSize: 10, marginLeft: 6, width: width * 85 / 100 - 30, fontFamily: 'Poppins-Regular' }}>Je confirme que la trappe à essence sur mon bateau est bien accessible pour les marins-pompistes de Fill Your Boat.</Text>
                            </TouchableOpacity>}


                            
                        {this.state.checked1 == true && this.state.checked2 == true ?
                        <TouchableOpacity
                            onPress={() => this.getBoatinfo()}
                            style={{ width: 210, backgroundColor: 'rgba(26,87,138,1)', marginTop: height * 5 / 100, alignSelf: 'center', height: 40, borderRadius: 20, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: 14, fontFamily: 'Poppins-SemiBold', color: 'white', }}>Étape suivante</Text>
                        </TouchableOpacity> :
                        <TouchableOpacity
                        onPress={() => this.getBoatinfo()}
                        style={{ width: 210, backgroundColor: '#e3f5fc', marginTop: height * 5 / 100, alignSelf: 'center', height: 40, borderRadius: 20, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontSize: 14, fontFamily: 'Poppins-SemiBold', color: 'white', }}>Étape suivante</Text>
                    </TouchableOpacity>
    }


                        <Modal
                            animationType={'fade'}
                            transparent={true}
                            visible={this.state.modalVisible}
                            onRequestClose={() => {
                                this.setState({ modalVisible: false });
                            }}
                        >

                            <View
                                style={{
                                    position: 'absolute',
                                    width: width,
                                    height: '100%',
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    backgroundColor: 'rgba(0,0,0,0.5)',
                                    padding: 20,
                                }}
                            >
                                <Image
                                    resizeMode='stretch'
                                    source={require('../assets/bg_image.png')}
                                    style={{ width: width * 85 / 100, padding: 15, alignSelf: 'center', justifyContent: 'center', alignItems: 'center' }}>
                                    <Image source={require("../assets/12.png")}
                                        style={{ width: 100, height: 100, borderRadius: 50, }}
                                    />
                                    <Text style={{ fontSize: 15, fontFamily: 'Poppins-SemiBold', textAlign: 'center', marginTop: 2 }}>Sélectionnez le bateau à</Text>
                                    <Text style={{ fontSize: 15, fontFamily: 'Poppins-SemiBold', textAlign: 'center', marginTop: -3 }}>enregistrer</Text>
                                    <View style={{ width: width * 76 / 100, alignSelf: 'center', minHeight: 50, maxHeight: 86 }}>
                                        {this.state.data2.length > 0 ?

                                            <FlatList
                                                keyExtractor={item => item.id}
                                                data={this.state.data2}
                                                renderItem={({ item }) => (
                                                    <ScrollView>
                                                        <TouchableOpacity
                                                            style={{
                                                                alignItems: 'center', marginTop: 10,
                                                                flexDirection: 'row'
                                                            }}
                                                            onPress={() => this.checkforContinue(item.id)}>
                                                            <Image source={require('../assets/81.png')} style={{ width: 15, height: 15, marginRight: 6 }} />

                                                            <Text style={{ fontFamily: 'Poppins-Regular', fontSize: 12 }}>{item.ship_name} - </Text>
                                                            <Text style={{ fontFamily: 'Poppins-Regular', fontSize: 12 }}>{item.brand} </Text>


                                                        </TouchableOpacity>
                                                    </ScrollView>
                                                )}
                                            />
                                            :
                                            <Text style={{ fontFamily: "Poppins-Regular", marginTop: 10, color: 'white' }}>Aucune donnée</Text>

                                        }
                                    </View>
                                    <TouchableOpacity
                                        onPress={() => this.addBoat()} style={{ width: width * 76 / 100, marginTop: 6, alignSelf: 'center', flexDirection: 'row' }}
                                    >
                                        <Image source={require('../assets/81.png')} style={{ width: 15, height: 15, marginRight: 6 }} />
                                        <Text style={{
                                            fontFamily: 'Poppins-Regular', fontSize: 12,
                                        }}>Ajouter un bateau </Text>

                                    </TouchableOpacity>


                                    <TouchableOpacity
                                        // onPress={() => this.checkforContinue()}
                                        style={{
                                            height: 40, width: 170,
                                            justifyContent: 'center', alignItems: 'center',
                                            backgroundColor: 'rgba(26,87,138,1)', marginBottom: 10, borderRadius: 20,
                                        }}>

                                        <Text style={{ fontSize: 14, fontFamily: 'Poppins-SemiBold', color: 'white', }}>Continuer </Text>
                                    </TouchableOpacity>


                                </Image>
                            </View>
                        </Modal>
                        <PopupDialog
                            onHardwareBackPress={() => { this.setState({ lodingDialog: false }) }}
                            width={0.3}
                            visible={this.state.lodingDialog}
                            dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
                            <DialogContent>
                                <View style={{ alignItems: 'center', }}>
                                    <ActivityIndicator
                                        animating={this.state.animating}
                                        style={[{ height: 10, marginBottom: 10, marginTop: 30, marginLeft: 20, marginRight: 20 }]}
                                        color="#C00"
                                        size="large"
                                        hidesWhenStopped={true}
                                    />
                                </View>
                            </DialogContent>
                        </PopupDialog>

                    </View>






                </View >
        </Drawer>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    image: {
        width: 413,
        height: 454,
        flexDirection: "row",
        marginTop: 29,
        alignSelf: "center"
    },
    image_imageStyle: {},
    rect: {
        width: 25,
        height: 25,
        backgroundColor: "rgba(26,87,138,1)",
        borderWidth: 0,
        borderColor: "#000000",
        borderRadius: 5
    },
    disponibleSur: {
        fontFamily: "poppins-700",
        color: "#121212",
        fontSize: 12,
        textAlign: "left",
        marginLeft: 9,
        marginTop: 4
    },
    rect2: {
        width: 25,
        height: 25,
        backgroundColor: "rgba(255,170,0,1)",
        borderWidth: 0,
        borderColor: "#000000",
        borderRadius: 5,
        marginLeft: 46
    },
    selectionne: {
        fontFamily: "poppins-700",
        color: "#121212",
        fontSize: 12,
        textAlign: "left",
        marginLeft: 13,
        marginTop: 4
    },
    rectRow: {
        height: 25,
        flexDirection: "row",
        flex: 1,
        marginRight: 78,
        marginLeft: 57,
        marginTop: 429
    },
    disponibleSur2: {
        fontFamily: "poppins-regular",
        color: "#121212",
        fontSize: 12,
        textAlign: "left",
        marginTop: 37,
        marginLeft: 38
    },
    materialButtonViolet: {
        height: 36,
        width: 79,
        backgroundColor: "rgba(26,87,138,1)",
        borderRadius: 12
    },
    materialButtonViolet2: {
        height: 36,
        width: 79,
        backgroundColor: "rgba(26,87,138,1)",
        borderRadius: 12,
        marginLeft: 23
    },
    materialButtonViolet3: {
        height: 36,
        width: 79,
        backgroundColor: "rgba(255,159,28,1)",
        borderRadius: 12,
        marginLeft: 23
    },
    materialButtonVioletRow: {
        height: 36,
        flexDirection: "row",
        marginTop: 9,
        marginLeft: 37,
        marginRight: 55
    },
    text: {
        fontFamily: "poppins-regular",
        color: "#121212",
        fontSize: 12,
        textAlign: "left",
        marginTop: 46,
        marginLeft: 37
    },
    text2: {
        fontFamily: "poppins-700",
        color: "rgba(255,159,28,1)",
        fontSize: 12,
        textAlign: "left",
        marginTop: 11,
        marginLeft: 37
    },
    materialButtonLight1: {
        height: 36,
        width: 254,
        borderRadius: 25,
        backgroundColor: "rgba(26,87,138,1)",
        marginTop: 32,
        alignSelf: "center"
    }
});

