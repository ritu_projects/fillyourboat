import React from 'react';
import { StatusBar,Alert, RefreshControl, Button, ActivityIndicator, FlatList, ScrollView, PixelRatio, TextInput, View, ImageBackground, Text, Dimensions, TouchableOpacity, Linking, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from "../styles/styles";
import Strings from '../strings/strings'
import { Card } from 'native-base';
import { Rating, AirbnbRating } from 'react-native-elements';

import Loading from 'react-native-whc-loading';
import PopupDialog, {
    DialogTitle, DialogContent, DialogFooter, DialogButton, SlideAnimation, ScaleAnimation,
} from 'react-native-popup-dialog';
import Image from 'react-native-fast-image'


export default class expertsList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            request_id:'',
            tech_id:'',
            current_cat_id:'',
            refreshing:false,

        };
        console.log("current_cat_id:::" + this.props.current_cat_id +"request_id::"+this.props.request_id)
    }

    Detailspage(tech_id){
        Actions.push("ExpertDetails",{tech_id:tech_id,current_cat_id:this.props.current_cat_id})
    }

    componentWillMount() {
        this.setState({
           
            animating: true,
            lodingDialog: true,

        });

        AsyncStorage.getItem("userid")
            .then(userid => {
                this.setState({ user_id: userid });

                AsyncStorage.getItem("token")
                    .then(token => {
                        var accessToken = JSON.parse(token);
                        console.log("accessToken::"+accessToken +":::User:::"+userid)
                        this.setState({ token: accessToken });

                        AsyncStorage.getItem("request_id")
                        .then(request_id => {
                            var request_id = JSON.parse(request_id);
                            console.log("accessToken::"+accessToken +":::request_id:::"+request_id)
                            this.setState({ request_id: request_id });

                            fetch(Strings.base_Url + "findTechnician", {
                                method: 'POST',
                                headers: {
                                    'Content-Type': 'application/json',
                                    'User-Id':userid,
                                    'token': accessToken,
                                },
                                body: JSON.stringify({
                                    cat_id: this.props.current_cat_id,
                                    user_id:userid
                                })
                            })
                                .then((response) => response.json())
                                .then((responseData) => {
                                    console.log(" responseData resoJSon===" + JSON.stringify(responseData));
                                    console.log("resoJSon===" + responseData.error);
                                    this.setState({
                                        data: []
                                    })
                                    if (responseData.error == "false") {
                                        this.setState({
                                            data: responseData.listTech,
                                            marker_contiditon: true,
                                            animating: false,
                                            lodingDialog: false,
                    
                                        });
                                    
                    
                                    } else if (responseData.error == "true") {
                                      
                                       this.showalerts(responseData.errorMessage)
                                        this.setState({
                                            passwordErr: responseData.errorMessage,
                                            animating: false,
                                            lodingDialog: false
                                        });
                                    }
                                }
                                )
    
                        });

                    });
            });
    }

    showalerts(body){
        Alert.alert(
            Strings.alert_tital,
            body,
            [
              {text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
            ],
            { cancelable: false });
    }

    ratingCompleted(rating) {
        console.log("Rating is: " + rating)
    }
    render() {
        return (
            <View style={styles.containerWhite}>
                <Image source={require("../assets/simble.png")}
                    style={{ width: '100%', height: 78, }}>
                    <View style={{ width: '100%', height: 50, flexDirection: 'row' }}>
                        <TouchableOpacity onPress={() => Actions.push("Location")} style={{ width: '15%', height: 50, justifyContent: 'center', alignItems: 'center', }}>
                            <Image source={require("../assets/back.png")}
                                style={{ width: 25, height: 25, }}
                                resizeMode="contain" />
                        </TouchableOpacity>
                        <View style={{ width: '80%', height: 50, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: 16, fontWeight: 'bold' }}>{Strings.list_expert_text}</Text>

                        </View>

                    </View>
                </Image>
                <ScrollView style={{ marginTop: -25 }}>
                    <View style={{ flex: 1, flexDirection: 'column', margin: 10 }}>
                        <Text style={{ textAlign: 'center', fontSize: 14 }}>{Strings.des_experts_text}</Text>


                        <FlatList
                            keyExtractor={item => item.id}
                            data={this.state.data}
                            refreshControl={
                                <RefreshControl
                                  refreshing={this.state.refreshing}
                                  onRefresh={this.componentWillMount.bind(this)}
                                />
                              }
                            renderItem={({ item }) => (                            
                        <TouchableOpacity  onPress={()=> this.Detailspage(item.techId)} style={{ justifyContent: 'center', alignItems: 'center', marginTop: 10 }}>
                            <Card  style={{ width: "100%", justifyContent: 'space-between', alignItems: 'center', flexDirection: 'column', padding: 10, borderRadius: 15 }}>
                                <View style={{ width: '100%', flexDirection: 'row' }}>
                                    <View style={{ width: '20%', justifyContent: 'center', alignItems: 'center'}}>
                                        <Image source={{uri: item.profile_pic_url+"/"+item.profile_pic}}
                                            style={{ width: 50, height: 50 ,borderRadius:50/2}}
                                             />

                                    </View>
                                    <View style={{ width: '80%', flexDirection: 'column' }}>
                                       {item.worked_before == 1 ?
                                        <Text style={{ padding: 10, fontSize: 14, fontWeight: 'bold',color:'#F1C368' }}>{item.techName + " " + item.techLastName}(Technicien interne)</Text>
                                        :
                                        <Text style={{ padding: 10, fontSize: 14, fontWeight: 'bold' }}>{item.techName + " " + item.techLastName}</Text>
                                       }
                                        
                                        <View style={{ alignItems: 'flex-start', marginTop: -10 }}>
                                            <AirbnbRating
                                                showRating={false}
                                                count={5}
                                                color={Strings.color_green_code}
                                                defaultRating={item.tech_rating}
                                                size={15} />
                                        </View>
                                        {/* <View style={{ flexDirection: 'row' }}>
                                            <Image source={require("../assets/clock.png")}
                                                style={{ width: 20, height: 20, }}
                                                resizeMode="contain" />
                                                
                                            <Text> {item.date_time} </Text>
                                            <Text> </Text>
                                        </View> */}

                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row', width: '100%', marginTop: 20 }}>
                                    <View style={{ width: '50%', alignItems: 'center' }}>
                                        <Text>{Strings.cout_text}</Text>
                                        <Text>{"€" + item.charge}</Text>
                                    </View>
                                    <View style={{ width: '50%', alignItems: 'center' }}>
                                        <Text>{Strings.travaux_text}</Text>
                                        <Text>{item.tech_complete_job}</Text>
                                    </View>

                                </View>
                            </Card>
                        </TouchableOpacity>
                            )}
                        />
                        <Loading
                        ref={"loading2"}
                        image={require("../assets/spinner-of-dots.png")}
                        // backgroundColor='transparent'
                        backgroundColor='white'
                        borderRadius={5}
                        size={100}
                        imageSize={80}
                        indicatorColor='gray'
                        easing={Loading.EasingType.ease} />
                    </View>

                    <PopupDialog
                            onHardwareBackPress={() => { this.setState({ lodingDialog: false }) }}
                            width={0.3}
                            visible={this.state.lodingDialog}
                            dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
                            <DialogContent>
                                <View style={{ alignItems: 'center', }}>
                                    <ActivityIndicator
                                        animating={this.state.animating}
                                        style={[{ height: 10, marginBottom: 10, marginTop: 30, marginLeft: 20, marginRight: 20 }]}
                                        color="#C00"
                                        size="large"
                                        hidesWhenStopped={true}
                                    />
                                </View>
                            </DialogContent>
                        </PopupDialog>
                </ScrollView>
            </View>
        );
    }
}
