import React from 'react';
import { StatusBar, ScrollView, Alert, TextInput, View, ActivityIndicator, ImageBackground, Text, Dimensions, TouchableOpacity, Linking, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from "../styles/styles";
import Strings from '../strings/strings';
import { Card, Radio, CheckBox } from 'native-base';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';
import DateTimePicker from "react-native-modal-datetime-picker";
import PopupDialog, {
    DialogTitle, DialogContent, DialogFooter, DialogButton, SlideAnimation, ScaleAnimation,
} from 'react-native-popup-dialog';
import { Dropdown } from 'react-native-material-dropdown';

let drop_down_data = [{
    value: 'Homme'
}, {
    value: 'Femme'
},];;

let clientType;
let avatar_image_user;
let date_select;
let sex_gender;

import Image from 'react-native-fast-image'


const { width, height } = Dimensions.get("window");

export default class register extends React.Component {
    // onRegionChange(region) {
    //   this.setState({ region });
    // }
    constructor(props, context) {
        super(props);
        this.state = {
            animating: false,
            lodingDialog: false,
            Particulier_radio: false,
            Entreprise_radio: false,
            Clienten_radio: false,
            isDateTimePickerVisible: false,
            datePickervisible: false,
            child_radio: false,
            girl_radio: false,
            man_radio: false,
            people_radio: false,
            fsPath: '',
            fsPath2: '',
            checked: false,
            first_name: '',
            first_nameErr: '',
            last_name: '',
            last_nameErr: '',
            address: '',
            addressErr: '',
            email: '',
            emailErr: '',
            password: '',
            passwordErr: '',
            confirm_password: '',
            confirm_password_err: '',
            telephone: '',
            telephoneErr: '',
            aniversary: Strings.Anniversarie_text,
            aniversaryErr: '',
            gender: '1',
            genderErr: '',
            tncAccept: false,
            avatar_Image: '',
            avatar_ImageErr: '',
            select_avatar: false,
            select_photo: false,
            profile_Err: '',
            type_client_Err: '',
            client_type: '',
            avatar_image: '',
            data: [],
            doc_path_id1: '',
            doc_path_id2: '',
            doc_path_id3: '',
            doc_path_id4: '',
            doc_path_id5: '',
            doc_path_id1Err: '',
            doc_path_id2Err: '',
            doc_path_id3Err: '',
            doc_path_id4Err: '',
            doc_path_id5Err: '',
        };
    }

    valueExtractor = val => {
        console.log("vehicle id::::****:$$::" + JSON.stringify(val));
    };
    onChangeTextPress(id) {

        if (id == "Homme") {
            sex_gender = "1"
        } else if (id == "Femme") {
            sex_gender = "2"
        }
        console.log("vehicle id::::****:::" + id);
    }

    selectProfilePic = () => {
        const options = {
            title: 'Select Avatar',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };

        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                const source = { uri: response.uri };

                const imageUrl = response.uri
                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };
                console.log("image url:###:" + JSON.stringify(response))
                this.setState({
                    fsPath: response.fileName,
                    fsPath2: response.data
                });
                console.log("image url::" + JSON.stringify(imageUrl))
                console.log("image url::" + JSON.stringify(source))


            }
        });
    }

    selectPhotoTapped(num) {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true
            }
        };
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);
            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };
                switch (num) {
                    case '1':
                        this.setState({
                            doc_path_id1: response.fileName,
                            data: [...this.state.data, response.data],
                        });
                        break;
                    case '2':
                        this.setState({
                            doc_path_id2: response.fileName,
                            data: [...this.state.data, response.data],
                        });
                        break;
                    case '3':
                        this.setState({
                            doc_path_id3: response.fileName,
                            data: [...this.state.data, response.data],
                        });
                        break;
                    case '4':
                        this.setState({
                            doc_path_id4: response.fileName,
                            data: [...this.state.data, response.data],
                        });
                        break;
                    case '5':
                        this.setState({
                            doc_path_id5: response.fileName,
                            data: [...this.state.data, response.data],
                        });
                        break;
                    default:
                        break;
                }
            }
        });
    }

    showDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: true });
    };


    hideDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: false });

    };

    handleDatePicked = date => {
        console.log("A date has been picked: ", date);
        var date_select = date.toString().substr(3, 12);
        this.setState({
            aniversary: date_select
        })
        this.hideDateTimePicker();
    };

    validationRegister() {
        // this.UserRegister();
        var isValidate = 0;
        if (this.state.first_name != "") {
            isValidate += 1;
        } else {
            isValidate -= 1
            this.setState({
                first_nameErr: Strings.ShouldemptyText,
            });
        }
        if (this.state.last_name != "") {
            isValidate += 1;
        } else {
            isValidate -= 1
            this.setState({
                last_nameErr: Strings.ShouldemptyText,
            });
        }
        if (this.state.email != "") {
            isValidate += 1;
            if (this.validateEmail(this.state.email)) {
                isValidate += 1;
            } else {
                isValidate -= 1;
                this.setState({
                    emailErr: "S'il vous plaît entrer email valide",
                });
            }
        } else {
            console.log("cat empty::::");
            isValidate -= 1;
            this.setState({
                emailErr: Strings.ShouldemptyText,
            });
        }

        if (this.state.telephone != "") {
            isValidate += 1
        } else {
            isValidate -= 1;
            this.setState({
                telephoneErr: Strings.ShouldemptyText,
            });
        }

        if (this.state.password != "") {
            isValidate += 1;
            if (this.validatePassword(this.state.password)) {
                isValidate += 1;
            } else {
                isValidate -= 1;
                this.setState({
                    passwordErr: "La longueur devrait être min 6",

                });
            }
        } else {
            console.log("cat empty::::");
            isValidate -= 1;
            this.setState({
                passwordErr: Strings.ShouldemptyText,
            });
        }

        if (this.state.confirm_password != "") {
            isValidate += 1;
            if (this.validatePassword(this.state.confirm_password)) {
                isValidate += 1;


            } else {
                isValidate -= 1;
                this.setState({
                    confirm_password_err: "La longueur devrait être min 6",

                });
            }
        }

        else {
            console.log("cat empty::::");
            isValidate -= 1;
            this.setState({
                confirm_password_err: Strings.ShouldemptyText,
            });
        }
        if (this.state.confirm_password == this.state.password) {

            isValidate += 1;

        }
        else {
            console.log("cat empty::::");
            isValidate -= 1;
            this.setState({
                confirm_password_err: Strings.confrm_passwrd_same,
            });
        }




        console.log("Use Login::::" + isValidate)


        if (isValidate == 10) {

            console.log("Use Login::::" + this.state.first_name + this.state.last_name)

            this.UserRegister();
            this.setState({
                animating: true,
                lodingDialog: true,

            });
        }
        else {

        }

    }
    validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    validatePassword(password) {
        if (password.length < 6) {
            return false;
        } else if (password.length > 16) {
            return false;
        } else {
            return true;
        }
    }

    UserRegister() {
        console.log("client_type::::" + this.state.client_type + " gender:::" + sex_gender)
        let headers = {
            'Content-Type': 'application/json',
            //  'User-Id': this.state.userid,
            // 'token': this.state.token,
        };
        RNFetchBlob.fetch('POST', Strings.base_Url + 'registration', headers, [
            { name: 'firstname', data: this.state.first_name },
            { name: 'lastname', data: this.state.last_name },

            { name: 'email', data: this.state.email },
            { name: 'password', data: this.state.password },
            { name: 'telephone', data: this.state.telephone },


        ],
        ).then((resp) => {

            console.log("response:::::::" + JSON.stringify(resp.text()));

            console.log("error:::" + resp.json().error)
            var error = resp.json().error

            if (resp.json().error === "false") {
                this.setState({
                    animating: false,
                    lodingDialog: false,
                });
                Actions.push("Login")

            } else if (resp.json().error === "true") {

                this.showalerts(resp.json().errorMessage)
                this.setState({
                    animating: false,
                    lodingDialog: false,
                });

            }



        }).catch((err) => {
            this.setState({
                animating: false,
                lodingDialog: false,
            });
            console.log("response::::err:::" + err);
        });
    }


    showalerts(body) {
        Alert.alert(
            Strings.alert_tital,
            body,
            [
                { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
            ],
            { cancelable: false });
    }



    render() {
        console.log("date::" + this.state.aniversary.toString().substr(4, 12))
        return (
            <ScrollView style={{ height: height + 10 }}>
                <Image 
                source={require("../assets/signup.png")}
                    style={{
                        width: width,
                        height: height + 10,
                        resizeMode: 'stretch',

                    }}
                    resizeMode="stretch">

                    <View style={{ justifyContent: 'center', alignItems: 'center', width: width * 88 / 100, alignSelf: 'center' }}>




                        <View style={{ width: '90%', marginTop: height * 17 / 100, }}>
                            <Text style={{ textAlign: 'center', fontSize: 22, fontFamily: 'Poppins-SemiBold' }}>
                                {Strings.register_text}
                            </Text>
                        </View>

                        <View style={{ marginTop: 0 }}>

                            <View style={{ marginTop: height * 4 / 100, width: width * 88 / 100 }}>

                                <View style={{ width: '100%', borderBottomWidth: 1, borderBottomColor: Strings.light_color, borderRadius: 10 }}>
                                    <TextInput placeholder='Nom'
                                        onChangeText={(last_name) => this.setState({ last_name, first_nameErr: '' })}
                                        style={{ padding: 3, fontFamily: 'Poppins-Medium', fontSize: width * 3.2 / 100 }}>

                                    </TextInput>
                                </View>
                                {!!this.state.first_nameErr && (
                                    <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.first_nameErr}</Text>
                                )}

                                <View style={{
                                    width: '100%', marginTop: height * 2.4 / 100, borderBottomWidth: 1,
                                    borderBottomColor: Strings.light_color, borderRadius: 10
                                }}>
                                    <TextInput placeholder='Prenom'
                                        onChangeText={(first_name) => this.setState({ first_name, last_nameErr: '' })}
                                        style={{ padding: 3, fontFamily: 'Poppins-Medium', 
                                        fontSize: width * 3.2 / 100 }}>

                                    </TextInput>
                                </View>
                                {!!this.state.last_nameErr && (
                                    <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.last_nameErr}</Text>
                                )}

                                <View style={{ width: '100%', marginTop: height * 2.4 / 100, borderBottomWidth: 1, borderBottomColor: Strings.light_color, borderRadius: 10 }}>
                                    <TextInput
                                     placeholder={Strings.email_address_text}
                                     autoCapitalize = {false}
                                        onChangeText={(email) => this.setState({ email, emailErr: '' })}
                                        style={{ padding: 3, fontFamily: 'Poppins-Medium', fontSize: width * 3.2 / 100 }}>

                                    </TextInput>
                                </View>
                                {!!this.state.emailErr && (
                                    <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.emailErr}</Text>
                                )}
                                <View style={{ width: '100%', marginTop: height * 2.4 / 100, borderBottomWidth: 1, borderBottomColor: Strings.light_color, borderRadius: 10 }}>
                                    <TextInput
                                        keyboardType="decimal-pad"
                                        maxLength={9}
                                        onChangeText={(telephone) => this.setState({ telephone, telephoneErr: '' })}
                                        placeholder={Strings.telephone_text}
                                        style={{ padding: 3, fontFamily: 'Poppins-Medium', fontSize: width * 3.2 / 100 }}>

                                    </TextInput>
                                </View>
                                {!!this.state.telephoneErr && (
                                    <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.telephoneErr}</Text>
                                )}

                                <View style={{ width: '100%', marginTop: height * 2.4 / 100, borderBottomWidth: 1, borderBottomColor: Strings.light_color, borderRadius: 10 }}>
                                    <TextInput placeholder={Strings.password_text}
                                        secureTextEntry={true}
                                        onChangeText={(password) => this.setState({ password, passwordErr: '' })}
                                        style={{ padding: 3, fontFamily: 'Poppins-Medium', fontSize: width * 3.2 / 100 }}>

                                    </TextInput>
                                </View>
                                {!!this.state.passwordErr && (
                                    <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.passwordErr}</Text>
                                )}
                                <View style={{ width: '100%', marginTop: height * 2.4 / 100, borderBottomWidth: 1, borderBottomColor: Strings.light_color, borderRadius: 10 }}>
                                    <TextInput placeholder={Strings.cnfrm_password_text}
                                        secureTextEntry={true}
                                        onChangeText={(confirm_password) => this.setState({ confirm_password, confirm_password_err: '' })}
                                        style={{ padding: 3, fontFamily: 'Poppins-Medium', fontSize: width * 3.2 / 100 }}>

                                    </TextInput>
                                </View>
                                {!!this.state.confirm_password_err && (
                                    <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.confirm_password_err}</Text>
                                )}


                                <View style={{ width: 150, alignItems: 'center', marginTop: height * 2 / 100, alignSelf: 'center' }}>
                                    <TouchableOpacity onPress={() => this.validationRegister()} style={{ width: 180, height: 32, borderRadius: 25, backgroundColor: '#1A578A', alignItems: 'center', justifyContent: 'center' }}>
                                        <Text style={{ fontSize: width * 3 / 100, color: 'white', fontFamily: 'Poppins-SemiBold' }}>Créer mon compte</Text>
                                    </TouchableOpacity>
                                </View>


                                <TouchableOpacity onPress={() => Actions.push("Login")} style={{ marginTop: height * 1.2 / 100, alignItems: 'center', alignSelf: 'center', width: width * 88 / 100 }}>
                                    <Text style={{ fontSize: width * 2.8 / 100, fontFamily: 'Poppins-Medium', alignItems: 'center', justifyContent: 'center', textAlign: 'center' }}> Pour creer un compte, j'ai pris connaissances </Text>
                                    <Text style={{ fontSize: width * 2.8 / 100, fontFamily: 'Poppins-Medium', alignItems: 'center', justifyContent: 'center', textAlign: 'center' }}> des termes légaux</Text>

                                </TouchableOpacity>



                                <TouchableOpacity onPress={() => Actions.push("Login")} style={{ marginTop: height * 0.3 / 100, width: width * 88 / 100, alignItems: 'center', alignItems: 'center' }}>
                                    <Text style={{ fontSize: width * 2.9 / 100, textAlign: 'center', fontFamily: 'Poppins-SemiBold', }}>j'ai deja un compte,<Text style={{ color: '#F1C368', marginLeft: 1 }}>je me connecte</Text></Text>
                                </TouchableOpacity>


                            </View>
                        </View>

                        <PopupDialog
                            onHardwareBackPress={() => { this.setState({ lodingDialog: false }) }}
                            width={0.3}
                            visible={this.state.lodingDialog}
                            dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
                            <DialogContent>
                                <View style={{ alignItems: 'center', }}>
                                    <ActivityIndicator
                                        animating={this.state.animating}
                                        style={[{ height: 10, marginBottom: 10, marginTop: 30, marginLeft: 20, marginRight: 20 }]}
                                        color="#C00"
                                        size="large"
                                        hidesWhenStopped={true}
                                    />
                                </View>
                            </DialogContent>
                        </PopupDialog>

                    </View>

                </Image>
            </ScrollView>
        );
    }
}
