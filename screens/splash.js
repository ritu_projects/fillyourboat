import React from 'react';
import {
  Platform,

  SafeAreaView, StatusBar, View, ImageBackground, ActivityIndicator, Text, Dimensions, TouchableOpacity, Linking, AsyncStorage, Alert,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from "../styles/styles";
import MapView, { Marker, AnimatedRegion } from "react-native-maps";
//import PubNubReact from "pubnub-react";
import Geolocation from '@react-native-community/geolocation';
import Strings from '../strings/strings'


const { width, height } = Dimensions.get("window");
const ASPECT_RATIO = width / height;
const LATITUDE = 37.78825;
const LONGITUDE = -122.4324;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

import PopupDialog, {
  DialogTitle, DialogContent, DialogFooter, DialogButton, SlideAnimation, ScaleAnimation,
} from 'react-native-popup-dialog';
import { EventRegister } from 'react-native-event-listeners'
let order_id = '',fcm_push_response = '';
import firebase from 'react-native-firebase';

import Image from 'react-native-fast-image'

export default class Splash extends React.Component {
  // onRegionChange(region) {
  //   this.setState({ region });
  // }
  constructor(props, context) {
    super(props);
    this.state = {
      setlangDefault: "", isUpdated: false,
      showUpdateDialog: false,
      isAuthorized: "1",
      userid: '',
      token: '',
      animating: false,
      lodingDialog: false,
    };
  }

  componentWillMount() {


    // this.listener = EventRegister.addEventListener('myCustomEvent', (data) => {

    //   console.log("Splash Data ", data)
    //   order_id = data.currentOrderId;
    //   fcm_push_response = data.fcm_push_response;
    // })
    // console.log('Order Id ',order_id)
    // if (order_id != '' && order_id != null && order_id != 'null') {

    //   if(fcm_push_response == "adminSendChtMessagePush"){
    //     Alert.alert(
    //       title, body,
    //       [
    //           { text: 'OK', onPress: () => Actions.push("ChatwithAdmin",{currentOrderId:order_id}) },
    //       ],
    //       { cancelable: false },
    //     );  
    //     console.log("fcm_push_response :$$$Done$$:::"+fcm_push_response);
    //   }

    // else  if(fcm_push_response == "order_chat_user_side"){
    //     Alert.alert(
    //       title, body,
    //       [
    //           { text: 'OK', onPress: () => Actions.push("Chat",{currentOrderId:fcm_order_id}) },
    //       ],
    //       { cancelable: false },
    //     );  
    //     console.log("fcm_push_response :$$$Done$$:::"+fcm_push_response);
    //   }
    // }

    AsyncStorage.getItem("Order_id_fcm")
    .then(Order_id_fcm => {
      console.log('Order Id FCM before ',Order_id_fcm)
    var  order_idd = JSON.parse(Order_id_fcm);
      console.log('Order Id FCM after ',order_id)
   
      if (order_idd != '' && order_idd != null && order_idd != 'null') {
        console.log('Order Id FCM Inside ',order_idd)
  
        AsyncStorage.getItem("fcmpushresponse_fcm")
        .then(fcmpushresponse_fcm => {
          console.log('Fcmpushresponse_fcm FCM before ',fcmpushresponse_fcm)
  
        var  fcm_push_responsee = fcmpushresponse_fcm;
          console.log('Fcmpushresponse_fcm FCM After ',fcm_push_responsee)
  
          if(fcm_push_responsee == "adminSendChtMessagePush"){
            Actions.push("ChatwithAdmin",{currentOrderId:order_idd})
              
          }
    
        else  if(fcm_push_responsee == "order_chat_user_side"){
          console.log('Fcmpushresponse_fcm order_id inside ',order_idd)
  
            Actions.push("Chat",{currentOrderId:order_idd}) 

            AsyncStorage.setItem('Order_id_fcm','')
AsyncStorage.setItem('fcmpushresponse_fcm','')
AsyncStorage.setItem('titla_fcm','')
AsyncStorage.setItem('body_fcm','')
firebase.notifications().cancelAllNotifications();

  
        }
             
        
      else
      {
        console.log('Order Id FCM else Inside ',order_idd)

        if(fcm_push_responsee == "order_completed")
        {
          Actions.push("Rating_screen",{currentOrderId : order_idd})
          console.log("fcm_push_response :$$$Done$$:::"+fcm_push_responsee);

          AsyncStorage.setItem('Order_id_fcm','')
          AsyncStorage.setItem('fcmpushresponse_fcm','')
          AsyncStorage.setItem('titla_fcm','')
          AsyncStorage.setItem('body_fcm','')
          firebase.notifications().cancelAllNotifications();
        }       

        else if(fcm_push_responsee == "reached_location")
        {
          Actions.push("Trackorders",{currentOrderId:order_idd})
          console.log("fcm_push_response :$$$Done$$:::"+fcm_push_responsee);

          AsyncStorage.setItem('Order_id_fcm','')
          AsyncStorage.setItem('fcmpushresponse_fcm','')
          AsyncStorage.setItem('titla_fcm','')
          AsyncStorage.setItem('body_fcm','')
          firebase.notifications().cancelAllNotifications();
        }
        else
        {
          Actions.push("Trackorders",{currentOrderId:order_idd})
          console.log("fcm_push_response :$$$Done$$:::"+fcm_push_responsee);

          AsyncStorage.setItem('Order_id_fcm','')
          AsyncStorage.setItem('fcmpushresponse_fcm','')
          AsyncStorage.setItem('titla_fcm','')
          AsyncStorage.setItem('body_fcm','')
          firebase.notifications().cancelAllNotifications();
        }
      }
  
        })
               
       
      
      }
  
      else if(order_idd == '' || order_idd == null || order_idd == 'null')
      {
        console.log('Order Id FCM Inside null ',order_idd)

        
        this.callAPIforcheckRating();
      }
  
    })

 


  
     // this.callAPIforBoat();
   



  }

  callAPIforcheckRating() {

    AsyncStorage.getItem("userid")
      .then(userid => {
        this.setState({ userid: userid });

        console.log("state userId============" + userid);

        if (userid != null && userid != "" && userid != undefined) {


          AsyncStorage.getItem("token")
            .then(token => {
              var accessToken = JSON.parse(token);
              this.setState({ token: accessToken });


              this.setState({
                animating: true,
                lodingDialog: true,

              });

              fetch(Strings.base_Url + "userLastOrder", {
                method: 'POST',
                headers: {
                  // 'Accept': 'application/json',
                  'Content-Type': 'application/json',
                  'User-Id': this.state.userid,
                  'token': this.state.token,
                },
                body: JSON.stringify({
                  user_id: this.state.userid


                })
              })


                .then((response) => response.json())
                .then((responseData) => {
                  console.log(" responseData userLastOrder resoJSon===" + JSON.stringify(responseData));
                  console.log("resoJSon userLastOrder===" + responseData.error);
                  if (responseData.error == "false") {
                    console.log("Error false massage userLastOrder" + JSON.stringify(responseData.past_order));

                    this.setState({
                      animating: false,
                      lodingDialog: false,
                    });

                    var do_rating = responseData.past_order[0].do_rating;
                    console.log("do_rating " , do_rating);

                    if(do_rating == 1)
                    {                
                          this.callAPIforBoat();

                    }

                    else
                    {
                      var id = responseData.past_order[0].id;
                      console.log("do_rating id " , id);
                      Actions.push("Rating_screen",{currentOrderId : id})
                    }


                  } else if (responseData.error == "true") {

                    if (responseData.errorMessage == 'Incompatibilité de jetons') {
                      console.log("resoJSon true===" + responseData.error);

                      this.setState({
                        animating: false,
                        lodingDialog: false,

                      });


                      Actions.push("Login")

                    }
                    else {


                      this.setState({
                        animating: false,
                        lodingDialog: false,

                      });
                      this.callAPIforBoat();



                    }
                  }

                }
                )

            })

        } else {

          Actions.push("Login")



        }

      })

  }
  callAPIforBoat() {

    AsyncStorage.getItem("userid")
      .then(userid => {
        this.setState({ userid: userid });

        console.log("state userId============" + userid);

        if (userid != null && userid != "" && userid != undefined) {


          AsyncStorage.getItem("token")
            .then(token => {
              var accessToken = JSON.parse(token);
              this.setState({ token: accessToken });


              this.setState({
                animating: true,
                lodingDialog: true,

              });

              fetch(Strings.base_Url + "getBoatInfo", {
                method: 'POST',
                headers: {
                  // 'Accept': 'application/json',
                  'Content-Type': 'application/json',
                  'User-Id': this.state.userid,
                  'token': this.state.token,
                },
                body: JSON.stringify({
                  user_id: this.state.userid


                })
              })


                .then((response) => response.json())
                .then((responseData) => {
                  console.log(" responseData getBoatInfo resoJSon===" + JSON.stringify(responseData));
                  console.log("resoJSon getBoatInfo===" + responseData.error);
                  if (responseData.error == "false") {
                    console.log("Error false massage getBoatInfo" + JSON.stringify(responseData.boat_record));

                    this.setState({
                      animating: false,
                      lodingDialog: false,
                    });

                    Actions.push("Location")


                  } else if (responseData.error == "true") {

                    if (responseData.errorMessage == 'Incompatibilité de jetons') {
                      console.log("resoJSon true===" + responseData.error);

                      this.setState({
                        animating: false,
                        lodingDialog: false,

                      });


                      Actions.push("Login")

                    }
                    else {


                      this.setState({
                        animating: false,
                        lodingDialog: false,

                      });
                      // this.showalerts(responseData.errorMessage)

                      //    Actions.push("AddBoat")
                      Actions.push("Location")
                      // Actions.push("Delvey_confirm_Screen")


                    }
                  }

                }
                )

            })

        } else {

          Actions.push("Login")



        }

      })

  }


  render() {
    return (
      <View style={styles.containerWhite}>
        <Image source={require("../assets/launchscreen-bg.png")}
          style={{ width: '100%', height: '100%' }}
          resizeMode='stretch'>
          <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'center' }}>

            {/* <Image source={require('../assets/hop-assist-logo.png')}
              style={{ width: 200, height: 200, resizeMode: 'contain' }} />
            <Image source={require('../assets/imgpsh_fullsize_anim(1).png')}
              style={{ width: 200, height: 100, resizeMode: 'contain', marginBottom: 50 }} /> */}

          </View>
        </Image>
        <PopupDialog
          onHardwareBackPress={() => { this.setState({ lodingDialog: false }) }}
          width={0.3}
          visible={this.state.lodingDialog}
          dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
          <DialogContent>
            <View style={{ alignItems: 'center', }}>
              <ActivityIndicator
                animating={this.state.animating}
                style={[{ height: 10, marginBottom: 10, marginTop: 30, marginLeft: 20, marginRight: 20 }]}
                color="#C00"
                size="large"
                hidesWhenStopped={true}
              />
            </View>
          </DialogContent>
        </PopupDialog>

      </View>
    );
  }
}







