import React from 'react';
import { StatusBar, Button, Alert, FlatList, ScrollView, PixelRatio, TextInput, View, ImageBackground, Text, Image, Dimensions, TouchableOpacity, Linking, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from "../styles/styles";
import Strings from '../strings/strings'
import ImagePicker from 'react-native-image-picker';
import Loading from 'react-native-whc-loading';
import { Card } from 'native-base';
import TravailRBSheet from 'react-native-raw-bottom-sheet';
import CancelRBSheet from 'react-native-raw-bottom-sheet';
import RescheduleRBSheet from 'react-native-raw-bottom-sheet';
import DatePickers from 'react-native-modal-datetime-picker'
import moment from 'moment';
import DateTimePicker from "react-native-modal-datetime-picker";



import { CirclesLoader, BubblesLoader, ColorDotsLoader, PulseLoader, TextLoader, DotsLoader, } from 'react-native-indicator';

export default class upComming extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            user_id: '',
            token: '',
            temps: '',
            hour_cost: '',
            mat_cost: '',
            total_cost: '',
            details: '',
            cancelDataList: [],
            request_Id: '',
            NoRecord: '',
            cancelcondition: false,
            date_select: '',
            req_id: '',
            time: '',
            request_no: '',
        };
        this.setDate = this.setDate.bind(this);
    }
    componentDidMount() {
        this.refs.loading2.show();
        setTimeout(() => {
            this.refs.loading2.close();
        }, 4000);
    }

    setDate(newDate) {
        this.setState({ chosenDate: newDate });

        console.log("chosenDate:::" + this.state.chosenDate)
    }

    showDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: true });
    };

    showDate = () => {
        this.setState({ datePickervisible: true });

    }

    hideDateTimePicker = () => {

        this.setState({ isDateTimePickerVisible: false, datePickervisible: false });
    };
    handleDatePicked = date => {


        console.log("A date has been picked: ", date);
        // var date_select = date.toString().substr(3, 12);
        var current_Date = moment(date).format('YYYY-MM-DD');

        console.log("A date has been picked $$$$$ ###: " + current_Date);
        //date_select=date_select
        this.setState({
            date_select: current_Date
        })
        this.hideDateTimePicker();
    };
    _handleDatePicked = time => {
        console.log("A date time has been picked: ", time);
        const selected1 = time.getHours() + ':' + time.getMinutes();
        console.log("A date time has been picked: ", selected1);
        this.setState({ time: selected1 })
        this.hideDateTimePicker();
    };

    componentWillMount() {
        AsyncStorage.getItem("userid")
            .then(userid => {
                this.setState({ user_id: userid });
                AsyncStorage.getItem("token")
                    .then(token => {
                        var accessToken = JSON.parse(token);
                        this.setState({ token: accessToken });
                        console.log("accessToken====$$$========" + accessToken);
                        console.log("state userId====$$$========" + userid);
                        this.refs.loading2.show();
                        fetch(Strings.base_Url + "user_pending_job", {
                            method: 'POST',
                            headers: {
                                // 'Accept': 'application/json',
                                'Content-Type': 'application/json',
                                'User-Id': userid,
                                'token': accessToken,
                            },
                            body: JSON.stringify({
                                user_id: userid,
                            })
                        })
                            .then((response) => response.json())
                            .then((responseData) => {
                                console.log(" responseData user_pending_job resoJSon===" + JSON.stringify(responseData));
                                console.log("resoJSon user_pending_job===" + responseData.error);
                                if (responseData.error == "false") {
                                    console.log("Error false massage" + JSON.stringify(responseData.errorMessage));

                                    this.setState({
                                        data: responseData.reqDetail,
                                        animating: false,
                                        lodingDialog: false,
                                        NoRecord: ''

                                    });

                                    this.getCancelApi();
                                } else if (responseData.error == "true") {
                                    this.setState({
                                        NoRecord: responseData.errorMessage,
                                        animating: false,
                                        lodingDialog: false

                                    });
                                    this.refs.loading2.close();
                                }
                            })

                    });
            });

    }

    validationLogin(id) {
        this.refs.loading2.show();
        this.setState({
            request_Id: id
        })

        fetch(Strings.base_Url + "getRequestEstimation", {
            method: 'POST',
            headers: {
                // 'Accept': 'application/json',
                'Content-Type': 'application/json',
                'User-Id': this.state.user_id,
                'token': this.state.token,
            },
            body: JSON.stringify({
                req_id: id,
            })
        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(" responseData resoJSon===" + JSON.stringify(responseData));
                console.log("resoJSon===" + responseData.error);
                if (responseData.error == "false") {
                    console.log("Error false massage" + JSON.stringify(responseData.errorMessage));

                    this.setState({
                        temps: responseData.estimat_Detail.require_hour,
                        hour_cost: responseData.estimat_Detail.price_per_hour,
                        mat_cost: responseData.estimat_Detail.mat_cost,
                        total_cost: responseData.estimat_Detail.total,
                        details: responseData.estimat_Detail.estimat_detail,
                        animating: false,
                        lodingDialog: false,
                    });
                    this.TravailRBSheet.open();
                    this.refs.loading2.close();
                } else if (responseData.error == "true") {
                    this.showalerts(responseData.errorMessage)
                    this.setState({
                        passwordErr: responseData.errorMessage,
                        animating: false,
                        lodingDialog: false

                    });
                    this.refs.loading2.close();
                }
            })



    }

    showalerts(body) {
        Alert.alert(
            Strings.alert_tital,
            body,
            [
                { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
            ],
            { cancelable: false });
    }

    getCancelApi() {
        console.log("accessToken====$$$========" + this.state.user_id);
        console.log("state userId====$$$========" + this.state.token);
        var token = JSON.stringify(this.state.token)

        fetch(Strings.base_Url + "user_cancel_reason", {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'User-Id': this.state.user_id,
                'token': this.state.token,
            },
        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(" responseData resoJSon===" + JSON.stringify(responseData));
                console.log("resoJSon===" + responseData.error);
                if (responseData.error == "false") {
                    console.log("Error false massage" + JSON.stringify(responseData.errorMessage));
                    this.setState({
                        animating: false,
                        lodingDialog: false,
                        cancelDataList: responseData.reason_detail
                    });
                    this.refs.loading2.close();
                    // alert(responseData.errorMessage)
                } else if (responseData.error == "true") {
                    this.showalerts(responseData.errorMessage)
                    this.setState({
                        passwordErr: responseData.errorMessage,
                        animating: false,
                        lodingDialog: false
                    });
                }
            })

    }

    cancelOrder(id) {
        this.CancelRBSheet.open();
        this.setState({
            request_Id: id,
            cancelcondition: true
        })
    }

    cancelValidation() {
        var isValidate = 0;
        if (this.state.value != null) {
            isValidate += 1
        } else {
            isValidate -= 1
            this.setState({
                cancel_TextErr: Strings.ShouldemptyText
            })
        }
        if (isValidate == 1) {
            console.log("cancel Text:::" + this.state.cancelcondition + this.state.cancel_Text)
            if (this.state.cancelcondition == true) {
                this.user_finish_request();
            } else {
                this.requestCancel();
            }

        }
    }

    user_finish_request() {
        console.log("requestAccept id :::" + this.state.request_Id)
        // this.AccceptRBSheet.close()
        this.refs.loading2.show();
        this.CancelRBSheet.close();
        console.log("user_finish_request Api call")
        fetch(Strings.base_Url + "user_finish_request", {
            method: 'POST',
            headers: {
                // 'Accept': 'application/json',
                'Content-Type': 'application/json',
                'User-Id': this.state.user_id,
                'token': this.state.token,
            },
            body: JSON.stringify({
                reason: this.state.cancel_Text,
                req_id: this.state.request_Id,
            })
        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(" responseData req_det. resoJSon===" + JSON.stringify(responseData));
                console.log("resoJSon===" + responseData.error);
                if (responseData.error == "false") {
                    console.log("Error false massage" + JSON.stringify(responseData.errorMessage));
                    this.setState({
                        animating: false,
                        lodingDialog: false,
                    });
                    this.refs.loading2.close();
                    this.CancelRBSheet.close();
                    Actions.push("Location")
                } else if (responseData.error == "true") {
                    this.setState({
                        passwordErr: responseData.errorMessage,
                        animating: false,
                        lodingDialog: false
                    });
                    this.refs.loading2.close();
                }
            })

    }

    requestCancel() {
        this.CancelRBSheet.close();
        console.log("requestAccept id :::" + this.state.request_Id)
        // this.AccceptRBSheet.close()
        this.refs.loading2.show();
        this.TravailRBSheet.close();
        this.CancelRBSheet.close();
        console.log("user_cancel_request api call")
        fetch(Strings.base_Url + "user_cancel_request", {
            method: 'POST',
            headers: {
                // 'Accept': 'application/json',
                'Content-Type': 'application/json',
                'User-Id': this.state.user_id,
                'token': this.state.token,
            },
            body: JSON.stringify({
                reason: this.state.cancel_Text,
                req_id: this.state.request_Id,
            })
        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(" responseData req_det. resoJSon===" + JSON.stringify(responseData));
                console.log("resoJSon===" + responseData.error);
                if (responseData.error == "false") {
                    console.log("Error false massage" + JSON.stringify(responseData.errorMessage));
                    this.setState({
                        animating: false,
                        lodingDialog: false,
                    });
                    this.refs.loading2.close();
                    this.TravailRBSheet.close();
                    this.CancelRBSheet.close();
                    Actions.push("Location")
                    //this.componentWillMount();
                } else if (responseData.error == "true") {
                    this.setState({
                        passwordErr: responseData.errorMessage,
                        animating: false,
                        lodingDialog: false
                    });
                    this.refs.loading2.close();
                }
            })
    }

    paynowpaypal(req_id, cat_id, total_amount) {
        Actions.push("PaypalPayment", { req_id: req_id, total_amount: total_amount, current_cat_id: cat_id })
    }

    AcceptEstimation() {
        this.refs.loading2.show();
        this.TravailRBSheet.close();
        this.CancelRBSheet.close();
        console.log("requestAccept id :::" + this.state.request_Id)
        fetch(Strings.base_Url + "user_accept_request", {
            method: 'POST',
            headers: {
                // 'Accept': 'application/json',
                'Content-Type': 'application/json',
                'User-Id': this.state.user_id,
                'token': this.state.token,
            },
            body: JSON.stringify({
                req_id: this.state.request_Id,
            })
        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(" responseData req_det. resoJSon===" + JSON.stringify(responseData));
                console.log("resoJSon===" + responseData.error);
                if (responseData.error == "false") {
                    console.log("Error false massage" + JSON.stringify(responseData.errorMessage));
                    this.setState({
                        animating: false,
                        lodingDialog: false,
                    });
                    this.refs.loading2.close();
                    this.TravailRBSheet.close();
                    this.CancelRBSheet.close();
                    this.componentWillMount();
                    // Actions.push("Location")
                } else if (responseData.error == "true") {
                    this.setState({
                        passwordErr: responseData.errorMessage,
                        animating: false,
                        lodingDialog: false
                    });
                    this.refs.loading2.close();
                }
            })

    }

    user_reassign_request(tech_id, cat_id, request_id) {
        AsyncStorage.setItem("request_id", JSON.stringify(request_id))
        Actions.push("ExpertDetails", { tech_id: tech_id, current_cat_id: cat_id, reason_id: "1" })

    }

    user_reassign_request1(tech_id, cat_id, request_id) {
        console.log("requestAccept id :::" + request_id)
        this.refs.loading2.show();
        fetch(Strings.base_Url + "user_reassign_request", {
            method: 'POST',
            headers: {
                // 'Accept': 'application/json',
                'Content-Type': 'application/json',
                'User-Id': this.state.user_id,
                'token': this.state.token,
            },
            body: JSON.stringify({
                req_id: request_id,
                user_id: this.state.user_id
            })
        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(" responseData req_det. resoJSon===" + JSON.stringify(responseData));
                console.log("resoJSon===" + responseData.error);
                if (responseData.error == "false") {
                    console.log("Error false massage" + JSON.stringify(responseData.errorMessage));
                    this.setState({
                        animating: false,
                        lodingDialog: false,
                    });
                    this.refs.loading2.close();
                    AsyncStorage.setItem("request_id", JSON.stringify(request_id))
                    Actions.push("ExpertsList", { current_cat_id: cat_id })
                    //this.componentWillMount();
                    // Actions.push("Location")
                } else if (responseData.error == "true") {
                    this.setState({
                        passwordErr: responseData.errorMessage,
                        animating: false,
                        lodingDialog: false
                    });
                    this.refs.loading2.close();
                }
            })

    }

    expretList(cat_id, request_id) {
        AsyncStorage.setItem("request_id", JSON.stringify(request_id))
        Actions.push("ExpertsList", { current_cat_id: cat_id })
    }
    getReschedule(req_id, req_date, req_time, request_no) {

        this.setState({
            time: req_time,
            date_select: req_date,
            req_id: req_id,
            request_no: request_no
        })
        this.RescheduleRBSheet.open()
    }

    user_reschedule_job() {
        console.log("user_reschedule_job  id :::" + this.state.req_id + "date" + this.state.date_select + "time " + this.state.time)
        this.RescheduleRBSheet.close();
        this.refs.loading2.show();
        fetch(Strings.base_Url + "user_reschedule_job", {
            method: 'POST',
            headers: {
                // 'Accept': 'application/json',
                'Content-Type': 'application/json',
                'User-Id': this.state.user_id,
                'token': this.state.token,
            },
            body: JSON.stringify({
                req_id: this.state.req_id,
                request_date: this.state.date_select,
                request_time: this.state.time
            })
        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(" responseData req_det. resoJSon===" + JSON.stringify(responseData));
                console.log("resoJSon===" + responseData.error);
                if (responseData.error == "false") {
                    console.log("Error false massage" + JSON.stringify(responseData.errorMessage));
                    this.setState({
                        animating: false,
                        lodingDialog: false,
                    });
                    this.componentWillMount();
                    this.refs.loading2.close();
                    // Actions.push("Location")
                } else if (responseData.error == "true") {
                    this.setState({
                        passwordErr: responseData.errorMessage,
                        animating: false,
                        lodingDialog: false
                    });
                    this.refs.loading2.close();
                }
            })


    }
    render() {
        return (
            <View style={styles.containerWhite}>
                <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center', backgroundColor: 'white', height: 500 }}>
                    {/* */}
                    {this.state.NoRecord != '' &&
                        (
                            <Text style={{ padding: 10, fontSize: 16, fontWeight: 'bold' }}>{this.state.NoRecord}</Text>
                        )

                    }
                    <ScrollView>
                        <FlatList
                            keyExtractor={item => item.id}
                            data={this.state.data}
                            renderItem={({ item }) => (
                                <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 10 }}>
                                    <Card style={{ width: '95%', flexDirection: 'row', marginBottom: 20, marginTop: 0 }}>
                                        <View style={{ width: '95%', flexDirection: 'column' }}>
                                            <View style={{ flexDirection: 'row', margin: 10 }}>
                                                <Text style={{ paddingLeft: 0, fontSize: 14, color: 'black', fontWeight: 'bold' }}> Commande ID :  </Text>
                                                <Text style={{ paddingTop: 0 }}>#{item.request_no}</Text>
                                            </View>
                                            <View style={{ flexDirection: 'row', marginLeft: 10 }}>
                                                <Text style={{ paddingLeft: 0, fontSize: 14, color: 'black', fontWeight: 'bold' }}> Nom de la catégorie :  </Text>
                                                <Text style={{ paddingTop: 0 }}>{item.name}</Text>
                                            </View>
                                            <View style={{ flexDirection: 'row', marginLeft: 10, marginTop: 10 }}>
                                                <Text style={{ paddingLeft: 0, fontSize: 14, color: 'black', fontWeight: 'bold' }}> Statut :  </Text>
                                                <Text style={{ paddingTop: 0 }}>{item.word_status}</Text>

                                            </View>
                                            <View style={{ flexDirection: 'row', margin: 10 }}>
                                                <Text style={{ paddingLeft: 0, fontSize: 14, color: 'black', fontWeight: 'bold' }}> Détails :  </Text>
                                                <Text style={{ paddingTop: 0, width: '80%' }}>{item.detail}</Text>
                                            </View>


                                            {item.reschdule == true ?
                                                <View>
                                                    {item.request_status == 6 ?
                                                        <View>
                                                            {item.estimation_status == 1 && (
                                                                <View style={{ margin: 10, width: '100%', alignItems: 'center' }}>
                                                                    <TouchableOpacity onPress={() => this.validationLogin(item.request_id)} style={{ alignItems: 'center', justifyContent: 'center', width: '90%', height: 40, borderRadius: 10, backgroundColor: '#F1C368' }}>
                                                                        <Text style={{ fontSize: 16, color: 'white' }}>{Strings.Consulter_text}</Text>
                                                                    </TouchableOpacity>
                                                                </View>
                                                            )
                                                            }
                                                        </View> :
                                                        <View style={{ margin: 10, width: '100%', alignItems: 'center' }}>
                                                            <TouchableOpacity onPress={() => this.getReschedule(item.request_id, item.request_date, item.request_time, item.request_no)} style={{ alignItems: 'center', justifyContent: 'center', width: '90%', height: 40, borderRadius: 10, backgroundColor: '#F1C368' }}>
                                                                <Text style={{ fontSize: 12, color: 'white' }}>{"Replanifier"}</Text>
                                                            </TouchableOpacity>
                                                        </View>

                                                    }

                                                </View> :

                                                <View>

                                                    {
                                                        item.request_status == 6 && (

                                                            <View>
                                                                {item.estimation_status == 1 && (
                                                                    <View style={{ margin: 10, width: '100%', alignItems: 'center' }}>
                                                                        <TouchableOpacity onPress={() => this.validationLogin(item.request_id)} style={{ alignItems: 'center', justifyContent: 'center', width: '90%', height: 40, borderRadius: 10, backgroundColor: '#F1C368' }}>
                                                                            <Text style={{ fontSize: 16, color: 'white' }}>{Strings.Consulter_text}</Text>
                                                                        </TouchableOpacity>
                                                                    </View>
                                                                )
                                                                }
                                                            </View>

                                                        )

                                                    }

                                                    {item.tech_request_status == 3 && (
                                                        <View style={{ margin: 10, width: '100%', alignItems: 'center' }}>
                                                            <TouchableOpacity onPress={() => this.expretList(item.cat_id, item.request_id)} style={{ alignItems: 'center', justifyContent: 'center', width: '90%', height: 40, borderRadius: 10, backgroundColor: '#F1C368' }}>
                                                                <Text style={{ fontSize: 12, color: 'white' }}>{"Rechercher un nouveau technicien"}</Text>
                                                            </TouchableOpacity>
                                                        </View>
                                                    )


                                                    }

                                                    {item.advance_pay_status == 1 && (
                                                        <View style={{ margin: 10, width: '100%', alignItems: 'flex-end' }}>
                                                            {item.request_status == 0 && (
                                                                <View style={{ flexDirection: 'row' }}>
                                                                    <TouchableOpacity onPress={() => this.cancelOrder(item.request_id)} style={{ alignItems: 'center', justifyContent: 'center', width: '40%', height: 40, borderRadius: 10, backgroundColor: '#F1C368', marginRight: 15 }}>
                                                                        <Text style={{ fontSize: 12, color: 'white' }}>{Strings.cancel_text}</Text>
                                                                    </TouchableOpacity>

                                                                    <TouchableOpacity onPress={() => this.expretList(item.cat_id, item.request_id)} style={{ alignItems: 'center', justifyContent: 'center', width: '50%', height: 40, borderRadius: 10, backgroundColor: '#F1C368' }}>
                                                                        <Text style={{ fontSize: 12, color: 'white' }}>{"Affecter"}</Text>
                                                                    </TouchableOpacity>

                                                                </View>



                                                            )
                                                            }

                                                            {item.request_status == 1 && (
                                                                <View style={{ flexDirection: 'row' }}>
                                                                    <TouchableOpacity onPress={() => this.cancelOrder(item.request_id)} style={{ alignItems: 'center', justifyContent: 'center', width: '40%', height: 40, borderRadius: 10, backgroundColor: '#F1C368', marginRight: 15 }}>
                                                                        <Text style={{ fontSize: 12, color: 'white' }}>{Strings.cancel_text}</Text>
                                                                    </TouchableOpacity>

                                                                    <TouchableOpacity onPress={() => this.user_reassign_request1(item.tech_id, item.cat_id, item.request_id)} style={{ alignItems: 'center', justifyContent: 'center', width: '50%', height: 40, borderRadius: 10, backgroundColor: '#F1C368' }}>
                                                                        <Text style={{ fontSize: 12, color: 'white' }}>{"Réaffecter"}</Text>
                                                                    </TouchableOpacity>

                                                                </View>

                                                            )
                                                            }
                                                            {item.request_status == 2 && (
                                                                <View style={{ flexDirection: 'row' }}>
                                                                    <TouchableOpacity onPress={() => this.cancelOrder(item.request_id)} style={{ alignItems: 'center', justifyContent: 'center', width: '40%', height: 40, borderRadius: 10, backgroundColor: '#F1C368', marginRight: 15 }}>
                                                                        <Text style={{ fontSize: 12, color: 'white' }}>{Strings.cancel_text}</Text>
                                                                    </TouchableOpacity>

                                                                    <TouchableOpacity onPress={() => this.user_reassign_request(item.tech_id, item.cat_id, item.request_id)} style={{ alignItems: 'center', justifyContent: 'center', width: '50%', height: 40, borderRadius: 10, backgroundColor: '#F1C368' }}>
                                                                        <Text style={{ fontSize: 12, color: 'white' }}>{"Réaffecter"}</Text>
                                                                    </TouchableOpacity>

                                                                </View>

                                                            )
                                                            }


                                                        </View>
                                                    )
                                                    }

                                                    {item.client_type != 3 ?
                                                        <View style={{ margin: 0, width: '100%', alignItems: 'flex-end' }}>
                                                            {item.advance_pay_status == 0 && (
                                                                <View>
                                                                    {item.pay_by == 1 ?
                                                                        <View style={{ margin: 10, width: '100%', alignItems: 'flex-end' }}>
                                                                            <TouchableOpacity onPress={() => this.paynowpaypal(item.request_id, item.cat_id, item.advance_payment)} style={{ alignItems: 'center', justifyContent: 'center', width: 150, height: 40, borderRadius: 10, backgroundColor: '#F1C368' }}>
                                                                                <Text style={{ fontSize: 14, color: 'white' }}>{"Payer maintenant "}</Text>
                                                                            </TouchableOpacity>
                                                                        </View> :
                                                                        <View style={{ margin: 10, width: '100%', alignItems: 'flex-end' }}>
                                                                            <TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center', width: 150, height: 40, borderRadius: 10, backgroundColor: '#F1C368' }}>
                                                                                <Text style={{ fontSize: 14, color: 'white' }}>{"Payer maintenant "}</Text>
                                                                            </TouchableOpacity>
                                                                        </View>
                                                                    }
                                                                </View>
                                                            )
                                                            }
                                                        </View> :

                                                        <View style={{ margin: 10, width: '100%', alignItems: 'flex-end' }}>
                                                            


                                                        </View>


                                                    }



                                                </View>

                                            }






                                        </View>
                                    </Card>
                                </View>
                            )}
                        />
                    </ScrollView>
                    <TravailRBSheet
                        ref={ref => { this.TravailRBSheet = ref; }}
                        height={500}
                        duration={200}
                        closeOnPressMask={false}
                        customStyles={{
                            container: {
                                alignItems: "center",
                                borderRadius: 10,
                                // backgroundColor: 'rgba(52, 52, 52, 0.8)'  
                                backgroundColor: 'transparent'
                            }
                        }}>
                        <View style={{ flexDirection: 'column', width: '90%', height: '100%', borderRadius: 10, alignItems: 'center', justifyContent: 'center' }}>
                            <ImageBackground source={require("../assets/Box(1).png")}
                                style={{ width: 360, height: 480, borderRadius: 50, alignItems: 'center' }}
                                resizeMode='stretch'>

                                <View style={{ width: '95%', height: 450, borderRadius: 10, alignItems: 'center' }}>
                                    <Text style={{ fontSize: 18, fontWeight: 'bold', marginTop: 30, padding: 10, textAlign: 'center' }}>{Strings.estimation_text}</Text>

                                    <Text style={{ fontSize: 14, marginTop: 40, textAlign: 'center' }}>{Strings.l_expert_text}</Text>

                                    <View style={{ width: '95%', flexDirection: 'row', marginTop: 10 }}>
                                        <View style={{ width: '50%', }}>
                                            <Text style={{ ontSize: 14, color: Strings.color_green_code, padding: 10 }}>{Strings.temps_text}</Text>
                                        </View>
                                        <View style={{ width: '50%', }}>
                                            <Text style={{ ontSize: 14, color: 'black', padding: 10 }}>{this.state.temps} {Strings.heure_text}</Text>
                                        </View>
                                    </View>

                                    <View style={{ width: '95%', flexDirection: 'row', }}>
                                        <View style={{ width: '50%', }}>
                                            <Text style={{ ontSize: 14, color: Strings.color_green_code, padding: 10 }}>{Strings.cout_traval_text}</Text>
                                        </View>
                                        <View style={{ width: '50%', }}>
                                            <Text style={{ ontSize: 14, color: 'black', padding: 10 }}> {this.state.hour_cost} €</Text>
                                        </View>
                                    </View>
                                    <View style={{ width: '95%', flexDirection: 'row', }}>
                                        <View style={{ width: '50%', }}>
                                            <Text style={{ ontSize: 14, color: Strings.color_green_code, padding: 10 }}>{Strings.cout_du_materiel_text}</Text>
                                        </View>
                                        <View style={{ width: '50%', }}>
                                            <Text style={{ ontSize: 14, color: 'black', padding: 10 }}> {this.state.mat_cost} €</Text>
                                        </View>
                                    </View>
                                    <View style={{ width: '95%', flexDirection: 'row', }}>
                                        <View style={{ width: '50%', }}>
                                            <Text style={{ ontSize: 14, color: Strings.color_green_code, padding: 10 }}>{"Description du coût annexe:"}</Text>
                                        </View>
                                        <View style={{ width: '50%', }}>
                                            <Text style={{ ontSize: 14, color: 'black', padding: 10 }}>{this.state.details}</Text>
                                        </View>
                                    </View>
                                    <View style={{ width: '95%', flexDirection: 'row', }}>
                                        <View style={{ width: '50%', }}>
                                            <Text style={{ ontSize: 14, color: Strings.color_green_code, padding: 10 }}>{Strings.cout_total_text}</Text>
                                        </View>
                                        <View style={{ width: '50%', }}>
                                            <Text style={{ ontSize: 14, color: 'black', padding: 10 }}> {this.state.total_cost} €</Text>
                                        </View>
                                    </View>
                                    <View style={{ width: '95%', flexDirection: 'row', marginTop: 15 }}>
                                        <View style={{ width: '60%', alignItems: 'center' }}>
                                            <TouchableOpacity onPress={() => this.TravailRBSheet.close() + this.CancelRBSheet.open()} style={{ width: '95%', height: 40, backgroundColor: 'red', borderRadius: 10, marginTop: 20, alignItems: 'center', justifyContent: 'center' }}>
                                                <Text style={{ color: 'white' }}>{Strings.annuler_la_text}</Text>
                                            </TouchableOpacity>
                                        </View>
                                        <View style={{ width: '40%', alignItems: 'center' }}>
                                            {/* + this.StartRideRBSheet.open() */}
                                            <TouchableOpacity onPress={() => this.AcceptEstimation()} style={{ width: '90%', height: 40, backgroundColor: Strings.color_green_code, borderRadius: 10, marginTop: 20, alignItems: 'center', justifyContent: 'center' }}>
                                                <Text style={{ fontSize: 16, color: 'white' }}>{Strings.accepter_text}</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            </ImageBackground>
                        </View>
                    </TravailRBSheet>


                    <RescheduleRBSheet
                        ref={ref => { this.RescheduleRBSheet = ref; }}
                        height={500}
                        duration={200}
                        closeOnPressMask={false}
                        customStyles={{
                            container: {
                                alignItems: "center",
                                borderRadius: 10,
                                // backgroundColor: 'rgba(52, 52, 52, 0.8)'  
                                backgroundColor: 'transparent'
                            }
                        }}>
                        <View style={{ flexDirection: 'column', width: '90%', height: '100%', borderRadius: 10, alignItems: 'center', justifyContent: 'center' }}>

                            <View style={{ width: '95%', height: 320, borderRadius: 10, alignItems: 'center', backgroundColor: 'white' }}>
                                <Text style={{ marginTop: 10, textAlign: 'center', fontSize: 16, fontWeight: 'bold', padding: 10, color: 'black' }}>Replanifier</Text>
                                <View style={{ flexDirection: 'row', margin: 10 }}>
                                    <Text style={{ paddingLeft: 0, fontSize: 14, color: 'black', fontWeight: 'bold' }}> Commande ID :  </Text>
                                    <Text style={{ paddingTop: 0 }}>#{this.state.request_no}</Text>
                                </View>
                                <View style={{ width: '95%', alignItems: 'center', marginTop: 20 }}>
                                    <TouchableOpacity onPress={this.showDate} style={{ width: '80%', backgroundColor: 'white', flexDirection: 'row', height: 40, borderRadius: 10, alignItems: 'center', borderWidth: 1, justifyContent: 'center' }}>
                                        <Image source={require("../assets/calender.png")}
                                            style={{ width: 20, height: 20, marginLeft: 2 }}
                                            resizeMode="contain" />
                                        <Text style={{ paddingLeft: 10 }}>{this.state.date_select}</Text>

                                        <DatePickers
                                            // mode="date"
                                            locale="en_GB" // Use "en_GB" here
                                            // is24Hour={false}
                                            isVisible={this.state.datePickervisible}
                                            onConfirm={this.handleDatePicked}
                                            onCancel={this.hideDateTimePicker}
                                        />
                                    </TouchableOpacity>
                                </View>

                                <View style={{ width: '95%', alignItems: 'center', marginTop: 20 }}>
                                    <TouchableOpacity onPress={this.showDateTimePicker} style={{ width: '80%', backgroundColor: 'white', flexDirection: 'row', height: 40, borderRadius: 10, alignItems: 'center', borderWidth: 1, justifyContent: 'center' }}>
                                        <Image source={require("../assets/clock.png")}
                                            style={{ width: 30, height: 20, }}
                                            resizeMode="contain" />
                                        <Text>{this.state.time}</Text>
                                        <DateTimePicker
                                            mode="time"
                                            locale="en_GB" // Use "en_GB" here
                                            is24Hour={true}
                                            isVisible={this.state.isDateTimePickerVisible}
                                            onConfirm={this._handleDatePicked}
                                            onCancel={this.hideDateTimePicker}
                                        />
                                    </TouchableOpacity>
                                </View>

                                <View style={{ width: '95%', flexDirection: 'row', borderRadius: 10 }}>
                                    <View style={{ width: '50%', alignItems: 'center' }}>
                                        <TouchableOpacity onPress={() => this.RescheduleRBSheet.close()} style={{ width: '95%', height: 40, backgroundColor: 'red', borderRadius: 10, marginTop: 20, alignItems: 'center', justifyContent: 'center' }}>
                                            <Text style={{ color: 'white' }}>{"Annuler"}</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ width: '50%', alignItems: 'center' }}>
                                        <TouchableOpacity onPress={() => this.user_reschedule_job()} style={{ width: '90%', height: 40, backgroundColor: Strings.color_green_code, borderRadius: 10, marginTop: 20, alignItems: 'center', justifyContent: 'center' }}>
                                            <Text style={{ fontSize: 16, color: 'white' }}>{"Soumettre"}</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>

                        </View>
                    </RescheduleRBSheet>





                    <CancelRBSheet
                        ref={ref => { this.CancelRBSheet = ref; }}
                        height={500}
                        duration={200}
                        closeOnPressMask={false}
                        customStyles={{
                            container: {
                                alignItems: "center",
                                borderRadius: 10,
                                // backgroundColor: 'rgba(52, 52, 52, 0.8)'  
                                backgroundColor: 'transparent'
                            }
                        }}>
                        <View style={{ flexDirection: 'column', width: '90%', height: '100%', borderRadius: 10, alignItems: 'center', justifyContent: 'center' }}>
                            <ImageBackground source={require("../assets/Box(1).png")}
                                style={{ width: 360, height: 500, borderRadius: 50, alignItems: 'center' }}
                                resizeMode='stretch'>
                                <View style={{ width: '95%', height: 420, borderRadius: 10, alignItems: 'center' }}>
                                    <Text style={{ fontSize: 18, fontWeight: 'bold', marginTop: 30, padding: 10, textAlign: 'center' }}>{Strings.annuler_la_text}</Text>
                                    <View style={{ width: '95%', marginTop: 30, }}>
                                        <Text style={{ fontSize: 16, padding: 10, textAlign: 'center' }}>{Strings.etse_vous_text}</Text>

                                        <FlatList
                                            data={this.state.cancelDataList}
                                            showsVerticalScrollIndicator={false}
                                            renderItem={({ item, index }) => {
                                                return (
                                                    <View style={{ marginTop: 10 }}>
                                                        <TouchableOpacity
                                                            onPress={() => this.setState({ value: item.reason_id, cancel_Text: item.reason })}>
                                                            <View style={{ marginLeft: 30, flexDirection: 'row' }}>
                                                                <View style={{ width: '10%', justifyContent: 'center' }}>
                                                                    <View style={{ width: 20, height: 20, borderWidth: 1, borderRadius: 20 / 2, alignItems: 'center', justifyContent: 'center' }}>
                                                                        {this.state.value === item.reason_id && (<View style={{ width: 16, height: 16, borderRadius: 16 / 2, backgroundColor: '#F1C368', alignItems: 'center', justifyContent: 'center' }}>
                                                                            <Image source={require("../assets/right.png")}
                                                                                style={{ width: 10, height: 10, }}
                                                                                resizeMode="contain" />
                                                                        </View>)}
                                                                    </View>
                                                                </View>
                                                                <View style={{ alignItems: 'center', marginLeft: 20, justifyContent: 'center' }}>
                                                                    <Text>{item.reason}</Text>
                                                                </View>
                                                            </View>
                                                        </TouchableOpacity>
                                                    </View>
                                                )
                                            }
                                            }
                                        />
                                    </View>

                                    {this.state.value === 5 && (<View style={{ width: '100%', alignItems: 'center' }}>
                                        <Card style={{ width: '90%', height: 40, borderRadius: 10, backgroundColor: 'white', marginTop: 10, justifyContent: 'center' }}>
                                            <TextInput
                                                onChangeText={(type_rease) => this.setState({ type_rease, cancel_TextErr: '' })}
                                                placeholder="Type......" style={{ fontSize: 16, padding: 10 }}>{this.state.address}</TextInput>
                                        </Card>
                                    </View>)

                                    }
                                    {!!this.state.cancel_TextErr && (
                                        <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.cancel_TextErr}</Text>
                                    )}


                                    <View style={{ width: '95%', flexDirection: 'row' }}>
                                        <View style={{ width: '60%', alignItems: 'center' }}>
                                            <TouchableOpacity onPress={() => this.CancelRBSheet.close()} style={{ width: '95%', height: 40, backgroundColor: 'red', borderRadius: 10, marginTop: 20, alignItems: 'center', justifyContent: 'center' }}>
                                                <Text style={{ color: 'white' }}>{Strings.annuler_la_text}</Text>
                                            </TouchableOpacity>
                                        </View>
                                        <View style={{ width: '40%', alignItems: 'center' }}>
                                            <TouchableOpacity onPress={() => this.cancelValidation()} style={{ width: '90%', height: 40, backgroundColor: Strings.color_green_code, borderRadius: 10, marginTop: 20, alignItems: 'center', justifyContent: 'center' }}>
                                                <Text style={{ fontSize: 16, color: 'white' }}>{Strings.next_text}</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            </ImageBackground>
                        </View>
                    </CancelRBSheet>

                    <Loading
                        ref={"loading2"}
                        image={require("../assets/spinner-of-dots.png")}
                        // backgroundColor='transparent'
                        backgroundColor='white'
                        borderRadius={5}
                        size={100}
                        imageSize={80}
                        indicatorColor='gray'
                        easing={Loading.EasingType.ease} />


                </View>


            </View>
        );
    }
}


// {/* <View style={{ marginTop: 200 }} >
//                         {/* <CirclesLoader /> */}
//                         <BubblesLoader
//                             size={40} />

//                         {/* <ColorDotsLoader></ColorDotsLoader>

//                         <TextLoader text="Loading" /> */}
//                     </View> */}