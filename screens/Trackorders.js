import React from 'react';
import {
    StatusBar, ScrollView, Alert, TextInput, View,
    ActivityIndicator, ImageBackground, Text, Modal, Dimensions, TouchableOpacity, Linking, AsyncStorage, FlatList,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from "../styles/styles";
import Strings from '../strings/strings';
import { Card, Radio, CheckBox } from 'native-base';
import PopupDialog, {
    DialogTitle, DialogContent, DialogFooter, DialogButton, SlideAnimation, ScaleAnimation,
} from 'react-native-popup-dialog';

const { width, height } = Dimensions.get("window");

import Image from 'react-native-fast-image'


export default class Trackorders extends React.Component {

    
    constructor(props, context) {
        super(props);
        this.state = {
            order_id: '',
            modalVisible: false,
            animating: false,
            lodingDialog: false,
            user_id: '',
            token: '',
            data: '',
            data2: '',

        }
        console.log("currentOrderId::::" + this.props.currentOrderId)

    };


    componentDidMount() {
      //  this.getOrderStatus()
      this.timer = setInterval(()=> this.getOrderStatus(), 3000);


    }

    callAPIforcheckRating() {

        AsyncStorage.getItem("userid")
          .then(userid => {
            this.setState({ userid: userid });
    
    
         
    
    
              AsyncStorage.getItem("token")
                .then(token => {
                  var accessToken = JSON.parse(token);
                  this.setState({ token: accessToken });
    
    -
                //   this.setState({
                //     animating: true,
                //     lodingDialog: true,
    
                //   });
    
                  fetch(Strings.base_Url + "userLastOrder", {
                    method: 'POST',
                    headers: {
                      // 'Accept': 'application/json',
                      'Content-Type': 'application/json',
                      'User-Id': this.state.userid,
                      'token': this.state.token,
                    },
                    body: JSON.stringify({
                      user_id: this.state.userid
    
    
                    })
                  })
    
    
                    .then((response) => response.json())
                    .then((responseData) => {
                      console.log(" responseData userLastOrder resoJSon===" + JSON.stringify(responseData));
                      console.log("resoJSon userLastOrder===" + responseData.error);
                      if (responseData.error == "false") {
                        console.log("Error false massage userLastOrder" + JSON.stringify(responseData.past_order));
    
                        this.setState({
                          animating: false,
                          lodingDialog: false,
                        });
    
                        var do_rating = responseData.past_order[0].do_rating;
                        console.log("do_rating " , do_rating);
    
                        if(do_rating == 1)
                        {                
                            //  this.callAPIforBoat();
    
                        }
    
                        else
                        {

                          var id = responseData.past_order[0].id;
                          console.log("do_rating id " , id);
                          Actions.push("Rating_screen",{currentOrderId : id})
                        }
    
    
                      } else if (responseData.error == "true") {
    
                        if (responseData.errorMessage == 'Incompatibilité de jetons') {
                          console.log("resoJSon true===" + responseData.error);
    
                          this.setState({
                            animating: false,
                            lodingDialog: false,
    
                          });
    
    
                          Actions.push("Login")
    
                        }
                        else {
    
    
                          this.setState({
                            animating: false,
                            lodingDialog: false,
    
                          });
                         
    
    
    
                        }
                      }
    
                    }
                    )
    
                })
    
         
    
          })
    
      }

    componentWillUnmount() {
        this.timer = null; // here...
      }
      
    getOrderStatus = () => {


        AsyncStorage.getItem("userid")
            .then(userid => {
                this.setState({ user_id: userid });

                AsyncStorage.getItem("token")
                    .then(token => {
                        var accessToken = JSON.parse(token);
                        this.setState({ token: accessToken });
                        // this.setState({
                        //     animating: true,
                        //     lodingDialog: true,

                        // });

                        fetch(Strings.base_Url + "getOrderTrack", {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json',
                                'User-Id': this.state.user_id,
                                'token': this.state.token,
                            },
                            body: JSON.stringify({
                                order_id: this.props.currentOrderId,

                            })

                        })
                            .then((response) => response.json())
                            .then((responseData) => {
                                console.log(" responseData getFuelList resoJSon===" + JSON.stringify(responseData));
                                console.log("resoJSon getFuelList===" + responseData.error);
                                if (responseData.error == "false") {
                                    console.log("Error false massage getFuelList" + JSON.stringify(responseData.record));

                                    this.setState({
                                        animating: false,
                                        lodingDialog: false,
                                        data: responseData.order_record,
                                        data2: responseData.category_record,


                                    });

                                    if(responseData.order_record.status == 4 || responseData.order_record.status == 5)
                                    {
                                        clearInterval(this.timer);
                                        Actions.push("Rating_screen",{currentOrderId : this.props.currentOrderId})

                                      //  this.callAPIforcheckRating()

                                    }

                                    //this.RBSheet.close()
                                } else if (responseData.error == "true") {

                                    this.showalerts(responseData.errorMessage)
                                    this.setState({
                                        animating: false,
                                        lodingDialog: false

                                    });
                                }
                            }
                            )
                    })
            })
    }

    forChatdlg = (chatvalue) => {
        this.setState({
            modalVisible: false,

        })
        if (chatvalue == 'ChatwithAdmin') {

            Actions.push("ChatwithAdmin", { currentOrderId: this.props.currentOrderId })
        }

        else if (chatvalue == 'ChatwithTechnicien') {
            Actions.push("Chat", { currentOrderId: this.props.currentOrderId })
        }

    }




    render() {
        return (
            <View style={styles.containerWhite}>

                <View style={{ width: '100%', height: 50, flexDirection: 'row',marginTop: Platform.OS == 'ios' ? 20 :-5 }}>
                    <TouchableOpacity
                       onPress={() => Actions.push("MyOrders")} style={{ width: '15%', height: 50, justifyContent: 'center', alignItems: 'center', }}>
                        <Image source={require("../assets/back.png")}
                            style={{ width: 25, height: 25, }}
                            resizeMode="contain" />
                    </TouchableOpacity>
                    <View style={{ width: '70%', height: 50, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontSize: 15, fontFamily: 'Poppins-Medium' }}>Suivi de la commande</Text>

                    </View>
                    <TouchableOpacity
                        onPress={() => this.setState({ modalVisible: true, })}
                        style={{ width: '15%', height: 50, justifyContent: 'center', alignItems: 'center', }}>
                        <Image source={require("../assets/chat_icon.png")}
                            style={{ width: 25, height: 25, }}
                            resizeMode="contain" />
                    </TouchableOpacity>

                </View>


                <ScrollView style={{}}>

                    <Text style={{ fontFamily: 'Poppins-Regular', fontSize: 11, textAlign: 'center',
                     color: '#28528F' }}>Délai de livraison estimé</Text>
                    <Text style={{ fontFamily: 'Poppins-Bold', fontSize: 28, textAlign: 'center', 
                    color: '#28528F' }}>{this.state.data.reservation_date}</Text>
                   
                   <Text style={{ fontFamily: 'Poppins-Bold', fontSize: 28, textAlign: 'center', 
                    color: '#28528F' }}>{this.state.data.reservation_time}</Text>
                    
                    <View style={{ width: width * 88 / 100, flexDirection: 'row', alignSelf: 'center', }}>
                        <View style={{ alignItems: 'center', marginTop: 20, marginRight: width * 2 / 100 }}>
                            <View style={{ width: 12, height: 12, borderRadius: 15 / 2, backgroundColor: '#28528F' }}></View>


                            {this.state.data.status == 2 || this.state.data.status == 3 ||this.state.data.status == 4 || this.state.data.status == 5 ?
                                <View style={{ alignItems:'center',  }}>


                                    <View style={{ height: 35, borderWidth: 1, borderColor: '#28528F', width: 1 }}></View>

                                    <View style={{ width: 12, height: 12, borderRadius: 15 / 2, backgroundColor: '#28528F' }}></View>
                                </View>


                                :
                                <View style={{ alignItems:'center',  }}>
                                    <View style={{ height: 35, borderWidth: 1, borderColor: '#grey', width: 1 ,}}></View>

                                    <View style={{ width: 12, height: 12, borderRadius: 15 / 2, backgroundColor: 'grey' }}></View>
                                </View>


                            }

                            {this.state.data.status == 4 || this.state.data.status == 5 ?



                                <View style={{ alignItems:'center',  }}>


                                    <View style={{ height: 35, borderWidth: 1, borderColor: '#28528F', width: 1 }}></View>
                                    <View style={{ width: 12, height: 12, borderRadius: 15 / 2, backgroundColor: '#28528F' }}></View>
                                </View>
                                :
                                <View style={{alignItems:'center',  }}>
                                    <View style={{ height: 35, borderWidth: 1, borderColor: '#grey', width: 1 }}></View>

                                    <View style={{ width: 12, height: 12, borderRadius: 15 / 2, backgroundColor: 'grey' }}></View>
                                </View>


                            }




                        </View>

                        <View>
                            <View style={{ flexDirection: 'row', marginTop: 20, width: width * 85 / 100, alignSelf: 'center' }}>
                                <Image source={require('../assets/7.png')} style={{ width: 25, height: 25 }} />

                                {this.state.data.order_status == 1 ?

                                    // <Text style={{ marginLeft: 20, fontFamily: 'Poppins-Medium', fontSize: 12 }} >Accepté</Text>
                                    <Text style={{ marginLeft: 20, fontFamily: 'Poppins-Medium', fontSize: 12 }} >Commande confirmée</Text>

                                    
                                    :
                                    null}


                                {this.state.data.order_status == 0 ?

                                    // <Text style={{ marginLeft: 20, fontFamily: 'Poppins-Medium', fontSize: 12 }} >  Commande confirmée</Text>
                                    <Text style={{ marginLeft: 20, fontFamily: 'Poppins-Medium', fontSize: 12 }} >Confirmation en cours</Text>

                                    
                                    :
                                    null}


                            </View>

                            <View style={{ flexDirection: 'row', marginTop: 20, width: width * 85 / 100, alignSelf: 'center' }}>
                                <Image source={require('../assets/12.png')} style={{ width: 25, height: 25 }} />
                                <Text style={{ marginLeft: 20, fontFamily: 'Poppins-Medium', fontSize: 12 }} >Fill Your Boat arrive</Text>
                            </View>



                            <View style={{ flexDirection: 'row', marginTop: 20, width: width * 85 / 100, alignSelf: 'center' }}>
                                <Image source={require('../assets/121.png')} style={{ width: 25, height: 20 }} />
                                <Text style={{ marginLeft: 20, fontFamily: 'Poppins-Medium', fontSize: 12 }} >Votre plein est fait, Naviguez !</Text>
                            </View>
                        </View>
                    </View>
                    <View style={{ width: width * 100 / 100, alignSelf: 'center', height: 40, backgroundColor: '#FFD773', borderRadius: 20, marginTop: 20, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontFamily: 'Poppins-SemiBold', color: '#28528F', textAlign: 'center', 
                        fontSize: width * 3.1 / 100 }}>Tout pour une journée en mer parfaite </Text>
                    </View>
                    {/* <Text>{this.state.order_id}</Text>


                    <View style={{ flexDirection: 'row' }}>
                        <Image source={require("../assets/back.png")}
                            style={{ width: 25, height: 25, }}
                            resizeMode="contain" />
                        <Text>{this.state.data.status_message}</Text>

                    </View> */}

                    {this.state.data2.length > 0 ?

                        <FlatList
                            keyExtractor={item => item.id}

                            data={this.state.data2}
                            renderItem={({ item, index }) => (
                                <TouchableOpacity 
                                style={{ justifyContent: 'center', alignItems: 'center', marginTop: 10 }}
                                onPress={() =>Actions.push("Advertisementlist", { category_id : item.id })}>
                                   <Card style={{
                                       width: width*74/100, justifyContent: 'space-between',
                                       alignItems: 'center', flexDirection: 'column', height:42, borderRadius: 30, backgroundColor: '#1A578A'
                                   }}>
                                       <View style={{ flexDirection: 'row',height:42,width:width*72/100,alignItems:'center'}}>
                                           <Image
                                               // source={require("../assets/image.png")}
                                               source={{ uri: item.image }}
                                               style={{ width: 36, height: 36,borderRadius:18 }} />


                                           <Text style={{ fontSize: 11,fontFamily:'Poppins-SemiBold',width:width*72/100-60, color: 'white',marginLeft:5,textAlign:'center' }}>{item.cat_name}</Text>

                                       </View>
                                   </Card>
                               </TouchableOpacity>

                            )}
                        />
                        :
                        <Text style={{ fontFamily: "Poppins-Medium", marginTop: 10, }}>Aucune donnée</Text>

                    }




                    <Modal
                        animationType={'fade'}
                        transparent={true}
                        visible={this.state.modalVisible}
                        onRequestClose={() => {
                            this.setState({ modalVisible: false });
                        }}
                    >

                        <View
                            style={{
                                position: 'absolute',
                                width: width,
                                height: '100%',
                                alignItems: 'center',
                                justifyContent: 'center',
                                backgroundColor: 'rgba(250,250,250,0.5)',
                                padding: 20,
                            }}
                        >
                            <Image
                                resizeMode={'stretch'}
                                source={require('../assets/bg_image.png')}
                                style={{ width: width * 85 / 100, padding: 10, alignSelf: 'center' }}>

                                <Image source={require("../assets/12.png")}
                                    style={{ width: width * 27 / 100, height: width * 27 / 100, borderRadius: 50, alignSelf: 'center' }}
                                />
                                <View>
                                    <TouchableOpacity
                                        style={{
                                            height: 35, marginTop: 10, backgroundColor: 'rgba(26,87,138,1)', width: width * 70 / 100,
                                            marginBottom: 10, borderRadius: 10, justifyContent: 'center', alignItems: 'center', alignSelf: 'center'
                                        }}
                                        onPress={() => this.forChatdlg('ChatwithAdmin')}>
                                        <Text style={{
                                            fontSize: width * 3.7 / 100, color: 'white',
                                            textAlign: 'center', fontFamily: 'Poppins-SemiBold'
                                        }}>Chat avec l'administration</Text>

                                    </TouchableOpacity>


                                    {/* <TouchableOpacity
                                    onPress={() => this.forChatdlg('ChatwithTechnicien')}
                                    style={{
                                        height: 50, backgroundColor: 'rgba(26,87,138,1)',
                                        marginBottom: 10, borderRadius: 10, marginRight: 20
                                    }}>

                                    <Text style={{ fontSize: 14, fontWeight: 'bold', color: 'white', }}>Chat avec un technicien </Text>
                                </TouchableOpacity> */}

                                    {this.state.data.order_status == 1 ?

                                        <TouchableOpacity
                                            style={{
                                                height: 35, marginTop: 10, backgroundColor: 'rgba(26,87,138,1)', width: width * 70 / 100,
                                                marginBottom: 10, borderRadius: 10, justifyContent: 'center', alignItems: 'center', alignSelf: 'center'
                                            }}
                                            onPress={() => this.forChatdlg('ChatwithTechnicien')}>
                                            <Text style={{
                                                fontSize: width * 3.7 / 100, color: 'white',
                                                textAlign: 'center', fontFamily: 'Poppins-SemiBold'
                                            }}>Chat avec un technician</Text>

                                        </TouchableOpacity>

                                        :
                                        null


                                    }


                                    {/* <TouchableOpacity

                                    onPress={() => this.setState({ modalVisible: false })}
                                    style={{
                                        flexDirection: 'row', height: 50, backgroundColor: 'rgba(26,87,138,1)',
                                        marginBottom: 20, borderRadius: 10, marginRight: 20
                                    }}>

                                    <Text style={{ fontSize: 14, fontWeight: 'bold', color: 'white', }}>Annuler </Text>
                                </TouchableOpacity> */}
                                    <TouchableOpacity
                                        style={{
                                            height: 35, marginTop: 10, backgroundColor: 'rgba(26,87,138,1)', width: width * 70 / 100,
                                            marginBottom: 20, borderRadius: 10, justifyContent: 'center', alignItems: 'center', alignSelf: 'center'
                                        }}
                                        onPress={() => this.setState({ modalVisible: false })}>
                                        <Text style={{
                                            fontSize: width * 3.7 / 100, color: 'white',
                                            textAlign: 'center', fontFamily: 'Poppins-SemiBold'
                                        }}>Annuler</Text>

                                    </TouchableOpacity>

                                </View>





                            </Image>
                        </View>
                    </Modal>
                    <PopupDialog
                        onHardwareBackPress={() => { this.setState({ lodingDialog: false }) }}
                        width={0.3}
                        visible={this.state.lodingDialog}
                        dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
                        <DialogContent>
                            <View style={{ alignItems: 'center', }}>
                                <ActivityIndicator
                                    animating={this.state.animating}
                                    style={[{ height: 10, marginBottom: 10, marginTop: 30, marginLeft: 20, marginRight: 20 }]}
                                    color="#C00"
                                    size="large"
                                    hidesWhenStopped={true}
                                />
                            </View>
                        </DialogContent>
                    </PopupDialog>

                </ScrollView>

            </View>
        );
    }


}