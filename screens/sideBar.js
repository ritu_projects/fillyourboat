import React, { Component } from "react";
import {  Alert, AsyncStorage, TouchableOpacity, ImageBackground, ScrollView, ActivityIndicator, Linking, } from "react-native";
import {
    Content,
    Text,
    List,
    ListItem,
    Icon,
    Container,
    Left,
    Right,
    Badge,
    View,
    Button

} from "native-base";

import PopupDialog, {
    DialogTitle, DialogContent, DialogFooter, DialogButton, SlideAnimation, ScaleAnimation,
} from 'react-native-popup-dialog';
import { Actions } from "react-native-router-flux";
import strings from '../strings/strings'
import Images from 'react-native-image-progress';
import Image from 'react-native-fast-image'

let sources;


class SideBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            shadowOffsetWidth: 1,
            shadowRadius: 4,
            emailId: '',
            emailUser: '',
            name: '',
            ProfileImage: '',
            userid: '',
            userName: '',
            email: '',
            profileImage: '',
            first_name: '',
            last_name: '',
            profile_pic: '',
            animating: false,
            lodingDialog: false,
        };
    }

    logout() {
        console.log("logout:::")
        AsyncStorage.setItem("userid", '');
        AsyncStorage.setItem("token", '');
        AsyncStorage.setItem("userName", '');
        AsyncStorage.setItem("email", '');
        AsyncStorage.setItem("password", '')

        Actions.push("Login")
    }
    logoutAlert() {
        Alert.alert(
            strings.alert,
            strings.Areyousureyouwanttologout,

            [
                {
                    text: strings.logoutText,
                    onPress: () => { this.logout() },
                    style: 'cancel',
                },
                { text: strings.cancelText, onPress: () => console.log('OK Pressed') },
            ],
            { cancelable: false },
        )
    }

    componentDidMount() {
        this.getProfile();

    }

    //   componentWillReceiveProps() {
    //     this.getProfile();

    //   }

    getProfile = () => {
        this.setState({
            animating: true,
            lodingDialog: true,

        })
        AsyncStorage.getItem("userid")
            .then(userid => {
                this.setState({ user_id: userid });

                AsyncStorage.getItem("token")
                    .then(token => {
                        var accessToken = JSON.parse(token);
                        this.setState({ token: accessToken });
                        console.log("accessToken====$$$========" + accessToken);
                        console.log("state userId====$$$========" + userid);

                        fetch(strings.base_Url + "getProfile", {
                            method: 'POST',
                            headers: {
                                // 'Accept': 'application/json',
                                'Content-Type': 'application/json',
                                'User-Id': userid,
                                'token': accessToken,
                            },
                            body: JSON.stringify({
                                user_id: userid,
                            })
                        })
                            .then((response) => response.json())
                            .then((responseData) => {
                                console.log(" responseData getProfile resoJSon===" + JSON.stringify(responseData));
                                console.log("resoJSon===" + responseData.error);
                                if (responseData.error == "false") {
                                    console.log("Error false massage" + JSON.stringify(responseData.errorMessage));
                                    // var image=responseData.usersDetails.image
                                    // let userName=JSON.stringify(responseData.usersDetails.userName)
                                    this.setState({
                                        animating: false,
                                        lodingDialog: false,
                                    })

                              
                                        sources = responseData.usersDetails.fullimage;
                                        console.log("Sources ", sources)
                                    
                                    this.setState({
                                        animating: false,
                                        lodingDialog: false,
                                        first_name: responseData.usersDetails.firstname,
                                        last_name: responseData.usersDetails.lastname,
                                        email: responseData.usersDetails.email,
                                        profile_pic: sources
                                    });
                                } else if (responseData.error == "true") {
                                    this.setState({
                                        passwordErr: responseData.errorMessage,
                                        animating: false,
                                        lodingDialog: false

                                    });
                                }
                            }
                            )
                    });
            });
    }

    openURL = () => {
        Linking.openURL('http://www.fillyourboat.fr/')

    }
    render() {
        return (
            <Container>
                <Content
                    bounces={false}
                    style={{ flex: 1, }}>
                    <ScrollView>


                        <Image source={require("../assets/menu_header.png")}
                            style={{ width: '100%', resizeMode: 'stretch', height: 150 }}
                        />
                        <TouchableOpacity onPress={() => Actions.push('Location')} style={{ marginLeft: 20, marginTop: -130 }}>
                            <Image source={require('../assets/houseicon.png')} style={{ height: 19, width: 19, }} />
                        </TouchableOpacity>
                        <View style={{ alignItems: 'center' }}>
                            <View style={{ flex: 1, flexDirection: 'row', width: 80, height: 80, 
                            marginTop: 50, backgroundColor: 'white', borderRadius: 250 / 2, 
                            marginLeft: -140, alignItems: 'center', justifyContent: 'flex-start' }}>

                                {this.state.profile_pic == 'https://www.fillyourboat.net/public/upload/usersprofile/' ?

                                    <Image
                                    style ={{ width: 70, height: 70, borderRadius: 35, marginTop: -40 }}

                                        source={require('../assets/default_profile_pic.png')}
                                    />

                                    :
                                    <Image 
                                    source={{ uri: this.state.profile_pic}}
                                        style={{ width: 70, height: 70, borderRadius: 35, marginTop: -40 }}

                                    />
                                }

                                <View style={{ flexDirection: 'column', width: 250, marginLeft: 90 }}>
                                    <Text style={{ fontSize: 14, fontWeight: 'bold', color: '#1A578A' }}>{this.state.first_name + " " + this.state.last_name}</Text>
                                    <Text style={{ fontSize: 12, color: '#1A578A' }}>{this.state.email}</Text>
                                </View>
                            </View>


                            <View style={{ width: '100%', flexDirection: 'column' }}>



                                <TouchableOpacity onPress={() => Actions.push("MyBoats")}
                                    style={{ marginTop: 20, height: 30, marginLeft: 20, flexDirection: 'row', alignItems: 'center' }}>
                                    <Image source={require("../assets/menu_boat.png")}
                                        style={{
                                            width: 20, height: 20,
                                            resizeMode: 'stretch'
                                        }} ></Image>
                                    <Text style={{ fontSize: 12, marginLeft: 10, fontFamily: 'Poppins-Medium' }}>{strings.myboat_text1}</Text>
                                </TouchableOpacity>


                                <TouchableOpacity onPress={() => Actions.push("MyOrders")}
                                    style={{ marginTop: 20, height: 30, marginLeft: 20, flexDirection: 'row', alignItems: 'center' }}>
                                    <Image source={require("../assets/myorders_menu.png")}
                                        style={{ width: 20, height: 20, resizeMode: 'stretch' }} ></Image>
                                    <Text style={{ fontSize: 12, marginLeft: 10, fontFamily: 'Poppins-Medium' }}>mes commandes...</Text>
                                </TouchableOpacity>

                                <TouchableOpacity onPress={() => Actions.push("Profile")}
                                    style={{ marginTop: 20, height: 30, marginLeft: 20, flexDirection: 'row', alignItems: 'center' }}>
                                    <Image source={require("../assets/user-2.png")}
                                        style={{
                                            width: 20, height: 20,
                                            resizeMode: 'stretch'
                                        }} ></Image>
                                    <Text style={{ fontSize: 12, marginLeft: 10, fontFamily: 'Poppins-Medium' }}>{strings.profile_text1}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => Actions.push("FuelRates")}
                                    style={{ marginTop: 20, height: 30, marginLeft: 20, flexDirection: 'row', alignItems: 'center' }}>
                                    <Image source={require("../assets/22.png")}
                                        style={{
                                            width: 20, height: 20,
                                            resizeMode: 'stretch'
                                        }} ></Image>
                                    <Text style={{ fontSize: 12, marginLeft: 10, fontFamily: 'Poppins-Medium' }}>Tarif</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => Actions.push("Payment_Drawe")}
                                    style={{ marginTop: 20, height: 30, marginLeft: 20, flexDirection: 'row', alignItems: 'center' }}>
                                    <Image source={require("../assets/myorders_menu.png")}
                                        style={{ width: 20, height: 20, resizeMode: 'stretch' }} ></Image>
                                    <Text style={{ fontSize: 12, marginLeft: 10, fontFamily: 'Poppins-Medium' }}>{strings.payment_text}</Text>
                                </TouchableOpacity>

                                <TouchableOpacity onPress={() => Actions.push("MyPartners")}
                                    style={{ marginTop: 20, height: 30, marginLeft: 20, flexDirection: 'row', alignItems: 'center' }}>
                                    <Image source={require("../assets/35.png")}
                                        style={{ width: 20, height: 20, resizeMode: 'stretch' }} ></Image>
                                    <Text style={{ fontSize: 12, marginLeft: 10, fontFamily: 'Poppins-Medium' }}>{strings.partners_text}</Text>
                                </TouchableOpacity>

                                <TouchableOpacity
                                    // onPress={() => Actions.push("Contactss")} 
                                    onPress={() => this.openURL()}

                                    style={{ marginTop: 20, height: 30, marginLeft: 20, flexDirection: 'row', alignItems: 'center' }}>
                                    <Image source={require("../assets/24.png")}
                                        style={{ width: 20, height: 20, resizeMode: 'stretch' }} ></Image>
                                    <Text style={{ fontSize: 12, marginLeft: 10, fontFamily: 'Poppins-Medium' }}>{strings.contact_text}</Text>
                                </TouchableOpacity>

                                {/* <TouchableOpacity onPress={() => Actions.push("FAQ")}  style={{ marginTop: 10, height: 40, marginLeft: 25, flexDirection: 'row', alignItems: 'center' }}>
                                    <Image source={require("../assets/Artboard8.png")}
                                        style={{ width: 30, height: 30,resizeMode:'stretch' }} ></Image>
                                    <Text style={{ fontSize: 16, marginLeft: 10,  }}>{"FAQ"}</Text>
                                </TouchableOpacity> */}

                                <TouchableOpacity onPress={() => this.logoutAlert()}
                                    style={{ marginTop: 20, marginBottom: 20, height: 30, marginLeft: 20, flexDirection: 'row', alignItems: 'center' }}>
                                    <Image source={require("../assets/logout.png")}
                                        style={{ width: 20, height: 20, resizeMode: 'stretch' }} ></Image>
                                    <Text style={{ fontSize: 12, marginLeft: 10, fontFamily: 'Poppins-Medium' }}>{strings.logout_text}</Text>
                                </TouchableOpacity>


                            </View>
                        </View>
                    </ScrollView>
                </Content>
            </Container>
        );
    }
}

export default SideBar