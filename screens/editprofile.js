import React from 'react';
import { StatusBar, Button, Alert, FlatList, ActivityIndicator, ScrollView, PixelRatio, TextInput, View, ImageBackground, Text, Dimensions, TouchableOpacity, Linking, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from "../styles/styles";
import Strings from '../strings/strings'
import ImagePicker from 'react-native-image-picker';
import { Card } from 'native-base';
const { width, height } = Dimensions.get("window");
import PopupDialog, {
    DialogTitle, DialogContent, DialogFooter, DialogButton, SlideAnimation, ScaleAnimation,
} from 'react-native-popup-dialog';
import RNFetchBlob from 'rn-fetch-blob';
import DateTimePicker from "react-native-modal-datetime-picker";
import Moment from 'moment';
import { Dropdown } from 'react-native-material-dropdown';

let drop_down_data = [{
    value: 'Homme'
}, {
    value: 'Femme'
},];;
let sex_gender;
let sources;
import Image from 'react-native-fast-image'

export default class editprofile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isDateTimePickerVisible: false,
            first_name: '',
            last_name: '',
            address: '',
            email: '',
            gender: '',
            telephone: '',
            Anniversarie: '',
            userid: '',
            first_nameErr: '',
            last_nameErr: '',
            telephoneErr: '',
            errEmail: '',
            addressErr: '',
            AnniversarieErr: '',
            genderErr: '',
            user_id: '',
            token: '',
            avatar: '',
            fsPath: '',
            profilePic: '',
        };

    }

    componentDidMount() {
        this.setState({
            animating: true,
            lodingDialog: true,

        })
        AsyncStorage.getItem("userid")
            .then(userid => {
                this.setState({ user_id: userid });

                AsyncStorage.getItem("token")
                    .then(token => {
                        var accessToken = JSON.parse(token);
                        this.setState({ token: accessToken });
                        console.log("accessToken====$$$========" + accessToken);
                        console.log("state userId====$$$========" + userid);

                        fetch(Strings.base_Url + "getProfile", {
                            method: 'POST',
                            headers: {
                                // 'Accept': 'application/json',
                                'Content-Type': 'application/json',
                                'user-id': this.state.user_id,
                                'token': this.state.token,
                            },
                            body: JSON.stringify({
                                user_id: this.state.user_id,
                            })
                        })
                            .then((response) => response.json())
                            .then((responseData) => {
                                console.log(" responseData resoJSon===" + JSON.stringify(responseData));
                                console.log("resoJSon===" + responseData.error);
                                if (responseData.error == "false") {
                                    console.log("Error false massage" + JSON.stringify(responseData.errorMessage));

                                    //  sources = { uri: responseData.usersDetails.fullimage };
                                    sources = responseData.usersDetails.fullimage;


                                    this.setState({
                                        animating: false,
                                        lodingDialog: false,

                                        first_name: responseData.usersDetails.firstname,
                                        last_name: responseData.usersDetails.lastname,
                                        address: responseData.usersDetails.address,
                                        email: responseData.usersDetails.email,
                                        telephone: responseData.usersDetails.telephone,
                                        avatar: responseData.usersDetails.fullimage,
                                        profilePic: sources,
                                    });
                                    console.log('avatar  ' + responseData.usersDetails.fullimage)
                                } else if (responseData.error == "true") {
                                    this.setState({
                                        passwordErr: responseData.errorMessage,
                                        animating: false,
                                        lodingDialog: false

                                    });
                                }
                            }
                            )
                    });
            });
    }




    showDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: true });
    };

    hideDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: false });
    };

    handleDatePicked = date => {

        console.log("A date has been picked: ", date);

        var date_select = Moment(date).format('DD/MM/YYYY');
        this.setState({
            Anniversarie: date_select
        })
        this.hideDateTimePicker();
    };

    validation() {

        var isValidate = 0;
        if (this.state.first_name != '') {
            isValidate += 1
        } else {
            isValidate -= 1
            this.setState({ first_nameErr: Strings.ShouldemptyText })
        }
        // if(this.state.address != ''){
        //     isValidate += 1
        // }else{
        //     isValidate -= 1
        //     this.setState({ addressErr: Strings.ShouldemptyText }) 
        // }
        if (this.state.last_name != "") {
            isValidate += 1
        } else {
            isValidate -= 1
            this.setState({ last_nameErr: Strings.ShouldemptyText })
        }
        if (this.state.telephone != "") {
            isValidate += 1
        } else {
            isValidate -= 1
            this.setState({ telephoneErr: Strings.ShouldemptyText })
        }
        if (this.state.email != "") {
            isValidate += 1
            if (this.validateEmail(this.state.email)) {
                isValidate += 1;
            } else {
                isValidate -= 1;
                this.setState({
                    errEmail: "S'il vous plaît entrer email valide",
                });
            }
        } else {
            isValidate -= 1
            this.setState({ errEmail: Strings.ShouldemptyText })
        }




        console.log("isvalidate::" + isValidate)

        if (isValidate == 5) {
            this.editProfile();
        }


    }

    validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    editProfile() {
        AsyncStorage.getItem("userid")
            .then(userid => {
                this.setState({ user_id: userid });

                AsyncStorage.getItem("token")
                    .then(token => {
                        var accessToken = JSON.parse(token);
                        this.setState({ token: accessToken });
                        this.setState({
                            animating: true,
                            lodingDialog: true,
                        })
                        let headers = {
                            'Content-Type': 'multipart/form-data',
                            'User-Id': this.state.user_id,
                            'token': this.state.token,
                        };

                        RNFetchBlob.fetch('POST', Strings.base_Url + 'edituserprofile', headers, [
                            { name: 'user_id', data: this.state.user_id },
                            { name: 'firstname', data: this.state.first_name },
                            { name: 'email', data: this.state.email },

                            { name: 'lastname', data: this.state.last_name },
                            { name: 'telephone', data: this.state.telephone },

                            //   { name: 'image', filename: 'photo.jpg', type: 'image/png', data: RNFetchBlob.wrap(this.state.fsPath)},
                            { name: 'image', filename: 'photo.jpg', type: 'image/png', data: this.state.fsPath },

                        ],
                        ).then((resp) => {

                            console.log("response:::::::" + JSON.stringify(resp.json()));



                            if (resp.json().error === "false") {
                                this.setState({
                                    animating: false,
                                    lodingDialog: false,
                                });
                                //  this.componentDidMount()
                                Actions.push('Location')
                            } else if (resp.json().error === "true") {
                                // alert(resp.json().errorMessage)
                                this.showalerts(resp.json().errorMessage)
                                this.setState({
                                    animating: false,
                                    lodingDialog: false,
                                });

                            }
                        }).catch((err) => {
                            this.setState({
                                animating: false,
                                lodingDialog: false,
                            });
                            console.log("response::::err:::" + err);
                        });
                    })
            })
    }

    showalerts(body) {
        Alert.alert(
            Strings.alert_tital,
            body,
            [
                { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
            ],
            { cancelable: false });
    }

    valueExtractor = val => {
        console.log("vehicle id::::****:$$::" + JSON.stringify(val));
    };
    onChangeTextPress(id) {

        if (id == "Homme") {
            sex_gender = "1"
        } else if (id == "Femme") {
            sex_gender = "2"
        }

        console.log("vehicle id::::****:::" + id);
    }

    selectProfilePic = () => {
        const options = {
            title: 'Select Avatar',
            storageOptions: {
                skipBackup: true,
                path: 'images',
                profilePic: '',
            },
        };

        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                const source = { uri: response.uri };

                const imageUrl = response.uri
                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };
                AsyncStorage.setItem("profilePic", JSON.stringify(source));
                this.setState({
                    profilePic: response.uri,
                    isImageAvailable: true,
                    image_Url: response.uri,
                    // fsPath: response.uri,
                    fsPath: response.data,
                });
                console.log("image url::" + JSON.stringify(imageUrl))
                console.log("image url::" + JSON.stringify(source))
            }
        });
    }


    onFocusChange = event => {
        this.setState({ isFocused: true, });
        if (this.props.onFocus) {
            this.props.onFocus(event);
        }
    };
    onFocusChange1 = event => {
        this.setState({ isFocused1: true, });
        if (this.props.onFocus) {
            this.props.onFocus(event);
        }
    };
    onFocusChange2 = event => {
        this.setState({ isFocused2: true, });
        if (this.props.onFocus) {
            this.props.onFocus(event);
        }
    };

    render() {
        return (
            <View style={styles.containerWhite}>
               
                <ScrollView
                    style={{
                        paddingTop: Platform.OS === 'ios' ? 20 : 0
                    }}>
                    <Image source={require("../assets/100.png")}
                        style={{
                            width: width,
                            height: height,
                            resizeMode: 'stretch',

                        }}
                        resizeMode="stretch">
                        <View style={{ width: width * 95 / 100, marginLeft: 5, marginTop: Platform.OS == 'ios' ? 20 :7, justifyContent: 'space-between', flexDirection: 'row', }}>
                            <TouchableOpacity onPress={() => Actions.pop()} style={{}}>
                                <Image source={require("../assets/back.png")}
                                    style={{ width: 16, height: 14, }}
                                    resizeMode="stretch" />
                            </TouchableOpacity>
                            <View style={{}}>
                                <Text style={{ fontSize: 16, color: 'white', fontWeight: 'bold' }}>Modifier le profil</Text>
                            </View>
                            <TouchableOpacity onPress={() => Actions.push("EditProfile")} style={{ marginRight: 5, }}>
                                <View style={{ width: 25, height: 25, }} />
                            </TouchableOpacity>

                        </View>

                        <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }}>

                            <TouchableOpacity onPress={this.selectProfilePic} style={{ width: 95, height: 95, backgroundColor: 'white', borderRadius: 95 / 2, marginTop: height * 5 / 100, alignItems: 'center', justifyContent: 'center' }}>
                                {this.state.profilePic == '' || this.state.profilePic == 'https://www.fillyourboat.net/public/upload/usersprofile/' ?
                                    <Image
                                        source={require('../assets/default_profile_pic.png')}
                                        style={{ width: 95, height: 95, borderRadius: 95 / 2 }}
                                    /> :
                                    <Image source={{ uri: this.state.profilePic }}
                                        style={{ width: 95, height: 95, borderRadius: 95 / 2 }}
                                    />}
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.selectProfilePic} style={{ width: 25, height: 25, backgroundColor: Strings.color_green_code, borderRadius: 25 / 2, marginTop: -35, marginLeft: 81, alignItems: 'center', justifyContent: 'center' }}>
                                <Image
                                    source={require("../assets/pencil.png")}
                                    style={{ width: 15, height: 15, }}
                                    resizeMode="contain" />
                            </TouchableOpacity>

                            <View style={{ width: '80%', flexDirection: 'column', marginTop: 60 }}>
                                {/* <Text style={{ fontSize: 16, paddingLeft: 10 }}>{Strings.prenom_text}</Text> */}
                                <View style={{ width: '100%', marginTop: 15, justifyContent: 'center' }}>
                                    <TextInput value={this.state.last_name}
                                        onFocus={this.onFocusChange}
                                        onBlur={this.handleBlur}
                                        underlineColorAndroid={this.state.isFocused ? '#1A578A' : 'grey'}

                                        onChangeText={(last_name) => this.setState({ last_name, first_nameErr: '' })}
                                        placeholder="Nom" 
                                        style={{ fontSize: 14, padding: 10, fontFamily: 'Poppins-Medium', }}></TextInput>
                                </View>
                                {!!this.state.first_nameErr && (
                                    <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.first_nameErr}</Text>
                                )}

                                {/* <Text style={{ fontSize: 16, paddingLeft: 10, marginTop: 30 }}>{Strings.Nom_de_familee}</Text> */}
                                <View style={{ width: '100%', borderRadius: 10, backgroundColor: 'white', marginTop: 15, justifyContent: 'center' }}>
                                    <TextInput value={this.state.first_name}
                                        onChangeText={(first_name) => this.setState({ first_name, last_nameErr: '' })}
                                        onFocus={this.onFocusChange1}
                                        onBlur={this.handleBlur}
                                        underlineColorAndroid={this.state.isFocused ? '#1A578A' : 'grey'}

                                        placeholder="Prénom" style={{ fontSize: 14, padding: 10, fontFamily: 'Poppins-Medium' }}></TextInput>
                                </View>
                                {!!this.state.last_nameErr && (
                                    <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.last_nameErr}</Text>
                                )}

                                {/*                             
                            <View style={{ width: '100%', height: 40, borderRadius: 10, backgroundColor: 'white', marginTop: 15, justifyContent: 'center' }}>
                                <TextInput value={this.state.address}
                                    onChangeText={(address) => this.setState({ address, addressErr: '' })}
                                    onFocus={this.onFocusChange2}
                                    onBlur={this.handleBlur}
                                    underlineColorAndroid={this.state.isFocused ? '#1A578A' : 'grey'}
                                   
                                    placeholder="Nom du bateau" style={{fontSize: 14, padding: 10,fontFamily:'Poppins-Medium'}}></TextInput>
                            </View>

                            {!!this.state.addressErr && (
                                <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.addressErr}</Text>
                            )} */}
                                <View style={{ width: '100%', borderRadius: 10, backgroundColor: 'white', marginTop: 15, justifyContent: 'center' }}>
                                    <TextInput value={this.state.telephone}
                                        onChangeText={(telephone) => this.setState({ telephone, telephoneErr: '' })}
                                        onFocus={this.onFocusChange3}
                                        onBlur={this.handleBlur}
                                        underlineColorAndroid={this.state.isFocused ? '#1A578A' : 'grey'}

                                        placeholder="Téléphone" style={{ fontSize: 14, padding: 10, fontFamily: 'Poppins-Medium' }}></TextInput>
                                </View>

                                {!!this.state.telephoneErr && (
                                    <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.telephoneErr}</Text>
                                )}


                                <View style={{ width: '100%', borderRadius: 10, backgroundColor: 'white', marginTop: 15, justifyContent: 'center' }}>
                                    <TextInput value={this.state.email}
                                        onChangeText={(email) => this.setState({ email, errEmail: '' })}
                                        onFocus={this.onFocusChange3}
                                        onBlur={this.handleBlur}
                                        underlineColorAndroid={this.state.isFocused ? '#1A578A' : 'grey'}

                                        placeholder="Email" style={{ fontSize: 14, padding: 10, fontFamily: 'Poppins-Medium' }}></TextInput>
                                </View>
                                {!!this.state.errEmail && (
                                    <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.errEmail}</Text>
                                )}


                            </View>

                            <TouchableOpacity onPress={() => this.validation()} style={{ width: 160, marginTop: 25, height: 35, borderRadius: 20, alignItems: 'center', justifyContent: 'center', backgroundColor: '#1A578A', marginBottom: 40 }}>
                                <Text style={{ fontSize: 15, color: 'white', fontFamily: 'Poppins-SemiBold' }}>Sauver</Text>
                            </TouchableOpacity>


                            <PopupDialog
                                onHardwareBackPress={() => { this.setState({ lodingDialog: false }) }}
                                width={0.3}
                                visible={this.state.lodingDialog}
                                dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
                                <DialogContent>
                                    <View style={{ alignItems: 'center', }}>
                                        <ActivityIndicator
                                            animating={this.state.animating}
                                            style={[{ height: 10, marginBottom: 10, marginTop: 30, marginLeft: 20, marginRight: 20 }]}
                                            color="#C00"
                                            size="large"
                                            hidesWhenStopped={true}
                                        />
                                    </View>
                                </DialogContent>
                            </PopupDialog>
                        </View>

                    </Image>

                </ScrollView>
            </View>
        );
    }
}
