import React from 'react';
import { StatusBar, TextInput, FlatList, ActivityIndicator, Keyboard, View, ScrollView, Text, Button, Dimensions, TouchableOpacity, Linking, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from "../styles/styles";
import strings from '../strings/strings';
const { width, height } = Dimensions.get("window");
import PopupDialog, {
    DialogTitle, DialogContent, DialogFooter, DialogButton, SlideAnimation, ScaleAnimation,
} from 'react-native-popup-dialog';

import {Card} from 'native-base'
import Image from 'react-native-fast-image'

const valuearr = []

export default class ChatwithAdmin extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            animating: false,
            lodingDialog: false,
            chatuser: [],
            disabled: false,
            textmsgText: '',
            currentOrderId: '',
            userid: '',
            order_id: '',
            token: '',
            textmsgTextErr: '',
        }
        this.index = 0;
    };

    componentDidMount() {

    }

    componentWillMount() {

        this.timer = setInterval(() => this.getchat(), 5000)
        this.setState({
            animating: true,
            lodingDialog: true
        });
        console.log("select id ::::" + this.props.currentOrderId);

    }

    getchat() {
        AsyncStorage.getItem("userid")
            .then(userid => {
                this.setState({ userid: userid });
                console.log("state userId============" + userid);
                AsyncStorage.getItem("order_id")
                    .then(order_id => {
                        this.setState({ order_id: order_id });
                        console.log("state order_id============" + this.props.currentOrderId);
                        AsyncStorage.getItem("token")
                            .then(token => {
                                var accessToken = JSON.parse(token);
                                this.setState({ token: accessToken });
                                console.log("accessToken============" + accessToken);

                                fetch(strings.base_Url + "getUserChatWithAdmin", {
                                    method: 'POST',
                                    headers: {
                                        // 'Accept': 'application/json',
                                        'Content-Type': 'application/json',
                                        'User-Id': userid,
                                        'token': accessToken,
                                    },
                                    body: JSON.stringify({
                                        order_id: this.props.currentOrderId
                                    })
                                })
                                    .then((response) => response.json())
                                    .then((responseData) => {
                                        console.log("getDriverorderchat responseData resoJSon===" + JSON.stringify(responseData));
                                        console.log("resoJSon===" + responseData.error);
                                        if (responseData.error == "false") {
                                            console.log("Error false massage" + JSON.stringify(responseData.errorMessage));
                                            this.setState({
                                                animating: false,
                                                lodingDialog: false,
                                                chatuser: responseData.DriverOrderChat,
                                            });
                                        } else if (responseData.error == "true") {
                                            this.setState({
                                                passwordErr: responseData.errorMessage,
                                                animating: false,
                                                lodingDialog: false
                                            });
                                        }
                                    })
                            })
                    })
            })
    }
    validation() {
        {
            Keyboard.dismiss
        }
        var isValidate = 0;
        if (this.state.textmsgText != "") {
            isValidate += 1;

        } else {
            isValidate -= 1;
            this.setState({
                textmsgTextErr: strings.ShouldemptyText,
            });
        }

        if (isValidate == 1) {
            this.userSendChat();
        }
    }

    userSendChat() {

        fetch(strings.base_Url + "userChatWithAdmin", {
            method: 'POST',
            headers: {
                // 'Accept': 'application/json',
                'Content-Type': 'application/json',
                'User-Id': this.state.userid,
                'token': this.state.token,
            },
            body: JSON.stringify({
                sender_id: this.state.userid,
                order_id: this.props.currentOrderId,
                message: this.state.textmsgText,
            })
        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(" responseData resoJSon===" + JSON.stringify(responseData));
                console.log("resoJSon===" + responseData.error);
                if (responseData.error == "false") {
                    console.log("chat mas false massage" + JSON.stringify(responseData.errorMessage));

                    this.setState({
                        animating: false,
                        lodingDialog: false,
                        textmsgText: ''
                    });
                } else if (responseData.error == "true") {
                    this.setState({
                        passwordErr: responseData.errorMessage,
                        animating: false,
                        lodingDialog: false

                    });
                }
            }
            )
    }

    render() {

        return (
            <View style={styles.containerWhite}>
                <View style={{ flex: 1, flexDirection: 'column', backgroundColor: 'white' }}>

                <Card style={{ width: '100%',marginTop: Platform.OS == 'ios' ? 20 :-5, height: 50, flexDirection: 'row' }}>
                        <TouchableOpacity
                            onPress={() => Actions.pop()} style={{ width: '15%', height: 50, justifyContent: 'center', alignItems: 'center', }}>
                            <Image source={require("../assets/back.png")}
                                style={{ width: 20, height: 20, }}
                                resizeMode="stretch" />
                        </TouchableOpacity>
                        <View style={{ width: '75%', height: 50, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: 16,fontFamily:'Poppins-SemiBold', }}>Chat</Text>

                        </View>


                    </Card>
                    {/* <View style={styles.headerWhite}>
                        <TouchableOpacity onPress={() => Actions.pop()} style={{ marginLeft: 10 }} >
                            <Image source={require("../assets/back.png")} style={styles.imageMenuIcon}
                            />
                        </TouchableOpacity>

                        <View style={styles.header}>
                            <Text style={styles.settingText}>    Chat</Text>
                        </View>
                    </View> */}
                    <View style={{
                        flex: 1,
                        backgroundColor: 'white',
                    }}>
                        <ScrollView>
                            <FlatList
                                data={this.state.chatuser}
                                showsVerticalScrollIndicator={false}
                                renderItem={({ item }) => {
                                    if (item.message_by == 1) {
                                        var statuscat = ""
                                        statuscat = strings.enableText

                                    } else {
                                        var statuscat = ""
                                        statuscat = strings.disableText
                                        console.log("status:::$$$$::::" + statuscat);
                                    }
                                    return (
                                        <View style={{ flex: 1, marginTop: 10 }}>

                                            {item.is_admin === 1 && (
                                                <View style={{ marginLeft: 60, backgroundColor: '#EDEDED', marginRight: 10, borderRadius: 40, flexDirection: 'row', alignSelf: 'flex-end' }}>

                                                    <Text style={{ color: '#000000', padding: 10, marginLeft: 10 }}>
                                                        {item.message}
                                                    </Text>
                                                    <Image source={{ uri: item.image }} style={{ width: 30, height: 30, borderRadius: 15, marginTop: -7 }} />

                                                </View>
                                            )
                                            }
                                            {item.is_admin === 2 && (
                                                <View style={{ marginLeft: 10, flexDirection: 'row', marginRight: 60, borderRadius: 40, backgroundColor: '#c5d8ff', alignSelf: 'flex-start' }}>
                                                    <Image source={{ uri: item.image }} style={{ width: 30, height: 30, borderRadius: 15, marginTop: -7 }} />

                                                    <Text style={{ color: '#000000', padding: 10 }}>
                                                        {item.message}
                                                    </Text>

                                                </View>


                                            )
                                            }
                                        </View>
                                    )
                                }
                                }
                            />
                        </ScrollView>
                    </View>
                    <View style={{
                        backgroundColor: 'white',
                        justifyContent: 'flex-end',
                    }}>
                        <View style={{ backgroundColor: 'white', marginBottom: 10 }}>
                            <Card style={{ backgroundColor: 'white', width: '98%', borderRadius: 50, marginLeft: 5, marginRight: 5, marginTop: 5, flexDirection: 'row' }}>
                                <View style={{ width: '15%', justifyContent: 'center' }}>
                                    <TouchableOpacity style={{ justifyContent: 'center', marginLeft: 15 }} >
                                        {/* <Image source={require("../assets/circle.png")} style={styles.imageMenuIcon}
                                        /> */}
                                    </TouchableOpacity>
                                </View>

                                <View style={{ width: '80%', marginLeft: -width * 10 / 100, flexDirection: 'row', alignItems: 'center' }}>
                                    <Image source={require("../assets/31.png")} style={{ width: 15, height: 15 }}
                                    />
                                    <TextInput
                                        value={this.state.textmsgText}
                                        onChangeText={(textmsgText) => this.setState({ textmsgText, textmsgTextErr: '' })}
                                        placeholder='Tapez quelque chose'
                                        placeholderTextColor='grey'
                                        style={{width:width*65/100, marginLeft: 10, color: 'black', fontSize: 12, fontFamily: 'Poppins-SemiBold' }}>
                                    </TextInput>
                                    {!!this.state.textmsgTextErr && (
                                        <Text style={{ color: 'red', marginLeft: 30, marginTop: -10, fontSize: 12 }}>{this.state.textmsgTextErr}</Text>
                                    )}
                                </View>

                                <View style={{ width: '15%', justifyContent: 'center' }}>
                                    <TouchableOpacity onPress={() => this.validation()} style={{ justifyContent: 'center', marginLeft: 15 }} >
                                        <Image source={require("../assets/blue.png")} style={styles.imageMenuIcon}
                                        />
                                    </TouchableOpacity>
                                </View>
                            </Card>
                        </View>
                    </View>
                </View>
                <PopupDialog
                    onHardwareBackPress={() => { this.setState({ lodingDialog: false }) }}
                    width={0.3}
                    visible={this.state.lodingDialog}
                    dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
                    <DialogContent>
                        <View style={{ alignItems: 'center', }}>
                            <ActivityIndicator
                                animating={this.state.animating}
                                style={[{ height: 10, marginBottom: 10, marginTop: 30, marginLeft: 20, marginRight: 20 }]}
                                color="#C00"
                                size="large"
                                hidesWhenStopped={true}
                            />
                        </View>
                    </DialogContent>
                </PopupDialog>
            </View>
        );
    }
}
