import React from 'react';
import { StatusBar, TextInput, FlatList, ActivityIndicator, ImageBackground, View, ScrollView, Text, Button, Dimensions, TouchableOpacity, Linking, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from "../styles/styles";
import strings from '../strings/strings';

import { Dropdown } from 'react-native-material-dropdown';
const { width, height } = Dimensions.get("window");
import RBSheet from "react-native-raw-bottom-sheet";
import { Card } from "native-base";
import PopupDialog, {
    DialogTitle, DialogContent, DialogFooter, DialogButton, SlideAnimation, ScaleAnimation,
} from 'react-native-popup-dialog';
import Image from 'react-native-fast-image'


export default class Advertisementlist extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            animating: false,
            lodingDialog: false,
            data: [],
            data2: [],
            currentOrderId: '',

        }


    }

    componentDidMount() {

        this.setState({
            animating: true,
            lodingDialog: true,

        });

        AsyncStorage.getItem("token")
            .then(token => {
                var accessToken = JSON.parse(token);
                console.log("accessToken============" + accessToken);

                this.setState({ token: accessToken });

            });


        AsyncStorage.getItem("userid")

            .then(userid => {
                var accessToken = JSON.parse(userid);
                this.setState({ userid: userid });
                console.log("userid============" + userid);


                fetch(strings.base_Url + "getAdervertisementList", {
                    method: 'POST',
                    headers: {
                        // 'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'user-id': this.state.userid,
                        'token': this.state.token,
                    },
                    body: JSON.stringify({
                        category_id: this.props.category_id,

                    })
                })
                    .then((response) => response.json())
                    .then((responseData) => {
                        console.log(" responseData Category page===" + JSON.stringify(responseData));
                        console.log("resoJSon Category error massage===" + responseData.error);
                        if (responseData.error == "false") {
                            console.log("Error false Category massage" + responseData.errorMessage);

                            this.setState({
                                animating: false,
                                lodingDialog: false,
                                data: responseData.record,

                            });


                        } else if (responseData.error == "true") {
                            this.setState({
                                animating: false,
                                lodingDialog: false

                            });
                        }
                    }
                    )
            })

    }

    search(name) {
        console.log('hello')
        fetch(strings.base_Url + "getAdervertisementList", {
            method: 'POST',
            headers: {
                // 'Accept': 'application/json',
                'Content-Type': 'application/json',
                'user-id': this.state.userid,
                'token': this.state.token,
            },
            body: JSON.stringify({
                category_id: this.props.category_id,
                shop_name: name

            })
        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(" responseData Category page===" + JSON.stringify(responseData));
                console.log("resoJSon Category error massage===" + responseData.error);
                if (responseData.error == "false") {
                    console.log("Error false Category massage" + responseData.errorMessage);

                    this.setState({
                        animating: false,
                        lodingDialog: false,
                        data: responseData.record,

                    });


                } else if (responseData.error == "true") {
                    this.setState({
                        animating: false,
                        lodingDialog: false

                    });
                }
            }
            )
    }




    render() {
        return (
            <View style={styles.containerWhite}>
                <Card style={{ width: width, marginTop: Platform.OS == 'ios' ? 20 :-5, height: 50, flexDirection: 'row' }}>
                    <TouchableOpacity
                        onPress={() => Actions.pop()} style={{ height: 50, alignItems: 'center' }}>
                        <Image source={require("../assets/left-arrow.png")}
                            style={{ width: 20, height: 20, marginTop: 13, marginLeft: 5, }}
                            resizeMode="stretch" />
                    </TouchableOpacity>
                    <View style={{ width: '75%', height: 50, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontSize: 16, fontFamily: 'Poppins-SemiBold', }}>Alimentaire et Boissons</Text>

                    </View>
                </Card>
                <ScrollView>
                    <View style={{ flexDirection: 'row', marginTop: 15, height: 35, alignItems: 'center', width: width * 88 / 100, borderRadius: 17, alignSelf: 'center', borderColor: 'grey', borderWidth: 1 }}>
                        <Image source={require('../assets/19.png')} style={{ width: 12, height: 12, marginLeft: 6 }} />
                        <TextInput
                            placeholder='Recherche'
                            onChangeText={(name) => this.search(name)}
                            style={{ fontFamily: 'Poppins-Medium', fontSize: 12, marginLeft: 9, marginBottom: -6, paddingTop: 5, color: 'grey', width: width * 60 / 100, }}
                        />
                    </View>
                    {this.state.data.length > 0 ?

                        <FlatList
                            //  keyExtractor={item => item.id}
                            style={{ marginLeft: 10, marginRight: 10, }}
                            listkey={item => item.id}
                            data={this.state.data}
                            renderItem={({ item, index }) => (

                                <View style={{ width: width * 88 / 100, alignSelf: 'center', marginTop: 10, }}>
                                    {item.value == '' ? null :
                                        <Text style={{ fontFamily: 'Poppins-SemiBold', fontSize: 13 }}>{item.heading}</Text>
                                    }
                                    <FlatList
                                        //  listkey={item => item.id}

                                        data={item.value}
                                        listkey={item => item.id}

                                        renderItem={({ item, index }) => (

                                            <TouchableOpacity
                                                onPress={() => Actions.push("AdvertisementPageDetails", { sub_cat_id: item.id })}

                                                style={{ justifyContent: 'center', alignItems: 'center', }}>
                                                <Card style={{ width: width * 88 / 100, justifyContent: 'space-between', alignItems: 'center', flexDirection: 'column', padding: 7, borderRadius: 15 }}>
                                                    <View style={{ flexDirection: 'row' }}>
                                                        <View style={{ width: width * 25 / 100 }}>
                                                            <Image
                                                                // source={require("../assets/image.png")}
                                                                source={{ uri: item.image_url }}
                                                                style={{ width: width * 25 / 100, height: width * 25 / 100, borderRadius: 8 }} />
                                                        </View>

                                                        <View style={{ width: width * 60 / 100 - 30 }}>
                                                            <View style={{ flexDirection: 'row', marginLeft: 9 }}>
                                                                <Text style={{ fontSize: width * 3 / 100, fontFamily: 'Poppins-SemiBold' }}>{item.title}</Text>
                                                            </View>
                                                            <View style={{ flexDirection: 'row', marginTop: 3, marginLeft: 9, alignItems: 'center' }}>
                                                                <Image source={require('../assets/itemicon1.png')} style={{ width: 15, height: 15 }} />
                                                                <Text style={{ fontSize: width * 2.8 / 100, marginLeft: 3, fontFamily: 'Poppins-Medium' }}>{item.address}</Text>
                                                            </View>
                                                            <View style={{ flexDirection: 'row', marginTop: 3, marginLeft: 9, alignItems: 'center' }}>
                                                                <Image source={require('../assets/2445.png')} style={{ width: 15, height: 15 }} />
                                                                <Text style={{ fontSize: width * 2.8 / 100, marginLeft: 3, fontFamily: 'Poppins-Medium' }}>{item.promocode}</Text>
                                                            </View>
                                                            <View style={{ flexDirection: 'row', marginTop: 3, marginLeft: 9, alignItems: 'center' }}>
                                                                <Image source={require('../assets/26.png')} style={{ width: 15, height: 15 }} />
                                                                <Text style={{ fontSize: width * 2.8 / 100, marginLeft: 3, fontFamily: 'Poppins-Medium' }}>{item.promo_date}</Text>
                                                            </View>
                                                            {/* <View style={{ flexDirection: 'column', marginTop: 5 }}>
                                                                <Text style={{ fontSize: 14, }}>{item.promocode}</Text>
                                                                <Text style={{ fontSize: 14, }}>{item.promo_date}</Text>

                                                            </View> */}

                                                        </View>
                                                    </View>


                                                </Card>
                                            </TouchableOpacity>

                                        )} />
                                </View>
                            )}
                        // keyExtractor={(item, index) => index}

                        />

                        :
                        <Text style={{ fontFamily: "Poppins-Medium", marginTop: height * 25 / 100, textAlign: 'center', color: 'grey' }}>Aucun enregistrement trouvé</Text>

                    }

                </ScrollView>




                <PopupDialog
                    onHardwareBackPress={() => { this.setState({ lodingDialog: false }) }}
                    width={0.3}
                    visible={this.state.lodingDialog}
                    dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
                    <DialogContent>
                        <View style={{ alignItems: 'center', }}>
                            <ActivityIndicator
                                animating={this.state.animating}
                                style={[{ height: 10, marginBottom: 10, marginTop: 30, marginLeft: 20, marginRight: 20 }]}
                                color="#C00"
                                size="large"
                                hidesWhenStopped={true}
                            />
                        </View>
                    </DialogContent>
                </PopupDialog>


            </View>
        );
    }
}
