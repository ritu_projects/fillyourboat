import React from 'react';
import { StatusBar, Button, DrawerLayoutAndroid, ActivityIndicator, FlatList, ScrollView, 
    PixelRatio, TextInput, View, ImageBackground, Text, Dimensions, TouchableOpacity, 
    Linking, AsyncStorage,Platform, } from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from "../styles/styles";
import Strings from '../strings/strings'
import { Card } from 'native-base';
import { Rating, AirbnbRating } from 'react-native-elements';
const { width, height } = Dimensions.get("window");
import PopupDialog, {
    DialogTitle, DialogContent, DialogFooter, DialogButton, SlideAnimation, ScaleAnimation,
} from 'react-native-popup-dialog';
import Sidebar from '../screens/sideBar'
import Moment from 'moment';
import Images from 'react-native-image-progress';
import Progress from 'react-native-progress';
let sources;
import Drawer from 'react-native-drawer'

import Image from 'react-native-fast-image'

export default class profile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            animating: false,
            lodingDialog: false,
            first_name: '',
            last_name: '',
            address: '',
            email: '',
            gender: '',
            telephone: '',
            Anniversarie: '',
            avatar: '',
            profile_pic: '',
            Particulier_radio: false,
            Entreprise_radio: false,
            Clienten_radio: false,
            overall_rating: '',
            reference_enable: '',
            referance_code: '',
            user_id: '',
            token: '',

        };

    }
    componentDidMount() {
        this.setState({
            animating: true,
            lodingDialog: true,

        })
        AsyncStorage.getItem("userid")
            .then(userid => {
                this.setState({ user_id: userid });

                AsyncStorage.getItem("token")
                    .then(token => {
                        var accessToken = JSON.parse(token);
                        this.setState({ token: accessToken });
                        console.log("accessToken====$$$========" + accessToken);
                        console.log("state userId====$$$========" + userid);

                        fetch(Strings.base_Url + "getProfile", {
                            method: 'POST',
                            headers: {
                                // 'Accept': 'application/json',
                                'Content-Type': 'application/json',
                                'user-id': this.state.user_id,
                                'token': this.state.token,
                            },
                            body: JSON.stringify({
                                user_id: this.state.user_id,
                            })
                        })
                            .then((response) => response.json())
                            .then((responseData) => {
                                console.log(" responseData resoJSon===" + JSON.stringify(responseData));
                                console.log("resoJSon===" + responseData.error);
                                if (responseData.error == "false") {
                                    console.log("Error false massage" + JSON.stringify(responseData.errorMessage));

                                 //   sources = { uri: responseData.usersDetails.fullimage };

                                 sources =  responseData.usersDetails.fullimage;

                                    this.setState({
                                        animating: false,
                                        lodingDialog: false,

                                        first_name: responseData.usersDetails.firstname,
                                        last_name: responseData.usersDetails.lastname,
                                        address: responseData.usersDetails.address,
                                        email: responseData.usersDetails.email,
                                        telephone: responseData.usersDetails.telephone,
                                        avatar: responseData.usersDetails.fullimage,
                                        profile_pic: sources,
                                    });
                                } else if (responseData.error == "true") {
                                    this.setState({
                                        passwordErr: responseData.errorMessage,
                                        animating: false,
                                        lodingDialog: false

                                    });
                                }
                            }
                            )
                    });
            });
    }
    _setDrawer() {
        this._drawer.open()
    }
    render() {
        var navigationView = (
            <Sidebar />
        )
        return (
            <Drawer
            openDrawerOffset={100}
           type="overlay"
             ref={(ref) => this._drawer = ref}
             content={<Sidebar />}
             >
                <View style={styles.containerWhite}>
                
                    <ScrollView
                    style={{
                        paddingTop: Platform.OS === 'ios' ? 20 : 0
                    }}
                    >
                        <Image source={require("../assets/100.png")}
                            style={{
                                width: width,
                                height: height,
                                resizeMode: 'stretch',

                            }}
                            resizeMode="stretch">
                            <View style={{ width: width * 95 / 100, alignSelf: 'center', marginTop: 7, justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row', }}>
                                <TouchableOpacity onPress={this._setDrawer.bind(this)} style={{}}>
                                    <Image source={require("../assets/menu.png")}
                                        style={{ width: 20, height: 20, }}
                                        resizeMode="stretch" />
                                </TouchableOpacity>
                                <View style={{}}>
                                    <Text style={{ fontSize: 16, color: 'white', fontWeight: 'bold' }}>{Strings.profile_text}</Text>
                                </View>
                                <TouchableOpacity onPress={() => Actions.push("EditProfile")} style={{ marginRight: 5, }}>
                                    <Image source={require("../assets/29.png")}
                                        style={{ width: 20, height: 20, }}
                                        resizeMode="stretch" />
                                </TouchableOpacity>

                            </View>

                            <View style={{ alignSelf: 'center', width: '100%', alignItems: 'center' }}>

                                {this.state.profile_pic == 'https://www.fillyourboat.net/public/upload/usersprofile/' ?
                                    <Image
                                    source={require('../assets/default_profile_pic.png')}

                                        style={{ width: 80, height: 80, borderWidth: 2, borderColor: 'white', borderRadius: 80 / 2, marginTop: height * 5 / 100 }}
                                    />
                                    :
                                    <Image
                                    source={{ uri:this.state.profile_pic}}

                                    style={{ width: 80, height: 80, borderWidth: 2, borderColor: 'white', borderRadius: 80 / 2, marginTop: height * 5 / 100 }}
                                />
                                }

                                <Text style={{
                                    fontSize: 14, textAlign: 'center',
                                    marginTop: 10, fontFamily: 'Poppins-SemiBold'
                                }}>{this.state.first_name} {this.state.last_name}</Text>
                                <Text style={{ fontSize: 13, textAlign: 'center', color: 'grey', fontFamily: 'Poppins-Medium' }}>{this.state.email}</Text>



                                <View style={{ width: '80%', flexDirection: 'column', marginTop: 40, paddingLeft: 10, }}>
                                    <Text style={{ fontSize: 11, fontFamily: 'Poppins-Regular' }}>Nom</Text>
                                    <View style={{ width: '100%', justifyContent: 'center' }}>
                                        <Text style={{ fontSize: 13, fontFamily: 'Poppins-Medium' }}>{this.state.first_name} {this.state.last_name}</Text>
                                    </View>

                                    <Text style={{ fontSize: 11, marginTop: height * 4 / 100, fontFamily: 'Poppins-Regular' }}>Prénom</Text>
                                    <View style={{ width: '100%', justifyContent: 'center' }}>
                                        <Text style={{ fontSize: 13, fontFamily: 'Poppins-Medium' }}>{this.state.first_name}</Text>
                                    </View>
                                    {/* <Text style={{ fontSize: 11,marginTop:height*4/100,fontFamily:'Poppins-Regular' }}>Nom du bateau </Text>
                            <View style={{ width: '100%',  justifyContent: 'center' }}>
                                {this.state.address=='' ||this.state.address== null?
                                                                <Text style={{ fontSize: 13, fontFamily:'Poppins-Medium'}}>--</Text>
:
                                <Text style={{ fontSize: 13, fontFamily:'Poppins-Medium'}}>{this.state.address}</Text>
                    }
                                </View> */}
                                    <Text style={{ fontSize: 11, marginTop: height * 4 / 100, fontFamily: 'Poppins-Regular' }}>Téléphone </Text>
                                    <View style={{ width: '100%', justifyContent: 'center' }}>
                                        <Text style={{ fontSize: 13, fontFamily: 'Poppins-Medium' }}>{this.state.telephone}</Text>
                                    </View>

                                    <Text style={{ fontSize: 11, marginTop: height * 4 / 100, fontFamily: 'Poppins-Regular' }}>Email </Text>
                                    <View style={{ width: '100%', justifyContent: 'center' }}>
                                        <Text style={{ fontSize: 13, fontFamily: 'Poppins-Medium' }}>{this.state.email}</Text>
                                    </View>



                                    <TouchableOpacity style={{ width: '100%', justifyContent: 'center',marginTop: height * 4 / 100, }}
                                    onPress={() => Actions.push("ChangePassword")}>
                                        <Text style={{ fontSize: 14.5 , fontFamily: 'Poppins-Medium',color: '#1A578A' }}>Changer le mot de passe</Text>
                                    </TouchableOpacity>
                                </View>

                                <PopupDialog
                                    onHardwareBackPress={() => { this.setState({ lodingDialog: false }) }}
                                    width={0.3}
                                    visible={this.state.lodingDialog}
                                    dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
                                    <DialogContent>
                                        <View style={{ alignItems: 'center', }}>
                                            <ActivityIndicator
                                                animating={this.state.animating}
                                                style={[{ height: 10, marginBottom: 10, marginTop: 30, marginLeft: 20, marginRight: 20 }]}
                                                color="#C00"
                                                size="large"
                                                hidesWhenStopped={true}
                                            />
                                        </View>
                                    </DialogContent>
                                </PopupDialog>
                            </View>

                        </Image>
                    </ScrollView>
                </View>
             </Drawer>
        );
    }
}
