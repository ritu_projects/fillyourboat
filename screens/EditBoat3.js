import React from 'react';
import { StatusBar, ScrollView, Alert, TextInput, View, ActivityIndicator, ImageBackground, Text, Dimensions, TouchableOpacity, Linking, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from "../styles/styles";
import Strings from '../strings/strings';
import { Card, Radio, CheckBox } from 'native-base';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';
import DateTimePicker from "react-native-modal-datetime-picker";
import PopupDialog, {
    DialogTitle, DialogContent, DialogFooter, DialogButton, SlideAnimation, ScaleAnimation,
} from 'react-native-popup-dialog';
import { Dropdown } from 'react-native-material-dropdown';
import FuelList_info from './FuelList_info';

let drop_down_data = [{
    value: 'Homme'
}, {
    value: 'Femme'
},];;
import Image from 'react-native-fast-image'


let drop_down_categorydata = [];


const { width, height } = Dimensions.get("window");

export default class EditBoat3 extends React.Component {
    // onRegionChange(region) {
    //   this.setState({ region });
    // }
    constructor(props, context) {
        super(props);
        this.state = {
            animating: false,
            lodingDialog: false,
            Particulier_radio: false,
            Entreprise_radio: false,
            Clienten_radio: false,
            isDateTimePickerVisible: false,
            datePickervisible: false,
            child_radio: false,
            girl_radio: false,
            man_radio: false,
            people_radio: false,
            fsPath: '',
            fsPath2: '',
            checked: false,
            first_name: '',
            first_nameErr: '',
            last_name: '',
            last_nameErr: '',
            address: '',
            addressErr: '',
            email: '',
            emailErr: '',
            password: '',
            passwordErr: '',
            selected: '',
            selectederror: '',
            user_id:'',
            token :'',            
            confirm_password: '',
            confirm_password_err: '',
            telephone: '',
            telephoneErr: '',
            aniversary: Strings.Anniversarie_text,
            aniversaryErr: '',
            gender: '1',
            genderErr: '',
            tncAccept: false,
            avatar_Image: '',
            avatar_ImageErr: '',
            select_avatar: false,
            select_photo: false,
            profile_Err: '',
            type_client_Err: '',
            client_type: '',
            avatar_image: '',
            data: [],
            doc_path_id1: '',
            doc_path_id2: '',
            doc_path_id3: '',
            doc_path_id4: '',
            doc_path_id5: '',
            doc_path_id1Err: '',
            doc_path_id2Err: '',
            doc_path_id3Err: '',
            doc_path_id4Err: '',
            doc_path_id5Err: '',
        };
    }
    componentDidMount()
    {
        this.getFuelList()
;    }

    valueExtractor = val => {
        console.log("vehicle id::::****:$$::" + JSON.stringify(val));
    };
    onChangeTextPress(id) {

        this.setState({
            selected: [],
        })

        this.setState({
            selected: id,
            selectederror: '',
        })
    }

    selectProfilePic = () => {
        const options = {
            title: 'Select Avatar',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };

        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                const source = { uri: response.uri };

                const imageUrl = response.uri
                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };
                console.log("image url:###:" + JSON.stringify(response))
                this.setState({
                    fsPath: response.fileName,
                    fsPath2: response.data
                });
                console.log("image url::" + JSON.stringify(imageUrl))
                console.log("image url::" + JSON.stringify(source))


            }
        });
    }

    selectPhotoTapped(num) {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true
            }
        };
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);
            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };
                switch (num) {
                    case '1':
                        this.setState({
                            doc_path_id1: response.fileName,
                            data: [...this.state.data, response.data],
                        });
                        break;
                    case '2':
                        this.setState({
                            doc_path_id2: response.fileName,
                            data: [...this.state.data, response.data],
                        });
                        break;
                    case '3':
                        this.setState({
                            doc_path_id3: response.fileName,
                            data: [...this.state.data, response.data],
                        });
                        break;
                    case '4':
                        this.setState({
                            doc_path_id4: response.fileName,
                            data: [...this.state.data, response.data],
                        });
                        break;
                    case '5':
                        this.setState({
                            doc_path_id5: response.fileName,
                            data: [...this.state.data, response.data],
                        });
                        break;
                    default:
                        break;
                }
            }
        });
    }

    showDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: true });
    };


    hideDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: false });

    };

    handleDatePicked = date => {
        console.log("A date has been picked: ", date);
        var date_select = date.toString().substr(3, 12);
        this.setState({
            aniversary: date_select
        })
        this.hideDateTimePicker();
    };

    validationRegister() {
        // this.UserRegister();
        var isValidate = 0;
        if (this.state.first_name != "") {
            isValidate += 1;
        } else {
            isValidate -= 1
            this.setState({
                first_nameErr: Strings.ShouldemptyText,
            });
        }
        if (this.state.last_name != "") {
            isValidate += 1;
        } else {
            isValidate -= 1
            this.setState({
                last_nameErr: Strings.ShouldemptyText,
            });
        }
        if (this.state.email != "") {
            isValidate += 1;
          
        } else {
            console.log("cat empty::::");
            isValidate -= 1;
            this.setState({
                emailErr: Strings.ShouldemptyText,
            });
        }

        if (this.state.telephone != "") {
            isValidate += 1
        } else {
            isValidate -= 1;
            this.setState({
                telephoneErr: Strings.ShouldemptyText,
            });
        }

        if (this.state.password != "") {
            isValidate += 1;
           
        } else {
            console.log("cat empty::::");
            isValidate -= 1;
            this.setState({
                passwordErr: Strings.ShouldemptyText,
            });
        }

        if (this.state.selected!= "" && this.state.selected!= "Type de carburant" ) {

            isValidate += 1;

        }
        else {
            console.log("cat empty::::");
            isValidate -= 1;
            this.setState({
                selectederror: Strings.ShouldemptyText,
            });
        }




        console.log("Use Login::::" + isValidate)


        if (isValidate == 6) {

            console.log("Use Login::::" + this.state.first_name + this.state.last_name)

            this.userAddBoat();
            this.setState({
                animating: true,
                lodingDialog: true,

            });
        }
        else {

        }

    }
getBoat(){
   


        AsyncStorage.getItem("userid")
            .then(userid => {
                var userid = JSON.parse(userid);

                this.setState({ user_id: userid });

                AsyncStorage.getItem("token")
                    .then(token => {
                        var accessToken = JSON.parse(token);
                        this.setState({ token: accessToken });
                        this.setState({
                            animating: true,
                            lodingDialog: true,
                            
                        });
                        fetch(Strings.base_Url + "getBoatInfoSingle", {
                            method: 'POST',
                            headers: {
                                // 'Accept': 'application/json',
                                'Content-Type': 'application/json',
                                'User-Id': this.state.user_id,
                                'token': this.state.token,
                            },
                            body: JSON.stringify({
                                id: this.props.currentboatid
        
        
                            })

                        })
                            .then((response) => response.json())
                            .then((responseData) => {
                                console.log(" responseData getFuelList resoJSon===" + JSON.stringify(responseData));
                                console.log("resoJSon getFuelList===" + responseData.error);
                                if (responseData.error == "false") {
                                    console.log("Error false massage getFuelList" + JSON.stringify(responseData.record));

                                 
                                    console.log("Error false SubzoneLisr massage" + JSON.stringify(responseData.errorMessage));
                                    this.setState({
                                        selected: '',

                                    })

                                    this.setState({
                                        animating: false,
                                        lodingDialog: false,
                                        first_name:responseData.record[0].ship_name,
                                        last_name:responseData.record[0].brand,
                                        email:responseData.record[0].size,
                                        telephone:responseData.record[0].registration,
                                        password:responseData.record[0].reserve_capacity+"",
                                       selected:responseData.record[0].fuel,
                                    selected:responseData.record[0].fuel_type


                
                                    });
                                  //  this.getFuelList(responseData.record[0].fuel_type)
                                }
                            })
                        })
                    })
                    


                }
    getFuelList = () => {


        AsyncStorage.getItem("userid")
            .then(userid => {
                var userid = JSON.parse(userid);

                this.setState({ user_id: userid });

                AsyncStorage.getItem("token")
                    .then(token => {
                        var accessToken = JSON.parse(token);
                        this.setState({ token: accessToken });
                        this.setState({
                            animating: true,
                            lodingDialog: true,

                        });

                        fetch(Strings.base_Url + "getFuelrates", {
                            method: 'GET',
                            headers: {
                                // 'Accept': 'application/json',
                                'Content-Type': 'application/json',
                                'User-Id': this.state.user_id,
                                'token': this.state.token,
                            },

                        })
                            .then((response) => response.json())
                            .then((responseData) => {
                                console.log(" responseData getFuelList resoJSon===" + JSON.stringify(responseData));
                                console.log("resoJSon getFuelList===" + responseData.error);
                                if (responseData.error == "false") {
                                    console.log("Error false massage getFuelList" + JSON.stringify(responseData.record));

                                 
                                    console.log("Error false SubzoneLisr massage" + JSON.stringify(responseData.errorMessage));

                                   
                                        drop_down_categorydata = []
                                
                                    this.setState({
                                        animating: false,
                                        lodingDialog: false,
                
                                    });
                
                                    var obj = responseData.record;
                                    console.log("resoJSon GetVehicle obj===" + obj);
                
                                    var count = Object.keys(obj).length;
                                    console.log("resoJSon GetVehicle count===" + count);
                                  
                                    for (var i = 0; i < count; i++) {
                                        drop_down_categorydata.push({
                                            value: obj[i].id,
                
                                            label: obj[i].fuel
                                        });
                                    }
                                    // this.setState({selected:
                                    //     drop_down_categorydata.label[0]})

                                    // for ( i = 0; i < count; i++) {
                                    // if(id==drop_down_categorydata.value[i]){
                                    // }
                             //   }
                             this.getBoat()

                                        
                                    //this.RBSheet.close()
                                } else if (responseData.error == "true") {

                                    this.showalerts(responseData.errorMessage)
                                    this.setState({
                                        animating: false,
                                        lodingDialog: false

                                    });
                                }
                            }
                            )
                    })
            })
    }
    validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

  

   
userAddBoat() {
    AsyncStorage.getItem("userid")
    .then(userid => {
    var userid = JSON.parse(userid);
    
    this.setState({ user_id: userid });
    
    AsyncStorage.getItem("token")
    .then(token => {
    var accessToken = JSON.parse(token);
    this.setState({ token: accessToken });
    this.setState({
    animating: true,
    lodingDialog: true,
    
    });
    
    fetch(Strings.base_Url + "editBoatInfo", {
    method: 'POST',
    headers: {
    // 'Accept': 'application/json',
    'Content-Type': 'application/json',
    'User-Id': this.state.user_id,
    'token': this.state.token,
    },
    body: JSON.stringify({
        id: this.props.currentboatid,
    user_id:this.state.user_id,
    ship_name:this.state.first_name,
    brand:this.state.last_name,
    size:this.state.email,
    registration:this.state.telephone,
    reserve_capacity:this.state.password,
    fuel_type:this.state.selected,
    
    
    })
    })
    .then((response) => response.json())
    .then((responseData) => {
    console.log("responseData addBoat resoJSon===" + JSON.stringify(responseData));
    console.log("resoJSon addBoat===" + responseData.error);
    if (responseData.error == "false") {
    
    this.setState({
    animating: false,
    lodingDialog: false
    
    });
    
    
    Actions.push("MyBoats")
    
    } else if (responseData.error == "true") {
    
    this.showalerts(responseData.errorMessage)
    this.setState({
    animating: false,
    lodingDialog: false
    
    });
    
    }
    }
    )
    })
    })
    
    }


    showalerts(body) {
        Alert.alert(
            Strings.alert_tital,
            body,
            [
                { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
            ],
            { cancelable: false });
    }



    render() {
        console.log("date::" + this.state.aniversary.toString().substr(4, 12))
        return (
            <View style={styles.containerWhite}>


                <ScrollView >
                <Card style={{ width: '100%',marginTop: Platform.OS == 'ios' ? 20 :-5, height: 50, flexDirection: 'row' }}>
                        <TouchableOpacity
                            onPress={() => Actions.pop()} style={{ width: '15%', height: 50, justifyContent: 'center', alignItems: 'center', }}>
                            <Image source={require("../assets/back.png")}
                                style={{ width: 15, height: 10, }}
                                resizeMode="stretch" />
                        </TouchableOpacity>
                        <View style={{ width: '75%', height: 50, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: 16,fontFamily:'Poppins-SemiBold', }}>Modifier le bateau </Text>

                        </View>


                    </Card>
                    <View style={{ marginTop: 0 }}>

                        <View style={{ margin: 20, width: width*90/100 }}>
                        <Text style={{ fontSize: 13,fontFamily:'Poppins-Bold', }}>Mon bateau </Text>

                            <View style={{ width: '100%',marginTop:15, height: 45, borderWidth: 1, borderColor: 'grey', borderRadius: 10 }}>
                                <TextInput placeholder='Nom du bateau'
                                value={this.state.first_name}
                                    onChangeText={(first_name) => this.setState({ first_name, first_nameErr: '' })}
                                    style={{ padding: 10,fontFamily:'Poppins-SemiBold',fontSize:12 }}>

                                </TextInput>
                            </View>
                            {!!this.state.first_nameErr && (
                                <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.first_nameErr}</Text>
                            )}

                            <View style={{ width: '100%', marginTop: 20, height: 45, borderWidth: 1, borderColor:'grey', borderRadius: 10 }}>
                                <TextInput
                                    placeholder='Marque'
                                    value={this.state.last_name}
                                    onChangeText={(last_name) => this.setState({ last_name, last_nameErr: '' })}
                                    style={{ padding: 10 ,fontFamily:'Poppins-SemiBold',fontSize:12 }}>

                                </TextInput>
                            </View>
                            {!!this.state.last_nameErr && (
                                <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.last_nameErr}</Text>
                            )}

                            <View style={{ width: '100%', marginTop: 20, height: 45, borderWidth: 1, borderColor:'grey', borderRadius: 10 }}>
                                <TextInput placeholder='Taille'
                                    keyboardType="decimal-pad"
                                    value={this.state.email}
                                    onChangeText={(email) => this.setState({ email, emailErr: '' })}
                                    style={{ padding: 10,fontFamily:'Poppins-SemiBold',fontSize:12  }}>

                                </TextInput>
                            </View>
                            {!!this.state.emailErr && (
                                <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.emailErr}</Text>
                            )}
                            <View style={{ width: '100%', height: 45, marginTop: 20, borderWidth: 1, borderColor: 'grey', borderRadius: 10 }}>
                                <TextInput
value={this.state.telephone}
                                    onChangeText={(telephone) => this.setState({ telephone, telephoneErr: '' })}
                                    placeholder={Strings.registration_number}
                                    style={{ padding: 10 ,fontFamily:'Poppins-SemiBold',fontSize:12 }}>

                                </TextInput>
                            </View>
                            {!!this.state.telephoneErr && (
                                <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.telephoneErr}</Text>
                            )}

                            <View style={{ width: '100%', height: 45, marginTop: 20, borderWidth: 1, borderColor:'grey', borderRadius: 10 }}>
                                <TextInput
                                    placeholder='Capacité du réservoir'
                                    //keyboardType="decimal-pad"
                                    value={this.state.password}
                                    onChangeText={(password) => this.setState({ password, passwordErr: '' })}
                                    style={{ padding: 10 ,fontFamily:'Poppins-SemiBold',fontSize:12 }}>

                                </TextInput>
                            </View>
                            {!!this.state.passwordErr && (
                                <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.passwordErr}</Text>
                            )}


                            <Dropdown
                                style={{ flex: 1,fontFamily:'Poppins-SemiBold',fontSize:12  }}
                                label='Type de carburant'
                                data={drop_down_categorydata}
                                rippleCentered={true}
                                fontFamily='Poppins-SemiBold'
                                fontSize={13}
                                value={this.state.selected}
                                valueExtractor={({ value }) => value}
                                onChangeText={(value) => { this.onChangeTextPress(value) }}
                            />

                            {!!this.state.selectederror && (
                                <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.selectederror}</Text>
                            )}


<View style={{ width: '100%', alignItems: 'center', marginTop: 30, marginBottom: 10 }}>
                                <TouchableOpacity onPress={() => this.validationRegister()}
                                 style={{ width: 200, height: 40, borderRadius: 25, backgroundColor: '#1A578A', alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ fontSize: 14, color: 'white',fontFamily:'Poppins-SemiBold' }}>Modifier mon bateau</Text>
                                </TouchableOpacity>
                            </View>


                        </View>
                    </View>
                    
                    <PopupDialog
                        onHardwareBackPress={() => { this.setState({ lodingDialog: false }) }}
                        width={0.3}
                        visible={this.state.lodingDialog}
                        dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
                        <DialogContent>
                            <View style={{ alignItems: 'center', }}>
                                <ActivityIndicator
                                    animating={this.state.animating}
                                    style={[{ height: 10, marginBottom: 10, marginTop: 30, marginLeft: 20, marginRight: 20 }]}
                                    color="#C00"
                                    size="large"
                                    hidesWhenStopped={true}
                                />
                            </View>
                        </DialogContent>
                    </PopupDialog>
                </ScrollView>
            </View>
        );
    }
}
