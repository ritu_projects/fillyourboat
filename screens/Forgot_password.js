import React from 'react';
import { PermissionsAndroid, Alert, StatusBar, ScrollView, ActivityIndicator, TextInput, View, ImageBackground, Text, Dimensions, TouchableOpacity, Linking, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Card, Radio, CheckBox } from 'native-base';

import styles from "../styles/styles";
import Strings from '../strings/strings'
import PopupDialog, {
    DialogTitle, DialogContent, DialogFooter, DialogButton, SlideAnimation, ScaleAnimation,
} from 'react-native-popup-dialog';
import RBSheet from "react-native-raw-bottom-sheet";
const { width, height } = Dimensions.get("window");
import Image from 'react-native-fast-image'

export default class Forgot_password extends React.Component {
    // onRegionChange(region) {
    //   this.setState({ region });
    // }
    constructor(props, context) {
        super(props);
        this.state = {
            setlangDefault: "", isUpdated: false,
            showUpdateDialog: false,
            isAuthorized: "1",
            userid: '',
            email: '',
            password: '', pass_text: '',
            forgotEmailErr: '',
            deviceToken: '',
            checked: false, email_text: ''
        };
    }
    validationLogin(){
        if(this.state.email==''){
            this.setState({ errEmail: Strings.ShouldemptyText,})
        }
        else{
            this.Forgot_password()
        }
    }
    Forgot_password(){
        this.setState({animating:true,lodingDialog:true})
        console.log("Forgot  EMAIL ==" + JSON.stringify(this.state.email))
        fetch(Strings.base_Url + "userForgotPassword", {
            method: 'POST',
            headers: {
                // 'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: this.state.email,
            })
        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(" responseData resoJSon===" + JSON.stringify(responseData));
                console.log("resoJSon===" + responseData.error);
                if (responseData.error == "false") {
                    console.log("Error false massage" + JSON.stringify(responseData.errorMessage));
                    this.setState({
                        animating: false,
                        lodingDialog: false

                    });
                    alert(responseData.errorMessage)
                 //   this.showalerts(responseData.errorMessage)

                 Actions.push('Login')
                } else if (responseData.error == "true") {
                    alert(responseData.errorMessage)
                    this.setState({
                        animating: false,
                        lodingDialog: false

                    });
                }
            }
            )
    }
    render() {
        console.log("user id:::" + this.state.userid)
        return (
            <View style={styles.containerWhite}>

                <Image source={require("../assets/100.png")}
                    style={{
                        width: width,
                        height: height,
                        resizeMode: 'stretch',
                       

                    }}
                    resizeMode="stretch">

                          <View style={{ marginTop:50, height: 50, flexDirection: 'row', marginLeft:15, }}>
                        <TouchableOpacity
                            onPress={() => Actions.pop()} style={{  height: 50,
                             justifyContent: 'center', alignItems: 'center', }}>
                            <Image source={require("../assets/back.png")}
                                style={{ width: 25, height: 25, }}
                                resizeMode="stretch" />
                        </TouchableOpacity>
                        {/* <View style={{ width: '75%', height: 50, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: 16,fontFamily:'Poppins-SemiBold', }}>Ajouter un bateau </Text>

                        </View> */}


                    </View>
                
                    <View style={{ width: width * 90 / 100, marginTop: height * 30 / 100 }}>
                            <Text style={{ textAlign: 'center', fontSize: 21, fontFamily: 'Poppins-SemiBold' }}>
                            Mot de passe oublié?
                            </Text>
                            <Text style={{ textAlign: 'center', fontSize: width*3/100,marginTop:5, fontFamily: 'Poppins-Medium',color:'grey' }}>
                            Entrez votre identifiant email, nous vous enverrons
                            </Text>
                            <Text style={{ textAlign: 'center', fontSize: width*3/100,marginTop:2, fontFamily: 'Poppins-Medium',color:'grey' }}>
                            un lien pour réinitialiser votre mot de passe </Text>
                        </View>

                        <View style={{ alignItems: 'center', marginTop: 10, }}>


                            <View style={{ margin: 20, width: '90%' }}>
                                {this.state.email_text != '' ?
                                    <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 12, paddingLeft: 10, color: 'grey' }}>Email</Text>
                                    : null}
                                <View style={{ width: width * 90 / 100, height: 45, marginBottom: 10,borderBottomColor: 'grey',borderBottomWidth: 1, }}>
                                    <TextInput placeholder='Email'
                                       // underlineColorAndroid={'grey'}
                                        onFocus={() => this.setState({ email_text: true })}
                                        onBlur={() => this.setState({ email_text: '' })}
                                        onChangeText={(email) => this.setState({ email, errEmail: '' })}
                                        style={{ padding: 10, fontSize: 13, fontFamily: 'Poppins-Medium', color: '#1A578A' }}>
                                        {this.state.email}
                                    </TextInput>
                                </View>

                                {!!this.state.errEmail && (
                                    <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.errEmail}</Text>
                                )}
                                {this.state.pass_text != '' ?
                                    <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 12, marginTop: 5, paddingLeft: 10, color: 'grey' }}>Mot de passe</Text>
                                    : null}
                                


                            </View>

                            <TouchableOpacity onPress={() => this.validationLogin()} style={{ alignItems: 'center', justifyContent: 'center', width: 180, height: 35, marginTop: 10, borderRadius: 25, backgroundColor: '#1A578A' }}>
                                <Text style={{ fontSize: 14, color: 'white', fontFamily: 'Poppins-SemiBold' }}>Continuer</Text>
                            </TouchableOpacity>

                        </View>
                   
                </Image>

                <PopupDialog
                    onHardwareBackPress={() => { this.setState({ lodingDialog: false }) }}
                    width={0.3}
                    visible={this.state.lodingDialog}
                    dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
                    <DialogContent>
                        <View style={{ alignItems: 'center', }}>
                            <ActivityIndicator
                                animating={this.state.animating}
                                style={[{ height: 10, marginBottom: 10, marginTop: 30, marginLeft: 20, marginRight: 20 }]}
                                color="#C00"
                                size="large"
                                hidesWhenStopped={true}
                            />
                        </View>
                    </DialogContent>
                </PopupDialog>


            </View>
        );
    }
}
