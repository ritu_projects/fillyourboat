import React from 'react';
import {
    Platform, TextInput, FlatList, ActivityIndicator,
    SafeAreaView, StatusBar, ScrollView, View, ImageBackground, Text, Dimensions, TouchableOpacity, Linking, AsyncStorage
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Styles from '../styles/styles';
import Strings from '../strings/strings';
import { Card } from 'native-base';
import PopupDialog, {
    DialogTitle, DialogContent, DialogFooter, DialogButton, SlideAnimation, ScaleAnimation,
} from 'react-native-popup-dialog';
import Image from 'react-native-fast-image'

export default class FAQ extends React.Component {
    constructor(props, context) {
        super(props);
        this.state = {
            categoryList: []
        };
    }

    componentWillMount() {
        this.setState({
            animating: true,
            lodingDialog: true
        });
        AsyncStorage.getItem("userid")
            .then(userid => {
                this.setState({ user_id: userid });
                AsyncStorage.getItem("token")
                    .then(token => {
                        var accessToken = JSON.parse(token);
                        this.setState({ token: accessToken });
                        console.log("accessToken====$$$========" + accessToken);
                        console.log("state userId====$$$========" + userid);
                        // this.refs.loading2.show();
                        fetch(Strings.base_Url + "faq", {
                            method: 'GET',
                            headers: {
                                // 'Accept': 'application/json',
                                'Content-Type': 'application/json',
                                // 'User-Id': userid,
                                //   'token': accessToken,
                            },

                        })
                            .then((response) => response.json())
                            .then((responseData) => {
                                console.log(" responseData resoJSon===" + JSON.stringify(responseData));
                                console.log("resoJSon===" + responseData.error);
                                if (responseData.error == "false") {
                                    console.log("Error false massage" + JSON.stringify(responseData.errorMessage));
                                    this.setState({
                                        categoryList: responseData.faqs,
                                        animating: false,
                                        lodingDialog: false,
                                    });
                                } else if (responseData.error == "true") {
                                    this.setState({
                                        no_record: "Aucun dossier n'existe",
                                        animating: false,
                                        lodingDialog: false
                                    });
                                }
                            })
                    });
            });
    }


    onValueChange(value, label) {
        this.setState({
            selected: value
        });

    }
    render() {
        return (
            <View style={Styles.containerWhite}>
                <Image source={require("../assets/simble.png")}
                    style={{ width: '100%', height: 78, }}>
                    <View style={{ width: '100%', height: 50, flexDirection: 'row' }}>
                        <TouchableOpacity onPress={() => Actions.pop()} style={{ width: '15%', height: 50, justifyContent: 'center', alignItems: 'center', }}>
                            <Image source={require("../assets/back.png")}
                                style={{ width: 25, height: 25, }}
                                resizeMode="contain" />
                        </TouchableOpacity>
                        <View style={{ width: '80%', height: 50, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: 16, fontWeight: 'bold' }}>{"FAQ"}</Text>
                        </View>

                    </View>
                </Image>
                <ScrollView style={{ marginTop: -20 }}>
                    <Text style={{ fontSize: 16, fontWeight: 'bold', padding: 10 }}>Questions frequemment posees</Text>

                    <FlatList
                        data={this.state.categoryList}
                        showsVerticalScrollIndicator={false}

                        renderItem={({ item }) => {
                            return (
                                <View style={{ justifyContent: 'center', alignItems: 'center', margin: 10 }}>
                                    <View style={{ flexDirection: 'row', margin: 0 }}>
                                        <View style={{ width: '90%', flexDirection: 'row' }}>
                                            <Text style={{ fontSize: 14, fontWeight: 'bold' }}>{item.id}.</Text>
                                            <Text style={{ fontSize: 14, fontWeight: 'bold' }}>{item.question}</Text>
                                        </View>
                                        <View style={{ width: '10%' }}>
                                            {/* <Image source={require("../assets/4.png")}
                             style={{width:25,height:25}}/> */}
                                        </View>
                                    </View>
                                    <Card style={{ width: '100%', backgroundColor: 'white', marginTop: 10, flexDirection: 'row' }}>
                                        <View style={{ width: '15%', alignItems: 'center' }}>
                                            <Text style={{ fontSize: 14, fontWeight: 'bold', marginTop: 10 }}>Ans.</Text>
                                        </View>
                                        <View style={{ width: '85%' }}>
                                            <Text style={{ fontSize: 16, padding: 10 }}>{item.answer}</Text>
                                        </View>
                                    </Card>
                                </View>
                            )
                        }
                        }
                    />

                    <PopupDialog
                        onHardwareBackPress={() => { this.setState({ lodingDialog: false }) }}
                        width={0.3}
                        visible={this.state.lodingDialog}
                        dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
                        <DialogContent>
                            <View style={{ alignItems: 'center', }}>
                                <ActivityIndicator
                                    animating={this.state.animating}
                                    style={[{ height: 10, marginBottom: 10, marginTop: 30, marginLeft: 20, marginRight: 20 }]}
                                    color="#C00"
                                    size="large"
                                    hidesWhenStopped={true}
                                />
                            </View>
                        </DialogContent>
                    </PopupDialog>

                </ScrollView>
            </View >
        );
    }
}








