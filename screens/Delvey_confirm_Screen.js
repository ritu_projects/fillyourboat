import React from 'react';
import { StatusBar, ScrollView, Alert, TextInput, View, ActivityIndicator, Modal, ImageBackground, Text, Dimensions, TouchableOpacity, Linking, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from "../styles/styles";
import Strings from '../strings/strings';
import { Card, Radio, CheckBox } from 'native-base';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';
import DateTimePicker from "react-native-modal-datetime-picker";
import PopupDialog, {
    DialogTitle, DialogContent, DialogFooter, DialogButton, SlideAnimation, ScaleAnimation,
} from 'react-native-popup-dialog';
const { width, height } = Dimensions.get("window");
import Image from 'react-native-fast-image'

export default class Delvey_confirm_Screen extends React.Component {

    constructor(props, context) {
        super(props);
        this.state = {
            user_id: '',
            token: '',
            orderid: '',
            order_record: '',
            boat_record: '',
            photo: '',
            reserve_capacity: '',
            animating: false,
            lodingDialog: false,
            modalVisible: false,
            picture_without_url: null,
            selection_boat_place: '',
        }
    };

    componentDidMount() {
        AsyncStorage.getItem("selection_boat_place")
        .then(selection_boat_place => {
            this.setState({ selection_boat_place: selection_boat_place });
        })
    
        this.getOrderSummery();
    }


    getOrderSummery = () => {
        AsyncStorage.getItem("userid")
            .then(userid => {
                var userid = JSON.parse(userid);

                this.setState({ user_id: userid });

                AsyncStorage.getItem("token")
                    .then(token => {
                        var accessToken = JSON.parse(token);

                        this.setState({ token: accessToken });
                        console.log("Order accessToken ", accessToken)


                        AsyncStorage.getItem("orderid")
                            .then(orderid => {

                                this.setState({ orderid: orderid });
                                console.log("Order Id ", orderid)
                                this.setState({
                                    animating: true,
                                    lodingDialog: true

                                });

                                fetch(Strings.base_Url + "getOrderBeforeComplition", {
                                    method: 'POST',
                                    headers: {
                                        // 'Accept': 'application/json',
                                        'Content-Type': 'application/json',
                                        'User-Id': this.state.user_id,
                                        'token': this.state.token,
                                    },
                                    body: JSON.stringify({
                                        order_id: this.state.orderid,

                                    })


                                })
                                    .then((response) => response.json())
                                    .then((responseData) => {
                                        console.log("responseData getBoatInfo Delivery resoJSon===" + JSON.stringify(responseData));
                                        console.log("resoJSon getBoatInfo Delivery===" + responseData.error);
                                        if (responseData.error == "false") {

                                            console.log("Error false massage getBoatInfo Delivery " + responseData);
                                            console.log("Error false massage getBoatInfo Delivery " + responseData.boat_record);




                                            this.setState({
                                                animating: false,
                                                lodingDialog: false,
                                                order_record: responseData.order_record,
                                                boat_record: responseData.boat_record,
                                                reserve_capacity: responseData.boat_record.reserve_capacity,
                                                picture_without_url: responseData.order_record.photo,

                                                photo: responseData.photo_url + responseData.order_record.photo,

                                            });
                                            console.log("Picture without url " + this.state.picture_without_url);
 

                                            //this.RBSheet.close()
                                        } else if (responseData.error == "true") {

                                            this.showalerts(responseData.errorMessage)
                                            this.setState({
                                                animating: false,
                                                lodingDialog: false

                                            });
                                        }
                                    }
                                    )
                            })
                    })
            })



    }


    render() {
        return (
            <View style={styles.containerWhite}>
                <View style={{ width: '100%', height: 50, flexDirection: 'row',marginTop: Platform.OS == 'ios' ? 20 :-5 }}>
                    <TouchableOpacity onPress={() => Actions.pop()} style={{ width: '15%', height: 50, justifyContent: 'center', alignItems: 'center', }}>
                        <Image source={require("../assets/back.png")}
                            style={{ width: 25, height: 25, }}
                            resizeMode="contain" />
                    </TouchableOpacity>
                    <View style={{ width: '80%', height: 50, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontSize: 16, fontWeight: 'bold' }}>{Strings.createReservation_text}</Text>

                    </View>

                </View>
                <ScrollView style={{}}>

                    <Text style={{
                        fontSize: 12.5,
                        marginLeft: 20,
                        marginRight: 10, color: '#1A578A', marginTop: 10, fontFamily: 'Poppins-SemiBold'
                    }}>Numéro de commande : {this.state.order_record.show_order_id}</Text>

                    <Card
                        style={{
                            width: width * 90 / 100, alignSelf: 'center',
                            backgroundColor: 'white',
                            borderRadius: 10, padding: 8
                        }}>

                        <View>
                            <View style={{ flexDirection: 'row', paddingBottom: 3, paddingTop: 3, width: '100%', justifyContent: 'space-between', alignItems: 'center' }}>
                                {this.state.picture_without_url == null || this.state.picture_without_url == 'null' ?
                                    //   <Image style={{
                                    //     width: width * 28 / 100,
                                    //     height: width * 28 / 100,
                                    //     borderRadius: 20
                                    // }}
                                    // source={require("../assets/box_icon.png")}  />
                                    null
                                    :
                                    <Image style={{
                                        width: width * 28 / 100,
                                        height: height * 28 / 100,
                                        borderRadius: 20
                                    }}
                                        source={{ uri: this.state.photo }}
                                    />

                                }


                                <View>


                                    <View
                                        style={{
                                            flexDirection: "row",
                                            marginLeft: 5,

                                        }}>

                                        <Text style={{
                                            fontSize: 11, marginLeft: 9, width: width * 62 / 100 - 25, fontFamily: 'Poppins-SemiBold'
                                        }}>Récapitulatif de la commande</Text>
                                    </View>

                                    <View
                                        style={{
                                            flexDirection: "row", paddingTop: 2, alignItems: 'center', paddingLeft: 10, width: width * 62 / 100 - 25,
                                        }}>
                                        <Text style={{
                                            paddingLeft: 10, width: width * 32 / 100,
                                            fontSize: 11, fontFamily: 'Poppins-Regular'


                                        }}>Tarif du carburant</Text>
                                        <Text style={{
                                            paddingLeft: 10,
                                            fontSize: 11, fontFamily: 'Poppins-Regular'
                                        }}>: {this.state.order_record.price_value}€</Text>

                                    </View>

                                    <View
                                        style={{
                                            flexDirection: "row", alignItems: 'center', paddingLeft: 10, width: width * 62 / 100 - 25,
                                        }}>
                                        <Text style={{
                                            paddingLeft: 10, width: width * 32 / 100,
                                            fontSize: 11, fontFamily: 'Poppins-Regular'


                                        }}>Équivalence en litre</Text>
                                        <Text style={{
                                            paddingLeft: 10,
                                            fontSize: 11, fontFamily: 'Poppins-Regular'


                                        }}>: {this.state.order_record.fuel_value}L</Text>

                                    </View>

                                    <View
                                        style={{
                                            flexDirection: "row", alignItems: 'center', paddingLeft: 10, width: width * 62 / 100 - 25,
                                        }}>
                                        <Text style={{
                                            paddingLeft: 10, width: width * 32 / 100,
                                            fontSize: 11, fontFamily: 'Poppins-Regular'

                                        }}>Tarif de livraison</Text>
                                         <Text style={{
                                            paddingLeft: 10,
                                            fontSize: 11, fontFamily: 'Poppins-Regular'


                                        }}>: {this.state.order_record.shipping_price}€</Text>
                                    </View>


                                    <View
                                        style={{
                                            flexDirection: "row", alignItems: 'center', paddingLeft: 10, width: width * 62 / 100 - 25,
                                        }}>
                                        <Text style={{
                                            paddingLeft: 10, width: width * 32 / 100,
                                            fontSize: 11, fontFamily: 'Poppins-Regular'

                                        }}>TVA</Text>
                                        <Text style={{
                                            paddingLeft: 10,
                                            fontSize: 11, fontFamily: 'Poppins-Regular'


                                        }}>: {this.state.order_record.tax}€</Text>
                                    </View>

                                    <View
                                        style={{
                                            flexDirection: "row", alignItems: 'center', paddingLeft: 10, width: width * 62 / 100 - 25,
                                        }}>
                                        <Text style={{
                                            paddingLeft: 10, width: width * 32 / 100,
                                            fontSize: 11, fontFamily: 'Poppins-SemiBold'

                                        }}>Total</Text>
                                        <Text style={{
                                            paddingLeft: 10,
                                            fontSize: 11, fontFamily: 'Poppins-SemiBold'


                                        }}>: {this.state.order_record.total} €</Text>
                                    </View>


                                </View>

                            </View>



                        </View>

                    </Card>
                    <Card
                        style={{
                            width: width * 90 / 100,
                            marginTop: 10, alignSelf: 'center',
                            backgroundColor: 'white',
                            padding: 7, borderRadius: 10
                        }}>

                        <View style={{ paddingBottom: 15, paddingTop: 15, width: width * 90 / 100, }}>

                            <View>
                                <View
                                    style={{
                                        flexDirection: "row",
                                        marginLeft: 10,

                                    }}>
                                    <Image style={{
                                        width: 16,
                                        height: 16,
                                        marginRight: 5,
                                    }}
                                        source={require("../assets/9.png")} />
                                    <Text style={{
                                        fontSize: 11, fontFamily: 'Poppins-SemiBold'
                                    }}>Mon emplacement et mon bateau </Text>
                                </View>
                            </View>
                            <View
                                style={{
                                    flexDirection: "row", marginLeft: 20,
                                }}>
                                <Text style={{
                                    paddingLeft: 10, color: 'grey',
                                    fontSize: 11, fontFamily: 'Poppins-Regular'


                                }}>Nom du bateau :{this.state.boat_record.ship_name}</Text>
                            </View>
                            <View
                                style={{
                                    flexDirection: "row", marginLeft: 20,
                                }}>
                                <Text style={{
                                    paddingLeft: 10,
                                    color: 'grey',
                                    fontSize: 11, fontFamily: 'Poppins-Regular'


                                }}>Sous-zone: {this.state.order_record.subzone}</Text>
                            </View>

                            {this.state.selection_boat_place == 0 ?
                            <View
                                style={{
                                    flexDirection: "row", marginLeft: 20,
                                }}>
                                <Text style={{
                                    paddingLeft: 10,
                                    color: 'grey',
                                    fontSize: 11, fontFamily: 'Poppins-Regular'


                                }}>Numéro de place: {this.state.order_record.seat_number} </Text>



                            </View>
                            :
                            <View
                            style={{
                                flexDirection: "row", marginLeft: 20,
                            }}>
                            <Text style={{
                                paddingLeft: 10,
                                color: 'grey',
                                fontSize: 11, fontFamily: 'Poppins-Regular'


                            }}>Numéro de corps-mort: {this.state.order_record.body_count} </Text>
                        </View>
    }
                       
                            {/* {this.state.selection_boat_place == 0 ?

                                <View
                                    style={{
                                        flexDirection: "row", marginLeft: 20,
                                    }}>

                                    <Text style={{
                                        paddingLeft: 10,
                                        color: 'grey',
                                        fontSize: 11, fontFamily: 'Poppins-Regular'


                                    }}>Numéro de place</Text>
                                </View>
                                :
                                
                            <View
                            style={{
                                flexDirection: "row", marginLeft: 20,
                            }}>

                            <Text style={{
                                paddingLeft: 10,
                                color: 'grey',
                                fontSize: 11, fontFamily: 'Poppins-Regular'


                            }}>Numéro de corps-mort</Text>
                        </View>
                            } */}

                        </View>

                        <View>
                            <View
                                style={{
                                    flexDirection: "row",
                                    marginLeft: 10, alignItems: 'center'

                                }}>
                                <Image style={{
                                    width: 15,
                                    height: 15,
                                    marginRight: 5,
                                }}
                                    source={require("../assets/calender.png")} />
                                <Text style={{
                                    fontSize: 11, fontFamily: 'Poppins-SemiBold'
                                }}>Date et horaire de livraison </Text>
                            </View>
                            <View style={{ flexDirection: 'row', paddingBottom: 15, marginLeft: 10, width: '100%', justifyContent: 'space-between', alignItems: 'center' }}>




                                <View
                                    style={{
                                        flexDirection: "row", paddingLeft: 10,
                                    }}>
                                    <Text style={{
                                        fontSize: 12, color: 'grey',
                                        paddingLeft: 10, fontFamily: 'Poppins-Regular'


                                    }}>{this.state.order_record.reservation_date} </Text>

                                    <Text style={{
                                        fontSize: 12, color: 'grey',
                                        paddingLeft: 10, fontFamily: 'Poppins-Regular'


                                    }}>{this.state.order_record.reservation_time}</Text>
                                </View>


                            </View>
                            {/* <View
                                style={{
                                    flexDirection: "row",
                                    marginLeft: 10,
                                    width: width * 80 / 100, alignSelf: 'center', alignItems: 'center', justifyContent: 'space-between'

                                }}>

                                <Text style={{
                                    fontSize: 13, fontFamily: 'Poppins-SemiBold'
                                }}> Paiement effectué par</Text>
                                <Image style={{
                                    width: 70,
                                    height: 60,
                                    marginRight: 5,
                                }}
                                    source={require("../assets/21.png")} />
                            </View> */}

                        </View>


                    </Card>

                    <TouchableOpacity
                        onPress={() => Actions.push("Payment", { currentOrderId: this.state.orderid })}
                        style={{ width: 250, backgroundColor: 'rgba(26,87,138,1)', marginTop: 20, height: 37, alignSelf: 'center', borderRadius: 20, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontSize: 11.5, fontFamily: 'Poppins-SemiBold', color: 'white', }}>Je confirme ma commande</Text>

                    </TouchableOpacity>

                </ScrollView>

                <TouchableOpacity
                    style={{

                        width: 40,
                        height: 40,
                        borderRadius: 40 / 2,
                        backgroundColor: 'rgba(26,87,138,1)',
                        position: 'absolute',
                        justifyContent: 'center', alignItems: 'center', bottom: 10, right: 5,

                    }}

                    onPress={() => Actions.push("ChatwithAdmin", { currentOrderId: this.state.orderid })}
                >
                    <Image source={require("../assets/chat_white_icon.png")}
                        style={{ width: 20, height: 20, }}
                    />


                </TouchableOpacity>



                <PopupDialog
                    onHardwareBackPress={() => { this.setState({ lodingDialog: false }) }}
                    width={0.3}
                    visible={this.state.lodingDialog}
                    dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
                    <DialogContent>
                        <View style={{ alignItems: 'center', }}>
                            <ActivityIndicator
                                animating={this.state.animating}
                                style={[{ height: 10, marginBottom: 10, marginTop: 30, marginLeft: 20, marginRight: 20 }]}
                                color="#C00"
                                size="large"
                                hidesWhenStopped={true}
                            />
                        </View>
                    </DialogContent>
                </PopupDialog>

            </View>
        );
    }


}