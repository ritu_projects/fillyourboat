import React from 'react';
import {
    PermissionsAndroid, Alert, CheckBox, StatusBar, ScrollView, ActivityIndicator, TextInput, View, ImageBackground,
    Text, Dimensions, TouchableOpacity, Linking, AsyncStorage
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from "../styles/styles";
import Strings from '../strings/strings'
import PopupDialog, {
    DialogTitle, DialogContent, DialogFooter, DialogButton, SlideAnimation, ScaleAnimation,
} from 'react-native-popup-dialog';
import RBSheet from "react-native-raw-bottom-sheet";

import Image from 'react-native-fast-image'

const { width, height } = Dimensions.get("window");

export async function requestLocationPermission() {
    try {
        const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            {
                'title': 'Example App',
                'message': 'Example App access to your location '
            }
        )
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            console.log("You can use the location")

        } else {
            console.log("location permission denied")

        }


        const granteds = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
            {
                title: 'Permissions for write access',
                message: 'Give permission to your storage to write a file',
                buttonPositive: 'ok',
            },
        );
        if (granteds === PermissionsAndroid.RESULTS.GRANTED) {
            console.log('You can use the storage');
        } else {
            console.log('permission denied');
            return;
        }


        const grantedss = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
            {
                title: 'Permissions for write access',
                message: 'Give permission to your storage to write a file',
                buttonPositive: 'ok',
            },
        );
        if (grantedss === PermissionsAndroid.RESULTS.GRANTED) {
            console.log('You can use the camera');
        } else {
            console.log('permission denied');
            return;
        }


    } catch (err) {
        console.warn(err)
    }
}

export default class login extends React.Component {
    // onRegionChange(region) {
    //   this.setState({ region });
    // }
    constructor(props, context) {
        super(props);
        this.state = {
            setlangDefault: "", isUpdated: false,
            showUpdateDialog: false,
            isAuthorized: "1",
            userid: '',
            email: '',
            password: '',
            pass_text: '',
            forgotEmailErr: '',
            deviceToken: '',
            checked: false, email_text: ''
        };
    }

    async componentWillMount() {
        await requestLocationPermission()
        let fcmToken = await AsyncStorage.getItem('fcmToken');
        console.log("FCM token::$$$$$:: " + fcmToken)

        this.setState({
            deviceToken: fcmToken

        })
        AsyncStorage.getItem("RemenberMe")
            .then(RemenberMe => {
                this.setState({ RemenberMe: RemenberMe });
                console.log("RemenberMe::============" + RemenberMe);
                if (RemenberMe == "true") {
                    console.log("RemenberMe true ============" + RemenberMe);
                    AsyncStorage.getItem("passwordrember")
                        .then(password => {
                            var password = JSON.parse(password);
                            this.setState({ password: password });
                            console.log("password::============" + password);
                        })
                    AsyncStorage.getItem("emailrember")
                        .then(email => {
                            var email = JSON.parse(email);
                            console.log("email::============" + email);
                            this.setState({ email: email });
                            const item = JSON.parse(email);

                        })
                    this.setState({
                        checked: true,
                    });

                } else {
                    console.log("RemenberMe false ============" + RemenberMe);
                    this.setState({
                        checked: false,
                    });

                }
            })
    }

    validationLogin() {
        var isValidate = 0;
        if (this.state.email != "") {
            isValidate += 1;
            if (this.validateEmail(this.state.email)) {
                isValidate += 1;
            } else {
                isValidate -= 1;
                this.setState({
                    errEmail: "Votre adresse mail n'est pas valide",
                });
            }
        } else {
            console.log("cat empty::::");
            isValidate -= 1;
            this.setState({
                errEmail: Strings.ShouldemptyText,
            });
        }

        if (this.state.password != "") {
            isValidate += 1;
            if (this.validatePassword(this.state.password)) {
                isValidate += 1;
            } else {
                isValidate -= 1;
                this.setState({
                    passwordErr: "La longueur devrait être min 6",

                });
            }
        } else {
            console.log("cat empty::::");
            isValidate -= 1;
            this.setState({
                passwordErr: Strings.ShouldemptyText,
            });
        }

        if (isValidate == 4) {

            console.log("Use Login::::" + this.state.email + "==" + this.state.password)
            this.userLoginApi();
            this.setState({
                animating: true,
                lodingDialog: true,

            });
        }
        else {

        }

    }
    validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    validatePassword(password) {
        if (password.length < 6) {
            return false;
        } else if (password.length > 16) {
            return false;
        } else {
            return true;
        }
    }

    forgotvalidation() {

        var isValidate = 0;
        if (this.state.email != "") {
            isValidate += 1;
            if (this.validateEmail(this.state.email)) {
                isValidate += 1;
            } else {
                isValidate -= 1;
                this.setState({
                    forgotEmailErr: "S'il vous plaît entrer email valide",
                });
            }
        } else {
            console.log("cat empty::::");
            isValidate -= 1;
            this.setState({
                forgotEmailErr: Strings.ShouldemptyText,
            });
        }
        if (isValidate == 2) {
            this.forgotpassword();
            this.setState({
                animating: true,
                lodingDialog: true,

            });
        }
        else {

        }

    }

    forgotpassword() {
        this.RBSheet.close();
        console.log("Forgot  EMAIL ==" + JSON.stringify(this.state.email))
        fetch(Strings.base_Url + "userForgotPassword", {
            method: 'POST',
            headers: {
                // 'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: this.state.email,
            })
        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(" responseData resoJSon===" + JSON.stringify(responseData));
                console.log("resoJSon===" + responseData.error);
                if (responseData.error == "false") {
                    console.log("Error false massage" + JSON.stringify(responseData.errorMessage));
                    this.setState({
                        animating: false,
                        lodingDialog: false

                    });

                    this.showalerts(responseData.errorMessage)
                } else if (responseData.error == "true") {
                    this.showalerts(responseData.errorMessage)
                    this.setState({
                        animating: false,
                        lodingDialog: false

                    });
                }
            }
            )
    }

    showalerts(body) {
        Alert.alert(
            Strings.alert_tital,
            body,
            [
                { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
            ],
            { cancelable: false });
    }
    userLoginApi() {
        console.log("EMAIL PSSS==" + JSON.stringify(this.state.email + "==" + this.state.deviceToken))
        fetch(Strings.base_Url + 'login', {
            method: 'POST',
            headers: {
                // 'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: this.state.email,
                password: this.state.password,
                devicetoken: this.state.deviceToken
            })
        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(" responseData resoJSon===" + JSON.stringify(responseData));
                console.log("resoJSon===" + responseData.error);

                if (responseData.error == "false") {
                    console.log("Error false massage" + JSON.stringify(responseData.errorMessage));
                    var userid = JSON.stringify(responseData.usersDetails.id)
                    var token = JSON.stringify(responseData.token)
                    var userName = JSON.stringify(responseData.usersDetails.name + responseData.usersDetails.lastname)
                    var email = JSON.stringify(responseData.usersDetails.email)
                    var password = JSON.stringify(this.state.password)

                    console.log("Email  ", email)
                    console.log("Password  ", password)

                    AsyncStorage.setItem("userid", userid);
                    AsyncStorage.setItem("token", token);
                    AsyncStorage.setItem("userName", userName);


                    if (this.state.checked == true) {
                        AsyncStorage.setItem("RemenberMe", "true");
                        AsyncStorage.setItem("emailrember", email);
                        AsyncStorage.setItem("passwordrember", password)
                    } else {
                        AsyncStorage.setItem("RemenberMe", "false");
                        AsyncStorage.setItem("emailrember", '');
                        AsyncStorage.setItem("passwordrember", '')
                    }
                    this.setState({
                        animating: false,
                        lodingDialog: false,
                    });


                    console.log("check::::===" + this.state.checked);
                    Actions.push("Location")


                    //  this.callAPIforBoat();
                } else if (responseData.error == "true") {
                    this.setState({
                        passwordErr: responseData.errorMessage,
                        animating: false,
                        lodingDialog: false

                    });
                }
            }
            )

    }

    callAPIforBoat() {

        AsyncStorage.getItem("userid")
            .then(userid => {
                this.setState({ userid: userid });

                console.log("state userId============" + userid);

                if (userid != null && userid != "" && userid != undefined) {


                    AsyncStorage.getItem("token")
                        .then(token => {
                            var accessToken = JSON.parse(token);
                            this.setState({ token: accessToken });


                            this.setState({
                                animating: true,
                                lodingDialog: true,

                            });

                            fetch(Strings.base_Url + "getBoatInfo", {
                                method: 'POST',
                                headers: {
                                    // 'Accept': 'application/json',
                                    'Content-Type': 'application/json',
                                    'User-Id': this.state.userid,
                                    'token': this.state.token,
                                },
                                body: JSON.stringify({
                                    user_id: this.state.userid


                                })
                            })


                                .then((response) => response.json())
                                .then((responseData) => {
                                    console.log(" responseData getBoatInfo resoJSon===" + JSON.stringify(responseData));
                                    console.log("resoJSon getBoatInfo===" + responseData.error);
                                    if (responseData.error == "false") {
                                        console.log("Error false massage getBoatInfo" + JSON.stringify(responseData.boat_record));

                                        this.setState({
                                            animating: false,
                                            lodingDialog: false,
                                        });

                                        Actions.push("Location")
                                    } else if (responseData.error == "true") {

                                        if (responseData.errorMessage == 'Incompatibilité de jetons') {
                                            console.log("resoJSon true===" + responseData.error);

                                            this.setState({
                                                animating: false,
                                                lodingDialog: false,

                                            });


                                            Actions.push("Login")

                                        }
                                        else {


                                            this.setState({
                                                animating: false,
                                                lodingDialog: false,

                                            });
                                            // this.showalerts(responseData.errorMessage)

                                            Actions.push("AddBoat")
                                        }
                                    }

                                }
                                )

                        })

                } else {

                    Actions.push("Login")



                }

            })

    }

    forgotPasswordCall = () => {
        console.log("Forgot Passwrd  ", "Forgot Passwrd  ")

        this.RBSheet.open();
    }
    render() {
        console.log("user id:::" + this.state.userid)
        return (
            <View style={styles.containerWhite}>

                <Image 
                source={require("../assets/100.png")}
                    style={{
                        width: width,
                        height: height,
                        resizeMode: 'stretch',

                    }}
                    resizeMode="stretch">
                    <ScrollView>
                        <View style={{ width: width * 90 / 100, marginTop: height * 20 / 100 }}>
                            <Text style={{ textAlign: 'center', fontSize: 22, fontFamily: 'Poppins-SemiBold' }}>
                                {Strings.login_header_text}
                            </Text>
                        </View>

                        <View style={{ alignItems: 'center', marginTop: 10, }}>


                            <View style={{ margin: 20, width: '90%' }}>
                                {this.state.email_text != '' ?
                                    <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 12, paddingLeft: 10, color: 'grey' }}>Email</Text>
                                    : null}
                                <View style={{ width: width * 90 / 100, marginBottom: 10, borderBottomColor: 'grey',borderBottomWidth: 1, }}>
                                    <TextInput
                                     placeholder='Email'
                                      //  underlineColorAndroid={'grey'}
                                      autoCapitalize = {false}
                                        onFocus={() => this.setState({ email_text: true })}
                                        onBlur={() => this.setState({ email_text: '' })}
                                        onChangeText={(email) => this.setState({ email, errEmail: '' })}
                                        style={{ padding: 10, fontSize: 13, fontFamily: 'Poppins-Medium', color: '#1A578A' }}>
                                        {this.state.email}
                                    </TextInput>
                                </View>

                                {!!this.state.errEmail && (
                                    <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.errEmail}</Text>
                                )}


                                {this.state.pass_text != '' ?
                                    <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 12, marginTop: height * 2.4 / 100, paddingLeft: 10, color: 'grey' }}>Mot de passe</Text>
                                    : null}
                                <View style={{ width: width * 90 / 100, height: 45, 
                                    marginTop: 5, borderBottomColor: 'grey', 
                                    flexDirection: 'row', borderBottomWidth: 1, 
                                    justifyContent: 'space-between' }}>

                                    <TextInput
                                     placeholder='Mot de passe'
                                        secureTextEntry={true}
                                        onFocus={() => this.setState({ pass_text: true })}
                                        onBlur={() => this.setState({ pass_text: '' })}
                                        onChangeText={(password) => this.setState({ password, passwordErr: '' })}
                                        style={{ padding: 10, fontSize: 13, fontFamily: 'Poppins-Medium', color: '#1A578A', width: width * 50 / 100 }}>
                                        {this.state.password}
                                    </TextInput>
                                    <TouchableOpacity onPress={() => Actions.push('Forgot_password')} 
                                    style={{ width: width * 40 / 100, flexDirection: 'column', marginRight: 0,
                                     marginTop: 10 }}>
                                        <Text style={{
                                            color:
                                                '#F1C368', textAlign: 'right', fontSize: 10.5, 
                                        }}>
                                            {Strings.forgotpassword_text}
                                        </Text>
                                    </TouchableOpacity>
                                </View>


                                {!!this.state.passwordErr && (
                                    <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.passwordErr}</Text>
                                )}
                                <View style={{ flexDirection: 'row', marginLeft: 10, marginTop: 20 }}>
                                    <TouchableOpacity
                                        onPress={() => this.setState({ checked: !this.state.checked })}
                                        style={{ width: '10%', justifyContent: 'center' }}>
                                        <View style={{ width: 20, height: 20, borderWidth: 2, alignItems: 'center', justifyContent: 'center', borderColor: Strings.light_color }}>
                                            {this.state.checked === true && (<View style={{ width: 20, height: 20, backgroundColor: '#F1C368', alignItems: 'center', justifyContent: 'center' }}>
                                                <Image source={require("../assets/right.png")}
                                                    style={{ width: 10, height: 10, }}
                                                    resizeMode="contain" />
                                            </View>)}
                                        </View>
                                    </TouchableOpacity>
                                    <Text style={{ color: '#F1C368', marginTop: 0, fontFamily: 'Poppins-Medium' }}>{Strings.rememberText}</Text>
                                </View>





                            </View>

                            <TouchableOpacity onPress={() => this.validationLogin()} style={{ alignItems: 'center', justifyContent: 'center', width: 180, height: 35, marginTop: 10, borderRadius: 25, backgroundColor: '#1A578A' }}>
                                <Text style={{ fontSize: 14, color: 'white', fontFamily: 'Poppins-SemiBold' }}>{Strings.login_Text}</Text>
                            </TouchableOpacity>


                            {/* <View style={{ width: 80, flexDirection: 'row', justifyContent: 'center', marginTop: 10 }}>


                                <TouchableOpacity style={{ width: 40, height: 40, alignItems: 'center', justifyContent: 'center', marginLeft: 5 }}>
                                    <Image source={require('../assets/linkedin.png')}
                                        style={{ width: 21, height: 21, resizeMode: 'contain' }} />
                                </TouchableOpacity>
                                <TouchableOpacity style={{ width: 40, height: 50, alignItems: 'center', justifyContent: 'center', marginLeft: 5 }}>
                                    <Image source={require('../assets/search(1).png')}
                                        style={{ width: 21, height: 21, resizeMode: 'contain' }} />
                                </TouchableOpacity>
                                <TouchableOpacity style={{ width: 40, height: 40, alignItems: 'center', justifyContent: 'center' }}>
                                    <Image source={require('../assets/facebook.png')}
                                        style={{ width: 21, height: 21, resizeMode: 'contain' }} />
                                </TouchableOpacity>

                            </View> */}

                            <TouchableOpacity onPress={() => Actions.push("Register")} style={{ flexDirection: 'row', marginTop: 20, height: 50, alignItems: 'center' }}>
                                <Text style={{ fontSize: 12, fontFamily: 'Poppins-Medium' }}>{Strings.register_free_text}</Text>
                                <Text style={{ fontSize: 12, fontFamily: 'Poppins-Medium', color: '#F1C368', marginLeft: 4 }}>{Strings.register_text}</Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </Image>

                <RBSheet
                    ref={ref => { this.RBSheet = ref; }}
                    height={250}
                    duration={250}
                    customStyles={{
                        container: {
                            alignItems: "center",
                            borderRadius: 10,
                        }
                    }}>
                    <View style={{ flexDirection: 'column', marginLeft: -10, marginRight: -10 }}>
                        <View style={{ flexDirection: 'row', marginTop: 20 }}>
                            <Text style={{ marginLeft: 20, fontSize: 16, fontWeight: 'bold', color: 'red', padding: 10 }}>{Strings.forgotpasswordText}</Text>

                        </View>
                        <View style={{ flexDirection: 'column', marginTop: 10, alignItems: 'center' }}>
                            <View style={{ width: "90%", height: 50, margin: 5, 
                            backgroundColor: '#F0F0F0', borderRadius: 10,
                             borderBottomColor: 'black', alignContent: 'center', }}>
                                <TextInput
                                autoCapitalize = {false}
                                    onChangeText={(email) => this.setState({ email, forgotEmailErr: '' })}
                                    placeholder={Strings.email_address_text} style={{ marginLeft: 20, fontSize: 16 }}>
                                </TextInput>
                            </View>
                            {!!this.state.forgotEmailErr && (
                                <Text style={{ width: "90%", color: 'red', marginLeft: 10, marginTop: -10, fontSize: 12 }}>{this.state.forgotEmailErr}</Text>
                            )}

                        </View>

                        <View style={{ flexDirection: 'row', marginTop: 30 }}>

                            <View style={{ width: '100%', alignItems: 'center' }}>
                                {/* <TouchableOpacity onPress={() => this.forgotvalidation()} style={{ backgroundColor: '#4128A5', width: '80%', height: 40, borderRadius: 18, alignItems: 'center', justifyContent: "center", }}>
                    
                  </TouchableOpacity> */}

                                <TouchableOpacity onPress={() => this.forgotvalidation()} style={{
                                    alignItems: 'center',
                                    justifyContent: 'center', width: '90%', height: 50, marginTop: 10, borderRadius: 10,
                                    backgroundColor: '#F1C368'
                                }}>
                                    <Text style={{ fontSize: 18, color: "white" }}>
                                        {"Soumettre"}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </RBSheet>
                <PopupDialog
                    onHardwareBackPress={() => { this.setState({ lodingDialog: false }) }}
                    width={0.3}
                    visible={this.state.lodingDialog}
                    dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
                    <DialogContent>
                        <View style={{ alignItems: 'center', }}>
                            <ActivityIndicator
                                animating={this.state.animating}
                                style={[{ height: 10, marginBottom: 10, marginTop: 30, marginLeft: 20, marginRight: 20 }]}
                                color="#C00"
                                size="large"
                                hidesWhenStopped={true}
                            />
                        </View>
                    </DialogContent>
                </PopupDialog>


            </View>
        );
    }
}
