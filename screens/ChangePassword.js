import React from 'react';
import { PermissionsAndroid, Alert, StatusBar, ScrollView, ActivityIndicator, TextInput, View, ImageBackground, Text, Dimensions, TouchableOpacity, Linking, AsyncStorage, } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Card, Radio, CheckBox } from 'native-base';

import styles from "../styles/styles";
import Strings from '../strings/strings'
import PopupDialog, {
    DialogTitle, DialogContent, DialogFooter, DialogButton, SlideAnimation, ScaleAnimation,
} from 'react-native-popup-dialog';
import RBSheet from "react-native-raw-bottom-sheet";
const { width, height } = Dimensions.get("window");
import Image from 'react-native-fast-image'

export default class ChangePassword extends React.Component {

    constructor(props, context) {
        super(props);
        this.state = {

            user_id: '',
            token: '',
            password: '',
            passwordErr: '',
            lodingDialog: false,
            animating: false,


        };
    }
    componentDidMount() {

        AsyncStorage.getItem("userid")
            .then(userid => {
                this.setState({ user_id: userid });

                AsyncStorage.getItem("token")
                    .then(token => {
                        var accessToken = JSON.parse(token);
                        this.setState({ token: accessToken });
                        console.log("accessToken====$$$========" + accessToken);
                        console.log("state userId====$$$========" + userid);

                    });
            });
    }
    validationLogin() {
        var isValidate = 0;

        if (this.state.password != "") {
            isValidate += 1;
            if (this.validatePassword(this.state.password)) {
                isValidate += 1;
            } else {
                isValidate -= 1;
                this.setState({
                    passwordErr: "La longueur devrait être min 6",

                });
            }
        } else {
            console.log("cat empty::::");
            isValidate -= 1;
            this.setState({
                passwordErr: Strings.ShouldemptyText,
            });
        }
        if (isValidate == 2) {

            console.log("Use Login::::" + this.state.password)
            this.Forgot_password();
            this.setState({
                animating: true,
                lodingDialog: true,

            });
        }
    }

    validatePassword(password) {
        if (password.length < 6) {
            return false;
        } else if (password.length > 16) {
            return false;
        } else {
            return true;
        }
    }
    Forgot_password() {
        this.setState({ lodingDialog: true,
            animating: true,
        })
        console.log("Forgot  EMAIL ==" + JSON.stringify(this.state.password))
        fetch(Strings.base_Url + "changeUserPassword", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'user-id': this.state.user_id,
                'token': this.state.token,
            },
            body: JSON.stringify({
                user_id: this.state.user_id,
                password: this.state.password,

            })
        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(" responseData resoJSon===" + JSON.stringify(responseData));
                console.log("resoJSon===" + responseData.error);
                if (responseData.error == "false") {
                    console.log("Error false massage" + JSON.stringify(responseData.errorMessage));
                    this.setState({
                        lodingDialog: false,
                        animating: false,
                    });
                    alert(responseData.errorMessage)

                    Actions.push('Profile')
                } else if (responseData.error == "true") {
                    alert(responseData.errorMessage)
                    this.setState({
                        lodingDialog: false,
                        animating: false,

                    });
                }
            }
            )
    }
    render() {
        console.log("user id:::" + this.state.userid)
        return (
            <View style={styles.containerWhite}>

                <ScrollView
                    style={{
                        paddingTop: Platform.OS === 'ios' ? 20 : StatusBar.currentHeight
                    }}>

                    <Image source={require("../assets/100.png")}
                        style={{
                            width: width,
                            height: height,
                            resizeMode: 'stretch',


                        }}
                        resizeMode="stretch">

                        <View style={{ height: 50, flexDirection: 'row', marginLeft: 15, }}>
                            <TouchableOpacity
                                onPress={() => Actions.pop()} style={{
                                    height: 50,
                                    justifyContent: 'center', alignItems: 'center',
                                }}>
                                <Image source={require("../assets/back.png")}
                                    style={{ width: 25, height: 25, }}
                                    resizeMode="stretch" />
                            </TouchableOpacity>


                        </View>

                        <View style={{ width: width * 90 / 100, marginTop: height * 30 / 100 }}>
                            <Text style={{ textAlign: 'center', fontSize: 21, fontFamily: 'Poppins-SemiBold' }}>
                                Changer le mot de passe
                            </Text>

                        </View>

                        <View style={{  marginTop: 10, marginLeft: 10, marginRight: 10, }}>


                            <View style={{
                                width: '100%',
                                marginTop: 5, borderBottomColor: 'grey',
                                borderBottomWidth: 1,
                                justifyContent: 'space-between'
                            }}>

                                <TextInput
                                value = {this.state.password}
                                    placeholder='Entrez un nouveau mot de passe'
                                    secureTextEntry={true}
                                    onChangeText={(password) => this.setState({ password, passwordErr: '' })}
                                    style={{ padding: 10, fontSize: 13, fontFamily: 'Poppins-Medium', color: '#1A578A', width: '100%' }}>
                                </TextInput>
                                    </View>

                            {!!this.state.passwordErr && (
                                    <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.passwordErr}</Text>
                                )}

                            <TouchableOpacity onPress={() => this.validationLogin()} style={{ alignItems: 'center', justifyContent: 'center', width: 180, height: 35, marginTop: 10, borderRadius: 25, backgroundColor: '#1A578A' }}>
                                <Text style={{ fontSize: 14, color: 'white', fontFamily: 'Poppins-SemiBold' }}>Continuer</Text>
                            </TouchableOpacity>

                        </View>


                        <PopupDialog
                            onHardwareBackPress={() => { this.setState({ lodingDialog: false }) }}
                            width={0.3}
                            visible={this.state.lodingDialog}
                            dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
                            <DialogContent>
                                <View style={{ alignItems: 'center', }}>
                                    <ActivityIndicator
                                        animating={this.state.animating}
                                        style={[{ height: 10, marginBottom: 10, marginTop: 30, marginLeft: 20, marginRight: 20 }]}
                                        color="#C00"
                                        size="large"
                                        hidesWhenStopped={true}
                                    />
                                </View>
                            </DialogContent>
                        </PopupDialog>
                    </Image>



                </ScrollView>
            </View>
        );
    }
}