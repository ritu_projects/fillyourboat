import React from 'react';
import { StatusBar, TextInput, FlatList, ActivityIndicator, ImageBackground, View, ScrollView, Text, Button, Dimensions, TouchableOpacity, Linking, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from "../styles/styles";
import strings from '../strings/strings';
import { Dropdown } from 'react-native-material-dropdown';
const { width, height } = Dimensions.get("window");
import RBSheet from "react-native-raw-bottom-sheet";
import { Card } from "native-base";
import PopupDialog, {
    DialogTitle, DialogContent, DialogFooter, DialogButton, SlideAnimation, ScaleAnimation,
} from 'react-native-popup-dialog';

import moment from 'moment';
import Image from 'react-native-fast-image'

export default class PastTabs extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            animating: false,
            lodingDialog: false,
            data: [


            ],
            date_dropdown: [

                "Sort by",
                "Date"

            ],

            date: "",
            start_date: "De",
            end_date: "À",
            order_from: '',
            order_to: '',
            DateHolder: null,
            DateHolder2: null,
            currentOrderId: '',


        }

    }

    componentDidMount() {

        console.log("Did mount Past%%%" + "Did mount Past%%%");

        this.setState({
            animating: true,
            lodingDialog: true,

        });

        AsyncStorage.getItem("token")
            .then(token => {
                var accessToken = JSON.parse(token);
                console.log("accessToken============" + accessToken);

                this.setState({ token: accessToken });

            });


        AsyncStorage.getItem("userid")

            .then(userid => {
                var accessToken = JSON.parse(userid);
                this.setState({ userid: userid });
                console.log("userid============" + userid);


                fetch(strings.base_Url + "userOrders", {
                    method: 'POST',
                    headers: {
                        // 'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'user-id': this.state.userid,
                        'token': this.state.token,
                    },
                    body: JSON.stringify({
                        user_id: this.state.userid,



                    })
                })
                    .then((response) => response.json())
                    .then((responseData) => {
                        console.log(" responseData Past page===" + JSON.stringify(responseData));
                        console.log("resoJSon Past error massage===" + responseData.error);
                        console.log("Start ")
                        data = [];

                        if (responseData.error == "false") {
                            console.log("Error false Past massage" + responseData.past_order);

                            this.setState({
                                animating: false,
                                lodingDialog: false,
                                data: responseData.past_order,
                                photo_url: responseData.photo_url,

                            });


                        } else if (responseData.error == "true") {
                            this.setState({
                                animating: false,
                                lodingDialog: false,
                                data: [],

                            });
                        }
                    }
                    )
            })





    }



    valueExtractor = val => {
        console.log("vehicle id::::****:::" + JSON.stringify(val.value));
    };
    onChangeTextPress(value) {

        if (value != 'Sort by') {



        }


    }

    DatePickerMainFunctionCall = () => {

        let DateHolder = this.state.DateHolder;

        if (!DateHolder || DateHolder == null) {

            DateHolder = new Date();
            this.setState({
                DateHolder: DateHolder
            });
        }

        //To open the dialog
        this.refs.DatePickerDialog.open({

            date: DateHolder,

        });
    }

    onDatePickedFunction = (date) => {

        this.setState({
            //   dobDate: date,
            start_date: moment(date).format('DD-MM-YYYY'),
            order_from: moment(date).format('DD-MM-YYYY'),
        });
    }


    DatePickerMainFunctionCall2 = () => {

        let DateHolder2 = this.state.DateHolder2;

        if (!DateHolder2 || DateHolder2 == null) {

            DateHolder2 = new Date();
            this.setState({
                DateHolder2: DateHolder2
            });
        }

        //To open the dialog
        this.refs.DatePickerDialog2.open({

            date: DateHolder2,

        });
    }

    onDatePickedFunction2 = (date) => {

        this.setState({
            //   dobDate: date,
            end_date: moment(date).format('DD-MM-YYYY'),
            order_to: moment(date).format('DD-MM-YYYY')


        });
    }

    search = () => {
        this.componentDidMount();

    }

    render() {
        return (
            <View style={{
                flex: 1,
                // backgroundColor:'white',
                flexDirection: 'column',

            }}>



                {/* <View
                    style={{
                        width: "100%",
                        flexDirection: 'row',
                        height: 50,
                    }}>

                    <TouchableOpacity onPress={this.DatePickerMainFunctionCall.bind(this)}
                        style={
                            {
                                width: "40%",
                                justifyContent: "center",
                                alignItems: "center",
                                flexDirection: 'row',

                            }
                        }>

                        <ImageBackground
                            style={{ width: 30, height: 30, }}
                            source={require('../assets/calendar.png')}
                        ></ImageBackground>

                        <Text style={{ paddingLeft: 10, }}> {this.state.start_date}</Text>

                    </TouchableOpacity>

                    <DatePickerDialog ref="DatePickerDialog"

                        onDatePicked={this.onDatePickedFunction.bind(this)} />


                    <TouchableOpacity
                        onPress={this.DatePickerMainFunctionCall2.bind(this)}
                        style={
                            {
                                width: "40%",
                                justifyContent: "center",
                                alignItems: "center",
                                flexDirection: 'row',

                            }
                        }>
                        <ImageBackground
                            style={{ width: 30, height: 30, }}
                            source={require('../assets/calendar.png')}
                        ></ImageBackground>
                        <Text style={{ paddingLeft: 10, }}> {this.state.end_date}</Text>

                    </TouchableOpacity>
                    <DatePickerDialog ref="DatePickerDialog2"

                        onDatePicked={this.onDatePickedFunction2.bind(this)} />

                    <TouchableOpacity
                        onPress={this.search.bind(this)}
                        style={{
                            width: "20%",
                            justifyContent: "center",
                            alignItems: "center",
                            flexDirection: 'row',

                        }}>
                        <Image
                            style={{ width: 30, height: 30, }}
                            source={require('../assets/search_icon.png')}
                        ></Image>
                    </TouchableOpacity>



                </View> */}

                {this.state.data.length > 0 ?

                    <View style={{
                        width: "100%",



                    }}>
                        <FlatList
                            style={
                                {
                                }
                            }
                            ref={flatList1 => {
                                this.flatList1 = flatList1
                            }}
                            keyExtractor={item => item.id}

                            data={this.state.data}
                            renderItem={({ item, index }) => (

                                <View style={{ justifyContent: 'center',alignItems:'center', marginTop: 10 }}>
                                <Card style={{
                                    width: "90%", justifyContent: 'space-between', flexDirection: 'column', padding: 7, borderRadius: 15
                                }}>
                                    <View style={{ flexDirection: 'row',}}>
                                         {item.photo == null || item.photo == 'null' ?
                                                  
                                                    null
                                                    :
                                                    <Image style={{
                                                        width: 70, height: 80, borderRadius: 8,
                                                    }}
                                                        source={{ uri: this.state.photo_url + item.photo }}
                                                    />

                                                }
                                            <View style={{ marginLeft : 5,  }}>
                                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                    <View style={{ width: width * 56 / 100, flexDirection: 'row', alignItems: 'center', alignSelf: 'center', justifyContent: 'space-between' }}>
                                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                            <Image
                                                                // source={require("../assets/image.png")}
                                                                source={require('../assets/9.png')}
                                                                style={{ width: 10, height: 10 }} />
                                                            <Text style={{ marginLeft: 5, fontSize: 11, fontFamily: 'Poppins-SemiBold' }}>Zone de livraison </Text>
                                                        </View>
                                                        {item.rating != '' && item.rating != null ?
                                                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                                <Image source={require('../assets/27.png')} style={{ width: 12, height: 12, marginTop: -3.5 }} />
                                                                <Text style={{ marginLeft: 3, fontSize: 11, fontFamily: 'Poppins-Medium', color: '#28528F' }}>{item.rating}</Text>
                                                            </View> : null}
                                                    </View>
                                                </View>
                                                <Text style={{ fontSize: 10, marginLeft: 15, color: 'grey', marginTop: -2, fontFamily: 'Poppins-Medium' }}>{item.zone}</Text>
                                                <Text style={{ fontSize: 10, marginLeft: 15, color: 'grey', marginTop: -2, fontFamily: 'Poppins-Medium' }}>{item.subzone}</Text>

                                                {/* <Text style={{ fontSize: 14, fontWeight: 'bold' }}>Commande Id : </Text>
                                                <Text style={{ fontSize: 14, fontWeight: 'bold' }}>{item.order_id}</Text> */}
                                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                    <Image
                                                        // source={require("../assets/image.png")}
                                                        source={require('../assets/calender.png')}
                                                        style={{ width: 10, height: 10 }} />

                                                    <Text style={{ marginLeft: 5, fontSize: 11, fontFamily: 'Poppins-SemiBold' }}>Date de livraison </Text>
                                                </View>
                                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                    <Text style={{ fontSize: 10, marginLeft: 15, color: 'grey', marginTop: -2, fontFamily: 'Poppins-Medium' }}>{item.reservation_date}</Text>
                                                    <Text style={{ fontSize: 10, marginLeft: 5, color: 'grey', marginTop: -2, fontFamily: 'Poppins-Medium' }}>{item.reservation_time}</Text>
                                                </View>
                                                <View style={{ flexDirection: 'row', marginTop: 5, marginBottom: 5 }}>
                                                    {/* {item.status != 0 ?
                                            <TouchableOpacity
                                                onPress={() => Actions.push("Trackorders", { currentOrderId: item.id })}
                                                style={{ backgroundColor: '#28528F', padding:5, height: 20, borderRadius: 14, alignItems: 'center', justifyContent: "center", marginLeft: 10 }}>
                                                <Text style={{ fontSize: 10,marginLeft:8,marginRight:8, fontFamily:'Poppins-SemiBold', color: "white",  textAlign : 'center',}}>Suivre la commande</Text>
                                            </TouchableOpacity>
                                            :
                                            null
                                        } */}
                                                    <TouchableOpacity
                                                        onPress={() => Actions.push("OrderView", { currentOrderId: item.id })}
                                                        style={{
                                                            backgroundColor: '#28528F', height: 20, borderRadius: 14,
                                                            alignItems: 'center', justifyContent: "center", marginLeft: 10, padding: 5
                                                        }}>
                                                        <Text style={{ fontSize: 10, fontFamily: 'Poppins-SemiBold', marginLeft: 8, marginRight: 8, color: "white", textAlign: 'center', }}>Voir</Text>
                                                    </TouchableOpacity>

                                                    {/* 0=None,1=Assign,2=Trip start,3=On the way,4=Reach location,5=Complete */}


                                                </View>


                                       
                                        </View>

                                        {/* <View style={{ flexDirection: 'row', marginTop: 5, marginBottom: 5 }}>
                                    <TouchableOpacity
                                    onPress={()=>Actions.push("OrderView", {currentOrderId : item.id })}
                                    style={{ backgroundColor: '#4128A5', width: '40%', height: 30, borderRadius: 14, alignItems: 'center', justifyContent: "center", marginLeft: 10 }}>
                                        <Text style={{ fontSize: 14, color: "white" }}>Voir les commandes</Text>
                                    </TouchableOpacity>
                                    </View> */}
                                    </View>
                                    </Card>

                                </View>
                            )}
                        />
                    </View>
                    :
                    <Text style={{ fontFamily: "Poppins-Medium", marginTop: '40%', textAlign: 'center' }}>Aucune donnée</Text>

                }
                <PopupDialog
                    onHardwareBackPress={() => { this.setState({ lodingDialog: false }) }}
                    width={0.3}
                    visible={this.state.lodingDialog}
                    dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
                    <DialogContent>
                        <View style={{ alignItems: 'center', }}>
                            <ActivityIndicator
                                animating={this.state.animating}
                                style={[{ height: 10, marginBottom: 10, marginTop: 30, marginLeft: 20, marginRight: 20 }]}
                                color="#C00"
                                size="large"
                                hidesWhenStopped={true}
                            />
                        </View>
                    </DialogContent>
                </PopupDialog>


            </View>
        );
    }
}
