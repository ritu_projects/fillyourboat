import React from 'react';
import { StatusBar, Button, ActivityIndicator, FlatList, ScrollView, PixelRatio, TextInput, View, ImageBackground, Text, Dimensions, TouchableOpacity, Linking, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from "../styles/styles";
import Strings from '../strings/strings'
import { Card } from 'native-base';
import { Rating, AirbnbRating } from 'react-native-elements';
import PopupDialog, {
    DialogTitle, DialogContent, DialogFooter, DialogButton, SlideAnimation, ScaleAnimation,
} from 'react-native-popup-dialog';
import { isReferenced } from '@babel/types';
import Image from 'react-native-fast-image'

export default class historyTab extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            NoRecord: '',
            data: [],
            animating: false,
            lodingDialog: false,
            req_id: '',
            user_id: '',
            token: '',
        };
    }

    componentWillMount() {
        this.setState({
            animating: true,
            lodingDialog: true,

        })
        AsyncStorage.getItem("userid")
            .then(userid => {
                this.setState({ user_id: userid });
                AsyncStorage.getItem("token")
                    .then(token => {
                        var accessToken = JSON.parse(token);
                        this.setState({ token: accessToken });
                        console.log("accessToken====$$$========" + accessToken);
                        console.log("state userId====$$$========" + userid);

                        fetch(Strings.base_Url + "user_completed_job", {
                            method: 'POST',
                            headers: {
                                // 'Accept': 'application/json',
                                'Content-Type': 'application/json',
                                'User-Id': userid,
                                'token': accessToken,
                            },
                            body: JSON.stringify({
                                user_id: userid,
                            })
                        })
                            .then((response) => response.json())
                            .then((responseData) => {
                                console.log(" responseData resoJSon===" + JSON.stringify(responseData));
                                console.log("resoJSon===" + responseData.error);
                                if (responseData.error == "false") {
                                    console.log("Error false massage" + JSON.stringify(responseData.errorMessage));

                                    this.setState({
                                        data: responseData.reqDetail,
                                        animating: false,
                                        lodingDialog: false,
                                    });
                                } else if (responseData.error == "true") {
                                    this.setState({
                                        NoRecord: responseData.errorMessage,
                                        animating: false,
                                        lodingDialog: false

                                    });

                                }
                            })

                    });
            });
    }

    invoice(id) {
        this.setState({
            animating: true,
            lodingDialog: true,

        })

        fetch(Strings.base_Url + "user_invoice", {
            method: 'POST',
            headers: {
                // 'Accept': 'application/json',
                'Content-Type': 'application/json',
                'User-Id': this.state.user_id,
                'token': this.state.token,
            },
            body: JSON.stringify({
                req_id: id,
            })
        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(" responseData resoJSon===" + JSON.stringify(responseData));
                console.log("resoJSon===" + responseData.error);
                if (responseData.error == "false") {
                    console.log("Error false massage" + JSON.stringify(responseData.errorMessage));
                    this.setState({
                        animating: false,
                        lodingDialog: false,
                    });
                    Actions.push("Invoice", { url: responseData.invoice })
                } else if (responseData.error == "true") {
                    this.setState({
                        animating: false,
                        lodingDialog: false

                    });

                }
            })

    }
    ratingCompleted(rating) {
        console.log("Rating is: " + rating)
    }
    render() {
        return (
            <View style={styles.containerWhite}>
                <View style={{ flex: 1, flexDirection: 'column', margin: 10 }}>
                    {this.state.NoRecord != '' && (
                        <Text style={{ padding: 10, fontSize: 16, fontWeight: 'bold', textAlign: 'center' }}>{this.state.NoRecord}</Text>
                    )
                    }
                    <FlatList
                        keyExtractor={item => item.id}

                        data={this.state.data}
                        renderItem={({ item, index }) => (

                            <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 10 }}>
                                <Card style={{ width: "90%", justifyContent: 'space-between', 
                                alignItems: 'center', flexDirection: 'column', padding: 10, borderRadius: 15 }}>
                                    <View style={{ flexDirection: 'row' ,alignItems:'center'}}>
                                        <View style={{ width: '30%' }}>
                                            <Image
                                                // source={require("../assets/image.png")}
                                                source={{ uri: this.state.photo_url + item.photo }}
                                                style={{ width: 70, height: 80, borderRadius: 8 }} />
                                        </View>

                                        <View style={{ width: '70%' }}>
                                            <View style={{ flexDirection: 'row',alignItems:'center' }}>
                                            <Image
                                                // source={require("../assets/image.png")}
                                                source={require('../assets/9.png')}
                                                style={{ width: 10, height: 10 }} />
                 
                                                <Text style={{marginLeft:5, fontSize: 11, fontFamily:'Poppins-SemiBold' }}>Zone de livraison </Text>
                                                  </View>
                                                <Text style={{ fontSize: 10,marginLeft:15,color:'grey',marginTop:-2, fontFamily:'Poppins-Medium' }}>{item.zone}</Text>
                                                <Text style={{ fontSize: 10,marginLeft:15,color:'grey',marginTop:-2, fontFamily:'Poppins-Medium' }}>{item.subzone}</Text>

                                                {/* <Text style={{ fontSize: 14, fontWeight: 'bold' }}>Commande Id : </Text>
                                                <Text style={{ fontSize: 14, fontWeight: 'bold' }}>{item.order_id}</Text> */}
                                                <View style={{ flexDirection: 'row',alignItems:'center' }}>
                                            <Image
                                                // source={require("../assets/image.png")}
                                                source={require('../assets/calender.png')}
                                                style={{ width: 10, height: 10 }} />
                 
                                                <Text style={{marginLeft:5, fontSize: 11, fontFamily:'Poppins-SemiBold' }}>Date de livraison </Text>
                                                  </View>
                                                  <View style={{ flexDirection: 'row',alignItems:'center' }}>
                                                <Text style={{ fontSize: 10,marginLeft:15,color:'grey',marginTop:-2, fontFamily:'Poppins-Medium' }}>{item.reservation_date}</Text>
                                                <Text style={{ fontSize: 10,marginLeft:5,color:'grey',marginTop:-2, fontFamily:'Poppins-Medium' }}>{item.reservation_time}</Text>
                                                </View>
                                                <View style={{ flexDirection: 'row', marginTop: 5, marginBottom: 5 }}>
 
                                        <TouchableOpacity
                                            onPress={() => Actions.push("OrderView", { currentOrderId: item.id })}
                                            style={{
                                                backgroundColor: '#28528F',  height: 20, borderRadius: 14,
                                                alignItems: 'center', justifyContent: "center", marginLeft: 10,padding:5
                                            }}>
                                            <Text style={{ fontSize: 10,fontFamily:'Poppins-SemiBold', marginLeft:8,marginRight:8,color: "white", textAlign:'center', }}>Voir</Text>
                                        </TouchableOpacity>

                                        {/* 0=None,1=Assign,2=Trip start,3=On the way,4=Reach location,5=Complete */}

                       
                                    </View>
                                        
                                        </View>
                                    </View>

                                   
                                </Card>
                            </View>

                        )}
                    />
                </View>

                <PopupDialog
                    onHardwareBackPress={() => { this.setState({ lodingDialog: false }) }}
                    width={0.3}
                    visible={this.state.lodingDialog}
                    dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
                    <DialogContent>
                        <View style={{ alignItems: 'center', }}>
                            <ActivityIndicator
                                animating={this.state.animating}
                                style={[{ height: 10, marginBottom: 10, marginTop: 30, marginLeft: 20, marginRight: 20 }]}
                                color="#C00"
                                size="large"
                                hidesWhenStopped={true}
                            />
                        </View>
                    </DialogContent>
                </PopupDialog>
            </View>
        );
    }
}
