import React from 'react';
import { StatusBar, Button,ActivityIndicator,Alert, FlatList, ScrollView, PixelRatio, TextInput, View, ImageBackground, Text, Dimensions, TouchableOpacity, Linking, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from "../styles/styles";
import Strings from '../strings/strings'
import { Card } from 'native-base';
import { Rating, AirbnbRating } from 'react-native-elements';
import PopupDialog, {
    DialogTitle, DialogContent, DialogFooter, DialogButton, SlideAnimation, ScaleAnimation,
} from 'react-native-popup-dialog';

let tech_rating;
import Image from 'react-native-fast-image'

export default class rating extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name:'',
            email:'',
            rating_user_id:'',
            profile_pic_url:'',
            ratings:'',
            ratingErr:'',
            rating_text:'',
            rating_textErr:'',
            tech_complete:'',
            price_per:'',
            user_id:'',
            token:'',
            req_id:this.props.req_Id,

        };

   console.log("req_id::::"+this.props.req_Id)
    }

    componentWillMount() {
        this.setState({
            animating: true,
            lodingDialog: true
        });
        AsyncStorage.getItem("userid")
            .then(userid => {
                this.setState({ user_id: userid });
                AsyncStorage.getItem("token")
                    .then(token => {
                        var accessToken = JSON.parse(token);
                        this.setState({ token: accessToken });
                        console.log("accessToken====$$$========" + accessToken);
                        console.log("state userId====$$$========" + userid);
                        // this.refs.loading2.show();
                        fetch(Strings.base_Url + "getTechDetail", {
                            method: 'POST',
                            headers: {
                                // 'Accept': 'application/json',
                                'Content-Type': 'application/json',
                                'User-Id': userid,
                                'token': accessToken,
                            },
                            body: JSON.stringify({
                                req_id: this.props.req_Id,
                            })
                        })
                            .then((response) => response.json())
                            .then((responseData) => {
                                console.log(" responseData resoJSon===" + JSON.stringify(responseData));
                                console.log("resoJSon===" + responseData.error);
                                if (responseData.error == "false") {
                                    console.log("Error false massage::::" + responseData.user_record.tech_complete_job +responseData.user_record.price_per_hour);
                                    this.setState({
                                        name:responseData.user_record.firstName +" "+responseData.user_record.lastName,
                                        email:responseData.user_record.email,
                                        rating_user_id:responseData.user_record.tech_id,
                                        tech_complete:responseData.tech_complete_job,
                                        profile_pic_url:responseData.profile_pic_url,
                                        price_per_hours:responseData.price_per_hour,
                                        animating: false,
                                        lodingDialog: false,
                                    });                                    
                                } else if (responseData.error == "true") {
                                    this.setState({
                                        no_record: "Aucun dossier n'existe",
                                        animating: false,
                                        lodingDialog: false
                                    });
                                }
                            })
                    });
            });
    }

    validaion(){

        var isValidation = 0;
        if(tech_rating != ''){
            isValidation += 1
        }else{
            isValidation -= 1
            this.setState({
                ratingErr:Strings.ShouldemptyText
            })
        }
        // if(this.state.rating_text != ''){
        //     isValidation += 1
        // }
        // else{
        //     isValidation -= 1
        //     this.setState({
        //         rating_textErr:Strings.ShouldemptyText
        //     })
        // }
        if(isValidation == 1){
            this. submitRating();
            console.log("Rating is: " + tech_rating)
        }else{

        }  
    }

    submitRating(){
        
        this.setState({
            animating: true,
            lodingDialog: true
        });
        fetch(Strings.base_Url + "techRating", {
            method: 'POST',
            headers: {
                // 'Accept': 'application/json',
                'Content-Type': 'application/json',
                'User-Id': this.state.user_id,
                'token': this.state.token,
            },
            body: JSON.stringify({
                user_id:this.state.user_id,
                tech_id:this.state.rating_user_id,
                req_id:this.state.req_id,
                rating:tech_rating,
                rating_desc:this.state.rating_text
            })
        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(" responseData resoJSon===" + JSON.stringify(responseData));
                console.log("resoJSon===" + responseData.error);
                if (responseData.error == "false") {
                    console.log("Error false massage" + JSON.stringify(responseData.errorMessage));
                    this.setState({
                        animating: false,
                        lodingDialog: false,
                    });  
                   // this.ThanksRBSheet.open();
                   Actions.push("Location")

                } else if (responseData.error == "true") {
                  
                    this.showalerts(responseData.errorMessage)
                    this.setState({
                        animating: false,
                        lodingDialog: false
                    });
                }
            })
    }

    showalerts(body){
        Alert.alert(
            Strings.alert_tital,
            body,
            [
              {text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
            ],
            { cancelable: false });
    }

    ratingCompleted(rating) {
        tech_rating=rating
        console.log("Rating is: " + rating)
    }
    render() {
        console.log("job::"+this.state.price_per_hour)
        return (
            <View style={styles.containerWhite}>
                <View style={{ width: '100%', height: 50, flexDirection: 'row', backgroundColor: 'white' }}>
                    <TouchableOpacity onPress={() => Actions.pop()} style={{ width: '20%', height: 50, justifyContent: 'center', alignItems: 'center', borderTopRightRadius: 20, borderBottomRightRadius: 20 }}>
                        <Image source={require("../assets/left-arrow.png")}
                            style={{ width: 30, height: 20, }}
                            resizeMode="contain" />
                    </TouchableOpacity>
                    <View style={{ width: '70%', height: 50, justifyContent: 'center', alignItems: 'center', }}>
                        <Text style={{ fontSize: 16, fontWeight: 'bold' }}>{Strings.avis_text}</Text>
                    </View>
                </View>
                <ScrollView>
                    <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }}>
                        <Image source={require("../assets/image.png")}
                            style={{ width: '100%', height: 100, backgroundColor: 'red' }}
                        />

                        <View style={{ width: 100, height: 100, borderRadius: 100 / 2, backgroundColor: 'white', marginTop: -50, alignItems: 'center', justifyContent: 'center' }}>
                           
                        {this.state.profile_pic_url != "" ?
                            <Image source={{uri: this.state.profile_pic_url}}
                            style={{ width: 95, height: 95, borderRadius: 95 / 2 }} />:
                            <Image source={require("../assets/images.jpeg")}
                                style={{ width: 95, height: 95, borderRadius: 95 / 2 }} />
                          }
                           
                           
                        </View>

                        <Text style={{ fontSize: 18, fontWeight: 'bold', padding: 5 }}>{this.state.name}</Text>
                        <Text style={{ fontSize: 16, }}>{this.state.email}</Text>
                        <View style={{ flexDirection: 'row', marginTop: 10, alignItems: 'center' }}>
                            <View style={{ width: 25, height: 25, borderWidth: 1, borderRadius: 4, borderColor: Strings.color_green_code, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ fontWeight: 'bold', color: Strings.color_green_code }}>{this.state.tech_complete}</Text>
                            </View>
                            <Text style={{ fontSize: 14, }}>{Strings.travaux_text}</Text>
                        </View>
                        <Text style={{ fontSize: 14, padding: 5 }}>{Strings.cout_text} € {this.state.price_per_hours}</Text>
                        <View style={{marginTop:50}}>
                        <AirbnbRating
                            showRating={false}
                            count={5}
                            color={Strings.color_green_code}
                            defaultRating={0}
                            size={35} 
                            onFinishRating={this.ratingCompleted}/>
                        </View>
                        {!!this.state.ratingErr && (
                                <Text style={{ color: 'red', marginLeft: 10,  fontSize: 12 }}>{this.state.ratingErr}</Text>
                            )}
                       
                       

                       
                            <Card style={{ width: '90%', backgroundColor: 'white', borderRadius: 10, marginLeft: 10,marginTop:20,height:150 }}>
                               <TextInput placeholder={Strings.ou_commenter_text}
                                onChangeText={(rating_text)=>this.setState({rating_text,rating_textErr:''})}
                               multiline={true} style={{padding:10}}>

                               </TextInput>
                            </Card>
                            {!!this.state.rating_textErr && (
                                <Text style={{ color: 'red', marginLeft: 10,  fontSize: 12 }}>{this.state.rating_textErr}</Text>
                            )}
                           

                        <View style={{ width: '95%', marginTop: 40, flexDirection: 'row', marginBottom: 50 }}>
                           
                            <View style={{ width: '100%', alignItems: 'center' }}>
                                <TouchableOpacity onPress={() =>this.validaion() }  style={{ width: '50%', height: 40, borderRadius: 10, backgroundColor: Strings.color_green_code, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ padding: 10, color: 'white' }}>{Strings.confirmer_text}</Text>

                                </TouchableOpacity>
                            </View>
                        </View>

                    </View>


                <PopupDialog
                    onHardwareBackPress={() => { this.setState({ lodingDialog: false }) }}
                    width={0.3}
                    visible={this.state.lodingDialog}
                    dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
                    <DialogContent>
                        <View style={{ alignItems: 'center', }}>
                            <ActivityIndicator
                                animating={this.state.animating}
                                style={[{ height: 10, marginBottom: 10, marginTop: 30, marginLeft: 20, marginRight: 20 }]}
                                color="#C00"
                                size="large"
                                hidesWhenStopped={true}
                            />
                        </View>
                    </DialogContent>
                </PopupDialog>
                </ScrollView>
            </View>
        );
    }
}
