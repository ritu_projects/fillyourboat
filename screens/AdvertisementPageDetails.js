import React from 'react';
import { StatusBar, ScrollView, Alert, TextInput, View, ActivityIndicator, Platform, ImageBackground, Share, Text, Dimensions, TouchableOpacity, Linking, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from "../styles/styles";
import Strings from '../strings/strings';
import PopupDialog, {
    DialogTitle, DialogContent, DialogFooter, DialogButton, SlideAnimation, ScaleAnimation,
} from 'react-native-popup-dialog';
const { width, height } = Dimensions.get("window");
//import Share from "react-native-share";
import Image from 'react-native-fast-image'



export default class AdvertisementPageDetails extends React.Component {

    constructor(props, context) {
        super(props);
        this.state = {
            user_id: '',
            token: '',
            orderid: '',
            order_record: '',
            animating: false,
            lodingDialog: false,
            d_lat_val: 0.0,
            d_lng_val: 0.0,
        }
    };

    componentDidMount() {
        this.getOrderSummery();
    }


    getOrderSummery = () => {
        AsyncStorage.getItem("userid")
            .then(userid => {
                var userid = JSON.parse(userid);

                this.setState({ user_id: userid });

                AsyncStorage.getItem("token")
                    .then(token => {
                        var accessToken = JSON.parse(token);

                        this.setState({ token: accessToken });
                        console.log("Order accessToken ", accessToken)



                        this.setState({
                            animating: true,
                            lodingDialog: true

                        });

                        fetch(Strings.base_Url + "getAdervertisementsingle", {
                            method: 'POST',
                            headers: {
                                // 'Accept': 'application/json',
                                'Content-Type': 'application/json',
                                'user-id': this.state.user_id,
                                'token': this.state.token,
                            },
                            body: JSON.stringify({
                                id: this.props.sub_cat_id,

                            })


                        })
                            .then((response) => response.json())
                            .then((responseData) => {
                                console.log(" responseData getAddInfo Delivery resoJSon===" + JSON.stringify(responseData));
                                console.log("resoJSon getAddInfo Delivery===" + responseData.error);
                                if (responseData.error == "false") {

                                    console.log("Error false massage getAddInfo Delivery " + responseData);
                                    console.log("Error false massage getAddInfo Delivery " + responseData.record);


                                    this.setState({
                                        animating: false,
                                        lodingDialog: false,
                                        order_record: responseData.record,



                                    });


                                    //this.RBSheet.close()
                                } else if (responseData.error == "true") {

                                    this.showalerts(responseData.errorMessage)
                                    this.setState({
                                        animating: false,
                                        lodingDialog: false

                                    });
                                }
                            }
                            )
                    })
            })
    }
    callNumber = () => {

        let phoneNumber = '';


        if (Platform.OS === 'android') {
            phoneNumber = 'tel:${' + this.state.order_record[0].contact_number + '}';
        }
        else {
            phoneNumber = 'telprompt:${' + this.state.order_record[0].contact_number + '}';
        }

        Linking.openURL(phoneNumber);
    }

    sendEmail = () => {
        var admin_email = this.state.order_record[0].contact_email;

        Linking.openURL('mailto:' + admin_email);
    }
    openURL = () => {
        Linking.openURL(this.state.order_record[0].contact_link)

    }

    shareApp = () => {
        Share.share(
            {
                message: 'Pour en savoir plus sur FillYourBoat, tÃ©lÃ©chargez l\'application sur le Play Store https://play.google.com/store/apps/details?id=fr.fillyourboat.technician Pour en savoir plus sur FillYourBoat, tÃ©lÃ©chargez l\'application sur le Play Store https://play.google.com/store/apps/details?id=fr.fillyourboat.userr télécharger FillYourBoat maintenant. https://play.google.com/store/apps/details?id=fr.fillyourboat.user',
                 url: 'https://play.google.com/store/apps/details?id=fr.fillyourboat.technician',
            }
        ).then(({ action, activityType }) => {
            if (action === Share.sharedAction)
                console.log('Share was successful');
            else
                console.log('Share was dismissed');
        });
        // Share.open(options)
        // .then((res) => { console.log(res) })
        // .catch((err) => { err && console.log(err); });
    }

    openMap = () => {


        var d_lat_val = this.state.order_record[0].latitude;
        var d_lng_val = this.state.order_record[0].longitude;

        Platform.select({
            ios: () => {
                Linking.openURL('http://maps.apple.com/maps?saddr=' + d_lat_val + ',' + d_lng_val);
            },
            android: () => {
                Linking.openURL('http://maps.google.com/maps?saddr=' + d_lat_val + ',' + d_lng_val);
            }
        })();

    }

    render() {
        return (
            <View style={styles.containerWhite}>

                {this.state.order_record.length > 0 ?

                    <View>

                        <View style={{ width: width, height: 40, alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between' }}>
                            <TouchableOpacity
                                onPress={() => Actions.pop()} style={{ height: 40, alignItems: 'center', justifyContent: 'center' }}>
                                <Image source={require("../assets/left-arrow.png")}
                                    style={{ width: 20, height: 15, marginLeft: 5, }}
                                    resizeMode="stretch" />
                            </TouchableOpacity>
                            <Text style={{ fontSize: 14, fontFamily: 'Poppins-Medium', }}>{this.state.order_record[0].title}</Text>


                            <View style={{ width: 15 }}>
                            </View>
                        </View>
                        <ScrollView style={{}}>


                            <Image style={{
                                width: width, alignSelf: 'center',
                                height: 200
                            }}

                                source={{ uri: this.state.order_record[0].image_url }}
                            />
                            <View style={{ width: width * 88 / 100, alignSelf: 'center', marginTop: height * 2 / 100 }}>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                    <Text style={{
                                        fontSize: width * 3.9 / 100,
                                        fontFamily: 'Poppins-Bold'
                                    }}>{this.state.order_record[0].title}</Text>
                                    <TouchableOpacity
                                        onPress={() => this.openMap()}
                                    >
                                        <Text style={{ fontFamily: 'Poppins-SemiBold', fontSize: width * 2.8 / 100, color: '#28528F' }}>Voir la carte</Text>

                                    </TouchableOpacity>
                                </View>
                                <View
                                    style={{
                                        flexDirection: "row",
                                        justifyContent: 'space-between',
                                        alignItems: 'center'

                                    }}>
                                    <View style={{ flexDirection: 'row', marginTop: 3, alignItems: 'center' }}>
                                        <Image style={{
                                            width: 15,
                                            height: 15,
                                        }}

                                            source={require("../assets/location_grey.png")}
                                        />

                                        <Text style={{
                                            fontSize: width * 2.7 / 100, width: width * 72 / 100, marginLeft: 4, fontFamily: 'Poppins-Medium'
                                        }}>{this.state.order_record[0].address}</Text>
                                    </View>
                                    {/* <View style={{width:width*10/100,backgroundColor:'#28528F',borderRadius:5,flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                                        <Text style={{fontSize:width*2/100,color:'white',fontFamily:'Poppins-SemiBold',marginRight:width*1.1/100}}>4.2</Text>
                                    <Image source={require('../assets/star.png')} style={{width:width*1.5/100,height:width*1.5/100}}/>
                                    </View> */}
                                </View>

                                <View style={{ flexDirection: 'row', marginTop: 4, alignItems: 'center' }}>
                                    <Image style={{
                                        width: 15,
                                        height: 15,
                                    }}

                                        source={require("../assets/2445.png")}
                                    />

                                    <Text style={{
                                        fontSize: width * 2.7 / 100, width: width * 72 / 100, marginLeft: 4, fontFamily: 'Poppins-Medium'
                                    }}>Promocode {this.state.order_record[0].promocode}</Text>
                                </View>
                                <View style={{ flexDirection: 'row', marginTop: 4, alignItems: 'center' }}>
                                    <Image style={{
                                        width: 15,
                                        height: 15,
                                    }}

                                        source={require("../assets/26.png")}
                                    />

                                    <Text style={{
                                        fontSize: width * 2.7 / 100, width: width * 72 / 100, marginLeft: 4, fontFamily: 'Poppins-Medium'
                                    }}>Jusqu'au {this.state.order_record[0].promo_date}</Text>
                                </View>


                                <Text style={{
                                    fontSize: width * 2.8 / 100, color: '#28528F',
                                    marginTop: height * 3.5 / 100, fontFamily: 'Poppins-Medium', width: width * 88 / 100, alignSelf: 'center'
                                }}>Description : {this.state.order_record[0].description}</Text>

                                <Text style={{
                                    fontSize: width * 2.8 / 100,
                                    marginTop: height * 3.5 / 100, fontFamily: 'Poppins-SemiBold'
                                }}>Contact on : </Text>
                                <TouchableOpacity

                                    onPress={() => this.callNumber()}>
                                    <Text style={{
                                        fontSize: width * 2.8 / 100,
                                        marginTop: 2, fontFamily: 'Poppins-Medium'
                                    }}>{this.state.order_record[0].contact_number}</Text>

                                </TouchableOpacity>

                                <TouchableOpacity
                                    onPress={() => this.sendEmail()} >

                                    <Text style={{
                                        fontSize: width * 2.8 / 100,
                                        marginTop: 2, fontFamily: 'Poppins-Medium'
                                    }}>{this.state.order_record[0].contact_email}</Text>
                                </TouchableOpacity>

                                <TouchableOpacity
                                    onPress={() => this.openURL()}

                                >

                                    <Text style={{
                                        fontSize: width * 2.8 / 100,
                                        marginTop: 2, fontFamily: 'Poppins-Medium'
                                    }}>{this.state.order_record[0].contact_link}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => this.openMap()}

                                >
                                    <Text style={{
                                        fontSize: width * 2.8 / 100, color: '#28528F',
                                        marginTop: 2, fontFamily: 'Poppins-Medium'
                                    }}>Voir le site</Text>
                                </TouchableOpacity>


                            </View>







                            <TouchableOpacity
                                onPress={() => this.shareApp()}

                                style={{
                                    flexDirection: 'row', height: 34, marginTop: height * 2.5 / 100, width: 150, justifyContent: 'center', alignItems: 'center'
                                    , backgroundColor: 'rgba(26,87,138,1)', marginBottom: 20, borderRadius: 20, alignSelf: 'center'
                                }}>

                                <Image source={require('../assets/share.png')} style={{ width: 16, height: 15 }} />
                                <Text style={{ fontSize: 11, fontFamily: 'Poppins-SemiBold', color: 'white', marginLeft: 6 }}>Partager</Text>

                            </TouchableOpacity>
                        </ScrollView>
                    </View>

                    :
                    <Text style={{ fontFamily: "Poppins-Medium", marginTop: 10, color: 'white' }}>Aucune donnée</Text>
                }





                <PopupDialog
                    onHardwareBackPress={() => { this.setState({ lodingDialog: false }) }}
                    width={0.3}
                    visible={this.state.lodingDialog}
                    dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
                    <DialogContent>
                        <View style={{ alignItems: 'center', }}>
                            <ActivityIndicator
                                animating={this.state.animating}
                                style={[{ height: 10, marginBottom: 10, marginTop: 30, marginLeft: 20, marginRight: 20 }]}
                                color="#C00"
                                size="large"
                                hidesWhenStopped={true}
                            />
                        </View>
                    </DialogContent>
                </PopupDialog>

            </View>
        );
    }


}