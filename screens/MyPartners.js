import React from 'react';
import {
    StatusBar, ScrollView, Alert, TextInput, View,
    ActivityIndicator,DrawerLayoutAndroid, ImageBackground, Text, Modal, Dimensions, TouchableOpacity, Linking, AsyncStorage, FlatList,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from "../styles/styles";
import Sidebar from '../screens/sideBar'
import Strings from '../strings/strings';
import { Card, Radio, CheckBox } from 'native-base';
import Drawer from 'react-native-drawer'


import PopupDialog, {
    DialogTitle, DialogContent, DialogFooter, DialogButton, SlideAnimation, ScaleAnimation,
} from 'react-native-popup-dialog';

const { width, height } = Dimensions.get("window");
import Image from 'react-native-fast-image'



export default class MyPartners extends React.Component {

    constructor(props, context) {
        super(props);
        this.state = {
            order_id: '',
            modalVisible: false,
            animating: false,
            lodingDialog: false,
            user_id: '',
            token: '',
            data: '',
            data2: '',

        }
        console.log("currentOrderId::::" + this.props.currentOrderId)

    };


    componentDidMount() {
        this.getOrderStatus()
        this._drawer.close()

    }
    getOrderStatus = () => {


        AsyncStorage.getItem("userid")
            .then(userid => {
                this.setState({ user_id: userid });

                AsyncStorage.getItem("token")
                    .then(token => {
                        var accessToken = JSON.parse(token);
                        this.setState({ token: accessToken });
                        this.setState({
                            animating: true,
                            lodingDialog: true,

                        });

                        fetch(Strings.base_Url + "getCategory", {
                            method: 'GET',
                            headers: {
                                'Content-Type': 'application/json',
                                'User-Id': this.state.user_id,
                                'token': this.state.token,
                            },
                          

                        })
                            .then((response) => response.json())
                            .then((responseData) => {
                                console.log(" responseData getFuelList resoJSon===" + JSON.stringify(responseData));
                                console.log("resoJSon getFuelList===" + responseData.error);
                                if (responseData.error == "false") {
                                    console.log("Error false massage getFuelList" + JSON.stringify(responseData.record));

                                    this.setState({
                                        animating: false,
                                        lodingDialog: false,
                                        data2: responseData.category_record,


                                    });


                                    //this.RBSheet.close()
                                } else if (responseData.error == "true") {

                                    this.showalerts(responseData.errorMessage)
                                    this.setState({
                                        animating: false,
                                        lodingDialog: false

                                    });
                                }
                            }
                            )
                    })
            })
    }

 


    _setDrawer() {
        this._drawer.open()
    }

    render() {
        var navigationView = (
            <Sidebar />
        )
        return (
            <Drawer
            openDrawerOffset={100}
           type="overlay"
             ref={(ref) => this._drawer = ref}
             content={<Sidebar />}
             >
            <View style={styles.containerWhite}>

<Card style={{ width: width,marginTop:-5, height: 50, flexDirection: 'row',marginTop: Platform.OS == 'ios' ? 20 :0, }}>
                        <TouchableOpacity
                            onPress={this._setDrawer.bind(this)} style={{  height: 50,alignItems:'center'  }}>
                            <Image source={require("../assets/menu_img.png")}
                                style={{ width: 35, height: 35,marginTop:8 ,marginLeft:-2}}
                                resizeMode="stretch" />
                        </TouchableOpacity>
                        <View style={{ width: '75%', height: 50, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: 16, fontFamily:'Poppins-SemiBold', }}>Nos partenaires</Text>

                        </View>


                    </Card>


                <ScrollView style={{ marginLeft: 10, marginRight: 10, }}>
<View style={{width:width*80/100,alignSelf:'center',marginTop:15}}>
                    <Text style={{textAlign:'center',color:'#1A578A', fontFamily:'Poppins-Bold',fontSize:14}}>Tout pour une journée en mer parfaite</Text>



                    {this.state.data2.length > 0 ?

                        <FlatList
                            keyExtractor={item => item.id}

                            data={this.state.data2}
                            renderItem={({ item, index }) => (

                                <TouchableOpacity 
                                 style={{ justifyContent: 'center', alignItems: 'center', marginTop: 10 }}
                                 onPress={() =>Actions.push("Advertisementlist", { category_id : item.id })}>
                                    <Card style={{
                                        width: width*74/100, justifyContent: 'space-between',
                                        alignItems: 'center', flexDirection: 'column', height:42, borderRadius: 30, backgroundColor: '#1A578A'
                                    }}>
                                        <View style={{ flexDirection: 'row',height:42,width:width*72/100,alignItems:'center'}}>
                                            <Image
                                                // source={require("../assets/image.png")}
                                                source={{ uri: item.image }}
                                                style={{ width: 36, height: 36,borderRadius:18 }} />


                                            <Text style={{ fontSize: 11,fontFamily:'Poppins-SemiBold',width:width*72/100-60, color: 'white',marginLeft:5,textAlign:'center' }}>{item.cat_name}</Text>

                                        </View>
                                    </Card>
                                </TouchableOpacity>

                            )}
                        />
                        :
                        <Text style={{ fontFamily: "Poppins-Medium", marginTop: 10, }}>Aucune donnée</Text>

                    }




                  
                    <PopupDialog
                        onHardwareBackPress={() => { this.setState({ lodingDialog: false }) }}
                        width={0.3}
                        visible={this.state.lodingDialog}
                        dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
                        <DialogContent>
                            <View style={{ alignItems: 'center', }}>
                                <ActivityIndicator
                                    animating={this.state.animating}
                                    style={[{ height: 10, marginBottom: 10, marginTop: 30, marginLeft: 20, marginRight: 20 }]}
                                    color="#C00"
                                    size="large"
                                    hidesWhenStopped={true}
                                />
                            </View>
                        </DialogContent>
                    </PopupDialog>
                    </View>
                </ScrollView>

            </View>
         </Drawer>
        );
    }


}