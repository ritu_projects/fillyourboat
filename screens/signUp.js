import React from 'react';
import {StatusBar,PermissionsAndroid, ScrollView, View, ImageBackground, Text, Dimensions, TouchableOpacity, Linking, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from "../styles/styles";
import Strings from '../strings/strings'
import AudioRecorderPlayer from 'react-native-audio-recorder-player';


const { width, height } = Dimensions.get("window");
const audioRecorderPlayer = new AudioRecorderPlayer();
import Image from 'react-native-fast-image'


export default class signUp extends React.Component {
    // onRegionChange(region) {
    //   this.setState({ region });
    // }
    constructor(props, context) {
        super(props);
        this.state = {
            setlangDefault: "", isUpdated: false,
            showUpdateDialog: false,
            isAuthorized: "1",
            userid: '',


        };
    }

  
 
// onStartRecord = async () => {
//   const result = await AudioRecorderPlayer.startRecorder();
//   console.log('onStartRecord ::::');
//   AudioRecorderPlayer.addRecordBackListener((e) => {
//     this.setState({
//       recordSecs: e.current_position,
//       recordTime: AudioRecorderPlayer.mmssss(
//         Math.floor(e.current_position),
//       ),
//     });
//     return;
//   });
//   console.log("result::::::::"+result);
// };
 
// onStopRecord = async () => {
//   const result = await AudioRecorderPlayer.stopRecorder();
//   AudioRecorderPlayer.removeRecordBackListener();
//   this.setState({
//     recordSecs: 0,
//   });
//   console.log(result);
// };
 
// onStartPlay = async () => {
//   console.log('onStartPlay');
//   const msg = await AudioRecorderPlayerstartPlayer();
//   console.log(msg);
//   AudioRecorderPlayer.addPlayBackListener((e) => {
//     if (e.current_position === e.duration) {
//       console.log('finished');
//       AudioRecorderPlayer.stopPlayer();
//     }
//     this.setState({
//       currentPositionSec: e.current_position,
//       currentDurationSec: e.duration,
//       playTime: AudioRecorderPlayer.mmssss(Math.floor(e.current_position)),
//       duration: AudioRecorderPlayer.mmssss(Math.floor(e.duration)),
//     });
//     return;
//   });
// };
 
// onPausePlay = async () => {
//   await AudioRecorderPlayer.pausePlayer();
// };
 
// onStopPlay = async () => {
//   console.log('onStopPlay');
//   AudioRecorderPlayer.stopPlayer();
//   AudioRecorderPlayer.removePlayBackListener();
// };
    render() {
        return (
           
            <Image 
            source={require("../assets/100.png")}
            style={{
                width: width,
                height: height,
                resizeMode: 'stretch',

            }}
            resizeMode="stretch">
           
                
                
                    <Text>{this.state.currentPositionSec}</Text>

                    <View style={{ alignItems: 'center' }}>
                        <Image source={require("../assets/hop-assist-logo.png")}
                            style={{ width: '100%', height: 150, marginTop: 50 }}
                            resizeMode="contain">

                        </Image>
                        <TouchableOpacity onPress={() => Actions.push("Login")} style={{ alignItems: 'center', justifyContent: 'center', width: '90%', height: 50, marginTop: 80, borderRadius: 10, backgroundColor: '#F1C368' }}>
                            <Text style={{ fontSize: 16, color: 'white' }}>{Strings.email_login_text}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => Actions.push("Login")} style={{ alignItems: 'center', justifyContent: 'center', width: '90%', height: 50, marginTop: 30, borderRadius: 10, backgroundColor: '#F1C368' }}>
                            <Text style={{ fontSize: 16, color: 'white' }}>{Strings.mobile_login_text}</Text>

                        </TouchableOpacity>

                        <TouchableOpacity onPress={()=> Actions.push("Register")} style={{ flexDirection: 'row', marginTop: 40, height: 50, alignItems: 'center' }}>
                            <Text style={{ fontSize: 15, fontWeight: 'bold' }}>{Strings.register_free_text}</Text>
                            <Text style={{ fontSize: 15, fontWeight: 'bold', color: '#F1C368',marginLeft:4 }}>{Strings.register_text}</Text>
                        </TouchableOpacity>

                        <View style={{width:'100%', marginTop: 50, }}>
                          <Image source={require("../assets/bg-3.png")}
                            style={{ width: '100%',marginBottom:-3 }}
                            resizeMode="contain"/>

                        </View>

                    </View>
                
         
       </Image>
        );
    }
}
