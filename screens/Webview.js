
import React, { Component } from 'react'
import { Alert, Text, AsyncStorage, ActivityIndicator, BackHandler, View, StyleSheet, Dimensions } from 'react-native'
import { WebView } from 'react-native-webview'
import Strings from '../strings/strings';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
import { Actions } from 'react-native-router-flux';

var currentOrderId = "";
class Webview extends React.Component {
    constructor(props) {
        super(props);
        this.webView = null;
        this.state = {
            user_id: '',
            orderid: '',

        }
    }
    componentDidMount() {



        this.callPaypalMethod();

      //  BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }
    componentWillUnmount() {
        this.callPaypalMethod()
      //  BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }
    callPaypalMethod() {
        AsyncStorage.getItem("userid")
            .then(userid => {
                var userid = JSON.parse(userid);

                this.setState({ user_id: userid });
            })
        AsyncStorage.getItem("orderid")
            .then(orderid => {
                // var userid = JSON.parse(userid);

                this.setState({ orderid: orderid });
            })

            currentOrderId = this.props.navigation.getParam('currentOrderId')
            console.log("Current Order  ",currentOrderId)

    }
    // handleBackPress = () => {
    //     this.props.navigation.navigate('Category')
    //     return true;
    // }
    onMessage(event) {
        console.log('On Message')
        Alert.alert(
            'On Message',
            event.nativeEvent.data,
            [
                { text: 'OK' },
            ],
            { cancelable: true }
        )
    }
    handleNavigation(event) {
        console.log(event)
        console.log("Url::::" + event.url)
        var String_url = event.url;
        console.log("Url: path:::" + String_url)

        if (String_url == 'http://fillyourboat.net/orderPaymentResponse') {


            // this.props.navigation.navigate('Puechases');
            Actions.push("FinalDeliver")

        }
        else if (String_url == 'http://fillyourboat.net/cancel') {
            alert('Paiement échoué')
            // this.props.navigation.navigate('MyCart');
            Actions.push("Payment")
        }
        else if (String_url == 'http://fillyourboat.net/fail') {
            alert('Défaut de paiement')
            // this.props.navigation.navigate('MyCart');
            Actions.push("Payment")
        }
        else if (String_url == 'http://fillyourboat.net/orderPaymentback') {
             
            alert('back')
            // this.props.navigation.navigate('MyCart');
            Actions.push("Payment")
        }
        

    }
    LoadingIndicatorView() {
        return (<View style={{ flex: 1 }}>
            <ActivityIndicator color='#009b88' size='large' style={styles.ActivityIndicatorStyle} />
        </View>)
    }
    render() {
        return (
            <View style={styles.container}>
                <WebView
                    source={{

                        uri:
                            'http://fillyourboat.net/orderPayment/' + currentOrderId
                    }}
                    renderLoading={this.LoadingIndicatorView}
                    startInLoadingState={true}
                    onMessage={(event) => this.handleMessage(event)}
                    onNavigationStateChange={(event) => this.handleNavigation(event)}
                    javaScriptEnabled={true}
                />
            </View>
        )
    }
}
export default Webview;
const styles = StyleSheet.create({
    container: { height: height, paddingBottom: 20 },
    ActivityIndicatorStyle: { flex: 1, alignSelf: 'center', justifyContent: 'center', alignItems: 'center' },
    header: {
        paddingTop: 40,
        paddingBottom: 10,
        backgroundColor: "#0c084c"
    },
    title: {
        color: "#fff",
        fontSize: 20,
        fontWeight: "bold",
        textAlign: "center"
    },
})