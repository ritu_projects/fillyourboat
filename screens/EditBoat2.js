import React from 'react';
import { StatusBar, ScrollView, Alert, TextInput, View, ActivityIndicator, ImageBackground, Text, Dimensions, TouchableOpacity, Linking, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from "../styles/styles";
import Strings from '../strings/strings';
import { Card, Radio, CheckBox } from 'native-base';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';
import DateTimePicker from "react-native-modal-datetime-picker";
import PopupDialog, {
    DialogTitle, DialogContent, DialogFooter, DialogButton, SlideAnimation, ScaleAnimation,
} from 'react-native-popup-dialog';

const { width, height } = Dimensions.get("window");
import Image from 'react-native-fast-image'

export default class EditBoat2 extends React.Component {
    // onRegionChange(region) {
    //   this.setState({ region });
    // }
    constructor(props, context) {
        super(props);
        this.state = {
            animating: false,
            lodingDialog: false,
            user_id: '',
            token: '',
            orderid: '',

            first_name: '',
            last_name: '',
            email: '',
            telephone: '',
            password: '',
            selected: '',
            pontoon_num: '',
            pontoon_numErr: '',
            name: '',
            name_Err: '',
            mobile_number: '',
            mobile_numberErr: '',
            uploads3: '',
            uploads_Err3: '',
            uploads_data:'',
            subzonename: '',
            presets :'',

        }
    }
    componentDidMount() {

        AsyncStorage.getItem("subzonename")
            .then(subzonename => {
                this.setState({ subzonename: subzonename });
            })

            AsyncStorage.getItem("deliver_person_name")
            .then(deliver_person_name => {
                console.log('Delivery Person Name ',deliver_person_name)
                if(deliver_person_name != null)
                {
            //   var deliver_person_name_prse = JSON.stringify(deliver_person_name);
                    console.log('Delivery Person Name inside ',deliver_person_name)

                    this.setState({ name: deliver_person_name,
                    name_Err :'',});
                }
            })


            AsyncStorage.getItem("deliver_person_mobilenumber")
            .then(deliver_person_mobilenumber => {
                console.log('Delivery Person Mobilenumber ',deliver_person_mobilenumber)
                if(deliver_person_mobilenumber != null)
                {
                    
                    console.log('Delivery Person Mobilenumber Parse ',deliver_person_mobilenumber)

                    this.setState({ mobile_number: deliver_person_mobilenumber,
                    mobile_numberErr: '', });
                }
            })

            AsyncStorage.getItem("port_number_edit")
            .then(port_number_edit => {
                console.log('Port Number Get ',port_number_edit)
                if(port_number_edit != null)
                {
                    
                    console.log('Port Number Parse ',port_number_edit)

                    this.setState({ pontoon_num: port_number_edit,
                    pontoon_numErr: '', });
                }
            })


        this.getBoatInformation();
    }

    getBoatInformation = () => {

        AsyncStorage.getItem("userid")
            .then(userid => {
                var userid = JSON.parse(userid);

                this.setState({ user_id: userid });

                AsyncStorage.getItem("token")
                    .then(token => {
                        var accessToken = JSON.parse(token);
                        this.setState({ token: accessToken });

                        AsyncStorage.getItem("orderid")
                            .then(orderid => {

                                this.setState({ orderid: orderid });

                                fetch(Strings.base_Url + "orderGetBoatInfo", {
                                    method: 'POST',
                                    headers: {
                                        // 'Accept': 'application/json',
                                        'Content-Type': 'application/json',
                                        'User-Id': this.state.user_id,
                                        'token': this.state.token,
                                    },
                                    body: JSON.stringify({
                                        order_id: this.state.orderid,

                                    })


                                })
                                    .then((response) => response.json())
                                    .then((responseData) => {
                                        console.log(" responseData getBoatInfo resoJSon===" + JSON.stringify(responseData));
                                        console.log("resoJSon getBoatInfo===" + responseData.error);
                                        if (responseData.error == "false") {
                                            console.log("Error false massage getBoatInfo" + JSON.stringify(responseData.boat_record));


                                            this.setState({
                                                animating: false,
                                                lodingDialog: false,
                                                first_name: responseData.boat_record.ship_name,
                                                last_name: responseData.boat_record.brand,
                                                email: responseData.boat_record.size,
                                                telephone: responseData.boat_record.registration,
                                                password: responseData.boat_record.reserve_capacity,
                                                selected: responseData.boat_record.fuel,

                                            });


                                            //this.RBSheet.close()
                                        } else if (responseData.error == "true") {

                                            this.showalerts(responseData.errorMessage)
                                            this.setState({
                                                animating: false,
                                                lodingDialog: false

                                            });
                                        }
                                    }
                                    )
                            })
                    })
            })

    }

    validationRegister() {
        // this.UserRegister();
        var isValidate = 0;
      
        if (this.state.pontoon_num != "") {
            isValidate += 1;
        } else {
            isValidate -= 1
            this.setState({

                pontoon_numErr: Strings.ShouldemptyText,
            });
        }
       

        // if (this.state.uploads3 != "") {
        //     isValidate += 1;
        // } else {
        //     isValidate -= 1
        //     this.setState({

        //         uploads_Err3: Strings.ShouldemptyText,
        //     });
        // }





        if (this.state.name != "") {

            isValidate += 1;

        }
        else {
            console.log("cat empty::::");
            isValidate -= 1;
            this.setState({
                name_Err: Strings.ShouldemptyText,
            });
        }
        if (this.state.mobile_number != "") {

            isValidate += 1;

        }
        else {
            console.log("cat empty::::");
            isValidate -= 1;
            this.setState({
                mobile_numberErr: Strings.ShouldemptyText,
            });
        }




        console.log("Use Login::::" + isValidate)


        if (isValidate == 3) {

            console.log("Use Login::::" + this.state.name + this.state.last_name)

            this.userAddBoat();
            this.setState({
                animating: true,
                lodingDialog: true,

            });
        }
        else {

        }

    }
    selectProfilePic = () => {
        const options = {
            title: 'Select Avatar',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };

        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                const source = { uri: response.uri };

                const imageUrl = response.uri
                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };
                console.log("image url:###:" + JSON.stringify(response))
                this.setState({
                    uploads_data: response.uri,
                    uploads3: response.data,
                    uploads_Err3: '',
                });
                console.log("image url::" + JSON.stringify(imageUrl))
                console.log("image url::" + JSON.stringify(source))


            }
        });
    }
    userAddBoat() {

        AsyncStorage.getItem("userid")
            .then(userid => {

                //  var userid = JSON.parse(userid);

                this.setState({ user_id: userid });

                AsyncStorage.getItem("token")
                    .then(token => {
                        var accessToken = JSON.parse(token);
                        this.setState({ token: accessToken });

                        AsyncStorage.getItem("orderid")
                            .then(orderid => {

                                this.setState({ orderid: orderid });

                                console.log("Photo is  ", this.state.uploads3)

                                let headers = {
                                    'Content-Type': 'application/json',
                                    'user-id': this.state.user_id,
                                    'token': this.state.token,
                                };
                                RNFetchBlob.fetch('POST', Strings.base_Url + 'createOrderStep5', headers, [
                                    { name: 'order_id', data: this.state.orderid },
                                    { name: 'body_count', data: this.state.pontoon_num },

                                    { name: 'comment', data: this.state.comment},
                                    { name: 'port_name', data: this.state.subzonename },
                                    { name: 'port_number', data: this.state.port_number },
                                    { name: 'full_name', data: this.state.name },

                                    { name: 'telephone_number', data: this.state.mobile_number },


                                    { name: 'photo', filename: 'photo.jpg', type: 'image/png', data: this.state.uploads3 },


                                ],
                                )
                                .uploadProgress((written, total) => {
                                    let presenteg = Math.floor((written * 100) / total)
                                    console.log('uploaded', written / total +" presente"+presenteg +"%")
                                    this.setState({
                                      presets:presenteg +" %"
                                    });
                                })
                                // listen to download progress event
                                .progress((received, total) => {
                                    console.log('progress', received / total)
                                })
                                .then((resp) => {

                                    console.log("response:::::::" + JSON.stringify(resp.json()));

                                    console.log("error:::" + resp.json().error)
                                    var error = resp.json().error

                                    if (resp.json().error === "false") {
                                        console.log("error:::" + resp.json().error)

                                        this.setState({
                                            animating: false,
                                            lodingDialog: false,
                                        });
                                        AsyncStorage.setItem('deliver_person_name',this.state.name)
                                        AsyncStorage.setItem('deliver_person_mobilenumber',this.state.mobile_number)
                                        AsyncStorage.setItem('port_number_edit',this.state.pontoon_num)

                                        Actions.push("Delvey_confirm_Screen")

                                    } else if (resp.json().error === "true") {

                                        this.showalerts(resp.json().errorMessage)
                                        this.setState({
                                            animating: false,
                                            lodingDialog: false,
                                        });

                                    }



                                }).catch((err) => {
                                    this.setState({
                                        animating: false,
                                        lodingDialog: false,
                                    });
                                    console.log("response::::err:::" + err);
                                });
                            })
                    })
            })

    }
    render() {
        return (
            <View style={styles.containerWhite}>


                <ScrollView style={{}}>

                    <Card style={{ width: '100%', marginTop: Platform.OS == 'ios' ? 20 :-8, height: 50, flexDirection: 'row' }}>
                        <TouchableOpacity onPress={() => Actions.pop()} style={{ marginLeft: 5, height: 50, justifyContent: 'center', alignItems: 'center', }}>
                            <Image source={require("../assets/back1.png")}
                                style={{ width: 15, height: 15, }}
                                resizeMode="contain" />
                        </TouchableOpacity>
                        <View style={{ marginLeft: width * 25 / 100, alignSelf: 'center', height: 50, justifyContent: 'center', alignItems: 'center' }}>
                            {/* <Text style={{ fontSize: 15, fontFamily: 'Poppins-SemiBold' }}>Ajouter un bateau {this.state.zone_name}</Text> */}
                            <Text style={{ fontSize: 15, fontFamily: 'Poppins-SemiBold' }}>Mon corps-mort</Text>

                        </View>

                    </Card>
                    <View style={{ margin: 20, width: '90%' }}>
                        {/* <Text style={{ fontSize: 15, fontFamily: 'Poppins-SemiBold', marginBottom: 7 }}>Mon corps-mort</Text> */}

                        <View style={{ width: '100%', borderWidth: 1, borderColor: 'grey', borderRadius: 10,backgroundColor:'#EDEDED', justifyContent: 'center', }}>
                            <Text

                                placeholderTextColor='grey'
                                style={{
                                    paddingLeft: 9, fontFamily: 'Poppins-Medium', fontSize: 13,
                                    height: 50, paddingTop: 15,
                                }} >{this.state.subzonename}</Text>
                        </View>
                        <View style={{ width: '100%', marginTop: 12, borderWidth: 1, borderColor: 'grey',backgroundColor:'#EDEDED', borderRadius: 10 }}>
                            <TextInput
                                placeholder='Numéro de corps-mort'
                                value={this.state.pontoon_num}
                                onChangeText={(pontoon_num) => this.setState({ pontoon_num, pontoon_numErr: '' })}
                                placeholderTextColor='grey'
                                style={{ paddingLeft: 9, fontFamily: 'Poppins-Medium', fontSize: 13, }} />
                        </View>
                        {!!this.state.pontoon_numErr && (
                            <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.pontoon_numErr}</Text>
                        )}



                        <View style={{ width: '100%', height: 80, marginTop: 12, borderWidth: 1, borderColor: 'grey', backgroundColor:'#EDEDED',borderRadius: 10 }}>
                            <TextInput
                                placeholder='Commentaire (aidez-nous à trouver votre bateau)'
                                value={this.state.comment}
                                onChangeText={(comment) => this.setState({ comment })}
                                placeholderTextColor='grey'
                                style={{ paddingLeft: 9, fontFamily: 'Poppins-Medium', fontSize: width*3/100,width:width*85/100, }} />
                        </View>
                        {this.state.uploads3 == '' || this.state.uploads3 == null ?
                            <View style={{ Width: '100%', flexDirection: 'row', marginTop: 16, justifyContent: 'space-between', alignItems: 'center' }}>
                                <Text style={{ fontSize: 14, fontFamily: 'Poppins-SemiBold' }}>Ajouter une photo</Text>
                                <TouchableOpacity onPress={() => this.selectProfilePic()}>
                                    <Card style={{ width: 40, height: 30, justifyContent: 'center', alignItems: 'center' }}>
                                        <Text style={{ color: 'grey', fontFamily: 'Poppins-Bold', marginTop: 5, fontSize: 20, alignSelf: 'center' }}>+</Text>
                                    </Card>
                                </TouchableOpacity>
                            </View> :
                            <TouchableOpacity onPress={() => this.selectProfilePic()}>
                                <Image source={{ uri: this.state.uploads_data }}
                                    style={{ marginTop: 25, height: 100, width: 100, alignSelf: 'center', resizeMode: 'stretch', borderRadius: 10 }} />
                            </TouchableOpacity>
                        }

                        {!!this.state.uploads_Err3 && (
                            <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.uploads_Err3}</Text>
                        )}
                        <Text style={{ fontSize: 14, fontFamily: 'Poppins-SemiBold', marginBottom: 7, marginTop: 5 }}>Mon bateau</Text>


                        <View style={{ width: '100%', marginTop: 12, borderWidth: 1, borderColor: 'grey',backgroundColor:'#EDEDED', borderRadius: 10 }}>
                            <Text

                                placeholderTextColor='grey'
                                style={{ paddingLeft: 9, fontFamily: 'Poppins-Medium', fontSize: 13, height: 50, paddingTop: 15, }}>{this.state.first_name}</Text>
                        </View>
                        <View style={{ width: '100%', marginTop: 12, borderWidth: 1, borderColor: 'grey',backgroundColor:'#EDEDED', borderRadius: 10 }}>
                            <Text

                                placeholderTextColor='grey'
                                style={{
                                    paddingLeft: 9, fontFamily: 'Poppins-Medium', fontSize: 13, height: 50,
                                    justifyContent: 'center', paddingTop: 15,
                                }} >{this.state.last_name}</Text>
                        </View>
                        <View style={{ width: '100%', marginTop: 12, borderWidth: 1, borderColor: 'grey',backgroundColor:'#EDEDED', borderRadius: 10 }}>
                            <Text

                                placeholderTextColor='grey'
                                style={{ paddingLeft: 9, fontFamily: 'Poppins-Medium', fontSize: 13, height: 50, paddingTop: 15, }} >{this.state.email}</Text>
                        </View>
                        <View style={{ width: '100%', marginTop: 12, borderWidth: 1, borderColor: 'grey',backgroundColor:'#EDEDED', borderRadius: 10 }}>
                            <Text

                                placeholderTextColor='grey'
                                style={{
                                    paddingLeft: 9, fontFamily: 'Poppins-Medium', fontSize: 13, height: 50, paddingTop: 15,
                                    justifyContent: 'center',
                                }} >{this.state.telephone}</Text>
                        </View>
                        <View style={{ width: '100%', marginTop: 12, borderWidth: 1, borderColor: 'grey',backgroundColor:'#EDEDED', borderRadius: 10 }}>
                            <Text

                                placeholderTextColor='grey'
                                style={{
                                    paddingLeft: 9, fontFamily: 'Poppins-Medium', fontSize: 13, height: 50, paddingTop: 15,
                                    justifyContent: 'center',
                                }} >{this.state.password}</Text>
                        </View>
                        <View style={{ width: '100%', marginTop: 12, borderWidth: 1, borderColor: 'grey',backgroundColor:'#EDEDED', borderRadius: 10, height: 50, }}>
                            <Text

                                placeholderTextColor='grey'
                                style={{ paddingLeft: 9, fontFamily: 'Poppins-Medium', fontSize: 13, paddingTop: 15, justifyContent: 'center', }} >{this.state.selected}</Text>
                        </View>
                        <Text style={{ fontSize: 15, fontFamily: 'Poppins-Medium', marginBottom: 4, marginTop: 14 }}>Contact Pour la livraison</Text>

                        <View style={{ width: '100%', marginTop: 12, borderWidth: 1, borderColor: 'grey',backgroundColor:'#EDEDED', borderRadius: 10 }}>
                            <TextInput
                                placeholder='Nom Prénom'
                                value={this.state.name}
                                onChangeText={(name) => this.setState({ name, name_Err:'' })}
                                placeholderTextColor='grey'
                                style={{ paddingLeft: 9, fontFamily: 'Poppins-Medium', fontSize: 13, }} />
                        </View>
                        {!!this.state.name_Err && (
                            <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.name_Err}</Text>
                        )}
                        <View style={{ width: '100%', marginTop: 12, borderWidth: 1, borderColor: 'grey',backgroundColor:'#EDEDED', borderRadius: 10 }}>
                            <TextInput
                                placeholder='Numéro de téléphone'
                                value={this.state.mobile_number}
                                keyboardType='number-pad'
                                onChangeText={(mobile_number) => this.setState({ mobile_number, mobile_numberErr: '' })}
                                placeholderTextColor='grey'
                                maxLength={12}
                                style={{ paddingLeft: 9, fontFamily: 'Poppins-Medium', fontSize: 13, }} />
                        </View>

                        {!!this.state.mobile_numberErr && (
                            <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.mobile_numberErr}</Text>
                        )}
                  
                        <View style={{ width: '100%', alignItems: 'center', marginTop: 30, marginBottom: 10 }}>
                            <TouchableOpacity onPress={() => this.validationRegister()}
                                style={{ width: 230, height: 38, borderRadius: 25, backgroundColor: '#28528F', alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={{ fontSize: 12.5, color: 'white', fontFamily: 'Poppins-SemiBold' }}>Enregistrer mon bateau</Text>
                            </TouchableOpacity>
                        </View>
                    
                    </View>
                    <PopupDialog
                        onHardwareBackPress={() => { this.setState({ lodingDialog: false }) }}
                        width={0.3}
                        visible={this.state.lodingDialog}
                        dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
                        <DialogContent>
                            <View style={{ alignItems: 'center', }}>
                                <ActivityIndicator
                                    animating={this.state.animating}
                                    style={[{ height: 10, marginBottom: 10, marginTop: 30, marginLeft: 20, marginRight: 20 }]}
                                    color="#C00"
                                    size="large"
                                    hidesWhenStopped={true}
                                />
                           <Text style={{color:'black',fontSize:14,marginTop:10}}>{this.state.presets}</Text>

                            </View>
                        </DialogContent>
                    </PopupDialog>
                </ScrollView>
            </View>
        )
    }
}