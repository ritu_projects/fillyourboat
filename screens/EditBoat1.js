import React from 'react';
import { StatusBar, ScrollView, Alert, TextInput, View, ActivityIndicator, ImageBackground, Text, Dimensions, TouchableOpacity, Linking, AsyncStorage, } from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from "../styles/styles";
import Strings from '../strings/strings';
import { Card, Radio, CheckBox } from 'native-base';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';
import DateTimePicker from "react-native-modal-datetime-picker";
import PopupDialog, {
    DialogTitle, DialogContent, DialogFooter, DialogButton, SlideAnimation, ScaleAnimation,
} from 'react-native-popup-dialog';
import { Dropdown } from 'react-native-material-dropdown';
let drop_down_categorydata = [];
const { width, height } = Dimensions.get("window");
import Image from 'react-native-fast-image'

export default class EditBoat1 extends React.Component {
    // onRegionChange(region) {
    //   this.setState({ region });
    // }
    constructor(props, context) {
        super(props);
        this.state = {
            animating: false,
            lodingDialog: false,
            port_name: '',
            port_nameErr: '',
            port_number: '',
            port_numberErr: '',
            seat_num: '',
            seat_numErr: '',
            comment: '',
            commentErr: '',
            GPS_coor: '',
            GPS_coorErr: '',
            GPS_coor2: '',
            GPS_coorErr2: '',
            uploads3: '',
            uploads_data : '',
            uploads_Err3: '',

            first_name: '',
            last_name: '',
            email: '',
            telephone: '',
            password: '',
            selected: '',
            selectederror: '',


            name: '',
            name_Err: '',
            mobile_number: '',
            mobile_numberErr: '',
            user_id: '',
            token: '',
            orderid: '',
            subzonename: '',
            presets:'',


        }
    }
    componentDidMount() {

        AsyncStorage.getItem("subzonename")
            .then(subzonename => {
                this.setState({ subzonename: subzonename });
            })
            AsyncStorage.getItem("deliver_person_name")
            .then(deliver_person_name => {
                console.log('Delivery Person Name ',deliver_person_name)
                if(deliver_person_name != null)
                {
            //   var deliver_person_name_prse = JSON.stringify(deliver_person_name);
                    console.log('Delivery Person Name inside ',deliver_person_name)

                    this.setState({ name: deliver_person_name,
                        name_Err :'',
                    });
                }
            })


            AsyncStorage.getItem("deliver_person_mobilenumber")
            .then(deliver_person_mobilenumber => {
                console.log('Delivery Person Mobilenumber ',deliver_person_mobilenumber)
                if(deliver_person_mobilenumber != null)
                {
                    
        //     var deliver_person_mobilenumber_prse = JSON.parse(deliver_person_name);
                    console.log('Delivery Person Mobilenumber Parse ',deliver_person_mobilenumber)

                    this.setState({ mobile_number: deliver_person_mobilenumber,
                        mobile_numberErr : '',
                     });
                }
            })
            AsyncStorage.getItem("pontoonumber")
            .then(pontoonumber => {
                console.log('Porton number ',pontoonumber)
                if(pontoonumber != null)
                {
                    
                    console.log('Porton number Parse ',pontoonumber)

                    this.setState({ port_number: pontoonumber,
                        port_numberErr : '',
                     });
                }
            })
            
            AsyncStorage.getItem("seatnum")
            .then(seatnum => {
                console.log('Seat number ',seatnum)
                if(seatnum != null)
                {
                    
                    console.log('Seat number Parse ',seatnum)

                    this.setState({ seat_num: seatnum,
                        seat_numErr : '',
                     });
                }
            })
        this.getBoatInformation();
    }

    getBoatInformation = () => {

        AsyncStorage.getItem("userid")
            .then(userid => {
                var userid = JSON.parse(userid);

                this.setState({ user_id: userid });

                AsyncStorage.getItem("token")
                    .then(token => {
                        var accessToken = JSON.parse(token);
                        this.setState({ token: accessToken });

                        AsyncStorage.getItem("orderid")
                            .then(orderid => {

                                this.setState({ orderid: orderid });

                                fetch(Strings.base_Url + "orderGetBoatInfo", {
                                    method: 'POST',
                                    headers: {
                                        // 'Accept': 'application/json',
                                        'Content-Type': 'application/json',
                                        'User-Id': this.state.user_id,
                                        'token': this.state.token,
                                    },
                                    body: JSON.stringify({
                                        order_id: this.state.orderid,

                                    })


                                })
                                    .then((response) => response.json())
                                    .then((responseData) => {
                                        console.log(" responseData getBoatInfo resoJSon===" + JSON.stringify(responseData));
                                        console.log("resoJSon getBoatInfo===" + responseData.error);
                                        if (responseData.error == "false") {
                                            console.log("Error false massage getBoatInfo" + JSON.stringify(responseData.boat_record));


                                            this.setState({
                                                animating: false,
                                                lodingDialog: false,
                                                first_name: responseData.boat_record.ship_name,
                                                last_name: responseData.boat_record.brand,
                                                email: responseData.boat_record.size,
                                                telephone: responseData.boat_record.registration,
                                                password: responseData.boat_record.reserve_capacity,
                                                selected: responseData.boat_record.fuel,

                                            });


                                            //this.RBSheet.close()
                                        } else if (responseData.error == "true") {

                                           // this.showalerts(responseData.errorMessage)
                                            this.setState({
                                                animating: false,
                                                lodingDialog: false

                                            });
                                        }
                                    }
                                    )
                            })
                    })
            })

    }
    selectProfilePic = () => {
        const options = {
            title: 'Select Avatar',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };

        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                const source = { uri: response.uri };

                const imageUrl = response.uri
                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };
                console.log("image url:###:" + JSON.stringify(response))
                this.setState({
                    uploads_data: response.uri,
                  uploads3: response.data,
                  uploads_Err3: '',
                });
                console.log("image url::" + JSON.stringify(imageUrl))
                console.log("image url::" + JSON.stringify(source))


            }
        });
    }
    validationRegister() {
        // this.UserRegister();
        var isValidate = 0;
        // if (this.state.port_name != "") {
        //     isValidate += 1;
        // } else {
        //     isValidate -= 1
        //     this.setState({
        //         port_nameErr: Strings.ShouldemptyText,
        //     });
        // }
        if (this.state.port_number != "") {
            isValidate += 1;
        } else {
            isValidate -= 1
            this.setState({

                port_numberErr: Strings.ShouldemptyText,
            });
        }
        if (this.state.seat_num != "") {
            isValidate += 1;
        } else {
            isValidate -= 1
            this.setState({

                seat_numErr: Strings.ShouldemptyText,
            });
        }

        // if (this.state.GPS_coor != "") {
        //     isValidate += 1;
        // } else {
        //     isValidate -= 1
        //     this.setState({

        //         GPS_coorErr: Strings.ShouldemptyText,
        //     });
        // }

        // if (this.state.GPS_coor2 != "") {
        //     isValidate += 1;
        // } else {
        //     isValidate -= 1
        //     this.setState({

        //         GPS_coorErr2: Strings.ShouldemptyText,
        //     });
        // }

        // if (this.state.comment != "") {
        //     isValidate += 1;
        // } else {
        //     isValidate -= 1
        //     this.setState({

        //         commentErr: Strings.ShouldemptyText,
        //     });
        // }
        // if (this.state.uploads3 != "") {
        //     isValidate += 1;
        // } else {
        //     isValidate -= 1
        //     this.setState({

        //         uploads_Err3: Strings.ShouldemptyText,
        //     });
        // }





        if (this.state.name != "") {

            isValidate += 1;

        }
        else {
            console.log("cat empty::::");
            isValidate -= 1;
            this.setState({
                name_Err: Strings.ShouldemptyText,
            });
        }
        if (this.state.mobile_number != "") {

            isValidate += 1;

        }
        else {
            console.log("cat empty::::");
            isValidate -= 1;
            this.setState({
                mobile_numberErr: Strings.ShouldemptyText,
            });
        }




        console.log("Use Login::::" + isValidate)


        if (isValidate == 4) {

            console.log("Use Login::::" + this.state.name + this.state.last_name)

            this.userAddBoat();
            this.setState({
                animating: true,
                lodingDialog: true,

            });
        }
        else {

        }

    }

    userAddBoat() {

        AsyncStorage.getItem("userid")
            .then(userid => {

                //  var userid = JSON.parse(userid);

                this.setState({ user_id: userid });

                AsyncStorage.getItem("token")
                    .then(token => {
                        var accessToken = JSON.parse(token);
                        this.setState({ token: accessToken });

                        AsyncStorage.getItem("orderid")
                            .then(orderid => {

                                this.setState({ orderid: orderid });

                                console.log("Photo is  ", this.state.uploads3)

                                let headers = {
                                    'Content-Type': 'application/json',
                                    'user-id': this.state.user_id,
                                    'token': this.state.token,
                                };
                                RNFetchBlob.fetch('POST', Strings.base_Url + 'createOrderStep5', headers, [
                                    { name: 'order_id', data: this.state.orderid },
                                    { name: 'full_name', data: this.state.name },

                                    { name: 'telephone_number', data: this.state.mobile_number },
                                    { name: 'port_name', data: this.state.subzonename },
                                    { name: 'port_number', data: this.state.port_number },
                                    { name: 'harbour_place', data: '' },
                                    { name: 'seat_number', data: this.state.seat_num },
                                    { name: 'gps_one', data: this.state.GPS_coor },
                                    { name: 'gps_two', data: this.state.GPS_coor2 },


                                    { name: 'comment', data: this.state.comment },

                                    { name: 'photo', filename: 'photo.jpg', type: 'image/png', data: this.state.uploads3 },


                                ],
                                )
                                
                                .uploadProgress((written, total) => {
                                    let presenteg = Math.floor((written * 100) / total)
                                    console.log('uploaded', written / total +" presente"+presenteg +"%")
                                    this.setState({
                                      presets:presenteg +" %"
                                    });
                                })
                                // listen to download progress event
                                .progress((received, total) => {
                                    console.log('progress', received / total)
                                }).then((resp) => {

                                    console.log("response:::::::" + JSON.stringify(resp.json()));

                                    console.log("error:::" + resp.json().error)
                                    var error = resp.json().error

                                    if (resp.json().error === "false") {
                                        console.log("error:::" + resp.json().error)

                                        this.setState({
                                            animating: false,
                                            lodingDialog: false,
                                        });
                                        AsyncStorage.setItem('deliver_person_name',this.state.name)
                                        AsyncStorage.setItem('deliver_person_mobilenumber',this.state.mobile_number)
                                        AsyncStorage.setItem('pontoonumber',this.state.port_number)
                                        AsyncStorage.setItem('seatnum',this.state.seat_num)

                                        Actions.push("Delvey_confirm_Screen")

                                    } else if (resp.json().error === "true") {

                                        //this.showalerts(resp.json().errorMessage)
                                        this.setState({
                                            animating: false,
                                            lodingDialog: false,
                                        });

                                    }



                                }).catch((err) => {
                                    this.setState({
                                        animating: false,
                                        lodingDialog: false,
                                    });
                                    console.log("response::::err:::" + err);
                                });
                            })
                    })
            })

    }


    render() {
        return (
            <View style={styles.containerWhite}>


                <ScrollView style={{}}>

                    <Card style={{ width: '100%', marginTop: Platform.OS == 'ios' ? 20 :-8, height: 50, flexDirection: 'row' }}>
                        <TouchableOpacity onPress={() => Actions.pop()} style={{ marginLeft: 5, height: 50, justifyContent: 'center', alignItems: 'center', }}>
                            <Image source={require("../assets/back1.png")}
                                style={{ width: 15, height: 15, }}
                                resizeMode="contain" />
                        </TouchableOpacity>
                        <View style={{ marginLeft: width * 25 / 100, alignSelf: 'center', height: 50, 
                        justifyContent: 'center', alignItems: 'center' }}>
                            {/* <Text style={{ fontSize: 15, fontFamily: 'Poppins-SemiBold' }}>Ajouter un bateau {this.state.zone_name}</Text> */}
                            <Text style={{ fontSize: 15, fontFamily: 'Poppins-SemiBold' }}>Ma place au port</Text>

                        </View>

                    </Card>
                    <View style={{ margin: 20, width: '90%' }}>
                        {/* <Text style={{ fontSize: 15, fontFamily: 'Poppins-SemiBold', marginBottom: 7 }}>Ma place u port</Text> */}

                        <View style={{ width: '100%', borderWidth: 1, borderColor: 'grey', borderRadius: 10,backgroundColor:'#EDEDED', justifyContent:'center', }}>
                            <Text

                                placeholderTextColor='grey'
                                style={{ paddingLeft: 9, fontFamily: 'Poppins-Medium', fontSize: 13, 
                                height: 50,paddingTop:15,  }} >{this.state.subzonename}</Text>
                        </View>
                        {/* {!!this.state.port_nameErr && (
                            <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.port_nameErr}</Text>
                        )} */}



                        <View style={{ width: '100%', marginTop: 12, borderWidth: 1, borderColor: 'grey',backgroundColor:'#EDEDED', borderRadius: 10 }}>
                            <TextInput
                                placeholder='Numéro de ponton'
                                value={this.state.port_number}
                                onChangeText={(port_number) => this.setState({ port_number, port_numberErr: '', })}
                                placeholderTextColor='grey'
                                style={{ paddingLeft: 9, fontFamily: 'Poppins-Medium', fontSize: 13, }} />
                        </View>
                        {!!this.state.port_numberErr && (
                            <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.port_numberErr}</Text>
                        )}

                        <View style={{ width: '100%', marginTop: 12, borderWidth: 1, borderColor: 'grey',backgroundColor:'#EDEDED', borderRadius: 10 }}>
                            <TextInput
                                placeholder='Numéro de place'
                                value={this.state.seat_num}
                                onChangeText={(seat_num) => this.setState({ seat_num, seat_numErr: '' })}
                                placeholderTextColor='grey'
                                style={{ paddingLeft: 9, fontFamily: 'Poppins-Medium', fontSize: 13, }} />
                        </View>
                        {!!this.state.seat_numErr && (
                            <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.seat_numErr}</Text>
                        )}
                        {/* <View style={{ width: '100%', marginTop: 12, borderWidth: 1, borderColor: 'grey',backgroundColor:'#EDEDED', borderRadius: 10 }}>
                            <TextInput
                                placeholder='Coordonnées GPS Latitude'
                                vaue={this.state.GPS_coor}

                                onChangeText={(GPS_coor) => this.setState({ GPS_coor, GPS_coorErr: '' })}
                                placeholderTextColor='grey'
                                keyboardType="decimal-pad"
                                style={{ paddingLeft: 9, fontFamily: 'Poppins-Medium', fontSize: 13, }} />
                        </View>
                        {!!this.state.GPS_coorErr && (
                            <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.GPS_coorErr}</Text>
                        )}

                        <View style={{ width: '100%', marginTop: 12, borderWidth: 1, borderColor: 'grey',backgroundColor:'#EDEDED', borderRadius: 10 }}>
                            <TextInput
                                placeholder='Coordonnées GPS Longitude'
                                vaue={this.state.GPS_coor2}

                                onChangeText={(GPS_coor2) => this.setState({ GPS_coor2, GPS_coorErr2: '' })}
                                placeholderTextColor='grey'
                                keyboardType="decimal-pad"
                                style={{ paddingLeft: 9, fontFamily: 'Poppins-Medium', fontSize: 13, }} />
                        </View>
                        {!!this.state.GPS_coorErr2 && (
                            <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.GPS_coorErr2}</Text>
                        )} */}
                        <View style={{ width: '100%', height: 80, marginTop: 12, borderWidth: 1, borderColor: 'grey',backgroundColor:'#EDEDED', borderRadius: 10 }}>
                            <TextInput
                                placeholder='Commentaire (aidez-nous à trouver votre bateau)'
                                value={this.state.comment}
                                onChangeText={(comment) => this.setState({ comment, commentErr: '' })}
                                placeholderTextColor='grey'
                                numberOfLines={3}
                                style={{ paddingLeft: 9, fontFamily: 'Poppins-Medium', fontSize: width*2.9/100,width:width*85/100, }} />
                        </View>
                        {!!this.state.commentErr && (
                            <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.commentErr}</Text>
                        )}
                        {this.state.uploads3 == '' || this.state.uploads3 == null ?
                            <View style={{ Width: '100%', flexDirection: 'row', marginTop: 16, justifyContent: 'space-between', alignItems: 'center' }}>
                                <Text style={{ fontSize: 14, fontFamily: 'Poppins-SemiBold' }}>Ajouter une photo</Text>
                                <TouchableOpacity onPress={() => this.selectProfilePic()}>
                                    <Card style={{ width: 40, height: 30, justifyContent: 'center', alignItems: 'center' }}>
                                        <Text style={{ color: 'grey', fontFamily: 'Poppins-Bold', marginTop: 5, fontSize: 20, alignSelf: 'center' }}>+</Text>
                                    </Card>
                                </TouchableOpacity>
                            </View> :
                            <TouchableOpacity onPress={() => this.selectProfilePic()}>
                                <Image source={{ uri: this.state.uploads_data }}
                                    style={{ marginTop: 25, height: 100, width: 100, alignSelf: 'center', resizeMode: 'stretch', borderRadius: 10 }} />
                            </TouchableOpacity>
                        }
                        {!!this.state.uploads_Err3 && (
                            <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.uploads_Err3}</Text>
                        )}

                        <Text style={{ fontSize: 14, fontFamily: 'Poppins-SemiBold', marginBottom: 7, marginTop: 5 }}>Mon bateau</Text>

                        <View style={{ width: '100%', marginTop: 12, borderWidth: 1, borderColor: 'grey',backgroundColor:'#EDEDED', borderRadius: 10 }}>
                            <Text

                                placeholderTextColor='grey'
                                style={{ paddingLeft: 9, fontFamily: 'Poppins-Medium', fontSize: 13, height: 50,paddingTop:15, }}>{this.state.first_name}</Text>
                        </View>
                        <View style={{ width: '100%', marginTop: 12, borderWidth: 1, borderColor: 'grey',backgroundColor:'#EDEDED', borderRadius: 10 }}>
                            <Text

                                placeholderTextColor='grey'
                                style={{ paddingLeft: 9, fontFamily: 'Poppins-Medium', fontSize: 13, height: 50, 
                                justifyContent: 'center',paddingTop:15, }} >{this.state.last_name}</Text>
                        </View>
                        <View style={{ width: '100%', marginTop: 12, borderWidth: 1, borderColor: 'grey',backgroundColor:'#EDEDED', borderRadius: 10 }}>
                            <Text

                                placeholderTextColor='grey'
                                style={{ paddingLeft: 9, fontFamily: 'Poppins-Medium', fontSize: 13, height: 50,paddingTop:15, }} >{this.state.email}</Text>
                        </View>
                        <View style={{ width: '100%', marginTop: 12, borderWidth: 1, borderColor: 'grey',backgroundColor:'#EDEDED', borderRadius: 10 }}>
                            <Text

                                placeholderTextColor='grey'
                                style={{ paddingLeft: 9, fontFamily: 'Poppins-Medium', fontSize: 13, height: 50, paddingTop:15,
                                 justifyContent: 'center', }} >{this.state.telephone}</Text>
                        </View>
                        <View style={{ width: '100%', marginTop: 12, borderWidth: 1, borderColor: 'grey',backgroundColor:'#EDEDED', borderRadius: 10 }}>
                            <Text

                                placeholderTextColor='grey'
                                style={{ paddingLeft: 9, fontFamily: 'Poppins-Medium', fontSize: 13, height: 50, paddingTop:15,
                                justifyContent: 'center', }} >{this.state.password}</Text>
                        </View>
                        <View style={{ width: '100%', marginTop: 12, borderWidth: 1, borderColor: 'grey',backgroundColor:'#EDEDED', borderRadius: 10, height: 50, }}>
                            <Text

                                placeholderTextColor='grey'
                                style={{ paddingLeft: 9, fontFamily: 'Poppins-Medium', fontSize: 13, paddingTop:15,justifyContent: 'center', }} >{this.state.selected}</Text>
                        </View>
                        <Text style={{ fontSize: 15, fontFamily: 'Poppins-Medium', marginBottom: 4, marginTop: 14 }}>Contact Pour la livraison</Text>

                        <View style={{ width: '100%', marginTop: 12, borderWidth: 1, borderColor: 'grey',backgroundColor:'#EDEDED', borderRadius: 10 }}>
                            <TextInput
                                placeholder='Nom Prénom'
                                value={this.state.name}
                                onChangeText={(name) => this.setState({ name, name_Err: '' })}
                                placeholderTextColor='grey'
                                style={{ paddingLeft: 9, fontFamily: 'Poppins-Medium', fontSize: 13, }} />
                        </View>
                        {!!this.state.name_Err && (
                            <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.name_Err}</Text>
                        )}


                        <View style={{ width: '100%', marginTop: 12, borderWidth: 1, borderColor: 'grey',backgroundColor:'#EDEDED', borderRadius: 10 }}>
                            <TextInput
                                placeholder='Numéro de téléphone'
                                value={this.state.mobile_number}
                                onChangeText={(mobile_number) => this.setState({ mobile_number, mobile_numberErr: '' })}
                                placeholderTextColor='grey'
                                keyboardType='phone-pad'
                                maxLength={12}

                                style={{ paddingLeft: 9, fontFamily: 'Poppins-Medium', fontSize: 13, }} />
                        </View>
                        {!!this.state.mobile_numberErr && (
                            <Text style={{ color: 'red', marginLeft: 10, fontSize: 12 }}>{this.state.mobile_numberErr}</Text>
                        )}
                        <View style={{ width: '100%', alignItems: 'center', marginTop: 30, marginBottom: 10 }}>
                            <TouchableOpacity onPress={() => this.validationRegister()}
                                style={{ width: 230, height: 38, borderRadius: 25, backgroundColor: '#28528F', alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={{ fontSize: 12.5, color: 'white', fontFamily: 'Poppins-SemiBold' }}>Enregistrer mon bateau</Text>
                            </TouchableOpacity>
                        </View>
                         </View>
                    <PopupDialog
                        onHardwareBackPress={() => { this.setState({ lodingDialog: false }) }}
                        width={0.3}
                        visible={this.state.lodingDialog}
                        dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
                        <DialogContent>
                            <View style={{ alignItems: 'center', }}>
                                <ActivityIndicator
                                    animating={this.state.animating}
                                    style={[{ height: 10, marginBottom: 10, marginTop: 30, marginLeft: 20, marginRight: 20 }]}
                                    color="#C00"
                                    size="large"
                                    hidesWhenStopped={true}
                                />
                                                           <Text style={{color:'black',fontSize:14,marginTop:10}}>{this.state.presets}</Text>

                            </View>
                        </DialogContent>
                    </PopupDialog>
                </ScrollView>
            </View>
        )
    }
}