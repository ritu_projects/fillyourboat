import React from 'react';
import { StatusBar, Button, ActivityIndicator, AsyncStorage, DrawerLayoutAndroid, FlatList, ScrollView, PixelRatio, TextInput, View, ImageBackground, Text, Dimensions, TouchableOpacity, Linking, } from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from "../styles/styles";
import Drawer from 'react-native-drawer'

import Strings from '../strings/strings'
import { Card } from 'native-base'
import Sidebar from '../screens/sideBar'
const { width, height } = Dimensions.get("window");
import Image from 'react-native-fast-image'

export default class Payment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user_id: '',
            token: '', 
            selected: 1,
            card: [{ img: require('../assets/21.png'), value: 1, text: 'Paiement par carte' },
            { img: require('../assets/paypal.png'), text: 'Paypal', value: 2 }],
            orderid:'',

        };

       // console.log("Current Id ::::" + this.props.currentOrderId)
    }
    _setDrawer() {
        this._drawer.open()
    }
    componentDidMount() {
        this._drawer.close()
        AsyncStorage.getItem("orderid")
        .then(orderid => {

            this.setState({ orderid: orderid });
        })


    }

    selectPaymentMethod = () => {

        if(this.state.selected==1)
        {
            Actions.push("Webview", { currentOrderId: this.state.orderid})

        }

      else if(this.state.selected==2)
      {
        Actions.push("Webview_Paypal", { currentOrderId: this.state.orderid })

      }



    }

      
    render() {
        var navigationView = (
            <Sidebar />
        );
        return (
           <Drawer
            openDrawerOffset={100}
           type="overlay"
             ref={(ref) => this._drawer = ref}
             content={<Sidebar />}
             >
                <View>

                    <Card style={{ width: '100%', marginTop: Platform.OS == 'ios' ? 20 :-5, alignItems: 'center', height: 50, flexDirection: 'row', justifyContent: 'space-between' }}>
                        <TouchableOpacity onPress={this._setDrawer.bind(this)} style={{
                            height: 50,
                            justifyContent: 'center', marginLeft: -4
                        }}>
                            <Image source={require("../assets/menu_img.png")}
                                style={{ width: 40, height: 40, }}
                                resizeMode="contain" />
                        </TouchableOpacity>
                        <View style={{ height: 50, justifyContent: 'center', alignItems: 'center', }}>
                            <Text style={{ fontFamily: "Poppins-SemiBold", fontSize: 16, }}>Mes moyens de paiement</Text>
                        </View>
                        <View style={{ width: 40, height: 20, marginRight: 8 }} />

                    </Card>


                    <FlatList
                        keyExtractor={item => item.id}
                        data={this.state.card}

                        //initialNumToRender={5}
                        renderItem={({ item, index }) => (
                            <View style={{ marginTop: 10 }}>
                                {this.state.selected == item.value ?
                                    <TouchableOpacity onPress={() => this.setState({ selected: item.value })}>
                                        <Card style={{ width: width * 88 / 100, alignSelf: 'center', padding: 10, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                                            <View style={{ flexDirection: 'row', 
                                            width: width * 78 / 100, alignItems: 'center' }}>
                                                <Image source={item.img} style={{ width: width * 15 / 100, height: width * 10 / 100, marginRight: width * 1.5 / 100 }} />
                                                <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 13 }}>{item.text}</Text>
                                            </View>
                                            <Image source={require('../assets/fill_home_radio.png')} 
                                            style={{ width: 12, height: 12 }} />

                                        </Card>
                                    </TouchableOpacity>
                                    :
                                    <TouchableOpacity onPress={() => this.setState({ selected: item.value })}>
                                        <Card style={{ width: width * 88 / 100, alignSelf: 'center', padding: 10, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                                            <View style={{ flexDirection: 'row', width: width * 78 / 100, alignItems: 'center' }}>
                                                <Image source={item.img} style={{ width: width * 15 / 100, height: width * 10 / 100, marginRight: width * 1.5 / 100 }} />
                                                <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 13 }}>{item.text}</Text>
                                            </View>
                                            <Image source={require('../assets/81.png')} style={{ width: 12, height: 12 }} />

                                        </Card>
                                    </TouchableOpacity>

                                }
                            </View>
                        )} />


                    <TouchableOpacity 
                    onPress={() => this.selectPaymentMethod()}
                    style={{ width: 240, height: 35, marginTop: 25, borderRadius: 20, alignItems: 'center', justifyContent: 'center', alignSelf: 'center', backgroundColor: '#28528F', marginBottom: 20 }}>
                        <Text style={{ fontSize: 14, fontFamily: 'Poppins-SemiBold', color: 'white' }}>Effectuer un paiement</Text>
                    </TouchableOpacity>


                </View>
             </Drawer>
        );
    }
}
