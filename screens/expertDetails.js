import React from 'react';
import { StatusBar, Button, FlatList, Alert, ScrollView, PixelRatio, TextInput, View, ImageBackground, Text, Dimensions, TouchableOpacity, Linking, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from "../styles/styles";
import Strings from '../strings/strings'
import { Card } from 'native-base';
import Loading from 'react-native-whc-loading';
import { Rating, AirbnbRating } from 'react-native-elements';
import ConfirmRBSheet from "react-native-raw-bottom-sheet";
import TravailRBSheet from 'react-native-raw-bottom-sheet';
import StartRideRBSheet from 'react-native-raw-bottom-sheet';
import AcceptRBSheet from 'react-native-raw-bottom-sheet';
import ArriveRBSheet from 'react-native-raw-bottom-sheet';
import CancelRBSheet from 'react-native-raw-bottom-sheet';
import CaseRBSheet from 'react-native-raw-bottom-sheet';
import Image from 'react-native-fast-image'

export default class expertDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: '',
            Att_radio: false,
            j_ai_radio: false,
            trauve_radio: false,
            fait_radio: false,
            autre_radio: false,
            user_id: '',
            token: '',
            request_id: '',
            firstname: '',
            lastname: '',
            email: '',
            cost_hour: '',
            cost_hour_min: '',
            cost_hour_max: '',
            description: '',
            tech_complete_job: '',
            tech_rating: '',
            profile_image: '',


        };
        console.log("currentTechId: +::" + this.props.current_cat_id + "  reason_id  " + this.props.reason_id + "  tech id  " + this.props.tech_id)
    }
    componentWillMount() {

        this.setState({

            animating: true,
            lodingDialog: true,

        });

        AsyncStorage.getItem("userid")
            .then(userid => {
                this.setState({ user_id: userid });

                AsyncStorage.getItem("token")
                    .then(token => {
                        var accessToken = JSON.parse(token);
                        console.log("accessToken::" + accessToken + ":::User:::" + userid)
                        this.setState({ token: accessToken });

                        AsyncStorage.getItem("request_id")
                            .then(request_id => {
                                var request_id = JSON.parse(request_id);
                                console.log("accessToken::" + accessToken + ":::request_id:::" + request_id)
                                this.setState({ request_id: request_id });

                                this.refs.loading2.show();

                                fetch(Strings.base_Url + "techDetail", {
                                    method: 'POST',
                                    headers: {
                                        'Content-Type': 'application/json',
                                        'User-Id': userid,
                                        'token': accessToken,
                                    },
                                    body: JSON.stringify({
                                        cat_id: this.props.current_cat_id,
                                        tech_id: this.props.tech_id
                                    })
                                })
                                    .then((response) => response.json())
                                    .then((responseData) => {
                                        console.log(" responseData resoJSon===" + JSON.stringify(responseData));
                                        console.log("resoJSon===" + responseData.error);
                                        if (responseData.error == "false") {
                                            this.refs.loading2.close();
                                            this.setState({
                                                firstname: responseData.techDetails.firstName,
                                                lastname: responseData.techDetails.lastName,
                                                email: responseData.techDetails.email,
                                                cost_hour: responseData.techDetails.price_per_hour,
                                                cost_hour_min: responseData.techDetails.min_price,
                                                cost_hour_max: responseData.techDetails.max_price,
                                                description: responseData.techDetails.tech_detail,
                                                tech_complete_job: responseData.techDetails.tech_complete_job,
                                                tech_rating: responseData.techDetails.tech_rating,
                                                profile_image: responseData.techDetails.profile_pic_url + "/" + responseData.techDetails.profile_pic,
                                                animating: false,
                                                lodingDialog: false,

                                            });
                                            //  alert(responseData.errorMessage)

                                        } else if (responseData.error == "true") {
                                            this.refs.loading2.close();
                                            this.showalerts(responseData.errorMessage)
                                            this.setState({
                                                passwordErr: responseData.errorMessage,
                                                animating: false,
                                                lodingDialog: false
                                            });
                                        }
                                    }
                                    )
                            });

                    });
            });
    }

    showalerts(body) {
        Alert.alert(
            Strings.alert_tital,
            body,
            [
                { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
            ],
            { cancelable: false });
    }

    showHome() {
        Alert.alert(
            Strings.alert_tital,
            "Nous avons contacté les experts sélectionnés,Veuillez attendre la réponse 5 min",
            [
                { text: 'OK', onPress: () =>  Actions.push("Location") },
            ],
            { cancelable: false });
    }

    confirmRequest(){
        console.log("requestAccept id :::" +this.state.request_id)
        this.refs.loading2.show();
        fetch(Strings.base_Url + "user_confirm_job", {
            method: 'POST',
            headers: {
                // 'Accept': 'application/json',
                'Content-Type': 'application/json',
                'User-Id': this.state.user_id,
                'token': this.state.token,
            },
            body: JSON.stringify({
                req_id: this.state.request_id
            })
        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(" responseData user_confirm_job resoJSon===" + JSON.stringify(responseData));
                console.log("resoJSon user_confirm_job===" + responseData.error);
                if (responseData.error == "false") {
                    console.log("Error false massage" + JSON.stringify(responseData.errorMessage));
                    this.setState({
                        animating: false,
                        lodingDialog: false,
                    });
                    this.refs.loading2.close();
                     Actions.push("Location")
                } else if (responseData.error == "true") {
                    this.setState({
                        passwordErr: responseData.errorMessage,
                        animating: false,
                        lodingDialog: false
                    });
                    this.refs.loading2.close();
                }
            })

    }

    finalsubmitRequest() {
        this.refs.loading2.show();
        fetch(Strings.base_Url + "updatePutRequest", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'User-Id': this.state.user_id,
                'token': this.state.token,
            },
            body: JSON.stringify({
                request_id: this.state.request_id,
                tech_id: this.props.tech_id
            })
        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(" responseData resoJSon===" + JSON.stringify(responseData));
                //console.log("resoJSon===" + responseData.error);
                if (responseData.error == "false") {
                    this.refs.loading2.close();
                    this.setState({
                        animating: false,
                        lodingDialog: false,

                    });
                    this.showHome();
                   
                } else if (responseData.error == "true") {
                    this.refs.loading2.close();
                    this.showalerts(responseData.errorMessage)
                    this.setState({
                        passwordErr: responseData.errorMessage,
                        animating: false,
                        lodingDialog: false
                    });
                }
            }
            )

    }

    user_reassign_request() {

        console.log("requestAccept id :::" +this.state.request_id)
        this.refs.loading2.show();
        fetch(Strings.base_Url + "user_reassign_request", {
            method: 'POST',
            headers: {
                // 'Accept': 'application/json',
                'Content-Type': 'application/json',
                'User-Id': this.state.user_id,
                'token': this.state.token,
            },
            body: JSON.stringify({
                req_id: this.state.request_id,
                user_id: this.state.user_id
            })
        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(" responseData req_det. resoJSon===" + JSON.stringify(responseData));
                console.log("resoJSon===" + responseData.error);
                if (responseData.error == "false") {
                    console.log("Error false massage" + JSON.stringify(responseData.errorMessage));
                    this.setState({
                        animating: false,
                        lodingDialog: false,
                    });
                    this.refs.loading2.close();
                    AsyncStorage.setItem("request_id", JSON.stringify(this.state.request_id))
                    Actions.push("ExpertsList", { current_cat_id: this.props.current_cat_id })
                    //this.componentWillMount();
                    // Actions.push("Location")
                } else if (responseData.error == "true") {
                    this.setState({
                        passwordErr: responseData.errorMessage,
                        animating: false,
                        lodingDialog: false
                    });
                    this.refs.loading2.close();
                }
            })

    }
    ratingCompleted(rating) {
        console.log("Rating is: " + rating)
    }
    render() {
        return (
            <View style={styles.containerWhite}>
                <View style={{ width: '100%', height: 50, flexDirection: 'row', backgroundColor: 'white' }}>
                    <TouchableOpacity onPress={() => Actions.pop()} style={{ width: '20%', height: 50, justifyContent: 'center', alignItems: 'center', borderTopRightRadius: 20, borderBottomRightRadius: 20 }}>
                        <Image source={require("../assets/left-arrow.png")}
                            style={{ width: 30, height: 20, }}
                            resizeMode="contain" />
                    </TouchableOpacity>
                    <View style={{ width: '70%', height: 50, justifyContent: 'center', alignItems: 'center', }}>
                        <Text style={{ fontSize: 16, fontWeight: 'bold' }}>{Strings.details_expert_text}</Text>
                    </View>
                </View>
                <ScrollView>
                    <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }}>
                        <Image source={require("../assets/image.png")}
                            style={{ width: '100%', height: 100, backgroundColor: 'red' }}
                        />

                        <View style={{ width: 100, height: 100, borderRadius: 100 / 2, backgroundColor: 'white', marginTop: -50, alignItems: 'center', justifyContent: 'center' }}>
                            <Image source={{ uri: this.state.profile_image }}
                                style={{ width: 95, height: 95, borderRadius: 95 / 2 }} />
                        </View>

                        <Text style={{ fontSize: 18, fontWeight: 'bold', padding: 5 }}>{this.state.firstname + " " + this.state.lastname}</Text>
                        <Text style={{ fontSize: 16, }}>{this.state.email}</Text>
                        <View style={{ flexDirection: 'row', marginTop: 10, alignItems: 'center' }}>
                            <View style={{ width: 25, height: 25, borderWidth: 1, borderRadius: 4, borderColor: Strings.color_green_code, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ fontWeight: 'bold', color: Strings.color_green_code }}>{this.state.tech_complete_job}</Text>
                            </View>
                            <Text style={{ fontSize: 14, }}>{Strings.travaux_text}</Text>
                        </View>
                        <Text style={{ fontSize: 14, padding: 5 }}>{Strings.cout_text} € {this.state.cost_hour}</Text>
                        <AirbnbRating
                            showRating={false}
                            count={5}
                            color={Strings.color_green_code}
                            defaultRating={this.state.tech_rating}
                            size={15} />

                        <Text style={{ fontSize: 18, fontWeight: 'bold', padding: 5, marginTop: 30, color: Strings.color_green_code }}>{''}</Text>
                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: Strings.color_green_code }}>{Strings.intervention_text}{this.state.cost_hour_max}€ *</Text>

                        <View style={{ width: '95%', marginTop: 30, }}>
                            <Text style={{ fontSize: 16, padding: 10 }}>{Strings.descripation_Personelle_text}</Text>
                            <Card style={{ width: '95%', backgroundColor: 'white', borderRadius: 10, marginLeft: 10 }}>
                                <Text style={{ padding: 10 }}>{this.state.description}</Text>
                            </Card>
                        </View>

                        {this.props.reason_id == 1 ?
                            <View style={{ width: '95%', marginTop: 30, flexDirection: 'row', marginBottom: 50 }}>
                                <View style={{ width: '50%', alignItems: 'center', justifyContent: 'center' }}>
                                    <Card style={{ width: '90%', height: 40, borderRadius: 10, backgroundColor: 'white', alignItems: 'center', justifyContent: 'center' }}>
                                        <TouchableOpacity onPress={() => this.user_reassign_request()} style={{ width: '100%', height: 40, borderRadius: 10, alignItems: 'center', justifyContent: 'center', backgroundColor: 'white' }}>
                                            <Text style={{ padding: 10, color: Strings.color_green_code }}>{"Annuler"}</Text>
                                        </TouchableOpacity>
                                    </Card>

                                </View>
                                <View style={{ width: '50%', alignItems: 'center', justifyContent: 'center' }}>
                                    <TouchableOpacity onPress={() => this.confirmRequest()} style={{ width: '90%', height: 40, borderRadius: 10, backgroundColor: Strings.color_green_code, alignItems: 'center', justifyContent: 'center' }}>
                                        <Text style={{ padding: 10, color: 'white' }}>{"Confirmer"}</Text>

                                    </TouchableOpacity>
                                </View>
                            </View> :

                            <View style={{ width: '95%', marginTop: 30, flexDirection: 'row', marginBottom: 50 }}>
                                <View style={{ width: '50%', alignItems: 'center', justifyContent: 'center' }}>
                                    <Card style={{ width: '90%', height: 40, borderRadius: 10, backgroundColor: 'white', alignItems: 'center', justifyContent: 'center' }}>
                                        <TouchableOpacity onPress={() => Actions.pop()} style={{ width: '100%', height: 40, borderRadius: 10, alignItems: 'center', justifyContent: 'center', backgroundColor: 'white' }}>
                                            <Text style={{ padding: 10, color: Strings.color_green_code }}>{Strings.Retour_text}</Text>
                                        </TouchableOpacity>
                                    </Card>

                                </View>
                                <View style={{ width: '50%', alignItems: 'center', justifyContent: 'center' }}>
                                    <TouchableOpacity onPress={() => this.finalsubmitRequest()} style={{ width: '90%', height: 40, borderRadius: 10, backgroundColor: Strings.color_green_code, alignItems: 'center', justifyContent: 'center' }}>
                                        <Text style={{ padding: 10, color: 'white' }}>{Strings.selectionnerr_text}</Text>

                                    </TouchableOpacity>
                                </View>
                            </View>


                        }



                        <Text style={{ fontWeight: 'bold', fontSize: 14, color: 'black', marginTop: 10, marginBottom: 10, textAlign: 'center' }}>{Strings.deplacement30_text}</Text>
                    </View>
                    <Loading
                        ref={"loading2"}
                        image={require("../assets/spinner-of-dots.png")}
                        // backgroundColor='transparent'
                        backgroundColor='white'
                        borderRadius={5}
                        size={100}
                        imageSize={80}
                        indicatorColor='gray'
                        easing={Loading.EasingType.ease} />
                </ScrollView>
            </View>
        );
    }
}
