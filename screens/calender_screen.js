import React from 'react';
import { StyleSheet, StatusBar, Modal, Alert, DrawerLayoutAndroid, ActivityIndicator, ScrollView, FlatList, TextInput, View, Text, Dimensions, TouchableOpacity, Linking, AsyncStorage } from 'react-native';
import { Calendar, LocaleConfig } from 'react-native-calendars';
import PopupDialog, {
    DialogTitle, DialogContent, DialogFooter, DialogButton, SlideAnimation, ScaleAnimation,
} from 'react-native-popup-dialog';
import Strings from '../strings/strings'
import { Actions } from 'react-native-router-flux';

const { width, height } = Dimensions.get("window");
LocaleConfig.locales['fr'] = {
    monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
    monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
    dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
    dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
    today: 'Aujourd\'hui'
};
LocaleConfig.defaultLocale = 'fr';
import moment from 'moment'
let combinestring = ''
let today = ''
let newDaysObject2 = {}
let newDaysObject4 = []
import Image from 'react-native-fast-image'

export default class calender_screen extends React.Component {

    constructor(props, context) {
        super(props);
        this.state = {
            selectedStartDate: null,
            data: '',
            zone_name: '',
            user_id: '',
            token: '',
            subzone_id: '',
            datee: '',
            time_slot: '',
            data2: '',
            orderid: '',
            modalVisible: '',
            boat_id: '',
            combi: '',
            markedDates: '',
            markedDates_blue: '',
            date1: ''

        };
        today = moment().format("YYYY-MM-DD");


        this.onDateChange = this.onDateChange.bind(this);

    }
    componentDidMount() {
        combinestring = ''
        AsyncStorage.getItem("userid")
            .then(userid => {
                this.setState({ user_id: userid });



                AsyncStorage.getItem("token")
                    .then(token => {
                        var accessToken = JSON.parse(token);
                        this.setState({ token: accessToken });



                    })
                AsyncStorage.getItem("zonename")
                    .then(zonename => {
                        var zonename = JSON.parse(zonename);
                        this.setState({ zone_name: zonename });



                    })
                AsyncStorage.getItem("suboneid")
                    .then(suboneid => {
                        var subzone_id = JSON.parse(suboneid);
                        this.setState({ subzone_id: subzone_id });



                    })
                AsyncStorage.getItem("orderid")
                    .then(orderid => {
                        this.setState({ orderid: orderid });



                    })
            })

        let newDate = new Date()
        let date = newDate.getDate();
        // let month = newDate.getMonth() + 1;
        // let year = newDate.getFullYear();
        // combinestring = date + '/' + month + '/' + year

        combinestring = moment(Date(date)).format('DD-MM-YYYY');

        // console.log("Current Date " + date)
        // console.log("Current Month  " + month)
        // console.log("Current Year " + year)
        console.log("Current Combine " + combinestring)

        this.setDate(combinestring)



    }

    setDate = (combinestring) => {
        console.log("Current Combine Set " + combinestring)

        this.setState({
            datee: combinestring + '',
            //date1:combinestring
        });

        console.log("Selected Date ", this.state.datee)

        this.getDateSlot()
        this.getTimeSlot();

    }




    onDateChange(date) {


        this.setState({
            selectedStartDate: date,
        });


        //pew  console.log("Date is  ", date)
        combinestring = moment(date).format('DD-MM-YYYY');

        console.log("Selected Date ", combinestring)
        console.log("Selected Combine ", combinestring)


        var combi = moment(date).format('DD-MM-YYYY')
        this.setState({
            datee: combinestring,
            combi: combi
        });
        console.log("Selected Date ", combinestring)
        this.getTimeSlot();


    }

    getDateSlot = () => {
        console.log("Selected Zone ", this.state.zone_name)
        console.log("Combine String ", combinestring)


        AsyncStorage.getItem("zonename")
            .then(zonename => {
                console.log("Zone Name Beforeparse ", zonename)

                this.setState({ zone_name: zonename });

                AsyncStorage.getItem("suboneid")
                    .then(suboneid => {
                        console.log("Zone Id Beforeparse ", suboneid)
                        this.setState({ subzone_id: suboneid });

                        this.setState({
                            animating: true,
                            lodingDialog: true,

                        });
                        fetch(Strings.base_Url + "getDateSloat", {
                            method: 'POST',
                            headers: {
                                // 'Accept': 'application/json',
                                'Content-Type': 'application/json',
                                'User-Id': this.state.user_id,
                                'token': this.state.token,
                            },
                            body: JSON.stringify({

                                zone: this.state.zone_name,
                                sub_zone: this.state.subzone_id,

                            })
                        })


                            .then((response) => response.json())
                            .then((responseData) => {
                                console.log(" responseData getZonelist resoJSon===" + JSON.stringify(responseData));
                                console.log("resoJSon getZonelist===" + responseData.error);
                                if (responseData.error == "false") {
                                    console.log("Error false massage getZonelist" + JSON.stringify(responseData.record));




                                    this.setState({
                                        animating: false,
                                        lodingDialog: false,

                                    });
                                    var obj1 = responseData.time_sloat;
                                    console.log("resoJSon GetVehicle obj===" + obj1);
                                    var count1 = Object.keys(obj1).length;

                                    for (var i = 0; i < count1; i++) {
                                        // var dates;
                                        //   var startdates = moment(obj1[i].startDate).format("YYYY-MM-DD");
                                        console.log("Available Date  " + obj1[i].available_date)

                                        var pay = obj1[i].available_date
                                        newDaysObject4[i] = pay
                                        //   newDaysObject4[i] = obj1[i].available_date;
                                        //newDaysObject5[i] = enddates;
                                        console.log("newDaysObject4:@@@::" + JSON.stringify(newDaysObject4))
                                        //console.log("newDaysObject5:@@@::" + JSON.stringify(newDaysObject5))
                                    }

                                    var posttt = newDaysObject4
                                    console.log("Post Data:@@@::" + posttt)

                                    posttt.forEach((day) => {
                                        this.state.markedDates = {
                                            ...this.state.markedDates,
                                            [day]: {
                                                selected: true, selectedColor: 'rgba(26,87,138,1)'
                                            }
                                        };
                                    });

                                    var posttt2 = newDaysObject4
                                    console.log("Post Data:@@@::" + posttt2)

                                    posttt2.forEach((day) => {
                                        this.state.markedDates_blue = {
                                            ...this.state.markedDates_blue,
                                            [day]: {
                                                selected: true, selectedColor: 'rgba(26,87,138,1)'
                                            }
                                        };
                                    });





                                    console.log(' mark dates   ' + this.state.markedDates)



                                    //this.RBSheet.close()
                                } else if (responseData.error == "true") {

                                    this.setState({
                                        animating: false,
                                        lodingDialog: false,


                                    });

                                    this.showalerts(responseData.errorMessage)

                                }
                            }
                            )
                    })
            })
    }

    getTimeSlot = () => {
        console.log("Selected Zone ", this.state.zone_name)
        console.log("Combine String ", combinestring)


        AsyncStorage.getItem("zonename")
            .then(zonename => {
                console.log("Zone Name Beforeparse ", zonename)

                this.setState({ zone_name: zonename });

                AsyncStorage.getItem("suboneid")
                    .then(suboneid => {
                        console.log("Zone Id Beforeparse ", suboneid)
                        this.setState({ subzone_id: suboneid });

                        this.setState({
                            animating: true,
                            lodingDialog: true,

                        });
                        fetch(Strings.base_Url + "getTimeSloat", {
                            method: 'POST',
                            headers: {
                                // 'Accept': 'application/json',
                                'Content-Type': 'application/json',
                                'User-Id': this.state.user_id,
                                'token': this.state.token,
                            },
                            body: JSON.stringify({
                                date: combinestring,
                                //  date: '02/07/2020',

                                zone: this.state.zone_name,
                                sub_zone: this.state.subzone_id,

                            })
                        })


                            .then((response) => response.json())
                            .then((responseData) => {
                                console.log(" responseData getZonelist resoJSon===" + JSON.stringify(responseData));
                                console.log("resoJSon getZonelist===" + responseData.error);
                                if (responseData.error == "false") {
                                    console.log("Error false massage getZonelist" + JSON.stringify(responseData.record));

                                    this.setState({

                                        data: [],

                                        time_slot: ''
                                    });


                                    this.setState({
                                        animating: false,
                                        lodingDialog: false,
                                        data: responseData.time_sloat,

                                        //  time_slot: responseData.time_sloat[0]
                                    });


                                    //this.RBSheet.close()
                                } else if (responseData.error == "true") {

                                    this.setState({
                                        animating: false,
                                        lodingDialog: false,
                                        data: [],

                                        time_slot: ''

                                    });

                                    this.showalerts(responseData.errorMessage)

                                }
                            }
                            )
                    })
            })
    }

    createOrder2 = () => {

        this.setState({
            animating: true,
            lodingDialog: true,

        }); fetch(Strings.base_Url + "createOrderStep2", {
            method: 'POST',
            headers: {
                // 'Accept': 'application/json',
                'Content-Type': 'application/json',
                'User-Id': this.state.user_id,
                'token': this.state.token,
            },
            body: JSON.stringify({
                order_id: this.state.orderid,
                reservation_date: combinestring,
                reservation_time: this.state.time_slot,

            })
        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(" responseData addAddress resoJSon===" + JSON.stringify(responseData));
                console.log("resoJSon===" + responseData.error);
                if (responseData.error == "false") {
                    console.log("Error false massage" + JSON.stringify(responseData.errorMessage));

                    this.setState({
                        animating: false,
                        lodingDialog: false,

                    });

                    //  this.getBoatinfo();
                    Actions.push("FuelList_info")



                } else if (responseData.error == "true") {

                    this.showalerts(responseData.errorMessage)
                    this.setState({
                        animating: false,
                        lodingDialog: false

                    });
                }
            }
            )
    }

    checkforContinue = () => {
        if (this.state.boat_id != '') {
            this.validation_for_timeslot();
        }
        else {
            Actions.push('CreateReservations')
        }


    }

    validation_for_timeslot = () => {
        var isValidate = 0;


        if (this.state.time_slot != "") {
            isValidate += 1;

        }

        else {
            isValidate -= 1;


            // this.setState({
            //     subzone_err: "Veuillez sélectionner la sous-zone",
            // });

            alert('Aucun technicien n\'a été trouvé pour cette sous-zone')
        }
        if (isValidate == 1) {
            this.createOrder2();
            this.setState({
                animating: true,
                lodingDialog: true,

            });
        }


    }
    createOrder3 = () => {
        console.log("Selected Zone ", this.state.orderid)
        console.log("Selected Zone fuel_type ", this.state.fuel_type)
        console.log("Selected Zone fuel_value ", this.state.fuel_value)


        this.setState({
            animating: true,
            lodingDialog: true,

        }); fetch(Strings.base_Url + "createOrderStep3", {
            method: 'POST',
            headers: {
                // 'Accept': 'application/json',
                'Content-Type': 'application/json',
                'User-Id': this.state.user_id,
                'token': this.state.token,
            },
            body: JSON.stringify({
                order_id: this.state.orderid,
                boat_id: this.state.boat_id,

            })
        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(" responseData addAddress resoJSon===" + JSON.stringify(responseData));
                console.log("resoJSon===" + responseData.error);
                if (responseData.error == "false") {
                    console.log("Error false massage" + JSON.stringify(responseData.errorMessage));

                    this.setState({
                        animating: false,
                        lodingDialog: false,

                    });


                    Actions.push("FuelList_info")

                } else if (responseData.error == "true") {

                    this.showalerts(responseData.errorMessage)
                    this.setState({
                        animating: false,
                        lodingDialog: false

                    });
                }
            }
            )
    }

    getBoatinfo = () => {
        console.log("Selected Zone ", this.state.orderid)
        console.log("Selected Zone UserId ", this.state.user_id)
        console.log("Selected Zone Token ", this.state.token)


        fetch(Strings.base_Url + "orderGetBoatInfo", {
            method: 'POST',
            headers: {
                // 'Accept': 'application/json',
                'Content-Type': 'application/json',
                'User-Id': this.state.user_id,
                'token': this.state.token,
            },
            body: JSON.stringify({
                order_id: this.state.orderid


            })
        })


            .then((response) => response.text())
            .then((responseData) => {
                console.log(" responseData getBoatInfo resoJSon===" + JSON.stringify(responseData));
                console.log("resoJSon getBoatInfo===" + responseData.error);
                if (responseData.error == "false") {
                    console.log("Error false massage getBoatInfo" + JSON.stringify(responseData.record));

                    this.setState({
                        animating: false,
                        lodingDialog: false,
                        data2: responseData.boat_record,
                        boat_id: responseData.boat_record[0].id,

                        modalVisible: true,

                    });


                } else if (responseData.error == "true") {

                    this.showalerts(responseData.errorMessage)
                    this.setState({
                        animating: false,
                        lodingDialog: false,
                        modalVisible: true,

                    });
                }
            }
            )

    }

    forTimeSLot = (time_slot_id) => {

        console.log("Time Slot id ", time_slot_id)

    }
    set_date = (day) => {

        const d = moment(day.dateString).format("YYYY-MM-DD")
        this.setState({ date1: d })
        console.log('Selected Date ', d)

        console.log('Selected Marked Date ', this.state.markedDates)

        if (!this.state.markedDates[d]) {
            // const color = 'rgba(255,170,0,1)'
            // const markedDates = {
            //     ...this.state.markedDates,
            //     [d]: {
            //         selected: true,
            //         selectedColor: color
            //     }
            // }


            // this.setState({ markedDates: markedDates })

            alert('La date n\'est pas disponible pour ces Réservations')



        } else {

            //  const color = 'rgba(26,87,138,1)'

            if (this.state.markedDates_blue[d]) {
                const color = 'rgba(255,170,0,1)'

                const markedDates_blue = {
                    ...this.state.markedDates_blue,
                    [d]: {
                        ...this.state.markedDates_blue[d],

                        selectedColor: color
                    }
                }
                this.setState({ markedDates: markedDates_blue })

                const d2 = moment(day.dateString).format("DD-MM-YYYY")

                combinestring = d2
                this.getTimeSlot()
            }

            // else if(!this.state.markedDates_blue[d]) {

            //     const color = 'rgba(255,170,0,1)'

            //     const markedDates = {
            //         ...this.state.markedDates,
            //         [d]: {
            //             ...this.state.markedDates[d],
            //             //                                         selected: true,
            //             selectedColor: color
            //         }
            //     }
            //     this.setState({ markedDates: markedDates })
            // }

        }





    }
    render() {
        const { selectedStartDate } = this.state;
        const startDate = selectedStartDate ? selectedStartDate.toString() : '';

        return (
            <View style={{ flex: 1, }}>
                <Image
                 source={require("../assets/simble.png")}
                    style={{ width: '100%', height: 78, marginTop: Platform.OS == 'ios' ? 20 :0 }}>
                    <View style={{ width: '100%', height: 50, flexDirection: 'row' }}>
                        <TouchableOpacity onPress={() => Actions.pop()} style={{ marginLeft: 5, height: 50, justifyContent: 'center', alignItems: 'center', }}>
                            <Image source={require("../assets/back1.png")}
                                style={{ width: 15, height: 15, }}
                                resizeMode="contain" />
                        </TouchableOpacity>
                        <View style={{ width: '90%', height: 50, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: 15, fontFamily: 'Poppins-SemiBold' }}>Calendrier de la  {this.state.zone_name}</Text>

                        </View>

                    </View>
                </Image>
                <ScrollView>
                    <View>

                        <Calendar
                            minDate={today}

                            onDayPress={(day) => this.set_date(day)}
                            onDateSelect={(date) => { console.log('selected Date', date) }}


                            markedDates={this.state.markedDates}

                        />


                        {/* <Calendar

                            markedDates={newDaysObject2}
                            style={{ height: 250, }}
                            months={['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre']}
                            weekdays={['L', 'M', 'M', 'J', 'V', 'S', 'D']}
                            onDayPress={(day) => { console.log('selected day', day) }}


                        /> */}
                        <View style={{
                            width: width * 88 / 100, marginTop: 20,
                            flexDirection: 'row', alignSelf: 'center', justifyContent: 'space-between', alignItems: 'center'
                        }}>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={styles.rect}></View>
                                <Text style={styles.disponibleSur}>Jour disponible</Text>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={styles.rect2}></View>
                                <Text style={styles.disponibleSur}>Sélectionné</Text>
                            </View>
                        </View>

                        {this.state.data.length > 0 ?

                            <View>
                                <Text style={{ marginTop: 15, fontFamily: 'Poppins-Regular', fontSize: 12, width: width * 88 / 100, alignSelf: 'center' }}>Sélectionnez le créneau horaire</Text>


                                <FlatList
                                    keyExtractor={item => item.id}
                                    data={this.state.data}
                                    horizontal
                                    style={{ marginLeft: 20, marginRight: 20, }}

                                    renderItem={({ item }) => (



                                        <View>
                                            {this.state.time_slot == item ?
                                                <TouchableOpacity
                                                    style={{
                                                        width: 82, height: 30, justifyContent: 'center', alignItems: 'center', marginTop: 10,
                                                        backgroundColor: 'rgba(255,170,0,1)',
                                                        borderRadius: 10, marginRight: 20,
                                                    }}
                                                    onPress={() => this.setState({ time_slot: item })} >


                                                    <Text style={{ color: 'white', fontFamily: "Poppins-Medium", fontSize: 11, }}>{item}</Text>

                                                </TouchableOpacity>
                                                :

                                                <TouchableOpacity
                                                    style={{
                                                        width: 82, height: 30, justifyContent: 'center', alignItems: 'center', marginTop: 10,
                                                        backgroundColor: 'rgba(26,87,138,1)',
                                                        borderRadius: 10, marginRight: 20,
                                                    }}
                                                    onPress={() => this.setState({ time_slot: item })} >
                                                    <Text style={{ color: 'white', fontFamily: "Poppins-Medium", fontSize: 11, }}>{item}</Text>
                                                </TouchableOpacity>

                                            }


                                        </View>


                                    )}
                                />
                            </View>


                            :
                            <Text style={{ fontFamily: "Poppins-Medium", marginTop: 10, color: 'white', fontSize: 11, }}>Aucune donnée</Text>

                        }
                        {this.state.date1 != '' ?
                            <View>
                                <Text style={{
                                    marginTop: 25, fontFamily: 'Poppins-Regular',
                                    fontSize: 12, width: width * 88 / 100, alignSelf: 'center'
                                }}>Vous avez sélectionné</Text>
                                <View
                                    style={{
                                        width: width * 88 / 100,
                                        alignSelf: 'center', flexDirection: 'row',
                                        marginTop: 2,
                                    }}
                                >
                                    <Text style={{
                                        fontFamily: 'Poppins-SemiBold',
                                        fontSize: 12, color: 'rgba(255,170,0,1)'
                                    }}>{combinestring}
                                    </Text>
                                    {this.state.time_slot != '' ?
                                        <Text style={{
                                            fontFamily: 'Poppins-SemiBold',
                                            fontSize: 12,
                                            color: 'rgba(255,170,0,1)'
                                        }}> ,{this.state.time_slot} </Text>

                                        : null}
                                </View>



                            </View>
                            : null}
                        <TouchableOpacity
                            onPress={() => this.validation_for_timeslot()}
                            style={{ width: 200, borderRadius: 20, marginTop: 10, height: 35, alignSelf: 'center', backgroundColor: 'rgba(26,87,138,1)', justifyContent: 'center', alignItems: 'center', marginBottom: 15 }}>
                            <Text style={{
                                fontSize: 13, fontFamily: "Poppins-SemiBold",
                                color: 'white',
                            }}>Continuer</Text>

                        </TouchableOpacity>


                        <Modal
                            animationType={'fade'}
                            transparent={true}
                            visible={this.state.modalVisible}
                            onRequestClose={() => {
                                this.setState({ modalVisible: false });
                            }}
                        >

                            <View
                                style={{
                                    position: 'absolute',
                                    width: width,
                                    height: '100%',
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    backgroundColor: 'rgba(0,0,0,0.9)',
                                    padding: 20,
                                }}
                            >
                                <Image 
                                source={require('../assets/bg.png')}
                                    style={{ width: 300, padding: 20, alignSelf: 'center' }}>
                                    <Image source={require("../assets/12.png")}
                                        style={{ width: 120, height: 120, borderRadius: 50, marginTop: 80, marginLeft: -20 }}
                                    />

                                    {this.state.data2.length > 0 ?

                                        <FlatList
                                            keyExtractor={item => item.id}
                                            data={this.state.data}
                                            renderItem={({ item }) => (
                                                <TouchableOpacity
                                                    style={{
                                                        justifyContent: 'center', alignItems: 'center', marginTop: 10,
                                                        flexDirection: 'row'
                                                    }}
                                                // Edit Screen
                                                //    onPress={() => this.setState({ fuel_type: item.id })}
                                                >

                                                    <Text style={{}}>{item.ship_name} - </Text>
                                                    <Text style={{}}>{item.brand} </Text>


                                                </TouchableOpacity>
                                            )}
                                        />
                                        :
                                        <Text style={{ fontFamily: "Poppins-Medium", marginTop: 10, color: 'white' }}>Aucune donnée</Text>

                                    }
                                    <TouchableOpacity
                                        onPress={() => Actions.push('AddBoat')}
                                    >
                                        <Text style={{
                                            color: 'white', fontFamily: 'Poppins-Medium', fontSize: 17,
                                            textAlign: 'center'
                                        }}>Ajouter un bateau </Text>

                                    </TouchableOpacity>

                                    <View>

                                        <TouchableOpacity
                                            onPress={() => this.checkforContinue()}
                                            style={{
                                                flexDirection: 'row', height: 50,
                                                backgroundColor: 'rgba(26,87,138,1)',
                                                marginBottom: 20, borderRadius: 10, marginLeft: 10, marginRight: 10
                                            }}>

                                            <Text style={{ fontSize: 14, fontWeight: 'bold', color: 'white', }}>Ajouter un bateau </Text>
                                        </TouchableOpacity>
                                    </View>



                                </Image>
                            </View>
                        </Modal>

                        <PopupDialog
                            onHardwareBackPress={() => { this.setState({ lodingDialog: false }) }}
                            width={0.3}
                            visible={this.state.lodingDialog}
                            dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
                            <DialogContent>
                                <View style={{ alignItems: 'center', }}>
                                    <ActivityIndicator
                                        animating={this.state.animating}
                                        style={[{ height: 10, marginBottom: 10, marginTop: 30, marginLeft: 20, marginRight: 20 }]}
                                        color="#C00"
                                        size="large"
                                        hidesWhenStopped={true}
                                    />
                                </View>
                            </DialogContent>
                        </PopupDialog>

                    </View>
                </ScrollView>





            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    image: {
        width: 413,
        height: 454,
        flexDirection: "row",
        marginTop: 29,
        alignSelf: "center"
    },
    image_imageStyle: {},
    rect: {
        width: 25,
        height: 25,
        backgroundColor: "rgba(26,87,138,1)",
        borderWidth: 0,

        borderColor: "#000000",
        borderRadius: 5
    },
    disponibleSur: {
        fontFamily: "Poppins-SemiBold",
        color: "#303030",
        fontSize: 12,
        textAlign: "left",
        marginLeft: 9,
        marginTop: 4
    },
    rect2: {
        width: 25,
        height: 25,
        backgroundColor: "rgba(255,170,0,1)",
        borderWidth: 0,
        borderColor: "#000000",
        borderRadius: 5,
        marginLeft: 46
    },
    selectionne: {
        fontFamily: "poppins-700",
        color: "#121212",
        fontSize: 12,
        textAlign: "left",
        marginLeft: 13,
        marginTop: 4
    },
    rectRow: {
        height: 25,
        flexDirection: "row",
        flex: 1,
        marginRight: 78,
        marginLeft: 57,
        marginTop: 20
    },
    disponibleSur2: {
        fontFamily: "Poppins-Bold",
        color: "#303030",
        fontSize: 12,
        textAlign: "left",
        marginLeft: 15,
        marginTop: 4
    },
    materialButtonViolet: {
        height: 36,
        width: 79,
        backgroundColor: "rgba(26,87,138,1)",
        borderRadius: 12
    },
    materialButtonViolet2: {
        height: 36,
        width: 79,
        backgroundColor: "rgba(26,87,138,1)",
        borderRadius: 12,
        marginLeft: 23
    },
    materialButtonViolet3: {
        height: 36,
        width: 79,
        backgroundColor: "rgba(255,159,28,1)",
        borderRadius: 12,
        marginLeft: 23
    },
    materialButtonVioletRow: {
        height: 36,
        flexDirection: "row",
        marginTop: 9,
        marginLeft: 37,
        marginRight: 55
    },
    text: {
        fontFamily: "poppins-regular",
        color: "#121212",
        fontSize: 12,
        textAlign: "left",
        marginTop: 46,
        marginLeft: 37
    },
    text2: {
        fontFamily: "poppins-700",
        color: "rgba(255,159,28,1)",
        fontSize: 12,
        textAlign: "left",
        marginTop: 11,
        marginLeft: 37
    },
    materialButtonLight1: {
        height: 36,
        width: 254,
        borderRadius: 25,
        backgroundColor: "rgba(26,87,138,1)",
        marginTop: 32,
        alignSelf: "center"
    }
});
