import React from 'react';
import {
    Platform, TextInput, FlatList, ActivityIndicator,
    SafeAreaView, StatusBar, ScrollView, View, ImageBackground, Text, Dimensions, TouchableOpacity, Linking, AsyncStorage
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Styles from '../styles/styles';
import Strings from '../strings/strings';
import { Card } from 'native-base';
import PopupDialog, {
    DialogTitle, DialogContent, DialogFooter, DialogButton, SlideAnimation, ScaleAnimation,
} from 'react-native-popup-dialog';
import Image from 'react-native-fast-image'

export default class FianlPayment extends React.Component {
    constructor(props, context) {
        super(props);
        this.state = {
            total_pay: '',
            total_amount: '',
            total_remaining: '',
            pay_require: null,
            animating: false,
            lodingDialog: false,
            req_id:this.props.req_Id,
            send_total_amount:'',
            job_desc:'',
        };
        console.log("req_id::::"+this.props.req_Id)
    }

    componentWillMount() {
        this.setState({
            animating: true,
            lodingDialog: true
        });
        AsyncStorage.getItem("userid")
            .then(userid => {
                this.setState({ user_id: userid });
                AsyncStorage.getItem("token")
                    .then(token => {
                        var accessToken = JSON.parse(token);
                        this.setState({ token: accessToken });
                        console.log("accessToken====$$$========" + accessToken);
                        console.log("state userId====$$$========" + userid);
                        // this.refs.loading2.show();
                        fetch(Strings.base_Url + "final_payment_view", {
                            method: 'POST',
                            headers: {
                                // 'Accept': 'application/json',
                                'Content-Type': 'application/json',
                                'User-Id': userid,
                                'token': accessToken,
                            },
                            body: JSON.stringify({
                                req_id: this.props.req_Id,
                            })

                        })
                            .then((response) => response.json())
                            .then((responseData) => {
                                console.log(" responseData resoJSon===" + JSON.stringify(responseData));
                                console.log("resoJSon===" + responseData.error);
                                if (responseData.error == "false") {
                                    console.log("Error false massage" + JSON.stringify(responseData.errorMessage));
                                    this.setState({
                                        total_pay: responseData.req_detail.advance_payment,
                                        total_amount: responseData.req_detail.total_payment,
                                        total_remaining: responseData.req_detail.paid_payment,
                                        pay_require: responseData.req_detail.pay_require,
                                        job_desc:responseData.req_detail.work_desc,
                                        send_total_amount:responseData.req_detail.paid_payment_hide,
                                        animating: false,
                                        lodingDialog: false,
                                    });
                                } else if (responseData.error == "true") {
                                    this.setState({
                                        no_record: "Aucun dossier n'existe",
                                        animating: false,
                                        lodingDialog: false
                                    });
                                }
                            })
                    });
            });
    }


    onValueChange(value, label) {
        this.setState({
            selected: value
        });

    }
    render() {
        return (
            <View style={Styles.containerWhite}>
                <Image source={require("../assets/simble.png")}
                    style={{ width: '100%', height: 78, }}>
                    <View style={{ width: '100%', height: 50, flexDirection: 'row' }}>
                        <TouchableOpacity onPress={() => Actions.pop()} style={{ width: '15%', height: 50, justifyContent: 'center', alignItems: 'center', }}>
                            <Image source={require("../assets/back.png")}
                                style={{ width: 25, height: 25, }}
                                resizeMode="contain" />
                        </TouchableOpacity>
                        <View style={{ width: '80%', height: 50, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: 16, fontWeight: 'bold' }}>{Strings.final_bill}</Text>
                        </View>

                    </View>
                </Image>
                <ScrollView style={{ marginTop: -20 }}>
                    <View style={{ alignItems: 'center', width: '100%' }}>
                        <Card style={{ width: '90%', borderRadius: 10 }}>
                            <View style={{ width: '90%', flexDirection: 'row', height: 40, marginTop: 20 }}>
                                <View style={{ width: '60%', justifyContent: 'center' }}>
                                    <Text style={{ color: Strings.color_green_code, fontSize: 14, fontWeight: 'bold',textAlign:'center' }}>{Strings.total_pay}</Text>
                                </View>
                                <View style={{ width: '40%', justifyContent: 'center', alignItems: 'flex-end' }}>
                                    <Text style={{ fontSize: 14, fontWeight: 'bold' }}>{this.state.total_pay} € </Text>
                                </View>
                            </View>
                            <View style={{ width: '90%', flexDirection: 'row', height: 40, marginLeft: 10 }}>
                                <View style={{ width: '50%', justifyContent: 'center' }}>
                                    <Text style={{ color: Strings.color_green_code, fontSize: 14, fontWeight: 'bold' }}>{Strings.total_amount}</Text>
                                </View>
                                <View style={{ width: '50%', justifyContent: 'center', alignItems: 'flex-end' }}>
                                    <Text style={{ fontSize: 14, fontWeight: 'bold' }}>{this.state.total_amount} € </Text>
                                </View>
                            </View>
                            <View style={{ width: '90%', flexDirection: 'row', height: 40, marginLeft: 10 }}>
                                <View style={{ width: '50%', justifyContent: 'center' }}>
                                    <Text style={{ color: Strings.color_green_code, fontSize: 16, fontWeight: 'bold' }}>{Strings.job_desc}</Text>
                                </View>
                                <View style={{ width: '50%', justifyContent: 'center', alignItems: 'flex-end' }}>
                                    <Text style={{ fontSize: 14, textAlign:'center' }}>{this.state.job_desc} </Text>
                                </View>
                            </View>
                            <View style={{ width: '100%', height: 1, backgroundColor: Strings.color_green_code, marginTop: 30 }}></View>

                            <View style={{ flexDirection: 'column', alignItems: 'center', width: '100%', marginTop: 20 }}>

                                <Text style={{ color: Strings.color_green_code, fontSize: 16, fontWeight: 'bold' }}>{Strings.remaining_amount}</Text>
                                <Text style={{ fontSize: 14, fontWeight: 'bold', marginTop: 10 }}>{this.state.total_remaining} € </Text>

                                {this.state.pay_require === false ?
                                    <View style={{width:'100%',alignItems:'center'}}>
                                        <TouchableOpacity onPress={()=>Actions.push("Rating",{req_Id:this.props.req_Id})}  style={{ alignItems: 'center', justifyContent: 'center', width: '50%', height: 40, marginTop:20,marginBottom:30, borderRadius: 10, backgroundColor: '#F1C368' }}>
                                            <Text style={{ fontSize: 16, color: 'white' }}>{Strings.ok_text}</Text>

                                        </TouchableOpacity>
                                    </View> :

                                    <View style={{width:'100%',alignItems:'center'}}>
                                   <TouchableOpacity onPress ={()=>Actions.push("FinalPaymentPaypal",{req_id:this.props.req_Id,total_amount:this.state.send_total_amount})}  style={{ alignItems: 'center', justifyContent: 'center', width: '50%', height: 40, marginTop:20,marginBottom:30, borderRadius: 10, backgroundColor: '#F1C368' }}>
                                            <Text style={{ fontSize: 16, color: 'white' }}>{Strings.accepter_text}</Text>

                                        </TouchableOpacity>
                                    </View>
                                }

                            </View>
                        </Card>
                    </View>

                    <PopupDialog
                            onHardwareBackPress={() => { this.setState({ lodingDialog: false }) }}
                            width={0.3}
                            visible={this.state.lodingDialog}
                            dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
                            <DialogContent>
                                <View style={{ alignItems: 'center', }}>
                                    <ActivityIndicator
                                        animating={this.state.animating}
                                        style={[{ height: 10, marginBottom: 10, marginTop: 30, marginLeft: 20, marginRight: 20 }]}
                                        color="#C00"
                                        size="large"
                                        hidesWhenStopped={true}
                                    />
                                </View>
                            </DialogContent>
                        </PopupDialog>
                </ScrollView>
            </View >
        );
    }
}








