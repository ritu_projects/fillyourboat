import { AsyncStorage, Platform, Alert } from 'react-native';
import { Actions, Router, Stack, Scene } from 'react-native-router-flux';
// import {Actions} from 'react-native-router-flux';
import React from 'react';
import { BackHandler } from 'react-native'
//import Home from './screens/home'
//import Login from './screens/login'
import Splash from './screens/splash'
import EditBoat3 from './screens/EditBoat3'
import ViewPager from './screens/viewPager'
import SignUp from './screens/signUp'
import Login from './screens/login'
import Rating_screen from './screens/rating_screen'
import Register from './screens/register'
import Location from './screens/location'
import MyOrders from './screens/MyOrders'
import CurrentTabs from './screens/CurrentTab'
import Location1 from './screens/location1'
import PastTabs from './screens/PastTabs'
import AddBoat from './screens/AddBoat'
import AddBoat_forOrder from './screens/AddBoat_forOrder'


import SideBar from './screens/sideBar'
import Profile from './screens/profile'
import EditProfile from './screens/editprofile'
import Notifiaction from './screens/notification'
import Contacts from './screens/Contacts'
import Payment from './screens/Payment'
import Payment_Drawe from './screens/Payment_Drawe'
import FinalPayment from './screens/FinalPayment'
import FinalPaymentPaypal from './screens/FinalPaypalPayment'
import Invoice from './screens/Invoice'
import EditBoat2 from './screens/EditBoat2'
import EditBoat1 from './screens/EditBoat1'
import Calender_Screen from './screens/calender_screen'
import FuelList_info from './screens/FuelList_info'
import Editinforfororder from './screens/Editinforfororder'
import Delvey_confirm_Screen from './screens/Delvey_confirm_Screen'
import Webview from './screens/Webview'
import FinalDeliver from './screens/FinalDeliver'
import OrderView from './screens/OrderView'
import Trackorders from './screens/Trackorders'
import Chat from './screens/Chat'
import ChatwithAdmin from './screens/ChatwithAdmin'

import Advertisementlist from './screens/Advertisementlist'
import AdvertisementPageDetails from './screens/AdvertisementPageDetails'
import MyBoats from './screens/MyBoats'
import Notifiaction1 from './screens/notification1'
import FuelRates from './screens/FuelRates'
import MyPartners from './screens/MyPartners'
import Webview_Paypal from './screens/Webview_Paypal'
import Forgot_password from './screens/Forgot_password'
import ChangePassword from './screens/ChangePassword'




_backAndroidHandler = () => {
  const scene = Actions.currentScene;
  // alert(scene)
  if (scene === 'Location1' || scene === 'Login') {

    Alert.alert(
      'Quitter l\'application',
      'Voulez-vous sortir?',
      [
        { text: 'Non', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        { text: 'Oui', onPress: () => BackHandler.exitApp() },
      ],
      { cancelable: false });
    // AsyncStorage.setItem("backEvent", "" + true);
    // console.log("backEvent test "+backEvent);
    // BackHandler.exitApp();
    return true;
  }
  Actions.pop();
  return true;
};


const Routes = () => (
  <Router
    navigationBarStyle={{ backgroundColor: '#8B008B', height: 45 }} t
    intColor='white' backAndroidHandler={this._backAndroidHandler}>

    <Stack key="root">


      <Scene key="Splash" component={Splash} left={() => null} hideNavBar />
      <Scene key="ViewPager" component={ViewPager} left={() => null} hideNavBar />
      <Scene key="SignUp" component={SignUp} left={() => null} hideNavBar />
      <Scene key="Login" component={Login} left={() => null} hideNavBar />
      <Scene key="Register" component={Register} left={() => null} hideNavBar />

      <Scene key="AddBoat" component={AddBoat} left={() => null} hideNavBar />

      {/* <Stack key="createreservationstack"> */}
        <Scene key="Location1" component={Location1} left={() => null} hideNavBar />

        <Scene key="Location" component={Location} left={() => null} hideNavBar />
        <Scene key="Calender_Screen" component={Calender_Screen} left={() => null} hideNavBar />
        <Scene key="FuelList_info" component={FuelList_info} left={() => null} hideNavBar />
        <Scene key="EditBoat1" component={EditBoat1} left={() => null} hideNavBar />
        <Scene key="EditBoat2" component={EditBoat2} left={() => null} hideNavBar />
        <Scene key="AddBoat_forOrder" component={AddBoat_forOrder} left={() => null} hideNavBar />
        <Scene key="Delvey_confirm_Screen" component={Delvey_confirm_Screen} left={() => null} hideNavBar />

        <Scene key="Payment" component={Payment} left={() => null} hideNavBar />

        <Scene key="FinalDeliver" component={FinalDeliver} left={() => null} hideNavBar />

      {/* </Stack> */}

      <Scene key="Webview" component={Webview} left={() => null} hideNavBar />


      <Scene key="MyOrders" component={MyOrders} left={() => null} hideNavBar />
      <Scene key="CurrentTabs" component={CurrentTabs} left={() => null} hideNavBar />
      <Scene key="PastTabs" component={PastTabs} left={() => null} hideNavBar />
      <Scene key="OrderView" component={OrderView} left={() => null} hideNavBar />
      <Scene key="Trackorders" component={Trackorders} left={() => null} hideNavBar />
      <Scene key="Chat" component={Chat} left={() => null} hideNavBar />
      <Scene key="ChatwithAdmin" component={ChatwithAdmin} left={() => null} hideNavBar />
      <Scene key="Advertisementlist" component={Advertisementlist} left={() => null} hideNavBar />
      <Scene key="AdvertisementPageDetails" component={AdvertisementPageDetails} left={() => null} hideNavBar />
      <Scene key="MyBoats" component={MyBoats} left={() => null} hideNavBar />
      <Scene key="FuelRates" component={FuelRates} left={() => null} hideNavBar />
      <Scene key="MyPartners" component={MyPartners} left={() => null} hideNavBar />
      <Scene key="Rating_screen" component={Rating_screen} left={() => null} hideNavBar />
      <Scene key="Notification1" component={Notifiaction1} left={() => null} hideNavBar />
      <Scene key="EditBoat3" component={EditBoat3} left={() => null} hideNavBar />




      <Scene key="Editinforfororder" component={Editinforfororder} left={() => null} hideNavBar />
      <Scene key="SideBar" component={SideBar} left={() => null} hideNavBar />
      <Scene key="Profile" component={Profile} left={() => null} hideNavBar />
      <Scene key="EditProfile" component={EditProfile} left={() => null} hideNavBar />
      <Scene key="Notifiaction" component={Notifiaction} left={() => null} hideNavBar />
      <Scene key="Contacts" component={Contacts} left={() => null} hideNavBar />
      <Scene key="Payment_Drawe" component={Payment_Drawe} left={() => null} hideNavBar />
      <Scene key="FinalPayment" component={FinalPayment} left={() => null} hideNavBar />
      <Scene key="FinalPaymentPaypal" component={FinalPaymentPaypal} left={() => null} hideNavBar />
      <Scene key="Invoice" component={Invoice} left={() => null} hideNavBar />
      <Scene key="Webview_Paypal" component={Webview_Paypal} left={() => null} hideNavBar />
      <Scene key="Forgot_password" component={Forgot_password} left={() => null} hideNavBar />
      <Scene key="ChangePassword" component={ChangePassword} left={() => null} hideNavBar />

       </Stack>
  </Router>
);
export default Routes;

