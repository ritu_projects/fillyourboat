import React, { Component } from "react";
import { StyleSheet, View, Image, ImageBackground, Text } from "react-native";
import Svg, { Ellipse } from "react-native-svg";

function HomeScreen(props) {
  return (
    <View style={styles.container}>
      <View style={styles.mapStack}>
        <ImageBackground
          source={require("../assets/images/MAP.png")}
          resizeMode="contain"
          style={styles.map}
          imageStyle={styles.map_imageStyle}
        >
          <View style={styles.ellipseStack}>
            <Svg viewBox="0 0 145 144.88" style={styles.ellipse}>
              <Ellipse
                stroke="rgba(230, 230, 230,1)"
                strokeWidth={0}
                fill="rgba(255,255,255,1)"
                cx={73}
                cy={72}
                rx={73}
                ry={72}
              ></Ellipse>
            </Svg>
            <Image
              source={require("../assets/images/FYB-logo-officiel.png")}
              resizeMode="contain"
              style={styles.image2}
            ></Image>
          </View>
        </ImageBackground>
        <View style={styles.rect}>
          <Text style={styles.loremIpsum2}>
            Sélectionnez la zone où votre {"\n"}bateau est amarré
          </Text>
          <Text style={styles.loremIpsum}></Text>
          <Text style={styles.balayer}>Balayer</Text>
          <Image
            source={require("../assets/images/Group_10398.png")}
            resizeMode="contain"
            style={styles.image3}
          ></Image>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  map: {
    top: 0,
    width: 490,
    height: 571,
    position: "absolute",
    left: 0
  },
  map_imageStyle: {},
  ellipse: {
    top: 0,
    width: 145,
    height: 145,
    position: "absolute",
    left: 0
  },
  image2: {
    top: 24,
    left: 21,
    width: 101,
    height: 98,
    position: "absolute"
  },
  ellipseStack: {
    width: 145,
    height: 145,
    marginTop: 369,
    marginLeft: 172
  },
  rect: {
    top: 437,
    left: 47,
    width: 394,
    height: 406,
    position: "absolute",
    backgroundColor: "rgba(255,255,255,1)",
    borderRadius: 46
  },
  loremIpsum2: {
    fontFamily: "poppins-700",
    color: "#121212",
    fontSize: 18,
    textAlign: "center",
    marginTop: 107,
    marginLeft: 62
  },
  loremIpsum: {
    fontFamily: "roboto-regular",
    color: "#121212",
    marginLeft: 61
  },
  balayer: {
    fontFamily: "poppins-700",
    color: "rgba(26,88,139,1)",
    fontSize: 18,
    marginTop: 63,
    marginLeft: 162
  },
  image3: {
    width: 37,
    height: 28,
    marginTop: 34,
    marginLeft: 178
  },
  mapStack: {
    width: 490,
    height: 843,
    marginTop: 13,
    marginLeft: -57
  }
});

export default HomeScreen;
