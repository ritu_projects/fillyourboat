
import React, { Component } from 'react';
import { AppRegistry,Alert ,View,AsyncStorage } from 'react-native';
import Routes from './Routes.js'
import firebase from 'react-native-firebase';
import { Actions } from "react-native-router-flux";
import { EventRegister } from 'react-native-event-listeners'
import PushNotificationIOS from '@react-native-community/push-notification-ios';

class FillYourBoat extends Component {
 
   async componentDidMount() {
      this.checkPermission();
      this.createNotificationListeners();
    }
    componentWillUnmount() {
      this.notificationListener();
      this.notificationOpenedListener();
      //this.messageListener();
     // this.createNotificationListeners();
    }
      //1
    async checkPermission() {
      const enabled = await firebase.messaging().hasPermission();
      if (enabled) {
          this.getToken();
      } else {
          this.requestPermission();
      }
    }
      //3
    async getToken() {
      let fcmToken = await AsyncStorage.getItem('fcmToken');
      console.log("FCM token%%%:::: "+fcmToken)
      if (!fcmToken) {
          fcmToken = await firebase.messaging().getToken();
          if (fcmToken) {
              // user has a device 
              await AsyncStorage.setItetokenm('fcmToken', fcmToken);
          }
      }
    }
    async createNotificationListeners() {
      /*
      * Triggered when a particular notification has been received in foreground
      * */
      console.log("call createNotificationListeners:::: "+"Done")
      this.notificationListener = firebase.notifications().onNotification((notification) => {
        const not=notification;
        console.log("message::::::"+notification._data.test_key_add);
        for (var key in notification) {
         console.log(notification[key]);
        }
       // console.log("message::::::"+notification._data.test_key_add);
          const { title, body} = notification;
          const {fcm_push_response,fcm_order_id} =notification._data
          console.log("message  notificationOpen  test_key_add::::::"+fcm_push_response);
          console.log("message  notificationOpen  test_key_add orderid::::::"+fcm_order_id);

          if(fcm_push_response == 'adminSendChtMessagePush')
          {
            const {order_id} =notification._data

            this.showAlert(title, body,fcm_push_response,order_id);

          }

          else
          {
            this.showAlert(title, body,fcm_push_response,fcm_order_id);

          }

      });    
      /*
      * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
      * */
      this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
          const { title, body } = notificationOpen.notification;
          const {fcm_push_response,fcm_order_id} =notificationOpen.notification._data
          console.log("message  notificationOpen  test_key_add::::::"+fcm_push_response);
          console.log("message  notificationOpen  test_key_add orderid::::::"+fcm_order_id);

          if(fcm_push_response == 'adminSendChtMessagePush')
          {
            const {order_id} =notificationOpen.notification._data

            this.showAlert(title, body,fcm_push_response,order_id);

          }

          else
          {
            this.showAlert(title, body,fcm_push_response,fcm_order_id);

          }

       //   this.showAlert(title, body,fcm_push_response,fcm_order_id);

       //   this.showAlert(title, body,fcm_push_response,req_id,client_type,fcm_cat_id,fcm_req_id,fcm_tech_id,reference_used);
      });
    
      /*
      * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
      * */
      const notificationOpen = await firebase.notifications().getInitialNotification(
        // {
        //   show_in_foreground: true,
        // }
      );
      if (notificationOpen) {  
        console.log("notificatioopwn::::::"+notificationOpen);

          const { title, body } =notificationOpen.notification._data
          const {fcm_push_response,fcm_order_id} =notificationOpen.notification._data
          console.log("message  notificationOpen::::::"+fcm_push_response +"title"+title +"body"+body);

          if(fcm_push_response == 'adminSendChtMessagePush')
          {
            const {order_id} =notificationOpen.notification._data

            this.showAlert(title, body,fcm_push_response,order_id);

          }

          else
          {
            this.showAlert(title, body,fcm_push_response,fcm_order_id);

          }
  //  this.showAlert(title, body,fcm_push_response,fcm_order_id);
      }
      /*
      * Triggered for data only payload in foreground
      * */
      this.messageListener = firebase.messaging().onMessage((message) => {
        //process data message
        console.log("message::::::"+JSON.stringify(message));
      });
    }
    
    showAlert(title, body,fcm_push_response,fcm_order_id) {
      let req_Id='';
      const details = {"alertTitle":title , "alertBody": body,"soundName":'uber.wav'};
      PushNotificationIOS.presentLocalNotification(details);
      console.log("fcm_push_response :$$$Done$$:::"+title+" "+body);
      console.log("Request Accept Reponse:::"+fcm_push_response+"fcm_order_id"+fcm_order_id);

AsyncStorage.setItem('Order_id_fcm',fcm_order_id)
AsyncStorage.setItem('fcmpushresponse_fcm',fcm_push_response)
AsyncStorage.setItem('titla_fcm',title)
AsyncStorage.setItem('body_fcm',body)

      // const objchatscreens= {currentOrderId: fcm_order_id,
      //   fcm_push_response: fcm_push_response,
      //   title : title,
      //   body : fcm_push_response,
      // }
      // EventRegister.emit('myCustomEvent', objchatscreens)   
    
      if(fcm_push_response == "adminSendChtMessagePush"){
        Alert.alert(
          title, body,
          [
              { text: 'OK', onPress: () => 
             {
              AsyncStorage.setItem('Order_id_fcm',''),
              AsyncStorage.setItem('fcmpushresponse_fcm',''),
              AsyncStorage.setItem('titla_fcm',''),
              AsyncStorage.setItem('body_fcm','')
              Actions.push("ChatwithAdmin",{currentOrderId:fcm_order_id})
             }  },
          ],
          { cancelable: false },
        );  
        console.log("fcm_push_response :$$$Done$$:::"+fcm_push_response);
      }

    else  if(fcm_push_response == "order_chat_user_side"){
        Alert.alert(
          title, body,
          [
              { text: 'OK', onPress: () => 
              {
                AsyncStorage.setItem('Order_id_fcm',''),
                AsyncStorage.setItem('fcmpushresponse_fcm',''),
                AsyncStorage.setItem('titla_fcm',''),
                AsyncStorage.setItem('body_fcm',''),
                Actions.push("Chat",{currentOrderId:fcm_order_id})
              }
          },
          ],
          { cancelable: false },
        );  
        console.log("fcm_push_response :$$$Done$$:::"+fcm_push_response);
      }

      else
      {

        if(fcm_push_response == "order_completed")
        {
          Alert.alert(
            title, body,
            [
                { text: 'OK', onPress: () => 
                {
                  AsyncStorage.setItem('Order_id_fcm',''),
                  AsyncStorage.setItem('fcmpushresponse_fcm',''),
                  AsyncStorage.setItem('titla_fcm',''),
                  AsyncStorage.setItem('body_fcm',''), 
                  Actions.push("Rating_screen",{currentOrderId:fcm_order_id}) },
                }
         
            ],
            { cancelable: false },
          );  
          console.log("fcm_push_response :$$$Done$$:::"+fcm_push_response);
        }       


        else
        {
          Alert.alert(
            title, body,
            [
                { text: 'OK', onPress: () => 
                {
                  AsyncStorage.setItem('Order_id_fcm',''),
                  AsyncStorage.setItem('fcmpushresponse_fcm',''),
                  AsyncStorage.setItem('titla_fcm',''),
                  AsyncStorage.setItem('body_fcm',''), 
                  Actions.push("Trackorders",{currentOrderId:fcm_order_id}) },
                }
            ],
            { cancelable: false },
          );  
          console.log("fcm_push_response :$$$Done$$:::"+fcm_push_response);
        }
      }

      




    
    }

    
   
    

   render() {
      return (
         <Routes />  
      )
   }
}
export default FillYourBoat
AppRegistry.registerComponent('FillYourBoat', () => FillYourBoat)